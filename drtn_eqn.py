import numpy as np
import pandas as pd
pd.set_option('display.width', 160)
import matplotlib.pyplot as plt
import seaborn as sb
import random
from manual_fns import *


### Import data
vers = 'v2_4'
dur  = '7mo'
df = pd.read_csv('{0}/swot_master_table_{1}.csv'.format(vers, dur), index_col=0)

### Convert durations to minutes, rounded, so get rid of precision difference between data sources
df[['duration','swotdur','swotstreamdur']] = (df[['duration','swotdur','swotstreamdur']]/60).round(0)

### Add some useful columns
df['vw_frac']    = 1 - df.swotdur / df.swotstreamdur
df['swot_frac']  = df.swotdur / df.swotstreamdur
df['swot_class'] = df.swot_frac > 0  # >= 0.05
df['hh']         = df.fsite.str.split('-', 1, expand=True).iloc[:, 0]
df['month'] = pd.to_datetime(df.start_time).map(lambda t: t.month)
### v2.4 stopped outputing make and model, so this is for backwards compatibility
if vers == 'v2_4':
    df['mk_mdl'] = df.device
    df[['make','model']] = df.device.str.split(' ', 1, expand=True)
else:
    df['mk_mdl'] = df.make.str.cat([df.model], sep='-')

########################################################################
########################################################################
########################################################################
### Create ideal duration equation templates for each provider,
### normalized to a max of 1
drtn_curves_dict = {}
for s in [20, 100, 300, 500, 1000, 3000]:
    drtn_curves_dict[s] = drtn_model(df, get_bin_key(df, binmodel='provider'),
                                     smoothing=s, do_flatten=False, min_periods=10)


f1, ax1 = plt.subplots(2, 3, figsize=[20, 12], sharex=True, sharey=True)
ax2 = ax1.flatten()
for i, s in enumerate([20, 100, 300, 500, 1000, 3000]):
    for p in drtn_curves_dict[s].columns:
        ax2[i].plot(drtn_curves_dict[s].index, drtn_curves_dict[s][p], label=p)
    ax2[i].set_xscale('log')
    ax2[i].set_title('Smoothing window = {0}'.format(s))
    ax2[i].legend()
f1.savefig('ModelPlots/EqnMdl/curves_v_window.png')
plt.close(f1)

f1, ax1 = plt.subplots(2, 3, figsize=[20, 12], sharex=True, sharey=True)
ax2 = ax1.flatten()
for i, s in enumerate([20, 100, 300, 500, 1000, 3000]):
    for p in drtn_curves_dict[s].columns:
        ax2[i].plot(drtn_curves_dict[s].index, drtn_curves_dict[s][p], label=p)
    ax2[i].set_xlim(0, 20)
    ax2[i].set_title('Smoothing window = {0}'.format(s))
    ax2[i].legend()
f1.savefig('ModelPlots/EqnMdl/curves_v_window_shorttimes.png')
plt.close(f1)

f1, ax1 = plt.subplots(2, 3, figsize=[20, 12], sharex=True)
ax2 = ax1.flatten()
for i, p in enumerate(drtn_curves_dict[s].columns):
    for s in [20, 100, 300, 500, 1000, 3000]:
        ax2[i].plot(drtn_curves_dict[s].index, drtn_curves_dict[s][p], label=s)
    ax2[i].set_xlim(0, 20)
    ax2[i].set_title(p)
    ax2[i].legend()
f1.savefig('ModelPlots/EqnMdl/curves_v_window_prv_shorttimes.png')
plt.close(f1)

########################################################################
### Construct df with curves using best smoothing window for each provider
def construct_drtn_eqns(df, prv_smoothing={'netflix': 1000,
                                           'youtube': 1000,
                                           'hulu':    1000,
                                           'amazon':   500,
                                           'hbo':      500,
                                           'crackle':   60},
                        peak_mins=[7,8,9,10], fname='ModelPlots/EqnMdl/drtn_norm.csv'):
    drtn_curves = pd.DataFrame([], index=range(1, int(df.swotstreamdur.max())))
    for p in prv_smoothing.keys():
        drtn_curves[p] = drtn_model(df, get_bin_key(df, binmodel='provider'),
                                    smoothing=prv_smoothing[p], do_flatten=False, min_periods=int(prv_smoothing[p]/2))[p]
    ### Force the first point to zero
    drtn_curves.loc[1] = [0]*drtn_curves.shape[1]
    ### Normalize how?
    ### divide by provider total swot_frac near peak
    prv_aggs = df.loc[df.swotstreamdur.isin(peak_mins)]\
                 .groupby('provider').agg({'fsite':        'count',
                                           'swotstreamdur':'sum',
                                           'duration':     'sum'})
    prv_aggs['swot_frac'] = (prv_aggs.swotstreamdur - prv_aggs.duration) / prv_aggs.swotstreamdur
    drtn_norm = drtn_curves / prv_aggs.swot_frac
    ### If crackle isn't sampled enough to justify using, try averaging the
    ### other curves together?
    # drtn_norm['crackle'] = drtn_norm[[c for c in drtn_norm.columns if c != 'crackle']].mean(axis=1)
    # drtn_norm['crackle'] = drtn_norm.crackle / drtn_norm.crackle.loc[1:100].max()
    ### Save to file
    drtn_norm.to_csv(fname)
    return drtn_norm

drtn_norm = construct_drtn_eqns(df)

# -------------------------------------------------------------------- #
f1, ax1 = plt.subplots(1)
for p in drtn_norm.columns:
    ax1.plot(drtn_norm.index, drtn_norm[p], label=p)
ax1.set_xscale('log')
ax1.set_title('Normalized shapes')
ax1.legend()
f1.savefig('ModelPlots/EqnMdl/normalized_shape.png')
plt.close(f1)

########################################################################
### Predict swot fraction for particular device and provider at each possible duration
def eqn_model(modeldf, bin_key):
    ### Read established provider-duration equation templates
    drtn_norm = pd.read_csv('ModelPlots/EqnMdl/drtn_norm.csv', index_col=0)
    ### Calculate stream/tune rates per provider/device
    strm_tot = modeldf.pivot_table(index='device', columns='provider', values='swotstreamdur', aggfunc='sum')
    tune_tot = modeldf.pivot_table(index='device', columns='provider', values='duration', aggfunc='sum')
    swot_tot = strm_tot - tune_tot
    swot_mn = swot_tot / strm_tot
    for p in swot_mn.columns:
        prov_mn_swot = swot_tot[p].sum() / strm_tot[p].sum()
        swot_mn.loc[swot_mn[p] == 0, p] = prov_mn_swot
        swot_mn[p] = swot_mn[p].fillna(prov_mn_swot)
    ### Multiple read-in provider-drtn equations by calculated prv-dvc swot fracs to get predictions
    pred_lookup = pd.DataFrame([], index=range(1, int(modeldf.swotstreamdur.max()) + 1), columns=bin_key.bin_name.unique())
    for c in pred_lookup.columns:
        d, p = c.split('_')
        pred_lookup[c] = drtn_norm[p] * swot_mn.loc[d, p]
    return pred_lookup

########################################################################
### Run like in model_swot
modeldf, predictdf = get_dfs(df, 7, 1)
bin_key = get_bin_key(modeldf, binmodel='v1')
pred_lookup = eqn_model(modeldf, bin_key)
preds = predict_from_lookup_and_key(predictdf, pred_lookup, bin_key)
