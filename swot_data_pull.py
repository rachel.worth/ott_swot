### Attempting to improve SWOT pull code
import pyodbc     as db
import sqlalchemy as sql
import pandas     as pd
import datetime as dt
import numpy as np
import matplotlib.pyplot as plt
import matplotlib
import os
pd.set_option('display.width', 170)

### Date range to pull
date_range = '2017-09-01', '2018-05-20'
# date_range = '2017-06-01','2018-03-31'
date_range_dt = [dt.datetime.strptime(d, '%Y-%m-%d') for d in date_range]
assert date_range_dt[1] >= date_range_dt[0], 'ERROR: start date later than end date'

### Use gap fix?
gapfix = True
strmsort = True

### SWOT definitions
end_streaming_event_threshold = 240  # seconds

### Use eric's tree_id list? (vs expanded sort-of automatic list)
eric_prv = True
eric_trees = [1690781, 777777, 156698, 1431024, 3481638, 214042, 1459202, 56472, 3213979]

### Is connection available? If so, write files at checkpoints?
connection = True
checkpoint = True

### Make plots of some random users' streaming and tuning activity?
make_activity_plots = True

### Directory to put files
dr = 'panel_swotdata/new/{}-{}/'.format(*[d.strftime('%y%m%d') for d in date_range_dt])
if not os.path.exists(dr):
    os.makedirs(dr)
print("Data directory: {}".format(dr))

########################################################################################################################
###################################### Netezza connection ##############################################################
########################################################################################################################
if connection == True:
    print('connecting...')
    con = db.connect('DRIVER=NetezzaSQL;SERVER=nantz88.nielsen.com;PORT=5480;DATABASE=opintel_prod;UID=baroer01;PWD=Password@6;')
    print('connected!')

strm_sample = pd.read_sql_query('''
              SELECT
                *
              FROM domain_event de WHERE metercollectiondate = '2018-05-21' AND dmxstreaming = true ''', con)
mime_sample = pd.read_sql_query('''
              SELECT
                *
              FROM mime_event de WHERE metercollectiondate = '2018-05-21' ''', con)
fct_sample  = pd.read_sql_query('''
              SELECT
                *
              FROM fct_domainevents de WHERE metercollectiondate = '2018-05-21' AND dmxstreaming = true ''', con)
########################################################################################################################
######################################## Tree_id table #################################################################
########################################################################################################################
if connection == True:
    print('read tree_id from server')
    tree_ids = pd.read_sql_query('''SELECT DISTINCT parent, brand, channel, treeid AS tree_id FROM domain_resolution_result ''', con)
    if checkpoint:
        tree_ids.to_csv(dr + 'tree_id_table.csv', encoding='utf-8')
else:
    print('read tree_id from file')
    tree_ids = pd.read_csv(dr + 'tree_id_table.csv', index_col=0)
tree_ids.columns = [c.lower() for c in tree_ids.columns]
tree_ids['parent']  = tree_ids.parent.fillna('').copy()
tree_ids['brand']   = tree_ids.brand.fillna('').copy()
tree_ids['channel'] = tree_ids.channel.fillna('').copy()
tree_ids['provider'] = ''
tree_prov = {'dtv': ['directv'],
             'cw':  ['cw seed', 'cwtv.com'],
             'hbo': ['hbo network', 'hbo now'],
             'netflix': ['netflix'],
             'youtube': ['youtube'],
             'amazon':  ['amazon'],
            }
prv_list = ['netflix', 'youtube', 'hulu', 'hbo', 'amazon', 'crackle', 'cw', 'dtv', 'discovery']
for p in prv_list:
    if (p not in tree_prov.keys()) & (p != 'discovery'):
        tree_ids.loc[tree_ids.brand.str.lower().str.contains(p), 'provider'] = p
        print(p, (tree_ids.provider!='').sum())
for p in tree_prov.keys():
    for s in tree_prov[p]:
        tree_ids.loc[tree_ids.brand.str.lower() == s, 'provider'] = p
        print(p, s, (tree_ids.provider != '').sum())
tree_ids.loc[tree_ids.parent.str.contains('Discovery Communications'), 'provider'] = 'discovery'
tree_ids.loc[(tree_ids.brand == 'Amazon') & (tree_ids.channel != 'LOVEFiLM.com'), 'provider'] = 'amazon'
tree_ids.loc[(tree_ids.brand == 'Amazon') & (tree_ids.channel == 'LOVEFiLM.com'), 'provider'] = ''

print((tree_ids.provider != '').sum())

if eric_prv:
    tree_ids['use'] = tree_ids.tree_id.isin(eric_trees)
    tree_ids.loc[~tree_ids.use, 'provider'] = ''
else:
    tree_ids['use'] = tree_ids.provider != ''
use_trees = tuple(tree_ids.loc[tree_ids.use, 'tree_id'].unique())

### how to choose correct channels to associate w/ providers?
### Many other urls contain 'youtube' but aren't, e.g. safeyoutube.com
### HBO Now and Network but not Watch (Hbowatch.com -- unrelated?) also not actually present in data
### ???one of two Hulus (both both Netflix, even though they look equally relevant)
### ???Why Amazon (no channel) but not channel = Amazon Prime Video or Amazon (c)
### ???Which CW, DirecTV, and Discovery to use

########################################################################################################################
######################################## Streaming events ##############################################################
########################################################################################################################
if connection == True:
    ### Pull only the most recent entry
    dvc_info = pd.read_sql_query('''
    SELECT DISTINCT 
      macaddress, folder, fsite, appliancetype, make, model,
      MAX(effectivetimestampest) AS effectivetimestampest, 
      MAX(creationdatetime     ) AS creationdatetime, 
      MAX(expirytimestampest   ) AS expirytimestampest
    FROM 
      (SELECT macaddress, effectivetimestampest, creationdatetime, expirytimestampest, 
         sourcelocationid || '-' || siteid AS fsite,
         sourcelocationid     AS folder,
         LOWER(appliancetype) AS appliancetype, 
         LOWER(make)          AS make, 
         LOWER(model)         AS model 
       FROM device_info WHERE setmetered = true) AS di
    GROUP BY macaddress, folder, fsite, appliancetype, make, model
    ORDER BY effectivetimestampest, creationdatetime, expirytimestampest
    ''', con)#.drop_duplicates(subset=['MACADDRESS'], keep='last')
    dvc_info.columns = [c.lower() for c in dvc_info.columns]
    for c in ['appliancetype', 'make', 'model']:
        dvc_info[c] = dvc_info[c].str.replace(' ', '')
    ### Remove macaddress entries with multiple fsite matches (~10%)
    n_dvc_rows = dvc_info.groupby('macaddress').fsite.nunique()
    uncnfl_dvcs = n_dvc_rows.loc[n_dvc_rows==1].index
    dvc_info_cln = dvc_info.loc[dvc_info.macaddress.isin(uncnfl_dvcs)].copy()
    ### Resolve macaddress entries with multiple device name matches
    dvc_info_cln = dvc_info_cln.fillna({'appliancetype': 'none',
                                        'make':          'none',
                                        'model':         'none'}).copy()
    dvc_info_cln.loc[dvc_info_cln.model=='na', 'model'] = 'none'
    dvc_info_cln = dvc_info_cln.drop_duplicates().copy()
    dvc_info_final = dvc_info_cln.groupby(['macaddress','folder','fsite'])\
                                 .agg({'appliancetype': lambda x: '/'.join(sorted(set(x))),
                                       'make':          lambda x: '/'.join(sorted(set(x))),
                                       'model':         lambda x: '/'.join(sorted(set(x))),
                                      }).reset_index()
    ### Deliberate device names
    dvc_info_final['dvc_cln'] = dvc_info_final.model
    dvc_info_final.loc[dvc_info_final.appliancetype.str.contains('stb'),      'dvc_cln'] = 'stb'
    dvc_info_final.loc[dvc_info_final.appliancetype.str.contains('tv'),       'dvc_cln'] = 'tv'
    dvc_info_final.loc[dvc_info_final.appliancetype.str.contains('dvd'),      'dvc_cln'] = 'dvd'
    dvc_info_final.loc[dvc_info_final.appliancetype.str.contains('computer'), 'dvc_cln'] = 'computer'
    dvc_type_count = dvc_info_final.dvc_cln.value_counts()
    dvc_info_final.loc[dvc_info_final.dvc_cln.isin(dvc_type_count.loc[dvc_type_count<10].index), 'dvc_cln'] = 'other'
    ### Naive device names
    # dvc_info_final['dvc_cln'] = ( dvc_info_final['appliancetype'].str.replace(' ','') + '-'
    #                             + dvc_info_final['make'         ].str.replace(' ','') + '-'
    #                             + dvc_info_final['model'        ].str.replace(' ','') )
    # assert dvc_info.macaddress.value_counts().iloc[0] == 1, 'ERROR: duplicate macaddress mappings'
    print('{} rows, {} dvcs, {} hhs'.format(dvc_info_final.shape[0], dvc_info_final.macaddress.nunique(),
                                            dvc_info_final.folder.nunique()))
    ### Standardize the device names
    # dvc_info['dvc_cln'] = ( dvc_info['appliancetype'].str.lower().str.replace(' ','').fillna('') + '-'
    #                       + dvc_info['make'         ].str.lower().str.replace(' ','').fillna('') + '-'
    #                       + dvc_info['model'        ].str.lower().str.replace(' ','').fillna('') )
### FCT table dates
    for name, table in [#('fct', 'fct_domainevents'),
                        ('strm', 'domain_event'),
                        # ('mime', 'mime_event'),
                        ]:
      if name == 'fct':
        t_st = 'TIMERANGESTARTEST'
        t_ed = 'TIMERANGEENDEST'
        sample = 'FOLDERID'
      else:
        t_st = 'STARTTIMEEST'
        t_ed = 'ENDTIMEEST'
        sample = 'MACADDRESS'
      for filter in [#'',
                     'WHERE dmxstreaming = true']:
        if not ((name=='mime') and (filter!='')):
          avail_dates_query = '''
              SELECT
                metercollectiondate AS date,
                MIN({})  AS t_st,
                MAX({})  AS t_ed,
                COUNT(*) AS n_events,
                COUNT(DISTINCT {}) AS samplesize
              FROM (SELECT * FROM {} {}) de
              GROUP BY metercollectiondate 
              ORDER BY metercollectiondate
              '''.format(t_st, t_ed, sample, table, filter)
          avail_dates_pull = pd.read_sql_query(avail_dates_query, con)
          avail_dates_pull.columns = [c.lower() for c in avail_dates_pull.columns]
          avail_dates = avail_dates_pull.set_index('date')
          avail_dates = avail_dates.reindex(pd.date_range(avail_dates.index.min(),
                                                          avail_dates.index.max(), freq='d'))\
                                           .fillna(0).reset_index().rename(columns={'index': 'date'})
          avail_dates['usable'] = ( (avail_dates.n_events   > 0)
                                  & (avail_dates.samplesize > 0)
                                  # & ( (avail_dates.metercollectiondate < dt.datetime(2017, 9, 1))
                                  #   | (avail_dates.samplesize > 1500))
                                  )
          f1, ax1 = plt.subplots(2, figsize=[16,6], sharex=True)
          ax1[0].plot(avail_dates['date'], avail_dates['n_events'], lw=1)
          ax1[0].set_ylabel('# events')
          ax1[0].set_title('SWOT Panel Data Supply (Netezza server, {}, {})'.format(table,
                                                                                    ['filtered', 'unfiltered'][int(filter=='')]))
          ax1[1].plot(avail_dates['date'], avail_dates['samplesize'], lw=1)
          ax1[1].set_ylabel('Sample size (# hhs or dvcs)')
          f1.savefig(dr+'data_availability_{}{}.png'.format(name, ['_filter', ''][int(filter=='')]))
          plt.close(f1)
          avail_dates_inscope = avail_dates.loc[ avail_dates.usable
                                               & avail_dates.date.between(*date_range) ]
          print('{}: {} dates available between {} and {}'.format(table, avail_dates_inscope.shape[0], date_range[0], date_range[1]))

    ### Get streaming data
    # pd.read_sql_query('''SELECT * FROM domain_event LIMIT 5''', con).columns
    print('reading streaming_events from server')
    # strm_query = '''
    # SELECT DISTINCT
    #   de.starttimeest, de.endtimeest, de.macaddress,
    #   dd.*
    # FROM
    #   (SELECT DISTINCT
    #      STARTTIMEEST, ENDTIMEEST, MACADDRESS, DOMAIN
    #    FROM domain_event
    #    WHERE 1=1
    #      AND metercollectiondate between '{st}' AND '{ed}'
    #                     ) de,
    #      fct_domainevents fd,
    #      dim_domain       dd
    # WHERE 1=1
    #   AND de.domain     = dd.domain
    #   AND fd.domainkey  = dd.domainkey
    #   AND fd.dmxstreaming = True
    #   AND fd.treeid in {tr}
    # LIMIT 10'''.format(tr=use_trees, st=date_range[0], ed=date_range[1])
    strm_query = '''
    SELECT DISTINCT 
            t.*
    FROM (
            SELECT DISTINCT 
                    starttimeest,
                    endtimeest,
                    macaddress,
                    treeid as tree_id
            FROM
                    domain_event de
            INNER JOIN
                    (SELECT DISTINCT
                            d.domainkey, 
                            d.domain,
                            de.treeid
                    FROM
                            fct_domainevents de,
                            dim_domain d
                    WHERE 1=1
                    AND de.domainkey = d.domainkey
                    --AND de.treeid in {tr}
                    AND de.dmxstreaming = True
                    ) as ids
            ON 
            de.domain = ids.domain
            WHERE 1=1
            AND de.metercollectiondate between '{st}' AND '{ed}'
    ) t
    '''.format(tr=use_trees, st='2018-02-15', ed='2018-03-15')#st=date_range[0], ed=date_range[1])
    streaming_events = pd.read_sql_query(strm_query, con)
    streaming_events.columns = [str.lower(x) for x in streaming_events.columns]

    test = streaming_events.merge(dvc_info_final, on='macaddress', how='outer')
    test['dvc_tbl'] = test.appliancetype.notnull()
    test['evt_tbl'] = test.starttimeest.notnull()
    test.groupby(['dvc_tbl', 'evt_tbl']).folder.nunique()
# -------------------------------------------------------------------------------------------------------------------- #
    print('reading mime_events from server')
    mime_query = '''SELECT DISTINCT
            starttimeest,
            endtimeest,
            macaddress AS mac_address,
            treeid as tree_id
        FROM
            mime_event me
        INNER JOIN
            (SELECT DISTINCT
                d.domainkey,
                d.domain,
                de.treeid
            FROM
                fct_domainevents de,
                dim_domain d
            WHERE 1=1
            AND de.domainkey = d.domainkey
          AND de.treeid in {0}
            AND de.dmxstreaming = True
            ) as ids
        ON
        me.domain = ids.domain
        WHERE 1=1
          AND me.metercollectiondate between '{1}' AND '{2}'
          AND lower(me.streaming) = 'true'
          AND macaddress in {3} '''.format(use_trees, *date_range, mw_macs)
    mime_events = pd.read_sql_query(mime_query, con)
    mime_events.columns = [str.lower(x) for x in mime_events.columns]
# -------------------------------------------------------------------------------------------------------------------- #
    print('building strm')
    strm_shape, mime_shape = streaming_events.shape, mime_events.shape
    strm_macs,  mime_macs  = streaming_events.mac_address.nunique(), mime_events.mac_address.nunique()
    strm1 = pd.concat([streaming_events, mime_events], ignore_index=True)
    del streaming_events
    del mime_events
    if checkpoint:
        strm1.to_csv(dr + 'checkpoint_strm1.csv')
else:
    print('read strm from file')
    strm1 = pd.read_csv(dr + 'checkpoint_strm1.csv', index_col=0)

for c in ['starttimeest','endtimeest']:
    strm1[c] = pd.to_datetime(strm1[c])

# -------------------------------------------------------------------------------------------------------------------- #
### join with mw to get device names
strm2 = strm1.merge(mw[['hh_id','mac_address','fsite','dvc_cln']],
                    on='mac_address', how='inner')
print('Drop streaming events with no MW table entry: {0} -> {1}'.format(strm1.shape[0], strm2.shape[0]))
strm = strm2[['hh_id', 'fsite', 'mac_address', 'dvc_cln', 'tree_id', 'starttimeest', 'endtimeest']]
strm.sort_values(['fsite', 'dvc_cln', 'starttimeest', 'endtimeest'], inplace=True)
strm.reset_index(drop=True, inplace=True)

########################################################################################################################
########################################### Tuning events ##############################################################
########################################################################################################################
if connection == True:
    print('reading tune from server')
    ### ??? why is the tree_id limit commented out? should it be?
    tune_query = '''SELECT DISTINCT
        t.folder_id || '-' || t.siteunitid as fsite,
        t.folder_id AS hh_id,
        t.dist_sourceid,
        t.viewed_startest as start_time,
        t.viewed_endest as end_time,
        t.tree_id,
        t.ottmdevicekey as dkey,
        d.ottm_devicename as device
    FROM
        admin.fct_ottM_byminute t,
        admin.dim_ottmdevice d
    WHERE 1=1
      AND d.ottmdevicekey = t.ottmdevicekey
      AND t.FOLDER_ID || '-' || t.SITEUNITID in {0}
      --AND t.tree_id in {1}
      AND t.metercollectiondate between '{2}' and '{3}'
      AND t.viewed_startest <= t.viewed_endest
    ORDER BY
        fsite asc,
        viewed_startest asc,
        viewed_endest asc,
        tree_id asc,
        dist_sourceid asc'''.format(tuple(strm.fsite.dropna().unique().tolist()), #  tuple(mw.fsite.dropna().unique().tolist()),
                                    use_trees, *date_range)
    tune1 = pd.read_sql_query(tune_query, con)
    if checkpoint:
        tune1.to_csv(dr + 'checkpoint_tune1.csv')
else:
    tune1 = pd.read_csv(dr + 'checkpoint_tune1.csv', index_col=0)

tune1.columns = [str.lower(x) for x in tune1.columns]
for c in ['start_time','end_time']:
    tune1[c] = pd.to_datetime(tune1[c])

########################################################################################################################
########################################################################################################################
########################################################################################################################
### Create common device categories, with mappings for strm/tune

tune1['dvc_cln'] = tune1.device.str.lower().str.replace(' ', '').fillna('')

tune_dvcs = tune1.dvc_cln.value_counts()
strm_dvcs =  strm.dvc_cln.value_counts()

### Save
tune_dvcs.to_csv('{}tune_dvcs.csv'.format(dr))
strm_dvcs.to_csv('{}strm_dvcs.csv'.format(dr))

### Get strm-tune matching file and common device assignments
from device_names import panel_common_device_mapping, get_device_key
st_tn_match = panel_common_device_mapping(strm_dvcs, tune_dvcs, group_roku=True)
st_tn_match.to_csv(dr + 'strm_tune_common_device_mappings.csv')

### Mapping key for each dataset
if 'roku' in tune_dvcs.index:
    tune_skip = None
else:
    tune_skip = 'roku'
tune_dvc_key = get_device_key(st_tn_match, 'tn', n_input_dvcs=tune_dvcs.shape[0], skip_val=tune_skip)
strm_dvc_key = get_device_key(st_tn_match, 'st', n_input_dvcs=strm_dvcs.shape[0])

### Join new device names to the data
if 'device' in tune1.columns:
    tune1 = tune1.drop('device', axis=1)
tune1 = tune1.merge(tune_dvc_key, on='dvc_cln', how='left')
if 'device' in strm.columns:
    strm = strm.drop('device', axis=1)
strm = strm.merge(strm_dvc_key, on='dvc_cln', how='left')

### Compare frequency of each type
dvc_counts = pd.DataFrame({'n_tn_ssns': tune1.device.value_counts(),
                           'n_st_ssns': strm.device.value_counts()}).fillna(0)\
               .sort_values(['n_tn_ssns','n_st_ssns'], ascending=False)
print(dvc_counts)

########################################################################################################################
########################################################################################################################
########################################################################################################################
print('            mw   strm  tune1:')
print('events: {0: >6} {1: >6} {2: >6}'.format(mw.shape[0], strm.shape[0], tune1.shape[0]))
print('macs:   {0: >6} {1: >6}'.format(mw.mac_address.nunique(), strm.mac_address.nunique()))

print('         mw strm tune')
print('hhs:   ', mw.hh_id.nunique(),       strm.hh_id.nunique(), tune1.hh_id.nunique())
print('fsites:', mw.fsite.nunique(),       strm.fsite.nunique(), tune1.fsite.nunique())
print('macs:  ', mw.mac_address.nunique(), strm.mac_address.nunique())

hhid_track = pd.DataFrame([], columns=['mw','strm','tune1','tune3','tune4','tune'], dtype='int')
hhid_track.loc[0] = [mw.hh_id.nunique(), strm.hh_id.nunique(), tune1.hh_id.nunique(), -1, -1, -1]

########################################################################################################################
### Join back to back identical events
tune1 = tune1.sort_values(['hh_id','fsite','dkey','start_time','end_time'])
tune1['gap'] = (tune1.start_time - tune1.end_time.shift(1)) / dt.timedelta(minutes=1)
tune1['new_grp'] = ( (tune1.fsite         != tune1.fsite.shift(1))
                   | (tune1.tree_id       != tune1.tree_id.shift(1))
                   | (tune1.dist_sourceid != tune1.dist_sourceid.shift(1))
                   | (tune1.dkey          != tune1.dkey.shift(1))
                   | (tune1.device        != tune1.device.shift(1))
                   | (tune1.gap           != 0))
tune1['ssn_id']  = tune1.new_grp.cumsum()
tune2 = tune1.groupby(['ssn_id','fsite','hh_id','dkey','dvc_cln','device','dist_sourceid','tree_id'])\
             .agg({'start_time': 'min',
                   'end_time':   'max'})\
             .reset_index().sort_values(['hh_id','fsite','start_time','end_time'])

print('merge back to back same-station events: {0} -> {1}'.format(tune1.shape[0], tune2.shape[0]))
# -------------------------------------------------------------------------------------------------------------------- #
### Label activity class (previously done on tune4)
tune2['activity'] = None
tune2.loc[ (tune2.dist_sourceid == 0),      'activity'] = 'off'
tune2.loc[ (tune2.dist_sourceid == 10202)
         & (tune2.tree_id       == 0),      'activity'] = 'mute'
tune2.loc[~(tune2.dist_sourceid.isin([0, 10202]))
         & (tune2.tree_id       == 0),      'activity'] = 'other'
tune2.loc[ (tune2.dist_sourceid != 0)
         & (tune2.tree_id       != 0),      'activity'] = 'streaming'

tune2.loc[ (tune2.dkey != tune2.dkey.shift(1))
         & (tune2.dkey != 3500001)
         & (tune2.activity.isin(['mute', 'other'])), 'activity'] = 'sourcechange'

tune2.loc[(tune2.activity == 'other') &
          (tune2.activity.shift( 1) == 'streaming')   &
          (tune2.activity.shift(-1) == 'streaming')   &
          (tune2.fsite == tune2.fsite.shift( 1)) &
          (tune2.fsite == tune2.fsite.shift(-1)), 'activity'] = 'streaming'

### Label provider based on tree_id
tree_ids_condensed = tree_ids[['tree_id','provider','brand','parent']]\
                       .groupby('tree_id').agg({'provider':'sum', 'brand': 'sum', 'parent':'count'}).sort_values('parent')\
                       .reset_index()
strm  =  strm.merge(tree_ids_condensed[['tree_id','provider','brand']], on='tree_id', how='left')
tune2 = tune2.merge(tree_ids_condensed[['tree_id','provider','brand']], on='tree_id', how='left')
tune2.loc[tune2.tree_id == 0, 'brand'] = 'nonstreaming'

# ### Reassign device names again?
# tune2['dvc_cln'] = tune2.device.str.lower().str.replace(' ', '')
# tune2['new_dvc'] = 'Other'
# for d in pnl_dvc_names:
#     tune2.loc[tune2.dvc_cln.str.contains(d.lower().replace(' ', '')), 'new_dvc'] = d
# # mw.loc[(mw.appliance_type=='TV')  & (mw.device=='Other'), 'device'] = 'Smart TV'
# tune2.loc[tune2.device  == 'DVD',  'new_dvc'] = 'Bluray'
# tune2.loc[tune2.dvc_cln == 'wiiu', 'new_dvc'] = 'Wii U'
# # tune2.loc[tune2.tree_id == 0, 'new_dvc'] = 'nonstreaming'
#
# ### Examine brand representation breakdown
# tune2.groupby(['provider', 'brand', 'tree_id']).fsite.count()
# tune2.groupby(['new_dvc', 'device', 'dkey']).fsite.count()

########################################################################################################################
### Analyze tree_id appearances
### How many streaming events from each tree id?
tree_ids['strm_events'] = list(strm.tree_id.value_counts().loc[list(tree_ids.tree_id)].fillna(0))
tree_ids['strm_hhs']    = list(strm.groupby('tree_id').agg({'hh_id':pd.Series.nunique}).hh_id.loc[list(tree_ids.tree_id)].fillna(0))

### How many streaming events from each tree id?
tree_ids['tune_events'] = list(tune2.tree_id.value_counts().loc[list(tree_ids.tree_id)].fillna(0))
tree_ids['tune_hhs']    = list(tune2.groupby('tree_id').agg({'hh_id':pd.Series.nunique}).hh_id.loc[list(tree_ids.tree_id)].fillna(0))

tree_ids.sort_values(['tune_events','strm_events'], ascending=False, inplace=True)

########################################################################################################################
### Why are there fsites in streaming but not tuning, and vice versa?
### If a tuning session otherwise appears to be streaming but has no associated streaming events, what does it mean???
### 156k of 543k events from fsites not in tune
### providers roughly even fractions
tune_fsites = tune2.loc[tune2.tree_id.isin(tree_ids.loc[tree_ids.use, 'tree_id'].unique())].fsite.unique()
strm_fsites =  strm.loc[ strm.tree_id.isin(tree_ids.loc[tree_ids.use, 'tree_id'].unique())].fsite.unique()

strm[ 'in_tune'] =  strm.fsite.isin(tune_fsites)
tune2['in_strm'] = tune2.fsite.isin(strm_fsites)

########################################################################################################################
# tune1['overlap'] = tune1.start_time - tune1.end_time.shift(1)
# tune1.loc[(tune1.overlap & (tune1.tree_id != 0)) | (tune1.overlap.shift(-1) & (tune1.tree_id.shift(-1) != 0))]

########################################################################################################################
strm['start_min'] = strm.starttimeest.dt.round('min')
strm_counts = strm.pivot_table(columns='in_tune', index='start_min', values='fsite', aggfunc='count')
strm_counts.rename(columns={False: 'no_tune', True: 'tune'}, inplace=True)
strm_counts = strm_counts.reindex(pd.date_range(strm_counts.index.min(), strm_counts.index.max(), freq='min')).fillna(0)
strm_counts['total'] = strm.groupby('start_min').fsite.count()
strm_counts.fillna(0, inplace=True)

tune_counts = tune2.pivot_table(columns='in_strm', index='start_time', values='fsite', aggfunc='count')
tune_counts.rename(columns={False: 'no_strm', True: 'strm'}, inplace=True)
tune_counts = tune_counts.reindex(pd.date_range(tune_counts.index.min(), tune_counts.index.max(), freq='min')).fillna(0)
tune_counts['total'] = tune2.groupby('start_time').fsite.count()
tune_counts.fillna(0, inplace=True)

outer_trange = (min(strm_counts.index.min(), tune_counts.index.min()),
                max(strm_counts.index.max(), tune_counts.index.max()))
inner_trange = (max(strm_counts.index.min(), tune_counts.index.min()),
                min(strm_counts.index.max(), tune_counts.index.max()))

f1, ax1 = plt.subplots(2, figsize = [18, 10], sharex=True)
f1.autofmt_xdate();
ax1[0].xaxis.set_major_formatter(matplotlib.dates.DateFormatter('%y-%m-%d'));
ax1[0].plot(strm_counts.index.to_pydatetime(), strm_counts.total,   c='blue',  label='All streams')
ax1[0].plot(strm_counts.index.to_pydatetime(), strm_counts.tune,    c='green', label='fsite w/ tune')
ax1[0].plot(strm_counts.index.to_pydatetime(), strm_counts.no_tune, c='red',   label='fsite w/o tune')
ax1[0].legend(loc='upper left', frameon=True, framealpha=0.5)

ax1[1].xaxis.set_major_formatter(matplotlib.dates.DateFormatter('%y-%m-%d'));
ax1[1].plot(tune_counts.index.to_pydatetime(), tune_counts.total,   c='blue',  label='All tunes')
ax1[1].plot(tune_counts.index.to_pydatetime(), tune_counts.strm,    c='green', label='fsite w/ stream')
# ax1[1].plot(tune_counts.index, tune_counts.no_strm, c='red',   label='fsite w/o stream')
ax1[1].set_xlim(*outer_trange)
ax1[1].set_ylim(0, 200)
ax1[1].legend(loc='upper left', frameon=True, framealpha=0.5)
f1.savefig('tune_v_strm_counts.png')
plt.close(f1)

########################################################################################################################
########################################### Process events #############################################################
########################################################################################################################
### Remove simultaneous events (with different devices -- just keeps first one) (i.e. credit_results_minwin)
### why are these overlaps here at all??? how do we know we're removing the right ones???
### Sorted by tree/dist id => keep last one to get streaming, if any
### Also filter overlapping events down to one (FUNCTIONAL CHANGE: sort to preferentially keep streaming if any)
tune2['nonstreaming'] = tune2.activity != 'streaming'
if strmsort:
    sort_cols = ['fsite','start_time','end_time','nonstreaming']
else:
    sort_cols = ['fsite', 'start_time', 'end_time']
tune3 = tune2.loc[tune2.start_time.between(*inner_trange) & tune2.end_time.between(*inner_trange)]\
             .sort_values(sort_cols)\
             .groupby(['fsite','start_time','end_time']).first().reset_index()

# hhid_track.loc[1] = [mw.hh_id.nunique(), strm.hh_id.nunique(), tune1.hh_id.nunique(), tune3.hh_id.nunique(), -1, -1]
# print(hhid_track)
print('before/after dropping simultaneous events', tune2.shape[0], tune3.shape[0])
# -------------------------------------------------------------------------------------------------------------------- #
### Change dkey while both dist_sourceid and tree_id are zero
### dkey = -1 is not in the original data
### This means these rows will cause the next row not to match, if it otherwise would have
tune3.loc[ (tune3.dist_sourceid == 0)
         & (tune3.tree_id == 0)      , 'dkey'] = -1

### Previous row values
tune3['prev_tree_id'] = tune3.tree_id.shift(1).fillna(0).astype(int)
tune3['prev_fsite']   = tune3.fsite.shift(1)
tune3['prev_dkey']    = tune3.dkey.shift(1)
# -------------------------------------------------------------------------------------------------------------------- #
### Determine which sessions to group together into one
tune3['new_group']    = 1
### Are fsite, tree_id, and dkey same as previous row? Then same group (why not distsource???, why no gap check?)
tune3.loc[ (tune3.prev_fsite   == tune3.fsite)
         & (tune3.prev_dkey    == tune3.dkey)
         & (tune3.prev_tree_id == tune3.tree_id), 'new_group'] = 0
### New group if previous dist_sourceid and tree_id were 0 but this one is not
tune3.loc[ (tune3.dist_sourceid          != 0)
         & (tune3.dist_sourceid.shift(1) == 0)
         & (tune3.tree_id.shift(1)       == 0), 'new_group'] = 1
### First row: if tree_id and dist_sourceid are both zero, don't start a new session
### (I don't think this actually affects anything??? just decreases all group ids by one
if tune3.loc[0,'tree_id'] == 0 and tune3.loc[0,'dist_sourceid'] == 0:
    tune3.loc[0,'new_group'] = 0
### Apply gapfix here too?
tune3['gap'] = (tune3.start_time - tune3.end_time.shift(1)) / dt.timedelta(seconds=1)
gapfix_changes3 = tune3.loc[(tune3.new_group == 0) & (tune3.gap != 0)] # list of events that would be affected
if gapfix:
    tune3.loc[tune3.gap != 0, 'new_group'] = 1

# -------------------------------------------------------------------------------------------------------------------- #
### Assign group ids based on where we put new session markers
tune3['groupID'] = tune3.new_group.cumsum()

#######################################################################################################################
## Group by assigned number (i.e. tune4)
agg_dict1 = {'fsite': 'first',
            'hh_id':  'first',
            'dkey':'first',
            'dvc_cln':'first',
            'device':'first',
            'provider': 'first',
            'start_time':'first',
            'end_time':'last',
            'activity': 'first',
            'dist_sourceid':'last',
            'tree_id':'first'}

tune4 = tune3.groupby('groupID').agg(agg_dict1).reset_index(drop=True)
t3_cols = ['fsite','hh_id','dkey','dvc_cln','device','provider','activity','start_time','end_time','dist_sourceid','tree_id']
t3_cols += [k for k in agg_dict1.keys() if k not in t3_cols]
tune4 = tune4[t3_cols]
tune4['duration'] = (tune4.end_time - tune4.start_time) / dt.timedelta(seconds=1)

# hhid_track.loc[2] = [mw.hh_id.nunique(), strm.hh_id.nunique(), tune1.hh_id.nunique(),
#                      tune3.hh_id.nunique(), tune4.hh_id.nunique(), -1]
# print(hhid_track)

# -------------------------------------------------------------------------------------------------------------------- #
### Assign new group ids based on rules
tune4['new_grp']     = 0

#rule 1: streaming => new session
tune4.loc[tune4.activity == 'streaming', 'new_grp'] = 1

#rule 2: off/sourcechange => new session
tune4.loc[tune4.activity.isin(['off','sourcechange']), 'new_grp'] = 1

#rule 3: streaming-mute-streaming => second stream is not new session
# doesn't check that device/provider are the same though
tune4.loc[ (tune4.activity          == 'streaming')
         & (tune4.activity.shift(1) == 'mute')
         & (tune4.activity.shift(2) == 'streaming'), 'new_grp'] = 0

#rule 4: fsite change => new session
tune4.loc[tune4.fsite != tune4.fsite.shift(1), 'new_grp'] = 1

# FUNCTIONAL CHANGE:
# rule 0: gap => new session
# do this last to override the lack of gap-checks in the other rules
tune4['gap'] = (tune4.start_time - tune4.end_time.shift(1)) / dt.timedelta(seconds=1)
gapfix_changes4 = tune4.loc[(tune4.new_grp == 0) & (tune4.gap != 0)] # list of events that would be affected
if gapfix:
    tune4.loc[tune4.gap != 0, 'new_grp'] = 1

### Assign new session ids
tune4['groupID'] = tune4.new_grp.cumsum()

########################################################################################################################
### Create final grouped tuning sessions
agg_dict2 = {'fsite':         'first',
             'hh_id':         'first',
             'dvc_cln':       'first',
             'device':        'first',
             'provider':      'first',
             'tree_id':       'first',
             'dist_sourceid': 'last',
             'activity':   lambda x: 'streaming' if 'streaming' in x.tolist() else 'nonstreaming',
             'start_time':    'first',
             'end_time':      'last',
             'duration':      'sum'}

print('creating swot_master_table')
tune = tune4.groupby('groupID').agg(agg_dict2)
tf_cols = ['fsite', 'hh_id', 'dvc_cln', 'device', 'provider',
           'tree_id', 'dist_sourceid', 'activity', 'start_time', 'end_time', 'duration']
tf_cols += [k for k in agg_dict2.keys() if k not in tf_cols]
tune = tune[tf_cols]
# hhid_track.loc[3] = [mw.hh_id.nunique(), strm.hh_id.nunique(), tune1.hh_id.nunique(),
#                      tune3.hh_id.nunique(), tune4.hh_id.nunique(), tune.hh_id.nunique()]

# -------------------------------------------------------------------------------------------------------------------- #
### remove sessions that aren't streaming (some hhs lost at this point)
tune.drop(tune[tune.activity != 'streaming'].index, axis=0, inplace=True)
tune.reset_index(inplace=True)
# hhid_track.loc[4] = [mw.hh_id.nunique(), strm.hh_id.nunique(), tune1.hh_id.nunique(),
#                      tune3.hh_id.nunique(), tune4.hh_id.nunique(), tune.hh_id.nunique()]

# -------------------------------------------------------------------------------------------------------------------- #
### Get time of next stream start
### If next line is a different device, assign nonsense value two months in the future
tune['next_tune_start'] = tune.start_time.shift(-1)
big_gap = (tune.end_time.max() - tune.start_time.min() + dt.timedelta(days=30)).ceil('d')
tune.loc[tune.fsite != tune.fsite.shift(-1),'next_tune_start'] += big_gap
### Assign the last row to the future also
tune.next_tune_start.fillna(big_gap + tune.end_time, inplace=True)

# hhid_track.loc[5] = [mw.hh_id.nunique(), strm.hh_id.nunique(), tune1.hh_id.nunique(),
#                      tune3.hh_id.nunique(), tune4.hh_id.nunique(), tune.hh_id.nunique()]

# -------------------------------------------------------------------------------------------------------------------- #
### Determine if event is final event, i.e. last event before a gap
strm['final_streaming_event'] = False
### Is it the last one on this device?
strm.loc[ (strm.fsite  != strm.fsite.shift( -1))
        | (strm.dvc_cln != strm.dvc_cln.shift(-1)), 'final_streaming_event'] = True
### If not the last one, and it ends more than 4 min away from next event start
### why require not already True??? I don't think this does anything
strm.loc[~(strm.final_streaming_event)
        & (abs(strm.starttimeest.shift(-1) - strm.endtimeest) / dt.timedelta(seconds=1) > end_streaming_event_threshold),
        'final_streaming_event'] = True
### If next row is new site, now change it to False
### why did we set this to True earlier then???
strm.loc[(strm.fsite != strm.fsite.shift(-1)), 'final_streaming_event'] = False
### Why was there a df filtering out to just final ones, then not used??? why is this sort here???
strm.sort_values(['fsite', 'starttimeest'], inplace=True)

# hhid_track.loc[6] = [   mw.hh_id.nunique(),  strm.hh_id.nunique(), tune1.hh_id.nunique(),
#                      tune3.hh_id.nunique(), tune4.hh_id.nunique(),  tune.hh_id.nunique()]

########################################################################################################################
tune.to_csv(dr + 'checkpoint_tune.csv')

### Drop unidentified tree_ids
# tune = tune.merge(use_trees[['tree_id','provider']], on='tree_id', how='left')
tune.dropna(subset=['provider'], inplace=True)
tune = tune.loc[tune.provider != '']
# hhid_track.loc[7] = [mw.hh_id.nunique(), strm.hh_id.nunique(), tune1.hh_id.nunique(),
#                      tune3.hh_id.nunique(), tune4.hh_id.nunique(), tune.hh_id.nunique()]
# print(hhid_track)

### Change device labels to be consistent with streaming
# tune['device'] = tune.new_dvc.copy()
# tune.drop(['new_dvc'], axis=1, inplace=True)
# hhid_track.loc[8] = [mw.hh_id.nunique(), strm.hh_id.nunique(), tune1.hh_id.nunique(),
#                      tune3.hh_id.nunique(), tune4.hh_id.nunique(), tune.hh_id.nunique()]
# print(hhid_track)

########################################################################################################################
tune['tune_ind'] = tune.index
strm['strm_ind'] = strm.index
timeline = pd.concat([tune[['fsite','device','tune_ind','start_time','end_time','next_tune_start']],
                      strm[['fsite','device','strm_ind','starttimeest', 'endtimeest', 'final_streaming_event']] ],
                     axis=0, join='outer', ignore_index=True)
tune.drop('tune_ind', axis=1, inplace=True)
strm.drop('strm_ind', axis=1, inplace=True)
assert timeline.shape[0] == tune.shape[0] + strm.shape[0], 'ERROR: timeline is not sum of swot and dmx'
timeline = timeline[['fsite','device',
                     'tune_ind','start_time','end_time','next_tune_start',
                     'strm_ind','starttimeest', 'endtimeest', 'final_streaming_event']]
# timeline['both_times'] = timeline.end_time.copy()
timeline['both_times'] = timeline.start_time.copy()
timeline.loc[timeline.both_times.isnull(), 'both_times'] = timeline.starttimeest
### Sort by: device and common time, then: sort by starttimeest to get swot before dmx if common time is the same
# timeline = timeline.sort_values(['fsite','device','both_times','start_time','end_time','starttimeest','endtimeest'])
timeline = timeline.sort_values(['fsite','device','both_times','starttimeest','endtimeest','start_time','end_time'])\
                   .reset_index()
### What is the endtime of the previous tuning session, and the start of the next one?
timeline['tn_endtime']   = timeline.end_time.fillna(method='ffill')
timeline['tn_nextstart'] = timeline.next_tune_start.fillna(method='ffill')
# timeline['tn_prevend']   = timeline.prev_end_time.fillna(method='bfill')
timeline['tn_nextstarttime'] = timeline.start_time.fillna(method='bfill')
### Does the stream start between the above two times?
timeline['strm_in_gap']  = timeline.starttimeest.between(timeline.tn_endtime, timeline.tn_nextstart)
### Eric's code only looks at strms in the gaps between tunes, so we filter down to this subset
btwn_tunes = timeline.loc[timeline.tune_ind.notnull() | timeline.strm_in_gap].copy()
### Identify start of new id/device
btwn_tunes['new_dvc'] = ( (btwn_tunes.fsite  != btwn_tunes.fsite.shift(1))
                        | (btwn_tunes.device != btwn_tunes.device.shift(1)) )
### Identify which streams are associated with this tune
btwn_tunes['new_ssn'] = btwn_tunes.tune_ind.notnull() | btwn_tunes.new_dvc
btwn_tunes['ssn_id']  = btwn_tunes.new_ssn.cumsum()
### Is the next row part of the same session?
btwn_tunes['next_samessn'] = (btwn_tunes.ssn_id.shift(-1) == btwn_tunes.ssn_id)
### Is the first following streaming session near the end of the tuning session?
btwn_tunes['next_close']   = ((btwn_tunes.starttimeest.shift(-1) - btwn_tunes.end_time)#.abs()
                              / dt.timedelta(seconds=1) <= end_streaming_event_threshold)
### Determining whether to change the endtime (if the next stream after the tune is in the same ssn and follows closely
btwn_tunes['change_endtime'] = ( btwn_tunes.tune_ind.notnull()
                               & btwn_tunes.next_samessn
                               & btwn_tunes.next_close
                               & (btwn_tunes.ssn_id != -1))
### Within each identified group, find the endtime of the first final_streaming_event
### and the endtime of the last event (why not latest endtime? sometimes there are multiple of same starttime, or overlap)
### ??? I don't think this is really appropriate, as the last stream may actually go with the following tune in many cases
### and this approach doesn't stop the SWOT when the next tune starts
ssn_aggs = btwn_tunes.groupby('ssn_id').agg({'endtimeest':'last',
                                             'change_endtime':'first',
                                             'tune_ind': 'first'})
ssn_aggs.rename(columns={'endtimeest':'last_event_endtime',
                         'change_endtime': 'valid_group'}, inplace=True)
ssn_aggs['first_finalevent_endtime'] = btwn_tunes.loc[ btwn_tunes.final_streaming_event
                                                     & btwn_tunes.final_streaming_event.notnull()]\
                                                 .groupby('ssn_id').endtimeest.first()
if -1 in ssn_aggs.index:
    ssn_aggs.drop(-1, inplace=True)
### If there is a final_streaming_event, assign that to the swot event
### otherwise assign the final time
ssn_aggs['streaming_endtime'] = ssn_aggs.first_finalevent_endtime.copy()
ssn_aggs.loc[ssn_aggs.first_finalevent_endtime.isnull(), 'streaming_endtime'] = ssn_aggs.last_event_endtime
ssn_aggs.loc[~ssn_aggs.valid_group, 'streaming_endtime'] = np.nan
### Get the rows that are changing
change_swot = ssn_aggs.loc[ssn_aggs.valid_group].copy()
### Make df alignable with tune df
change_swot['tune_ind'] = change_swot.tune_ind.astype('int')
change_swot = change_swot.set_index('tune_ind')


# ### Identify which streams are associated with this tune
# timeline['new_ssn'] = timeline.tune_ind.notnull() | timeline.new_dvc
# timeline['ssn_id']  = timeline.new_ssn.cumsum()
# ### Is the next row part of the same session?
# timeline['next_samessn'] = (timeline.ssn_id.shift(-1) == timeline.ssn_id)
# ### Only look at streams that start after the end of the tune
# ### ??? I think this is wrong, it should be that overlap the gap in between at all
# ### so (endtimeest > tn_endtime) & (starttimeest < tn_nextstart)
# timeline.loc[~timeline.strm_in_gap & timeline.tune_ind.isnull(), 'ssn_id'] = -1
# ### Is the first following streaming session near the end of the tuning session?
# timeline['next_close']   = (timeline.starttimeest.shift(-1) - timeline.end_time).abs()/dt.timedelta(seconds=1) <= end_streaming_event_threshold
# ### Determining whether to change the endtime (this is not right -- needs to be done in ssn_agg step)
# timeline['change_endtime'] = timeline.tune_ind.notnull() & timeline.next_samessn & timeline.next_close & (timeline.ssn_id != -1)
# ### Identify the last stream (stream that's part of a session, and following row is either a different session or null)
# timeline['is_last_strm'] = ( timeline.tune_ind.isnull() & timeline.ssn_id.notnull()
#                            & (timeline.ssn_id.shift(-1).isnull() | (timeline.ssn_id.shift(-1) != timeline.ssn_id)))
# ### If there are two streams with identical times, should both count as last?
# timeline.loc[ (timeline.starttimeest == timeline.starttimeest.shift(-1))
#             & (timeline.endtimeest   == timeline.endtimeest.shift(-1))
#             & (timeline.ssn_id       == timeline.ssn_id.shift(-1))
#             & timeline.is_last_strm.shift(-1), 'is_last_strm'] = True
# ### Drop dmx sessions that aren't used for the next step
# used_lines = timeline.loc[( timeline.tune_ind.notnull()
#                           | timeline.final_streaming_event
#                           | timeline.is_last_strm)].copy()
# ### Within each identified group, find the endtime of the first final_streaming_event
# ### and the endtime of the last event (why not latest endtime? sometimes there are multiple of same starttime, or overlap)
# ssn_aggs = used_lines.groupby('ssn_id').agg({'endtimeest':'last',
#                                              'change_endtime':'first'})
# ssn_aggs.rename(columns={'endtimeest':'last_event_endtime',
#                          'change_endtime': 'valid_group'}, inplace=True)
# ssn_aggs['first_finalevent_endtime'] = used_lines.loc[used_lines.final_streaming_event & used_lines.final_streaming_event.notnull()].groupby('ssn_id').endtimeest.first()
# ssn_aggs.drop(-1, inplace=True)
# ### If there is a final_streaming_event, assign that to the swot event
# ### otherwise assign the final time
# ssn_aggs['use_endtime'] = ssn_aggs.first_finalevent_endtime.copy()
# ssn_aggs.loc[ssn_aggs.first_finalevent_endtime.isnull(), 'use_endtime'] = ssn_aggs.last_event_endtime
# ssn_aggs.loc[~ssn_aggs.valid_group, 'use_endtime'] = np.nan
### many of the new endtimes are after the next starttime -- was that true before?

# change_swot = timeline.loc[timeline.tune_ind.notnull() & timeline.change_endtime,
#                            ['tune_ind','ssn_id','end_time','next_tune_start','change_endtime']].copy()
# change_swot['new_endtime'] = list(ssn_aggs.loc[list(change_swot.ssn_id), 'use_endtime'])
# assert change_swot.new_endtime.isnull().sum() == 0, 'ERROR: missing endtime'
# # change_swot.loc[~change_swot.change_endtime, 'new_endtime'] = change_swot.end_time
# change_swot['new_endmin'] = change_swot.new_endtime.dt.round('min')
# change_swot['tune_ind'] = change_swot.tune_ind.astype('int')
# change_swot = change_swot.set_index('tune_ind')

### Fill in streaming endtime in original table
tune['streaming_endtime'] = tune.end_time.copy()
# tune.loc[change_swot.index, 'streaming_endtime'] = change_swot.new_endmin
tune.loc[change_swot.index, 'streaming_endtime'] = change_swot.streaming_endtime
### ??? Restrict trailing streaming to before start of next tune
# tune.loc[tune.streaming_endtime > tune.next_tune_start, 'streaming_endtime'] = tune.loc[tune.streaming_endtime > tune.next_tune_start, 'next_tune_start']
assert tune.streaming_endtime.isnull().sum() == 0, 'ERROR: missing endtime'
#----------------------------------------------------------------------------------------------------------------------#
### Visualize a few sets of activity
if make_activity_plots:
    from session_plot_fns import plot_ssn_rows
    import random as rd
    timeline['vw_id'] = timeline.fsite + '-' + timeline.device# +'-' +  timeline.provider
    tune['vw_id']     =     tune.fsite + '-' +     tune.device# +'-' +  timeline.provider
    vw_id_list = timeline.groupby('vw_id').agg({'tune_ind': 'count',
                                                'strm_ind': 'count'})
    # v_list = rd.sample(list(vw_id_list.loc[(vw_id_list > 0).all(axis=1)].index), 3)
    v_list = []
    v_list += ['17871700-1-xboxone',#-netflix',
               '20373613-3-dvd',#-netflix',
               '20037881-2-playstation3',#-netflix']
               '18594350-1-dvd',#-netflix'
               '18246549-1-amazonfirestick']#-netflix'
    ### Examples where new version grouped things old version didn't
    v_list += list(timeline.loc[timeline.fsite.isin(['19308666-1', '19388849-1', '19603649-1']), 'vw_id'].unique())
    # assert 1==0
    ### Rows where gapfix would have changed something
    v_list += list(timeline.loc[timeline.fsite.isin(
        gapfix_changes3.loc[gapfix_changes3.activity.isin(['streaming','other','mute'])]\
                       .sort_values('gap', ascending=False)\
                       .fsite.unique()[:3]), 'vw_id'].unique())
    # v_list += list(timeline.loc[timeline.fsite.isin(gapfix_changes4.sort_values('gap', ascending=False).fsite.unique()[:3]), 'vw_id'].unique())
    v_list = list(set(v_list))
    v_list.sort()
    for v in v_list:
        this_dvc = timeline.loc[ (timeline.vw_id == v)
                               # & timeline.both_times.between(dt.datetime(2017, 9, 1, 18, 30),
                               #                               dt.datetime(2017, 9, 1, 21))
                               ]\
                             .sort_values(['fsite','device','both_times']).reset_index(drop=True)
        ssns = tune.loc[tune.vw_id == v, ['start_time','streaming_endtime']]\
                   .rename(columns={'start_time': 'sttm',
                                    'streaming_endtime': 'edtm'})
        print(v, this_dvc.shape[0])
        f1, ax1 = plot_ssn_rows(this_dvc,
                                ssn_sttms=['starttimeest', 'start_time'],
                                ssn_edtms=['endtimeest', 'end_time'],
                                cols=['red', 'blue'],
                                ssns=ssns,
                                titl='Panel streaming and tuning sessions, {}'.format(v))
        # f1, ax1 = plot_ssn_rows2(this_dvc, ssns=ssns,
        #                          titl='Panel streaming and tuning sessions, {}'.format(v))
        # ax1.set_xticks(pd.date_range(dt.datetime(2017, 9, 1, 18, 30), dt.datetime(2017, 9, 1, 21), freq='30min'))
        f1.savefig('{}time_v_line_{}.png'.format(dr, v), dpi=300)
        plt.close(f1)

#----------------------------------------------------------------------------------------------------------------------#
### Post-processing tuning data, to check what went wrong if the assertions below fail
tune.to_csv(dr + 'checkpoint_swot.csv')

########################################################################################################################
########################################################################################################################
########################################################################################################################
tune['swotdur']       = (tune.streaming_endtime - tune.end_time) / dt.timedelta(seconds=1)
tune['swotstreamdur'] = tune.duration + tune.swotdur
tune.reset_index(drop=True, inplace=True)

### Check for nonsense
assert (tune.duration      >= 0).all(), 'ERROR: negative durations'
assert (tune.swotdur       >= 0).all(), 'ERROR: negative swot durations'
assert (tune.swotstreamdur >= 0).all(), 'ERROR: negative total durations'

print('Overall SWOT fraction = {:0.2f}%'.format(100 * tune.swotdur.sum() / tune.swotstreamdur.sum()))
########################################################################################################################
### Save to file
tune.to_csv(dr + 'swot_master_table.csv')

### Record version effects
vers_track_fname ='{}/version_track.csv'.format(dr)
# version_track = pd.DataFrame([], ### for starting a new tracker
#                              columns=['gapfix','strmsort',
#                                       'end_streaming_event_threshold',
#                                       'tune_mins','strm_mins','swot_mins','swot_frac'])
version_track = pd.read_csv(vers_track_fname, index_col=0) ### For adding to existing tracker
version_track.loc[dt.datetime.now()] = [gapfix, strmsort, end_streaming_event_threshold,
                                        tune.duration.sum(), tune.swotstreamdur.sum(), tune.swotdur.sum(),
                                        tune.swotdur.sum() / tune.swotstreamdur.sum()]
version_track.sort_values(['gapfix','end_streaming_event_threshold'])\
             .to_csv(vers_track_fname)

# hhid_track.loc[9] = [mw.hh_id.nunique(), strm.hh_id.nunique(), tune1.hh_id.nunique(),
#                      tune3.hh_id.nunique(), tune4.hh_id.nunique(), tune.hh_id.nunique()]
# print(hhid_track)
########################################################################################################################
########################################################################################################################
########################################################################################################################
