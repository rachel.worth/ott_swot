import pyodbc     as db
import pandas     as pd
import sqlalchemy as sql
import matplotlib
import matplotlib.pyplot as plt
import seaborn as sb
import datetime as dt
import numpy as np
pd.set_option('display.width', 180)

### Date range to pull
pull_dates = ['2017-08-19', '2017-09-16']

week_subplots = [['2017-08-19', '2017-08-26'],
                 ['2017-08-26', '2017-09-02'],
                 ['2017-09-02', '2017-09-09'],
                 ['2017-09-09', '2017-09-16']]

######################################################################################
print('connecting...')
con = db.connect('DRIVER=NetezzaSQL;SERVER=nantz75.nielsen.com;PORT=5480;DATABASE=ING_GTAM_VIEWS_PROD;UID=msciuser;PWD=msciuser;')
print('connected!')
### Comparison between tuning and viewing data
# view = pd.read_sql_query('''SELECT DISTINCT *
# FROM ING_B3B_LIVE_V
# WHERE OTTM_ELIGIBLE_ID = 1
#   AND STREAMING_INDICATOR = 'Y'
#   AND VIEWED_START_OF_VIEWING_UTC BETWEEN '2017-09-01' AND '2017-09-03' ''', con)
# view.to_csv('Coviewing/view_sample.csv')
view = pd.read_csv('Coviewing/view_sample.csv', index_col=0)

# ---------------------------------------------------------------------------------- #
# print('connecting...')
# con2 = db.connect('DRIVER=NetezzaSQL;SERVER=nantz88.nielsen.com;PORT=5480;DATABASE=OPINTEL_PROD;UID=baroer01;PWD=Password@6;')
# print('connected!')
# tune_all = pd.read_sql_query('''SELECT DISTINCT
#    HOUSEHOLDMEDIACONSUMERDEVICEKEY, FOLDER_ID, SITEUNITID, VIEWINGTYPEID, DISTRIBUTIONSOURCEKEY, DIST_SOURCEID, INTABSTATUS, OTTM_INTABSTATUS,
#    RECORDED_STARTEST, RECORDED_ENDEST, RECORDED_STARTLOCAL, RECORDED_ENDLOCAL, VIEWED_STARTEST, VIEWED_ENDEST, VIEWED_STARTLOCAL, VIEWED_ENDLOCAL, SID_CODE,
#    SID_DATE, SID_TIME, KEY_LDS, METERNUMBER, LDS, NUBADDR, STANDARDCODE, NIELSENDAY, METEREDSERVICECODE, STATIONCODE, ENCODETYPE, RTVOD,
#    OTTMDEVICEKEY, SID, TIC, OFF_SET, MAID, STREAMINDKEY, TREE_ID, MEDIADEVICETYPENAME, MSONAME, MARKETCODE, METERTYPE, SOFTWAREVERSION,
#    METERCOLLECTIONDATE, CREATIONTIMESTAMP, FILENAME, PARENT_ID, BRAND_ID, CHANNEL_ID
# FROM
# 	ADMIN.FCT_OTTM_BYMINUTE t
# WHERE 1=1
#   AND t.metercollectiondate between '2017-09-01' and '2017-09-02'
#   AND t.viewed_startest <= t.viewed_endest''', con2)
# tune_all.to_csv('Coviewing/tune_sample.csv')
tune_all = pd.read_csv('Coviewing/tune_sample.csv', index_col=0)

print(view.shape, tune_all.shape)

######################################################################################
print('mw read')
mediaworks_user         = 'baroer01'
mediaworks_pass         = 'baroer01_changeme'
mw_query = '''SELECT DISTINCT
  d1.hh_id AS folder, d1.site, d1.make, d1.model, d1.appliance_type, m2.mac_address
FROM
   ("MEDIAWORKS"."collections"."mac_device"            m3
   LEFT OUTER JOIN "MEDIAWORKS"."collections"."device" d1 ON (m3."device_id" = d1."id"))
   LEFT OUTER JOIN "MEDIAWORKS"."collections"."mac"    m2 ON (m3."mac_id" = m2."id") AND (m2.hh_id = d1.hh_id)'''

mw_connection = 'postgresql://{}:{}@TPARHELMPP057.enterprisenet.org:5432/MEDIAWORKS'.format(mediaworks_user,mediaworks_pass)
mw_engine     = sql.create_engine(mw_connection)
mw            = pd.read_sql_query(mw_query, mw_engine)
### all columns:
# m3.id, m3.mac_id, m3.device_id, m3.insert_time, m3.update_time,
# d1.id, d1.hh_id, d1.site, d1.make, d1.model, d1.expiration_date, d1.insert_time, d1.update_time, d1.appliance_type, d1.combo_id, d1.port,
# m2.id, m2.hh_id, m2.mac_address, m2.insert_time, m2.update_time

tune_all['in_mw'] = tune_all.FOLDER_ID.isin(mw.hh_id_dvc)

tune_all.rename(columns={'FOLDER_ID': 'folder',
                         'SITEUNITID': 'site'})

tune = tune_all.merge(tune_all.in_mw, how='inner',
                      on=)
print(view.shape, tune.shape)

######################################################################################
### need: house, device, start, end, station
### tune: HOUSEHOLDMEDIACONSUMERDEVICEKEY, FOLDER_ID,
###       SITEUNITED,
###       VIEWED_STARTEST, VIEWED_ENDEST / VIEWED_STARTLOCAL, VIEWED_ENDLOCAL
###       MAID = content_id

print(tune.iloc[:3, :9])
print(tune.iloc[:3, 9:19])
print(tune.iloc[:3, 19:36])
print(tune.iloc[:3, 36:44])
print(tune.iloc[:3, 44:])

view['hh_id'] = view.HOUSEHOLD_ID.astype('int64')
# view['dvc_id'] = ???
view['sttm'] = view.VIEWED_START_OF_VIEWING_UTC
view['edtm'] = view.VIEWED_END_OF_VIEWING_UTC

tune['hh_id'] = tune.FOLDER_ID
# tune['dvc_id'] = ???
tune['sttm'] = tune.VIEWED_STARTEST + dt.timedelta(hours=7)
tune['edtm'] = tune.VIEWED_ENDEST   + dt.timedelta(hours=7)

### Plot counts per minute to check if time conversion is right?
by_min = pd.DataFrame({'tn': tune.groupby('sttm').edtm.count(),
                       'vw': view.groupby('sttm').edtm.count()})\
                     .fillna(0).sort_index()
totals = by_min.sum()
f1, ax1 = plt.subplots(1)
f1.autofmt_xdate();
for t in by_min.index:
    if (t.minute == 0):
        ax1.axvline(t, alpha=0.2, lw=1, c='black')
ax1.plot(by_min.index, by_min.vw.rolling(60, center=True).mean()*totals.tn/totals.vw, alpha=0.5, label='view (smoothed)')
ax1.plot(by_min.index, by_min.tn, alpha=0.5, label='tune')
ax1.xaxis.set_major_formatter(matplotlib.dates.DateFormatter('%y-%m-%d %H:%M'))
ax1.legend()
ax1.set_ylim(0, 20000)

### hh_id overlap
view['hh_overlap'] = [h in tune.hh_id for h in view.hh_id]
print(view.hh_overlap.mean())

both = view.merge(tune, on=['hh_id','sttm','edtm','MAID'], how='left')

### Fraction unmatched?
print(both.HOUSEHOLDMEDIACONSUMERDEVICEKEY.isnull().mean())

######################################################################################
print('Read data -- test')
### Check columns available
test = pd.read_sql_query('''SELECT *
FROM ING_B3B_LIVE_V
WHERE OTTM_ELIGIBLE_ID = 1
  AND STREAMING_INDICATOR = 'Y'
  AND VIEWED_START_OF_VIEWING_UTC BETWEEN '2017-09-01' AND '2017-09-02'
LIMIT 3''', con)
print(test.columns)
print(test.iloc[:,:16])
print(test.iloc[:,16:27])
print(test.iloc[:,27:38])
print(test.iloc[:,38:45])
print(test.iloc[:,45:54])
print(test.iloc[:,54:])
test.to_csv('CoViewing/viewer_sample.csv')
######################################################################################
############################### Pull event data ######################################
######################################################################################
### Real data pull
print('Read data -- primary')
event_pull_query = '''SELECT DISTINCT
    HOUSEHOLD_ID                AS hh_id,
    PERSON_NUMBER               AS prsn_id,
    AGE                         AS age,
    GENDER                      AS gender,
    MARKET_ID                   AS dma,
    DEVICE_NAME                 AS device_name,
    STATION_CODE                AS sttn_cd,
    MAID                        AS maid,
    OTTM_SID                    AS ottm_sid,
    OTTM_TIC                    AS ottm_tic,
    TIC_OFFSET                  AS tic_offset,
    OTTM_ELIGIBLE_ID            AS ottm_eligible_id,
    STREAMING_INDICATOR         AS streaming_indicator,
    STREAMING_SOURCE_TREE_ID    AS tree_id,
    DATA_DATE                   AS data_date,
    START_DATE                  AS st_date,
    START_TIME                  AS st_time,
    VIEWED_START_OF_VIEWING_UTC AS vw_sttm,
    VIEWED_END_OF_VIEWING_UTC   AS vw_edtm,
    CREDIT_START_OF_VIEWING_UTC AS cr_sttm,
    CREDIT_END_OF_VIEWING_UTC   AS cr_edtm
FROM ING_B3B_LIVE_V
WHERE OTTM_ELIGIBLE_ID = 1
  AND STREAMING_INDICATOR = 'Y'
  AND vw_sttm BETWEEN '{0}' AND '{1}'
ORDER BY hh_id, prsn_id, vw_sttm, vw_edtm'''.format(pull_dates[0], pull_dates[1])
view_events = pd.read_sql_query(event_pull_query, con)
view_events.columns = [c.lower() for c in view_events.columns]

######################################################################################
### Add columns
view_events['hh_prsn'] = view_events.hh_id.astype('str') + '-' + view_events.prsn_id.astype('str')
view_events['drtn'] = (view_events.vw_edtm.astype('int64') - view_events.vw_sttm.astype('int64'))/1e9/60.
view_events['gap']  = (view_events.vw_sttm.astype('int64') - view_events.vw_edtm.shift(1).astype('int64'))/1e9/60.
view_events.loc[view_events.hh_prsn != view_events.hh_prsn.shift(1), 'gap'] = np.nan

### Assign a provider, to compare with previous SWOT experiences (also check device_name)
tree_id_dict = {
    'hulu'    : [1690781],
    'amazon'  : [777777,156698],
    'youtube' : [1431024],
    'netflix' : [3481638,214042],
    'crackle' : [1459202],
    'hbo'     : [56472,3213979]}

view_events['provider'] = 'other'
for k in tree_id_dict:
    view_events.loc[view_events.tree_id.isin(tree_id_dict[k]), 'provider'] = k

######################################################################################
################################ Minute explode ######################################
######################################################################################
date_range = pd.date_range(view_events.vw_sttm.min().date(), view_events.vw_edtm.max().date())

for i, date in enumerate(date_range):
    today = view_events.loc[ (pd.DatetimeIndex(view_events.vw_edtm).normalize() >= date)
                           & (pd.DatetimeIndex(view_events.vw_sttm).normalize() <= date)].copy()
    print('Minute explode {0:%y-%m-%d}: {1} events'.format(date, today.shape[0]))
    min_df = pd.DataFrame({'mnt': pd.date_range(date, date + dt.timedelta(hours=23, minutes=59), freq='min'),
                           'key': 1})
    today['key'] = 1
    full_crossjoin = today.merge(min_df, on='key')
    if i == 0:
        view_expl = full_crossjoin.loc[full_crossjoin.mnt.between(full_crossjoin.vw_sttm, full_crossjoin.vw_edtm-dt.timedelta(minutes=1))]
    else:
        view_expl = pd.concat([view_expl,
                               full_crossjoin.loc[full_crossjoin.mnt.between(full_crossjoin.vw_sttm,
                                                                             full_crossjoin.vw_edtm - dt.timedelta(
                                                                                 minutes=1))]])
    print('Minute explode {0:%y-%m-%d}: {1} minutes total'.format(date, view_expl.shape[0]))
    del full_crossjoin

######################################################################################
############################## Minute aggregates #####################################
######################################################################################
### Count people watching per hh per minute
### to do: check for non-live tuning?
print('Calculate coviewing')
ppl_per_hh_per_min = view_expl.groupby(['hh_id', 'device_name', 'tree_id',
                                        'sttn_cd', 'mnt']).agg({'prsn_id': pd.Series.nunique})
ppl_per_hh_per_min.columns = ['n_viewers']
ppl_per_hh_per_min['is_coview'] = ppl_per_hh_per_min.n_viewers > 1

print('Overall coviewing fraction: {0: 0.1f}% of minutes have more than one viewer'.format(ppl_per_hh_per_min.is_coview.mean()*100))

######################################################################################
### Viewing over time
n_hhs = view_events.hh_id.nunique()
n_ppl = view_events.hh_prsn.nunique()
views_per_min = ppl_per_hh_per_min.reset_index().groupby('mnt').agg({'n_viewers': 'sum',
                                                                     'hh_id': pd.Series.nunique,
                                                                     'is_coview': 'mean'})
views_per_min = views_per_min.reindex(pd.date_range(views_per_min.index.min(), views_per_min.index.max(), freq='min')).fillna(0)
views_per_min['rating_hh'] = views_per_min.hh_id/n_hhs
views_per_min['rating_ppl'] = views_per_min.n_viewers/n_hhs

######################################################################################
############################### Plot vs min ##########################################
######################################################################################
for plot_dates_str in week_subplots:
    plot_dates = [pd.to_datetime(s) for s in plot_dates_str]
    print(plot_dates)
    subset = views_per_min.loc[ (views_per_min.index >= plot_dates[0])
                              & (views_per_min.index <= plot_dates[1])]
    print(subset.shape)
    f1, ax1 = plt.subplots(2, 1, sharex=True, figsize=[20, 10])
    f1.autofmt_xdate();
    ax1[0].set_xlim(plot_dates[0], plot_dates[1])
    ax1[0].plot(subset.index, subset.rating_hh,  label='Fraction of homes')
    ax1[0].plot(subset.index, subset.rating_ppl, label='Fraction of people')
    ax1[0].set_title('OTT Panel Viewing ({0} HHs, {1} ppl)'.format(n_hhs, n_ppl))
    ax1[0].set_ylabel('Ratings approximation')
    ax1[0].set_ylim(0, views_per_min[['rating_hh','rating_ppl']].max().max()*1.05)
    ax1[0].legend(loc='upper right', frameon=True, framealpha=0.5)
    ax1[1].plot(subset.index, subset.is_coview,
                color='blue', alpha=0.4, lw=1, label='Minute average')
    ax1[1].plot(subset.index, subset.is_coview.rolling(120, center=True).mean(),
                color='blue', alpha=1.0, lw=2, label='2-hr rolling average')
    ax1[1].axhline(ppl_per_hh_per_min.is_coview.mean(),
                color='green', alpha=0.7, lw=2, label='Overall average = {0:0.1f}%'.format(100*ppl_per_hh_per_min.is_coview.mean()))
    ax1[1].legend(loc='upper right', frameon=True, framealpha=0.5)
    ax1[1].set_ylim(0, 1.0)
    ax1[1].set_title('OTT Coviewing')
    ax1[1].set_ylabel('Coviewing fraction')
    ax1[1].set_xlabel('Time (UTC)')
    ax1[1].xaxis.set_major_formatter(matplotlib.dates.DateFormatter('%y-%m-%d %H:%M'))
    plt.tight_layout()
    f1.savefig('CoViewing/viewing_over_time_{0:%y%m%d}-{1:%y%m%d}.png'.format(plot_dates[0], plot_dates[1]))
    plt.close(f1)

######################################################################################
######################################################################################
######################################################################################
