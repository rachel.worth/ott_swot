import pyodbc     as db
import pandas     as pd
import sqlalchemy as sql
import tqdm
import winsound as ws
from v2_4 import swot_helper_functions as shf
import numpy as np
import datetime as dt
import os

pd.set_option('display.width', 170)

create_report = False
debug_report = True

dirpath = 'DCR_data/NewPanelData/'#'v2_4/'
opintel_streaming_event_path = dirpath+'streaming_events_sql.txt'
opintel_mime_event_path      = dirpath+'mime_events_sql.txt'
mw_path                      = dirpath+'mw_query.txt'
ottm_credit_path             = dirpath+'ottm_credit_results_wdev.txt'
mac_device                   = dirpath+'mac_device_map.csv'

mediaworks_user         = 'baroer01'
mediaworks_pass         = 'baroer01_changeme'
opintel_user            = 'OPINTEL_SPOTFIRE'
opintel_pass            = 'spotfire'

next_line = -1  #just to make the .shift lines that are used in this script more readable
prev_line =  1

### Changes/corrections?
gapfix        = False   # Don't bridge over gaps between sessions
strmsort      = True    # Sort streaming rows ahead of identical non-streaming ones so the latter gets dropped
inherit_prov  = False   # when converting stream-other-stream to stream-stream-stream, inherit previous provider? (alternative: group first two)
trim_ends     = True    # remove events from one dataset that fall outside the timespan of the other
distsource_fn = 'first' # which function to apply to dist_sourceid when aggregating

date_range = '2017-09-01','2017-09-01'
# tag        = '_1701-1709_old'
date_range_dt = [dt.datetime.strptime(d, '%Y-%m-%d') for d in date_range]
assert date_range_dt[1] >= date_range_dt[0], 'ERROR: start date later than end date'
# tag = '{}-{}_old'.format(*[d.strftime('%y%m') for d in date_range_dt])
### Directory to put files
dr = 'panel_swotdata/old/{}-{}/'.format(*[d.strftime('%y%m%d') for d in date_range_dt])
if not os.path.exists(dr):
    os.makedirs(dr)
print("Data directory: {}".format(dr))


end_streaming_event_threshold = 240  # seconds
one_second = pd.Timedelta('1s')
two_months = pd.Timedelta(days=60)

hulu    = 1690781
amazon  = 777777,156698
youtube = 1431024
netflix = 3481638,214042
crackle = 1459202
hbo     = 56472,3213979

providers = [hulu, amazon, youtube, netflix, crackle, hbo]

pids = []
for x in providers:
    if type(x) == tuple:
        for j in x:
            pids.append(j)
    else:
        pids.append(x)
pids = tuple(pids)

provider_names = pd.DataFrame({'tree_id': pids,
                               'provider':['hulu', 'amazon', 'amazon', 'youtube', 'netflix', 'netflix', 'crackle', 'hbo', 'hbo']})

mac_by_device = pd.read_csv(mac_device)

print('mw read')
mw_query = open(mw_path).read()
# mw_connection = 'postgresql://{}:{}@TPARHELMPP057.enterprisenet.org:5432/MEDIAWORKS'.format(mediaworks_user,mediaworks_pass)
# mw_engine     = sql.create_engine(mw_connection)
# mw            = pd.read_sql_query(mw_query,mw_engine)
print('mw read from file')
mw = pd.read_csv(dr + 'checkpoint_mw1.csv', index_col=0)\
       .rename(columns={'hh_id': 'hhid',
                        'appliance_type': 'appliancetype',
                        'mac_address': 'macaddress'})

mw.columns    = [str(str(x).lower()) for x in mw.columns.tolist()]
mw['fsite']   = mw.hhid.astype(str) + '-' + mw.site.astype(str)
ws.PlaySound("*", ws.SND_ALIAS)

# ### entries by month
# mw['year']  = [d.year  for d in mw.insert_time]
# mw['month'] = [d.month for d in mw.insert_time]
# mw.groupby(['year','month']).agg({'macaddress': pd.Series.nunique})

### Assign device names based on make/model?
pnl_dvc_fracs = pd.read_csv('dvc_fraction_panel.csv', index_col=0)
mw['make_cln']  = mw.make.str.lower().str.replace(' ','').fillna('')
mw['model_cln'] = mw.model.str.lower().str.replace(' ','').fillna('')
mw['mk_model_cln'] = mw.make_cln + mw.model_cln

### Assign my device names
mw['device'] = 'Other'
mw.loc[mw.appliancetype=='Computer', 'device'] = 'Computer'
for d in pnl_dvc_fracs.index:
    mw.loc[ mw.mk_model_cln.str.contains(d.lower().replace(' ', '')), 'device'] = d
    mw.loc[mw.model_cln.str.contains(d.lower().replace(' ', '')), 'device'] = d
mw.loc[(mw.appliancetype=='TV')  & (mw.device=='Other'), 'device'] = 'Smart TV'
mw.loc[(mw.appliancetype=='DVD') & (mw.device=='Other'), 'device'] = 'Bluray'
mw.loc[mw.mk_model_cln.str.contains('wiiu') & (mw.device=='Wii'), 'device'] = 'Wii U'
mw.loc[mw.mk_model_cln.str.contains('roku'), 'device'] = 'Roku'

mw.loc[mw.device=='Other'].groupby(['appliancetype','make','model']).fsite.count().sort_values()

print('connecting...')
con = db.connect('DRIVER=NetezzaSQL;SERVER=nantz88.nielsen.com;PORT=5480;DATABASE=OPINTEL_PROD;UID=baroer01;PWD=Password@6;')
print('connected!')

### look up other tree ids
treeid_table = pd.read_sql_query('''select distinct parent, brand, channel, treeid
from domain_resolution_result ''', con)

disc_list = treeid_table.loc[treeid_table.PARENT.str.contains('Discovery Communications').fillna(False)]
dtv_list  = treeid_table.loc[treeid_table.BRAND.str.lower().str.contains('directv').fillna(False)]
cw_list   = treeid_table.loc[ treeid_table.BRAND.str.lower().str.contains('cw seed').fillna(False)
                            | treeid_table.BRAND.str.lower().str.contains('cwtv').fillna(False)]
for i, l in enumerate([disc_list, dtv_list, cw_list]):
    p = ['discovery','directv','cw'][i]
    providers.append(tuple(l.TREEID))
    for t in list(l.TREEID):
        provider_names.loc[provider_names.index.max()+1] = [p, t]

pids = tuple(provider_names.tree_id)

streaming_events_query_text = open(opintel_streaming_event_path).read()
mime_events_query_text = open(opintel_mime_event_path).read()
ottm_credit_results_query_text = open(ottm_credit_path).read()

mw_macs = tuple(mw.macaddress.unique())

print('reading streaming_events')
streaming_events = pd.read_sql_query(streaming_events_query_text.format(pids,*date_range, mw_macs), con)
streaming_events.columns = [str.lower(x) for x in streaming_events.columns]

print('reading mime_events')
mime_events = pd.read_sql_query(mime_events_query_text.format(pids,*date_range, mw_macs), con)
mime_events.columns = [str.lower(x) for x in mime_events.columns]

print('building dmx_events')
dmx_events = pd.concat([streaming_events, mime_events], ignore_index=True)
### my-device version
dmx_events = dmx_events.merge(mw[['hhid','macaddress','fsite','appliancetype','make','model','device']], on='macaddress', how='inner')
### eric's-device version
# dmx_events = dmx_events.merge(mw[['macaddress','fsite']], on='macaddress', how='inner')
# dmx_events = dmx_events.merge(mac_by_device, on='macaddress', how='left')
### regular
cols = ['starttimeest', 'endtimeest', 'hhid', 'macaddress', 'fsite', 'device', 'tree_id']
dmx_events = dmx_events[cols]
dmx_events.sort_values(['fsite', 'device', 'starttimeest'],inplace=True)
dmx_events.reset_index(drop=True,inplace=True)
strm_shape, mime_shape = streaming_events.shape, mime_events.shape
del streaming_events
del mime_events
ws.PlaySound("*", ws.SND_ALIAS)

print('reading credit_results')
credit_results = pd.read_sql_query(ottm_credit_results_query_text.format(tuple(dmx_events.fsite.dropna().unique().tolist()),pids,*date_range), con)
credit_results.columns = [str.lower(x) for x in credit_results.columns]
credit_results['hhid'] = credit_results.fsite.str.split('-', expand=True)[0]
ws.PlaySound("*", ws.SND_ALIAS)

print('mw.shape, streaming_events.shape, mime_events.shape, dmx_events.shape, credit_results.shape:')
print( mw.shape,             strm_shape,        mime_shape, dmx_events.shape, credit_results.shape)

print('        mw   dmx  credit')
print('hhs:   ', mw.hhid.nunique(),       dmx_events.hhid.nunique(),  credit_results.hhid.nunique())
print('fsites:', mw.fsite.nunique(),      dmx_events.fsite.nunique(), credit_results.fsite.nunique())
print('macs:  ', mw.macaddress.nunique(), dmx_events.macaddress.nunique())

hhid_track = pd.DataFrame([], columns=['mw','dmx','result','minwin','ranges','swot'], dtype='int')
hhid_track.loc[0] = [mw.hhid.nunique(), dmx_events.hhid.nunique(), credit_results.hhid.nunique(), -1, -1, -1]

#######################################################################################################
### Determine edges of the two datset timespans
inner_trange = [max( dmx_events['starttimeest'].min(), credit_results['start_time'].min() ),
                min( dmx_events['starttimeest'].max(), credit_results['start_time'].max() )]


# volume_counts = pd.DataFrame({'strm_start': dmx_events.starttimeest.dt.floor('min').value_counts(),
#                               'strm_end':   dmx_events.endtimeest.dt.floor('min').value_counts(),
#                               'tune_start': credit_results.start_time.value_counts(),
#                               'tune_end':   credit_results.end_time.value_counts(),
#                               })
# volume_counts = volume_counts.reindex(pd.date_range(volume_counts.index.min(),
#                                                     volume_counts.index.max(), freq='min')).fillna(0)
#
# import matplotlib as mpl
# import matplotlib.pyplot as plt
#
# f1, ax1 = plt.subplots(2, figsize=[16, 6], sharex=True)
# for c in ['strm_start','strm_end']:
#     ax1[0].plot(volume_counts.index, volume_counts[c], lw=1, label=c)
# ax1[0].legend(loc='best')
#
# for c in ['tune_start','tune_end']:
#     ax1[1].plot(volume_counts.index, volume_counts[c], lw=1, label=c)
# ax1[1].legend(loc='best')
#
# ax1[-1].xaxis.set_major_formatter(mpl.dates.DateFormatter('%y-%m-%d %H:%M'));
#
#######################################################################################################
tree_id_table = treeid_table.rename(columns={'TREEID':'tree_id'})
### Save to file
tree_id_table.to_csv(dr+'tree_id_table.csv')

# credit_results2 = credit_results.merge(tree_id_table, on='tree_id', how='left')
# dmx_events2     = dmx_events.merge(tree_id_table, on='tree_id', how='left')
# # credit_results2 = credit_results.merge(tree_id_table, on='tree_id', how='left')
#
# ### How many of each?
# treeid_counts = credit_results2.groupby(['tree_id','PARENT','BRAND','CHANNEL']).fsite.count().sort_values()
#
# searched_for = treeid_counts.reset_index().set_index('tree_id').loc[provider_names.tree_id]
# omitted      = treeid_counts.reset_index().set_index('tree_id').loc[~provider_names.tree_id]

#######################################################################################################

for element in providers:
    if type(element) != int and len(element) > 1:
        for i in range(1, len(element)):
            dmx_events.loc[dmx_events.tree_id == element[i],'tree_id'] = element[0]
            credit_results.loc[credit_results.tree_id == element[i],'tree_id'] = element[0]

### CHANGED FROM ORIGINAL:
### When dropping overlapping events, make sure that we keep the streaming event if there is one
if strmsort:
    sort_cols = ['fsite','start_time','end_time','nonstreaming']
else:
    sort_cols = ['fsite', 'start_time', 'end_time']
credit_results['nonstreaming'] = credit_results.tree_id == 0
credit_results = credit_results.sort_values(sort_cols)
### Save to file
credit_results.to_csv(dr+'credit_results.csv')
### If two events overlap completely, keep the first one (same as original from here on)
credit_results_minwin = credit_results.groupby(['fsite','start_time','end_time']).first().reset_index()
print('credit_results before/after dropping simultaneous events', credit_results.shape[0], credit_results_minwin.shape[0])
hhid_track.loc[1] = [mw.hhid.nunique(), dmx_events.hhid.nunique(), credit_results.hhid.nunique(), credit_results_minwin.hhid.nunique(), -1, -1]
print(hhid_track)

# setting the off moments to a different device than unknown device while the tv is on
credit_results_minwin.loc[(credit_results_minwin.dist_sourceid == 0) &
                          (credit_results_minwin.tree_id == 0),'dkey'] = -1

credit_results_minwin['groupID'] = 0
credit_results_minwin['ptid'] = credit_results_minwin.tree_id.shift(prev_line).fillna(0).astype(int)
credit_results_minwin['pf'] = credit_results_minwin.fsite.shift(prev_line)
credit_results_minwin['pd'] = credit_results_minwin.dkey.shift(prev_line)
credit_results_minwin['temp'] = 1
credit_results_minwin.loc[(credit_results_minwin.pf==credit_results_minwin.fsite) &
                          (credit_results_minwin.ptid==credit_results_minwin.tree_id) &
                          (credit_results_minwin.pd==credit_results_minwin.dkey),'temp']=0
credit_results_minwin.loc[(credit_results_minwin.dist_sourceid != 0) &
                          (credit_results_minwin.dist_sourceid.shift(prev_line) == 0) &
                          (credit_results_minwin.tree_id.shift(prev_line) == 0),'temp'] = 1

if credit_results_minwin.loc[0,'tree_id'] == 0 and credit_results_minwin.loc[0,'dist_sourceid'] == 0:
    credit_results_minwin.loc[0,'temp'] = 0

### OPTIONAL CHANGE: Only combine if consecutive?
credit_results_minwin['gap'] = (credit_results_minwin.start_time - credit_results_minwin.end_time.shift(1)) / dt.timedelta(seconds=1)
gapfix_changes1 = credit_results_minwin.loc[(credit_results_minwin.temp == 0) & (credit_results_minwin.gap != 0)] # list of events that would be affected
if gapfix:
    credit_results_minwin.loc[credit_results_minwin.gap != 0, 'temp'] = 1

### Assign new group numbers
credit_results_minwin.groupID = credit_results_minwin.temp.cumsum()
### Save to file
credit_results_minwin.to_csv(dr+'credit_results_minwin.csv')

agg_dict1 = {'fsite': 'first',
            'hhid':  'first',
            'dist_sourceid':distsource_fn,
            'start_time':'first',
            'end_time':'last',
            'tree_id':'first',
            'dkey':'first',
            'device':'first'}

credit_ranges = credit_results_minwin.groupby('groupID').agg(agg_dict1).reset_index(drop=True)
hhid_track.loc[2] = [mw.hhid.nunique(), dmx_events.hhid.nunique(), credit_results.hhid.nunique(),
                     credit_results_minwin.hhid.nunique(), credit_ranges.hhid.nunique(), -1]
print(hhid_track)

credit_ranges['range1class'] = None
credit_ranges.loc[(credit_ranges.dist_sourceid == 0),'range1class'] = 'off'
credit_ranges.loc[(credit_ranges.dist_sourceid == 10202) & (credit_ranges.tree_id == 0),'range1class'] = 'mute'
credit_ranges.loc[~(credit_ranges.dist_sourceid.isin([0, 10202])) & (credit_ranges.tree_id == 0),'range1class'] = 'other'
credit_ranges.loc[(credit_ranges.dist_sourceid != 0) & (credit_ranges.tree_id != 0),'range1class'] = 'streaming'

credit_ranges.loc[(credit_ranges.dkey != credit_ranges.dkey.shift(prev_line)) &
                  (credit_ranges.dkey != 3500001) &
                  (credit_ranges.range1class.isin(['mute', 'other'])), 'range1class'] = 'sourcechange'


str_oth_str = ((credit_ranges.range1class == 'other') &
               (credit_ranges.range1class.shift(prev_line) == 'streaming')   &
               (credit_ranges.range1class.shift(next_line) == 'streaming')   &
               (credit_ranges.fsite == credit_ranges.fsite.shift(prev_line)) &
               (credit_ranges.fsite == credit_ranges.fsite.shift(next_line)))
credit_ranges.loc[str_oth_str, 'range1class'] = 'streaming'
if inherit_prov:
    credit_ranges.loc[str_oth_str, 'tree_id'] = credit_ranges['tree_id'].shift(1).loc[str_oth_str]

credit_ranges['duration'] = (credit_ranges.end_time - credit_ranges.start_time) / one_second

credit_ranges['groupID'] = 0
credit_ranges['temp'] = 0
credit_ranges['temp2'] = 0 # to differentiate between mutes in off ranges and mutes in stream ranges

#rule 1
credit_ranges.loc[credit_ranges.range1class == 'streaming', 'temp'] = 1

#rule 2
credit_ranges.loc[credit_ranges.range1class.isin(['off','sourcechange']), 'temp'] = 1

#rule 3
credit_ranges.loc[(credit_ranges.range1class == 'streaming') &
                  (credit_ranges.range1class.shift(prev_line) == 'mute') &
                  (credit_ranges.range1class.shift(2*prev_line) == 'streaming'), 'temp'] = 0

#rule 4
credit_ranges.loc[credit_ranges.fsite != credit_ranges.fsite.shift(prev_line), 'temp'] = 1

# OPTIONAL CHANGE:
# rule 0: gap => new session
# do this last to override the lack of gap-checks in the other rules
credit_ranges['gap'] = (credit_ranges.start_time - credit_ranges.end_time.shift(1)) / dt.timedelta(seconds=1)
gapfix_changes2 = credit_ranges.loc[(credit_ranges.temp == 0) & (credit_ranges.gap != 0)] # list of events that would be affected
if gapfix:
    credit_ranges.loc[credit_ranges.gap != 0, 'temp'] = 1

credit_ranges['groupID'] = credit_ranges.temp.cumsum()
### Save to file
credit_ranges.to_csv(dr+'credit_ranges.csv')

agg_dict2 = {'fsite': 'first',
            'hhid': 'first',
            'range1class': lambda x: 'streaming' if 'streaming' in x.tolist() else 'nonstreaming',
            'duration': 'sum',
            'dist_sourceid':distsource_fn,
            'start_time':'first',
            'end_time':'last',
            'tree_id':'first',
            'device':'first'}

print('creating swot_master_table')
swot_master_table=credit_ranges.groupby('groupID').agg(agg_dict2)
hhid_track.loc[3] = [mw.hhid.nunique(), dmx_events.hhid.nunique(), credit_results.hhid.nunique(),
                     credit_results_minwin.hhid.nunique(), credit_ranges.hhid.nunique(), swot_master_table.hhid.nunique()]

### remove sessions that aren't streaming (some hhs lost at this point)
swot_master_table.drop(swot_master_table[swot_master_table.range1class != 'streaming'].index, axis=0, inplace=True)
swot_master_table.reset_index(inplace=True)
hhid_track.loc[4] = [mw.hhid.nunique(), dmx_events.hhid.nunique(), credit_results.hhid.nunique(),
                     credit_results_minwin.hhid.nunique(), credit_ranges.hhid.nunique(), swot_master_table.hhid.nunique()]

swot_master_table['next_stream_start'] = swot_master_table.start_time.shift(next_line)
swot_master_table.loc[swot_master_table.fsite != swot_master_table.fsite.shift(next_line),'next_stream_start'] += two_months
swot_master_table.next_stream_start.fillna(two_months + swot_master_table.end_time, inplace=True)

hhid_track.loc[5] = [mw.hhid.nunique(), dmx_events.hhid.nunique(), credit_results.hhid.nunique(),
                     credit_results_minwin.hhid.nunique(), credit_ranges.hhid.nunique(), swot_master_table.hhid.nunique()]

dmx_events['final_streaming_event'] = False
dmx_events.loc[(dmx_events.fsite != dmx_events.fsite.shift(next_line)) |
               (dmx_events.device != dmx_events.device.shift(next_line)), 'final_streaming_event'] = True
hhid_track.loc[6] = [mw.hhid.nunique(), dmx_events.hhid.nunique(), credit_results.hhid.nunique(),
                     credit_results_minwin.hhid.nunique(), credit_ranges.hhid.nunique(), swot_master_table.hhid.nunique()]

dmx_events.loc[~(dmx_events.final_streaming_event) &
        (abs(dmx_events.starttimeest.shift(next_line) - dmx_events.endtimeest) / one_second > end_streaming_event_threshold),
        'final_streaming_event'] = True
dmx_events.loc[(dmx_events.fsite != dmx_events.fsite.shift(next_line)), 'final_streaming_event'] = False
dmx_filtered_events = dmx_events[dmx_events.final_streaming_event]
dmx_events.sort_values(['fsite', 'starttimeest'],inplace=True)
hhid_track.loc[7] = [mw.hhid.nunique(), dmx_events.hhid.nunique(), credit_results.hhid.nunique(),
                     credit_results_minwin.hhid.nunique(), credit_ranges.hhid.nunique(), swot_master_table.hhid.nunique()]
### Save to file
dmx_events.to_csv(dr+'dmx_events.csv')

swot_master_table['macaddress'] = None
swot_master_table['streaming_endtime'] = swot_master_table.end_time.copy()
m = None

swot_master_table2 = swot_master_table.copy()    # for debugging/rapid analysis
#swot_master_table = swot_master_table2.copy()

column_names = ['groupID',
                'fsite',
                'range1class',
                'start_time',
                'end_time',
                'duration',
                'next_stream_start',
                'streaming_endtime',
                'swotdur',
                'macaddress',
                'make',
                'model',
                'tree_id',
                'dist_sourceid',
                'provider',
                'swotstreamdur',
                'device']

### Drop unidentified tree_ids
swot_master_table7 = swot_master_table.copy()
swot_master_table = swot_master_table.merge(provider_names,'left')
swot_master_table.dropna(subset= ['provider'], inplace=True)
hhid_track.loc[8] = [mw.hhid.nunique(), dmx_events.hhid.nunique(), credit_results.hhid.nunique(),
                     credit_results_minwin.hhid.nunique(), credit_ranges.hhid.nunique(), swot_master_table.hhid.nunique()]
print(hhid_track)

### Changes device labels!
### Add missing devices
new_dvc_table = shf.dfdev()
swot_master_table = swot_master_table.merge(shf.dfdev(), left_on='device', right_on='old', how='left')
swot_master_table.loc[swot_master_table.device.str.lower().str.contains('roku'), 'new_device'] = 'Roku'
swot_master_table['new_device'] = swot_master_table.new_device.fillna('Other')
swot_master_table.drop(['device','old'],inplace=True, axis=1)
hhid_track.loc[9] = [mw.hhid.nunique(), dmx_events.hhid.nunique(), credit_results.hhid.nunique(),
                     credit_results_minwin.hhid.nunique(), credit_ranges.hhid.nunique(), swot_master_table.hhid.nunique()]
cols = swot_master_table.columns.tolist()
cols[cols.index('new_device')] = 'device'
swot_master_table.columns = cols
hhid_track.loc[10] = [mw.hhid.nunique(), dmx_events.hhid.nunique(), credit_results.hhid.nunique(),
                     credit_results_minwin.hhid.nunique(), credit_ranges.hhid.nunique(), swot_master_table.hhid.nunique()]
print(hhid_track)

########################################################################################################################
### swot_master_table.to_csv(dr+'swot_master_table_checkpoint.csv')
### dmx_events.to_csv(dr+'dmx_events_checkpoint.csv')
# swot_master_table = pd.read_csv('swot_master_table_checkpoint.csv', index_col=0)
# dmx_events        = pd.read_csv('dmx_events_checkpoint.csv', index_col=0)
# for c in ['end_time','start_time','streaming_endtime','next_stream_start']:
#     swot_master_table[c] = pd.to_datetime(swot_master_table[c])
# for c in ['starttimeest','endtimeest']:
#     dmx_events[c] = pd.to_datetime(dmx_events[c])

for fsite in tqdm.tqdm(swot_master_table.fsite.unique()):
    swot= swot_master_table[swot_master_table.fsite == fsite]
    dmx = dmx_events[dmx_events.fsite == fsite]
    for i in swot.index:
        line = swot.loc[i]
        device = line.device
        streaming_events = dmx[(dmx.starttimeest.between(line.end_time,line.next_stream_start)) &
                               (dmx.device == device)]
        # If there are strm events starting between the end of this tune and the start of the next
        if len(streaming_events):
            #m=counter(streaming_events.macaddress).most_common(1)[0][0]
            # If the time to the start of first following strm is less than the threshold
            if (abs(streaming_events[:1].starttimeest - line.end_time) / one_second).values <= end_streaming_event_threshold:
                # If there is at least one final_streaming_event, use the first final event's endtime
                if streaming_events.final_streaming_event.sum():
                    val = streaming_events.loc[streaming_events.final_streaming_event,'endtimeest'].values[0]
                # if no final event, use the last endtime
                else:
                    val = streaming_events.endtimeest.values[-1]
            # If gap larger than threshold before first following strm, then use original endtime
            else:
                val = swot_master_table.end_time[i]
        # If no strm events start between this tune and the next, keep original endtime
        else:
            val = swot_master_table.end_time[i]

        swot_master_table.loc[i, 'streaming_endtime'] = val
        swot_master_table.loc[i, 'macaddress'] = m
########################################################################################################################
### Trim off any data that extends beyond the timerange covered by both datasets
if trim_ends:
    swot_master_table = swot_master_table.loc[ swot_master_table.start_time.between(*inner_trange)
                                             & swot_master_table.end_time.between(*inner_trange)]
### Calculate the swot amounts
swot_master_table['swotdur'] = (swot_master_table.streaming_endtime - swot_master_table.end_time) / one_second
swot_master_table['swotstreamdur'] = swot_master_table.duration + swot_master_table.swotdur
print('Overall SWOT fraction = {:0.2f}%'.format(100 * swot_master_table.swotdur.sum() / swot_master_table.swotstreamdur.sum()))

swot_master_table.reset_index(drop=True,inplace=True)
swot_master_table = swot_master_table.reindex(columns=column_names)
swot_master_table['folder'] = swot_master_table.fsite.apply(lambda x: x[:8])
### Save to file
swot_master_table.to_csv(dr+'swot_master_table.csv')
### Record version effects
vers_track_fname ='{}/version_track.csv'.format(dr)
# version_track = pd.DataFrame([], ### for starting a new tracker
#                              columns=['gapfix','strmsort', 'inherit_prov', 'trim_ends',
#                                       'distsource_fn', 'end_streaming_event_threshold',
#                                       'tune_mins','strm_mins','swot_mins','swot_frac'])
version_track = pd.read_csv(vers_track_fname, index_col=0) ### For adding to existing tracker
version_track.loc[dt.datetime.now()] = [gapfix, strmsort, inherit_prov, trim_ends, distsource_fn, end_streaming_event_threshold,
                                        swot_master_table.duration.sum(), swot_master_table.swotstreamdur.sum(),
                                        swot_master_table.swotdur.sum(),
                                        swot_master_table.swotdur.sum() / swot_master_table.swotstreamdur.sum()]
version_track.sort_values(['gapfix', 'strmsort', 'inherit_prov', 'trim_ends', 'distsource_fn' ,'end_streaming_event_threshold'])\
             .to_csv(vers_track_fname)

swot_master_table11 = swot_master_table.copy()
hhid_track.loc[12] = [mw.hhid.nunique(), dmx_events.hhid.nunique(), credit_results.hhid.nunique(),
                     credit_results_minwin.hhid.nunique(), credit_ranges.hhid.nunique(), swot_master_table.folder.nunique()]
print(hhid_track)

swot_master_table_app=swot_master_table.merge(shf.dfdev(),left_on='device',right_on='old')

if create_report:
    shf.finalreport(df=swot_master_table, date=date_range, debug=debug_report)