### Attempting to improve SWOT pull code
import pyodbc     as db
import pandas     as pd
import numpy as np
import scipy as sp
import datetime as dt
import sqlalchemy as sql
import matplotlib.pyplot as plt
import seaborn as sb
from difflib import SequenceMatcher
import os
import random as rd
pd.set_option('display.width', 170)

### Pull parameters
# date_range = '2017-09-01', '2017-09-03'
date_range = '2017-09-01', '2017-09-01'
date_range_dt = [dt.datetime.strptime(d, '%Y-%m-%d') for d in date_range]
assert date_range_dt[1] >= date_range_dt[0], 'ERROR: start date later than end date'
# tag = '{}-{}_alt'.format(*[d.strftime('%y%m%d') for d in date_range_dt])
dr = 'panel_swotdata/alt/{}-{}/'.format(*[d.strftime('%y%m%d') for d in date_range_dt])
if not os.path.exists(dr):
    os.makedirs(dr)
print("Data directory: {}".format(dr))

### Is connection available? If so, write files at checkpoints?
connection = False
checkpoint = True

### SWOT definitions
end_streaming_event_threshold = 240  # seconds

### Make plots of some random users' streaming and tuning activity?
make_activity_plots = True

### Options to turn on/off for how sessions are formed
### Break sessions at large gaps?
gapfix     = True
### Cap SWOT at the start of the next tune -- not implemented yet
cap_swot = True
### Ceiling start of stream sessions?
ceil_start = True
### Ceiling end of stream sessions?
ceil_end   = True
### Count streaming that occurs before the tune start as SWOT
include_preceding = True
### Count 'other' towards SWOT if same device? (i.e. tv watching, game playing)
other_swot = False
### Count streaming sessions that aren't associated with any tunes towards SWOT?
include_nontunes = False

### gapfix, ceil_start, ceil_end, other_swot include_preceding include_nontunes  swot_frac total (for 9/1/2017)
### False   False       False     False      False             False             1.43%
### True    False       False     False      False             False             2.43%
### True    True        False     False      False             False             2.21%
### True    True        True      False      False             False             2.90%
### True    True        True      True       False             False             5.48%
### False   False       False     True       False             False             4.90%
### True    True        True      False      True              False             6.83% <-- probably correct?
### True    True        True      False      True              True             45.56% !!! unless this is
### True    True        True      False      True              True             12.50% reran after awhile and got this???

########################################################################################################################
##################################### Mediaworks table #################################################################
########################################################################################################################
if connection == True:
    mediaworks_user         = 'baroer01'
    mediaworks_pass         = 'baroer01_changeme'
    mw_connection = 'postgresql://{}:{}@TPARHELMPP057.enterprisenet.org:5432/MEDIAWORKS'.format(mediaworks_user, mediaworks_pass)
    mw_query = '''SELECT DISTINCT
       d1."hh_id",
       d1."site",
       d1."id",
       d1."combo_id",
       m2."mac_address",
       m3."mac_id",
       d1."port",
       d1."expiration_date",
       m3."device_id",
       d1."model",
       d1."make",
       d1."appliance_type"
    FROM
       ("MEDIAWORKS"."collections"."mac_device"            m3
       LEFT OUTER JOIN "MEDIAWORKS"."collections"."device" d1 ON m3."device_id" = d1."id")
       LEFT OUTER JOIN "MEDIAWORKS"."collections"."mac"    m2 ON m3."mac_id" = m2."id" '''
    print('mw read from server')
    mw_engine    = sql.create_engine(mw_connection)
    mw1          = pd.read_sql_query(mw_query, mw_engine)
    if checkpoint:
        mw1.to_csv(dr + 'checkpoint_mw1.csv')
else:
    print('mw read from file')
    mw1 = pd.read_csv(dr + 'checkpoint_mw1.csv', index_col=0)
### Unique TV identifier
mw1['fsite'] = mw1.hh_id.astype(str) + '-' + mw1.site.astype(str)
### Append fields to get temporary device type
mw1['dvc_cln'] = ( mw1.appliance_type.str.lower().str.replace(' ','').fillna('') + '-'
                 + mw1.make.str.lower().str.replace(' ','').fillna('')           + '-'
                 + mw1.model.str.lower().str.replace(' ','').fillna(''))

### Resolve duplicate mac addresses - first the ones that are essentially the same
mw1 = mw1.sort_values(['mac_address', 'expiration_date'])
mw2 = mw1.drop_duplicates(subset=['fsite', 'mac_address', 'dvc_cln'], keep='last')
### If the device or fsite don't match, use the most recent?
### (many don't have an expiration date though! not sure what to use. this isn't well defined right now)
mw  = mw2.drop_duplicates(subset=['mac_address'], keep='last')
print('Drop duplicate MW entries: {0} -> {1} -> {2}'.format(mw1.shape[0], mw2.shape[0], mw.shape[0]))

mac_counts = mw.groupby(['mac_address']).id.count()
assert (mac_counts > 1).sum() == 0, 'ERROR: {0} conflicting MW table entries'.format((mac_counts > 1).sum())

### List of macaddresses, fsites present in mw
mw_macs   = tuple(mw.mac_address.unique())
mw_fsites = tuple(mw.fsite.unique())

assert len(mw_macs)==mw.shape[0], 'ERROR: duplication of mac addresses remains'

########################################################################################################################
###################################### Netezza connection ##############################################################
########################################################################################################################
if connection == True:
    print('connecting...')
    con = db.connect('DRIVER=NetezzaSQL;SERVER=nantz88.nielsen.com;PORT=5480;DATABASE=OPINTEL_PROD;UID=baroer01;PWD=Password@6;')
    print('connected!')
########################################################################################################################
######################################## Tree_id table #################################################################
########################################################################################################################
if connection == True:
    print('read tree_id from server')
    tree_ids = pd.read_sql_query('''SELECT DISTINCT parent, brand, channel, treeid AS tree_id FROM domain_resolution_result ''', con)
    if checkpoint:
        tree_ids.to_csv(dr + 'tree_id_table.csv', encoding='utf-8')
else:
    print('read tree_id from file')
    tree_ids = pd.read_csv(dr + 'tree_id_table.csv', index_col=0)
tree_ids.columns = [c.lower() for c in tree_ids.columns]
tree_ids['parent']  = tree_ids.parent.fillna('').copy()
tree_ids['brand']   = tree_ids.brand.fillna('').copy()
tree_ids['channel'] = tree_ids.channel.fillna('').copy()
tree_ids['provider'] = ''
tree_prov = {'dtv': ['directv'],
             'cw':  ['cw seed', 'cwtv.com'],
             'hbo': ['hbo network', 'hbo now'],
             'netflix': ['netflix'],
             'youtube': ['youtube'],
             'amazon':  ['amazon'],
             'hulu':    ['hulu'],
            }
prv_list = ['netflix', 'youtube', 'hulu', 'hbo', 'amazon', 'crackle', 'cw', 'dtv', 'discovery']
for p in prv_list:
    if (p not in tree_prov.keys()) & (p != 'discovery'):
        tree_ids.loc[tree_ids.brand.str.lower().str.contains(p), 'provider'] = p
        print(p, (tree_ids.provider!='').sum())
for p in tree_prov.keys():
    for s in tree_prov[p]:
        tree_ids.loc[tree_ids.brand.str.lower() == s, 'provider'] = p
        print(p, s, (tree_ids.provider != '').sum())
tree_ids.loc[tree_ids.parent.str.contains('Discovery Communications'), 'provider'] = 'discovery'
tree_ids.loc[(tree_ids.brand == 'Amazon') & (tree_ids.channel != 'LOVEFiLM.com'), 'provider'] = 'amazon'
tree_ids.loc[(tree_ids.brand == 'Amazon') & (tree_ids.channel == 'LOVEFiLM.com'), 'provider'] = ''

print((tree_ids.provider != '').sum())

tree_ids['use'] = tree_ids.provider != ''
use_trees = tuple(tree_ids.loc[tree_ids.use, 'tree_id'].unique())

### how to choose correct channels to associate w/ providers?
### Many other urls contain 'youtube' but aren't, e.g. safeyoutube.com
### HBO Now and Network but not Watch (Hbowatch.com -- unrelated?) also not actually present in data
### ???one of two Hulus (both both Netflix, even though they look equally relevant)
### ???Why Amazon (no channel) but not channel = Amazon Prime Video or Amazon (c)
### ???Which CW, DirecTV, and Discovery to use

########################################################################################################################
######################################## Streaming events ##############################################################
########################################################################################################################
if connection == True:
    print('reading streaming_events from server')
    strm_query = '''SELECT DISTINCT
        starttimeest,
        endtimeest,
        macaddress AS mac_address,
        treeid as tree_id
    FROM
        domain_event de
    INNER JOIN
        (SELECT DISTINCT
            d.domainkey,
            d.domain,
            de.treeid
        FROM
            fct_domainevents de,
            dim_domain d
        WHERE 1=1
        AND de.domainkey = d.domainkey
        AND de.treeid in {0}
        AND de.dmxstreaming = True
        ) as ids
    ON
    de.domain = ids.domain
    WHERE 1=1
      AND de.metercollectiondate between '{1}' AND '{2}'
      AND macaddress in {3} '''.format(use_trees, *date_range, mw_macs)
    streaming_events = pd.read_sql_query(strm_query, con)
    streaming_events.columns = [str.lower(x) for x in streaming_events.columns]
# -------------------------------------------------------------------------------------------------------------------- #
    print('reading mime_events from server')
    mime_query = '''SELECT DISTINCT
        starttimeest,
        endtimeest,
        macaddress AS mac_address,
        treeid as tree_id
    FROM
        mime_event me
    INNER JOIN
        (SELECT DISTINCT
            d.domainkey,
            d.domain,
            de.treeid
        FROM
            fct_domainevents de,
            dim_domain d
        WHERE 1=1
        AND de.domainkey = d.domainkey
      AND de.treeid in {0}
        AND de.dmxstreaming = True
        ) as ids
    ON
    me.domain = ids.domain
    WHERE 1=1
      AND me.metercollectiondate between '{1}' AND '{2}'
      AND lower(me.streaming) = 'true'
      AND macaddress in {3} '''.format(use_trees, *date_range, mw_macs)
    mime_events = pd.read_sql_query(mime_query, con)
    mime_events.columns = [str.lower(x) for x in mime_events.columns]
# -------------------------------------------------------------------------------------------------------------------- #
    print('building strm')
    strm_shape, mime_shape = streaming_events.shape, mime_events.shape
    strm_macs,  mime_macs  = streaming_events.mac_address.nunique(), mime_events.mac_address.nunique()
    strm1 = pd.concat([streaming_events, mime_events], ignore_index=True)
    del streaming_events
    del mime_events
    if checkpoint:
        strm1.to_csv(dr + 'checkpoint_strm1.csv')
else:
    print('read strm from file')
    strm1 = pd.read_csv(dr + 'checkpoint_strm1.csv', index_col=0)

### Convert timestamps
for c in ['starttimeest','endtimeest']:
    strm1[c] = pd.to_datetime(strm1[c])

### Round up to nearest minute? might align better with tuning data
if ceil_start:
    strm1['starttimeest'] = strm1['starttimeest'].dt.ceil(freq='min')
if ceil_end:
    strm1['endtimeest'] = strm1['endtimeest'].dt.ceil(freq='min')

# -------------------------------------------------------------------------------------------------------------------- #
### join with mw to get device names
strm2 = strm1.merge(mw[['hh_id','mac_address','fsite','dvc_cln']],
                      on='mac_address', how='inner')
print('Drop streaming events with no MW table entry: {0} -> {1}'.format(strm1.shape[0], strm2.shape[0]))
strm2 = strm2[['hh_id', 'fsite', 'mac_address', 'dvc_cln', 'tree_id', 'starttimeest', 'endtimeest']]
strm2.sort_values(['fsite', 'dvc_cln', 'starttimeest', 'endtimeest'], inplace=True)
strm2.reset_index(drop=True, inplace=True)

########################################################################################################################
########################################### Tuning events ##############################################################
########################################################################################################################
### distributionsource table has dist_sourceid names DISTRIBUTIONSOURCEID

if connection == True:
    print('reading tune from server')
    ### ??? why is the tree_id limit commented out? should it be?
    tune_query = '''SELECT DISTINCT
        t.folder_id || '-' || t.siteunitid as fsite,
        t.folder_id AS hh_id,
        t.ottmdevicekey as dkey,
        d.ottm_devicename as tn_dvc,
        t.dist_sourceid,
        t.tree_id,
        t.viewed_startest as start_time,
        t.viewed_endest as end_time
    FROM
        ADMIN.FCT_OTTM_BYMINUTE t,
        admin.DIM_OTTMDEVICE d
    WHERE 1=1
      AND d.ottmdevicekey = t.ottmdevicekey
      AND t.FOLDER_ID || '-' || t.SITEUNITID in {0}
      --AND t.tree_id in {1}
      AND t.metercollectiondate between '{2}' and '{3}'
      AND t.viewed_startest <= t.viewed_endest
    ORDER BY
        fsite asc,
        dkey asc,
        viewed_startest asc,
        viewed_endest asc,
        tree_id asc,
        dist_sourceid asc'''.format(tuple(strm2.fsite.dropna().unique().tolist()), #  tuple(mw.fsite.dropna().unique().tolist()),
                                    use_trees, *date_range)
    tune1 = pd.read_sql_query(tune_query, con)
    if checkpoint:
        tune1.to_csv(dr + 'checkpoint_tune1.csv')
else:
    tune1 = pd.read_csv(dr + 'checkpoint_tune1.csv', index_col=0)

tune1.columns = [str.lower(x) for x in tune1.columns]
for c in ['start_time','end_time']:
    tune1[c] = pd.to_datetime(tune1[c])

########################################################################################################################
########################################################################################################################
### Join back to back identical events
# tune1 = tune1.sort_values(['hh_id','fsite','dkey','start_time','end_time'])
tune1['gap'] = (tune1.start_time - tune1.end_time.shift(1)) / dt.timedelta(minutes=1)
tune1['new_grp'] = ( (tune1.fsite         != tune1.fsite.shift(1))
                   | (tune1.tree_id       != tune1.tree_id.shift(1))
                   | (tune1.dist_sourceid != tune1.dist_sourceid.shift(1))
                   | (tune1.dkey          != tune1.dkey.shift(1))
                   | (tune1.tn_dvc        != tune1.tn_dvc.shift(1))
                   | (tune1.gap           != 0))
tune1['b2b_ssn_id']  = tune1.new_grp.cumsum()
tune2 = tune1.groupby(['b2b_ssn_id','fsite','hh_id','dkey','tn_dvc','dist_sourceid','tree_id']).agg({'start_time':'min',
                                                                                                     'end_time':'max',
                                                                                                     'gap': 'first',
                                                                                                     'new_grp': 'first'})\
             .reset_index().sort_values(['hh_id','fsite','dkey','start_time','end_time', 'tree_id','dist_sourceid'])
tune2 = tune2[tune1.columns]
tune2['gap'] = (tune2.start_time - tune2.end_time.shift(1)) / dt.timedelta(minutes=1)

print('merge back to back same-station events: {0} -> {1}'.format(tune1.shape[0], tune2.shape[0]))
# -------------------------------------------------------------------------------------------------------------------- #
### Label activity class (previously done on tune4)
tune2['activity'] = None
tune2.loc[ (tune2.dist_sourceid == 0),      'activity'] = 'off'
tune2.loc[ (tune2.dist_sourceid == 10202)
         & (tune2.tree_id       == 0),      'activity'] = 'mute'
tune2.loc[~(tune2.dist_sourceid.isin([0, 10202]))
         & (tune2.tree_id       == 0),      'activity'] = 'other'
tune2.loc[ (tune2.dist_sourceid != 0)
         & (tune2.tree_id       != 0),      'activity'] = 'streaming'

tune2.loc[ (tune2.dkey != tune2.dkey.shift(1))
         & (tune2.dkey != 3500001)
         & (tune2.activity.isin(['mute', 'other'])), 'activity'] = 'sourcechange'

assert tune2.activity.isnull().sum()==0, 'ERROR: sessions with unidentified activity'

### Label provider based on tree_id
### First combine any entries with the same tree_id (only affects uncommon ones)
tree_ids_condensed = tree_ids[['tree_id','provider','brand','parent']]\
                       .groupby('tree_id').agg({'provider':'sum', 'brand': 'sum', 'parent':'count'}).sort_values('parent')\
                       .reset_index()
### Tuning data
tune2 = tune2.merge(tree_ids_condensed[['tree_id','provider','brand']], on='tree_id', how='left')
tune2.loc[tune2.tree_id == 0, ['provider','brand']] = 'nonstreaming'
### Streaming data also
strm = strm2.merge(tree_ids_condensed[['tree_id','provider','brand']], on='tree_id', how='left')

########################################################################################################################
### Resolve conflicted/overlapping events?
tune3 = tune2.copy()
########################################################################################################################
### Group sessions based on activity
### Assign new group ids based on rules
### Why does this group 'other' in with preceding streaming? isn't that non-streaming, i.e. what we want to measure?
tune3['new_grp']     = 0

#rule 1: streaming => new session
tune3.loc[tune3.activity == 'streaming', 'new_grp'] = 1

#rule 2: off/sourcechange => new session (other can include gaming, etc -- currently not counted as SWOT, but why?)
tune3.loc[tune3.activity.isin(['off','sourcechange']), 'new_grp'] = 1
if other_swot:
    tune3.loc[tune3.activity.isin(['other']), 'new_grp'] = 1

#rule 3: streaming-mute-streaming => second stream is not new session
tune3.loc[ (tune3.activity          == 'streaming')
         & (tune3.activity.shift(1) == 'mute')
         & (tune3.activity.shift(2) == 'streaming'), 'new_grp'] = 0

#rule 4: fsite change => new session
tune3.loc[tune3.fsite != tune3.fsite.shift(1), 'new_grp'] = 1

# rule 0: gap => new session
# do this last to override the lack of gap-checks in the other rules
tune3['gap'] = (tune3.start_time - tune3.end_time.shift(1)) / dt.timedelta(seconds=1)
if gapfix:
    tune3.loc[tune3.gap != 0, 'new_grp'] = 1

### Assign new session ids
tune3['groupID'] = tune3.new_grp.cumsum()

########################################################################################################################
### Create final grouped tuning sessions
agg_dict = {}
for c in tune3.columns:
    if c == 'groupID':
        pass
    elif c == 'activity':
        agg_dict[c] = lambda x: 'streaming' if 'streaming' in x.tolist() else x.tolist()[0]
    elif c == 'end_time':
        agg_dict[c] = 'last'
    else:
        agg_dict[c] = 'first'

print('creating swot_master_table')
tune4 = tune3.groupby('groupID').agg(agg_dict).reset_index()
tune4 = tune4[tune3.columns]
tune4['drtn'] = (tune4.end_time - tune4.start_time)/dt.timedelta(minutes=1)

print('Merge events based on activity: {0} -> {1}'.format(tune3.shape[0], tune4.shape[0]))
########################################################################################################################
### remove sessions that aren't streaming (some hhs lost at this point)
# tune = tune4.loc[tune4.activity].copy()
tune = tune4.copy()

print('Drop events with no streaming activity: {0} -> {1}'.format(tune4.shape[0], tune.shape[0]))
# -------------------------------------------------------------------------------------------------------------------- #
### Get time of next stream start
### If next line is a different device, assign nonsense value far in the future
future = tune.end_time.max() - tune.start_time.min() + dt.timedelta(days=10*365.25) #full span of the data + 10 years
tune = tune.sort_values(['fsite', 'start_time', 'end_time'])
tune['next_start_fsite'] = tune.start_time.shift(-1)
tune.loc[tune.fsite != tune.fsite.shift(-1),'next_start_fsite'] += future
tune = tune.sort_values(['fsite', 'tn_dvc', 'start_time', 'end_time'])
tune['next_start_dvc'] = tune.start_time.shift(-1)
tune.loc[(tune.fsite != tune.fsite.shift(-1)) | (tune.tn_dvc != tune.tn_dvc.shift(-1)),'next_start_dvc'] += future
### Assign the last row to the future also
tune.next_start_fsite.fillna(future + tune.end_time, inplace=True)
tune.next_start_dvc.fillna(  future + tune.end_time, inplace=True)

# assert (tune.next_start_fsite < tune.end_time).sum() == 0, 'ERROR: next_start_fsite is before end_time'
if (tune.next_start_fsite < tune.end_time).sum() > 0:
    print('WARNING: next_start_fsite is before end_time')
assert (tune.next_start_dvc < tune.end_time).sum() == 0, 'ERROR: next_start_dvc is before end_time'
# -------------------------------------------------------------------------------------------------------------------- #
### Assign stream session ids based on where there are gaps?
strm.sort_values(['fsite', 'dvc_cln', 'provider', 'starttimeest'], inplace=True)
strm['strm_gap'] = (strm.starttimeest - strm.endtimeest.shift(1)) / dt.timedelta(seconds=1)
strm['strm_new_ssn'] = ( (strm.strm_gap >  5*60)
                       | (strm.fsite    != strm.fsite.shift(1))
                       | (strm.dvc_cln  != strm.dvc_cln.shift(1))
                       | (strm.provider != strm.provider.shift(1)) )
strm['strm_ssn_id'] = strm.strm_new_ssn.cumsum()

# -------------------------------------------------------------------------------------------------------------------- #
# ### Determine if event is final event, i.e. last event before a gap
# strm['final_streaming_event'] = False
# ### Is it the last one on this device?
# strm.loc[ (strm.fsite    != strm.fsite.shift( -1))
#         | (strm.dvc_cln  != strm.dvc_cln.shift(-1))
#         | (strm.provider != strm.provider.shift(-1)), 'final_streaming_event'] = True
# ### If not the last one, and it ends more than 4 min away from next event start
# ### why require not already True??? I don't think this does anything
# strm.loc[~(strm.final_streaming_event)
#         & (abs(strm.starttimeest.shift(-1) - strm.endtimeest) / dt.timedelta(seconds=1) > end_streaming_event_threshold),
#         'final_streaming_event'] = True
# ### If next row is new site, now change it to False
# ### why did we set this to True earlier then???
# strm.loc[(strm.fsite != strm.fsite.shift(-1)), 'final_streaming_event'] = False
# ### Why was there a df filtering out to just final ones, then not used??? why is this sort here???

########################################################################################################################
### Drop unidentified tree_ids
# tune = tune.loc[tune.provider != ''].copy()

########################################################################################################################
########################################################################################################################
########################################################################################################################
### Unify the device types between the two datasets
tune['dvc_cln'] = tune.tn_dvc.str.lower().str.replace(' ', '').fillna('')
# strm['dvc_cln'] = strm.mw_dvc.str.lower().str.replace(' ', '')

tune_dvcs = tune[['fsite','dvc_cln']].drop_duplicates().dvc_cln.value_counts()
strm_dvcs = strm[['fsite','dvc_cln']].drop_duplicates().dvc_cln.value_counts()

tune_dvcs.to_csv('{}tune_dvcs.csv'.format(dr))
strm_dvcs.to_csv('{}strm_dvcs.csv'.format(dr))

### Get strm-tune matching file and common device assignments
from device_names import panel_common_device_mapping, get_device_key
st_tn_match = panel_common_device_mapping(strm_dvcs, tune_dvcs, group_roku=True)
st_tn_match.to_csv(dr + 'strm_tune_common_device_mappings.csv')

### Mapping key for each dataset
if 'roku' in tune_dvcs.index:
    tune_skip = None
else:
    tune_skip = 'roku'
tune_dvc_key = get_device_key(st_tn_match, 'tn', n_input_dvcs=tune_dvcs.shape[0], skip_val=tune_skip)
strm_dvc_key = get_device_key(st_tn_match, 'st', n_input_dvcs=strm_dvcs.shape[0])

### Join new device names to the data
if 'device' in tune.columns:
    tune = tune.drop('device', axis=1)
tune = tune.merge(tune_dvc_key, on='dvc_cln', how='left')
if 'device' in strm.columns:
    strm = strm.drop('device', axis=1)
strm = strm.merge(strm_dvc_key, on='dvc_cln', how='left')

### Compare frequency of each type
dvc_counts = pd.DataFrame({'n_tn_ssns': tune.device.value_counts(),
                           'n_st_ssns': strm.device.value_counts()}).fillna(0)\
               .sort_values(['n_tn_ssns','n_st_ssns'], ascending=False)
print(dvc_counts)

########################################################################################################################
########################################################################################################################
########################################################################################################################
### Can these be modified to give a partitioned rolling window (that's fast enough)?
# def f(x):
#     ser = df.ToRoll[(df.RollBasis >= x) & (df.RollBasis < x+5)]
#     return ser.sum()
# df['Rolled'] = df.RollBasis.apply(f)
#
# def rollBy(what, basis, window_lo, window_hi, func):
#     def applyToWindow(val):
#         chunk = what[(val<=basis-window_lo) & (basis<val+window_hi)]
#         return func(chunk)
#     return basis.apply(applyToWindow)
### Copy indices to columns, so they can be included in the timeline and later used to map the results back
tune['vw_id'] = tune.fsite + '-' + tune.device +'-' +  tune.provider
strm['vw_id'] = strm.fsite + '-' + strm.device +'-' +  strm.provider

# tune['midtm'] = tune.start_time   + (tune.end_time   - tune.start_time  )/2
# strm['midtm'] = strm.starttimeest + (strm.endtimeest - strm.starttimeest)/2

tune['tune_ind'] = tune.index
strm['strm_ind'] = strm.index
### Cut back to streaming tunes only
tune_strm_only = tune.copy()#.loc[tune.activity].copy()
tune_strm_only['tune_next_start'] = tune_strm_only.start_time.shift(-1)
tune_strm_only.loc[tune_strm_only.vw_id != tune_strm_only.vw_id.shift(-1), 'tune_next_start'] = np.nan
### Columns in timeline
join_cols = ['vw_id']#,'midtm'] # time must be last if present
tune_cols = ['tune_ind','start_time','end_time','tune_next_start','activity']
strm_cols = ['strm_ind','starttimeest', 'endtimeest', 'strm_ssn_id']
timeline = pd.concat([tune_strm_only[join_cols + tune_cols],
                      strm[join_cols + strm_cols] ],
                     axis=0, join='outer', ignore_index=True)
tune.drop('tune_ind', axis=1, inplace=True)
strm.drop('strm_ind', axis=1, inplace=True)
# assert timeline.shape[0] == tune.loc[tune.activity].shape[0] + strm.shape[0], 'ERROR: timeline is not sum of swot and dmx'
timeline = timeline[join_cols + tune_cols + strm_cols]
timeline['both_times'] = timeline.start_time.copy()
timeline.loc[timeline.both_times.isnull(), 'both_times'] = timeline.starttimeest
assert timeline.both_times.isnull().sum() == 0, 'ERROR: both_times contains nulls'

### Sort by: device and provider, then common time
### then: sort by starttimeest to get swot before dmx if common time is the same
timeline = timeline.sort_values(join_cols + ['both_times']
                                + ['start_time','end_time','starttimeest','endtimeest']).reset_index(drop=True)
### Identify start of new id/device
timeline['new_dvc'] = (timeline.vw_id != timeline.vw_id.shift(1))
### Calculate event duration
timeline['drtn'] = (timeline.end_time - timeline.start_time) / dt.timedelta(seconds=1)
timeline.loc[timeline.tune_ind.isnull(), 'drtn'] = (timeline.endtimeest - timeline.starttimeest) / dt.timedelta(seconds=1)
assert timeline.drtn.isnull().sum() == 0, 'ERROR: {0} invalid drtn'.format(timeline.drtn.isnull().sum())
### Calculate gap between this event start and prev event end
timeline['gap']  = ((timeline.both_times - (timeline.both_times.shift(1) )) / dt.timedelta(seconds=1)) - timeline.drtn.shift(1)
timeline.loc[timeline.new_dvc.shift(-1).fillna(False), 'gap'] = np.nan
### Are events touching (within some tolerance)
timeline['contiguous'] = (timeline.gap <= end_streaming_event_threshold).fillna(False)

# #----------------------------------------------------------------------------------------------------------------------#
# ### Stream-based sessions
# ### Strm ssn aggs to get start/end times for each group:
# strm_ssn_aggs = timeline.groupby('strm_ssn_id').agg({'starttimeest': 'min',
#                                                       'endtimeest':  'max'})\
#                         .reset_index()\
#                         .rename(columns={'starttimeest': 'ssn_start',
#                                          'endtimeest':   'ssn_end'})
# timeline = timeline.merge(strm_ssn_aggs, on='strm_ssn_id', how='left')
# ### Make a vw_id column that's nan on the tune rows, then forward/back fill to compare with surrounding tunes
# timeline['strm_vw_id'] = timeline.vw_id
# timeline.loc[timeline.tune_ind.notnull(), 'strm_vw_id'] = np.nan
# timeline['prev_vw_id'] = timeline.strm_vw_id.fillna(method='ffill')
# timeline['next_vw_id'] = timeline.strm_vw_id.fillna(method='bfill')
# timeline['prev_ssn_id'] = timeline.strm_ssn_id.fillna(method='ffill')
# timeline['next_ssn_id'] = timeline.strm_ssn_id.fillna(method='bfill')
# timeline['prev_strm_end']   = timeline.ssn_end.fillna(  method='ffill')
# timeline['next_strm_start'] = timeline.ssn_start.fillna(method='bfill')
# timeline['to_prev_end']     = (timeline.start_time - timeline.prev_strm_end)/dt.timedelta(seconds=1)
# timeline['to_next_start']   = (timeline.next_strm_start - timeline.end_time)/dt.timedelta(seconds=1)
# ### Unless they are different devices, then overwrite
# for c in ['prev_ssn_id','prev_strm_end','to_prev_end']:
#     timeline.loc[timeline.prev_vw_id != timeline.vw_id, c] = np.nan
# for c in ['next_ssn_id','next_strm_start','to_next_start']:
#     timeline.loc[timeline.next_vw_id != timeline.vw_id, c] = np.nan
#
# ### Assign tunes to strm ssns:
# tl_cols = ['vw_id','midtm','both_times','end_time','endtimeest','strm_ssn_id','to_prev_end','to_next_start']
# tune_rows = timeline.tune_ind.notnull()
# prev_match = timeline.vw_id == timeline.prev_vw_id
# next_match = timeline.vw_id == timeline.next_vw_id
# prev_close = timeline.to_prev_end   <= end_streaming_event_threshold
# next_close = timeline.to_next_start <= end_streaming_event_threshold
# prev_ovrlp = (timeline.to_prev_end   <= 0)
# next_ovrlp = (timeline.to_next_start <= 0)
# ### reset
# timeline.loc[tune_rows, 'strm_ssn_id'] = np.nan
# print('{:30}: {}'.format('Tunes with no strm_ssn_id', timeline.strm_ssn_id.isnull().sum()))
# ### If there are no strms for that dvc/prv, or none that are close, create new ssn id
# unassigned = timeline.strm_ssn_id.isnull()
# no_strms = unassigned & ~prev_match & ~next_match
# timeline.loc[no_strms, 'strm_ssn_id'] = timeline.strm_ssn_id.max() + np.arange(1,no_strms.sum()+1)
# print('{:30}: {}'.format('No strms for dvc/prv', timeline.strm_ssn_id.isnull().sum()))
# ### If tune is between two strms, and the surrounding strms are from same ssn, use that id
# unassigned = timeline.strm_ssn_id.isnull()
# sandwiched = (timeline.prev_ssn_id == timeline.next_ssn_id) & timeline.prev_ssn_id.notnull()
# timeline.loc[sandwiched, 'strm_ssn_id'] = timeline.prev_ssn_id.loc[sandwiched]
# print('{:30}: {}'.format('Embedded within one ssn', timeline.strm_ssn_id.isnull().sum()))
# ### Proximity to surrounding streams:
# ### If one side matches vw_id and is close, and other doesn't or isn't
# unassigned = timeline.strm_ssn_id.isnull()
# only_prev = unassigned &   prev_match & prev_close  & ~(next_match & next_close)
# only_next = unassigned & ~(prev_match & prev_close) &   next_match & next_close
# timeline.loc[only_prev, 'strm_ssn_id'] = timeline.loc[only_prev, 'prev_ssn_id']
# timeline.loc[only_next, 'strm_ssn_id'] = timeline.loc[only_next, 'next_ssn_id']
# print('{:30}: {}'.format('Only close on one side', timeline.strm_ssn_id.isnull().sum()))
# ### If both close but one overlaps and one doesn't, use that one
# unassigned = timeline.strm_ssn_id.isnull()
# surrounded = prev_close & next_close & unassigned
# only_prev_ovrlp = surrounded &  prev_ovrlp & ~next_ovrlp
# only_next_ovrlp = surrounded & ~prev_ovrlp &  next_ovrlp
# timeline.loc[only_prev_ovrlp, 'strm_ssn_id'] = timeline.loc[only_prev_ovrlp, 'prev_ssn_id']
# timeline.loc[only_next_ovrlp, 'strm_ssn_id'] = timeline.loc[only_next_ovrlp, 'next_ssn_id']
# print('{:30}: {}'.format('Only overlaps on one side', timeline.strm_ssn_id.isnull().sum()))
# ### If both close but not overlapping, which is closer?
# unassigned = timeline.strm_ssn_id.isnull()
# surrounded = prev_close & next_close & unassigned
# both_close = surrounded & prev_close & next_close & ~prev_ovrlp & ~next_ovrlp
#
# prev_closer = surrounded & (timeline.to_prev_end < timeline.to_next_start)
# next_closer = surrounded & (timeline.to_prev_end > timeline.to_next_start)
# timeline.loc[prev_closer, 'strm_ssn_id'] = timeline.loc[prev_closer, 'prev_ssn_id']
# timeline.loc[next_closer, 'strm_ssn_id'] = timeline.loc[next_closer, 'next_ssn_id']
# print('Tunes with no strm_ssn_id: {}'.format(timeline.strm_ssn_id.isnull().sum()))
#
# ### If overlapping both, bridge the gap and make them all one session?
# ### This breaks the next/prev columns -- need to fix in case this happens twice in a row!!!
# ### Also currently relies on a loop, which is not ideal
# unassigned = timeline.strm_ssn_id.isnull()
# surrounded = prev_close & next_close & unassigned
# both_overlap = surrounded & prev_ovrlp & next_ovrlp
# for i in timeline.loc[both_overlap].index:
#     prev_ind, next_ind = timeline.loc[i, ['prev_ssn_id','next_ssn_id']]
#     ### Assign tune to the previous session
#     timeline.loc[i, 'strm_ssn_id'] = prev_ind
#     ### Assign second session to be part of the first
#     ssn2 = timeline.strm_ssn_id == next_ind
#     # for c in ['prev_vw_id', 'prev_ssn_id', 'prev_strm_end', 'to_prev_end']:
#     #     timeline.loc[timeline.prev_vw_id != timeline.vw_id, c] = np.nan
#     # for c in ['next_vw_id', 'next_ssn_id', 'next_strm_start', 'to_next_start']:
#     #     timeline.loc[timeline.next_vw_id != timeline.vw_id, c] = np.nan
#     timeline.loc[ssn2, 'strm_ssn_id'] = prev_ind
#
#
#
# ### If tune is between two strms from different ssns, use the closest one? or next unless previous is very close?
#
# ### If tune is before all strms for that dvc/prv
#
# ### If tune is after all strms for that dvc/prv
#
# ### If tune is in group of a few tunes?
#
#----------------------------------------------------------------------------------------------------------------------#
### Tune-based sessions
# ### Get st/ed times of prev and next tuning sessions (with tune rows as their own values)
timeline['tn_prev_sttm'] = timeline.start_time.fillna(method='ffill')
timeline['tn_prev_edtm'] = timeline.end_time.fillna(  method='ffill')
timeline['tn_next_sttm'] = timeline.start_time.fillna(method='bfill')
timeline['tn_next_edtm'] = timeline.end_time.fillna(  method='bfill')
### Get st/ed times of prev and next tuning sessions (with tune rows as the next value)
timeline['tn_next_sttm2'] = timeline.tune_next_start.fillna(method='ffill')

### Also get prev/next device info to confirm it matches
tn_only_dvc = timeline[join_cols].copy()
tn_only_dvc.loc[timeline.start_time.isnull()] = None
timeline['tn_prev_vw'] = tn_only_dvc.vw_id.fillna(method='ffill')
timeline['tn_next_vw'] = tn_only_dvc.vw_id.fillna(method='bfill')
## Remove the entries that extended into the next device
timeline.loc[ (timeline.vw_id != timeline.tn_prev_vw),
             ['tn_prev_sttm', 'tn_prev_edtm']] = np.nan
timeline.loc[ (timeline.vw_id != timeline.tn_next_vw),
             ['tn_next_sttm', 'tn_next_edtm']] = np.nan
### Gap between stream and previous/next tunes?
timeline['gap_tn_prev'] = (timeline.starttimeest - timeline.tn_prev_edtm) / dt.timedelta(seconds=1)
timeline['gap_tn_next'] = (timeline.endtimeest   - timeline.tn_next_sttm) / dt.timedelta(seconds=1)
### Is this event contiguous with the prev (within a buffer)?
### ??? How to make this tolerant to overlapping stream sessions?
### sometimes strm1 is long, strm2 is short and overlapping, so strm2 has a gap before strm3 but strm1 doesn't
### this method won't understand that because it only looks at adjacent rows
### could break up overlapping sessions, put second half of strm1 in new row after strm2
### Or extend endtime of strm2 to match strm1, but only if they very definitely match

### How is each event categorized?
### If event is on threshold and meets multiple criteria, put the one that should override last
### buffer, within which a streaming event can be grouped with a tune event, as long as it's not already within another
bffr = dt.timedelta(seconds=end_streaming_event_threshold)
### Is it any more efficient to work with integers, and map to strings? didn't seem like it
# event_type_map = pd.DataFrame({'event_type_id': [0, 1, 2, 3, 4, 5, 6, 7],
#                                'event_type': ['invalid', 'tune', 'during_prev',
#                                               'during_next', 'between', 'before_first', 'after_last', 'no_tuning']})
# timeline['event_type_id'] = 0
# timeline.loc[timeline.start_time.notnull(),  'event_type_id'] = 1
# timeline.loc[timeline.tn_prev_sttm.isnull()  &  timeline.tn_next_sttm.isnull(), 'event_type_id'] = 7
# timeline.loc[timeline.tn_prev_sttm.isnull()  & timeline.tn_next_sttm.notnull(), 'event_type_id'] = 5
# timeline.loc[timeline.tn_prev_sttm.notnull() &  timeline.tn_next_sttm.isnull(), 'event_type_id'] = 6
# timeline.loc[timeline.starttimeest.between(timeline.tn_prev_edtm+bffr, timeline.tn_next_sttm-bffr, inclusive=False), 'event_type_id'] = 4
# timeline.loc[timeline.starttimeest.between(timeline.tn_next_sttm-bffr, timeline.tn_next_edtm+bffr), 'event_type_id'] = 3
# timeline.loc[timeline.starttimeest.between(timeline.tn_prev_sttm-bffr, timeline.tn_prev_edtm+bffr), 'event_type_id'] = 2
# if 'event_type' in timeline.columns:
#     timeline = timeline.drop(columns='event_type')
# timeline = timeline.merge(event_type_map, on='event_type_id', how='left')
### directy assign strings
timeline['event_type'] = 'invalid'
timeline.loc[timeline.start_time.notnull(),  'event_type'] = 'tune'
timeline.loc[timeline.tn_prev_sttm.isnull()  &  timeline.tn_next_sttm.isnull(),           'event_type'] = 'no_tuning'
timeline.loc[timeline.tn_prev_sttm.isnull()  & timeline.tn_next_sttm.notnull(),           'event_type'] = 'before_first'
timeline.loc[timeline.tn_prev_sttm.notnull() &  timeline.tn_next_sttm.isnull(),           'event_type'] = 'after_last'
timeline.loc[timeline.starttimeest.between(timeline.tn_prev_edtm, timeline.tn_next_sttm, inclusive=False), 'event_type'] = 'between'
# timeline.loc[timeline.starttimeest.between(timeline.tn_prev_edtm+bffr, timeline.tn_next_sttm-bffr, inclusive=False), 'event_type'] = 'between'
# ### Being within buffer of an event will override all of the above. during_prev is preferred when both
# timeline.loc[timeline.starttimeest.between(timeline.tn_next_sttm-bffr, timeline.tn_next_edtm+bffr), 'event_type'] = 'during_next'
# timeline.loc[timeline.starttimeest.between(timeline.tn_prev_sttm-bffr, timeline.tn_prev_edtm+bffr), 'event_type'] = 'during_prev'
timeline.loc[timeline.starttimeest.between(timeline.tn_next_sttm, timeline.tn_next_edtm), 'event_type'] = 'during_next'
timeline.loc[timeline.starttimeest.between(timeline.tn_prev_sttm, timeline.tn_prev_edtm), 'event_type'] = 'during_prev'

tl_cols = ['new_dvc', 'both_times',
           'endtimeest', 'end_time',
           'drtn', 'gap', 'contiguous', 'event_type' ] #'gap_prev','gap_next']
timeline.loc[120:135, tl_cols]
timeline.loc[285:295, tl_cols]

assert (timeline.event_type == 'invalid').sum() == 0, 'ERROR: {0} invalid event_type remaining'.format(
       (timeline.event_type == 'invalid').sum())

### Assign tune_ssn, a session id that identifies streaming during a given tune session
###  And   swot_ssn, which also includes trailing streams
timeline['initial_ssn_start'] = ( (timeline.event_type=='tune') | timeline.new_dvc
                                | (~timeline.contiguous & ~timeline.event_type.isin(['during_prev'])))
timeline['tune_ssn'] = timeline.initial_ssn_start.cumsum()
timeline['swot_ssn'] = timeline['tune_ssn'].copy()
### Make a column that keeps track of the vw_id for the value being propagated, to make sure it matches the rows it fills
timeline['slide_vw'] = timeline.vw_id.copy()
timeline.loc[~timeline.initial_ssn_start, 'slide_vw'] = np.nan
timeline['slide_vw'] = timeline.slide_vw.fillna(method='ffill')
timeline.loc[timeline.vw_id != timeline.slide_vw, ['tune_ssn','swot_ssn']] = np.nan
### Blank anywhere it's not currently correct, then use ffill and bfill to group together as appropriate
### First, blank preceding rows in preparation for backfill (both the same b/c preceding events aren't SWOT)
### Need to preserve the entries in between rows where the next row is non-contig, to prevent backfill-merging
preceding_rows = ( timeline.contiguous.shift(-1).fillna(False)
                 & ~timeline.event_type.isin(['tune','during_prev','after_last']))
timeline.loc[preceding_rows, 'tune_ssn'] = np.nan
timeline.loc[preceding_rows, 'swot_ssn'] = np.nan
timeline.loc[preceding_rows, 'slide_vw'] = np.nan
### Backfill through the preceding streams -- this also fills through other vw_ids though
timeline['tune_ssn'] = timeline.tune_ssn.fillna(method='bfill')
timeline['swot_ssn'] = timeline.swot_ssn.fillna(method='bfill')
timeline['slide_vw'] = timeline.slide_vw.fillna(method='bfill')
### Now group trailing sessions with previous
### Blank and forward-fill through following streams for swot_ssn only
trailing_rows = ( timeline.event_type.isin(['between','after_last'])
                & timeline.contiguous)
timeline.loc[trailing_rows, 'swot_ssn'] = np.nan
timeline.loc[trailing_rows, 'slide_vw'] = np.nan
timeline['swot_ssn'] = timeline.swot_ssn.fillna(method='ffill')
timeline['slide_vw'] = timeline.slide_vw.fillna(method='ffill')
timeline.loc[timeline.vw_id != timeline.slide_vw, ['swot_ssn','slide_vw']] = np.nan
# ### Blank for streams that aren't in a tune session, to remove over-ffill/bill
# timeline.loc[timeline.event_type.isin(['before_first','after_last','between','no_tuning']), 'tune_ssn'] = np.nan
# timeline.loc[timeline.event_type.isin(['before_first','no_tuning'                       ]), 'swot_ssn'] = np.nan
# ### Fill empty spots witn -1 and set all to integer type
# # timeline.loc[timeline.event_type.isin(['before_first','after_last','between','no_tuning']), 'tune_ssn'] = -1
# # timeline['tune_ssn'] = timeline.tn_ssn.astype('int')

tl_cols = ['vw_id', 'both_times',
           'endtimeest', 'end_time','gap','contiguous', 'event_type',
           'tune_ssn', 'swot_ssn', 'slide_vw' ]

ssns_w_swot = timeline.loc[(timeline.tune_ssn!=timeline.swot_ssn) & timeline.swot_ssn.notnull(), 'swot_ssn'].unique()

timeline.loc[timeline.swot_ssn == ssns_w_swot[0], tl_cols]

#----------------------------------------------------------------------------------------------------------------------#
#----------------------------------------------------------------------------------------------------------------------#
#----------------------------------------------------------------------------------------------------------------------#

ssn_aggs = timeline.groupby('swot_ssn').agg({'vw_id': 'first',
                                             'tune_ind': 'count',
                                             'strm_ind': 'count',
                                             'activity': lambda x: 'streaming' if 'streaming' in x.tolist() else x.tolist()[0],
                                             # 'provider': 'first',
                                             'strm_ssn_id': pd.Series.nunique,
                                             'start_time': 'min',
                                             'end_time':   'max',
                                             'starttimeest': 'min',
                                             'endtimeest':   'max',
                                             'contiguous':   'sum',
                                             'tune_next_start': 'first'
                                            })\
                   .rename(columns={'tune_ind': 'n_tunes',
                                    'strm_ind': 'n_strms',
                                    'strm_ssn_id': 'n_strm_ssns',
                                    'start_time': 'tune_sttm',
                                    'end_time':   'tune_edtm',
                                    'starttimeest': 'strm_sttm',
                                    'endtimeest': 'strm_edtm',
                                    'contiguous': 'n_contig',
                                    })[['vw_id','activity','n_tunes','n_strms','n_strm_ssns',#'provider',
                                        'tune_sttm','tune_edtm','strm_sttm','strm_edtm','tune_next_start','n_contig']]
ssn_aggs['n_strm_ssns'] = ssn_aggs.n_strm_ssns.astype('int')
# ###
ssns = ssn_aggs.copy()
ssns['sttm'] = ssn_aggs[['tune_sttm','strm_sttm']].min(axis=1)
ssns['edtm'] = ssn_aggs[['tune_edtm','strm_edtm']].max(axis=1)
ssns['edtm'] = ssns[['edtm','tune_next_start']].min(axis=1)

# ssn_aggs['diff'] = (ssn_aggs.n_tunes + ssn_aggs.n_strms) - ssn_aggs.n_contig
# ###
# ssn_aggs['strm_v_tune_st'] = (ssn_aggs.tune_sttm - ssn_aggs.strm_sttm) / dt.timedelta(seconds=1)
# ssn_aggs['strm_drtn'] = (ssn_aggs.strm_edtm - ssn_aggs.strm_sttm) / dt.timedelta(seconds=1)
# ssn_aggs['tune_drtn'] = (ssn_aggs.tune_edtm - ssn_aggs.tune_sttm) / dt.timedelta(seconds=1)

ssn_aggs.loc[ssn_aggs.tune_next_start < ssn_aggs.strm_edtm]

# ### Tune vs strm duration (by provider, which is no longer passed through)
# pal = sb.color_palette("Set1", tune.provider.nunique(), desat=0.7)
# f1, ax1 = plt.subplots(1)
# ax1.plot([1, 1e9], [1, 1e9], c='black', lw=1)
# for i, p in enumerate(tune.provider.unique()):
#     ax1.scatter(ssn_aggs.loc[ssn_aggs.provider==p, 'tune_drtn']/60.,
#                 ssn_aggs.loc[ssn_aggs.provider==p, 'strm_drtn']/60., c=pal[i], label=p)
# ax1.set_xscale('log')
# ax1.set_yscale('log')
# ax1.set_xlim(1, 1e4)
# ax1.set_ylim(1, 1e6)
# ax1.set_xlabel('Tune length (min)')
# ax1.set_ylabel('Stream length (min)')
# ax1.legend()

#----------------------------------------------------------------------------------------------------------------------#
#----------------------------------------------------------------------------------------------------------------------#
#----------------------------------------------------------------------------------------------------------------------#
### Visualize one set of activity
if make_activity_plots:
    from session_plot_fns import plot_ssn_rows

    vw_id_list = timeline.groupby('vw_id').agg({'tune_ind': 'count',
                                                'strm_ind': 'count'})
    # v_list = rd.sample(list(vw_id_list.loc[(vw_id_list > 0).all(axis=1)].index), 5)
    v_list = []
    v_list += ['17871700-1-xboxone-netflix',
               '20373613-3-dvd-netflix',
               '20037881-2-playstation3-netflix',
               '18594350-1-dvd-netflix',
               '18246549-1-amazonfirestick-netflix']
    for v in v_list:
        this_dvc = timeline.loc[ (timeline.vw_id == v)
                               ]#.reset_index(drop=True)
        # if v == '17871700-1-xboxone-netflix':
        #     this_dvc = this_dvc.loc[this_dvc.both_times.between(dt.datetime(2017, 9, 1, 18, 30),
        #                                                         dt.datetime(2017, 9, 1, 21))]#.reset_index(drop=True)
        print('Plotting: {} rows, visitor {}'.format(this_dvc.shape[0], v))
        f1, ax1 = plot_ssn_rows(this_dvc,
                  ssn_sttms = ['starttimeest', 'start_time'],
                  ssn_edtms = ['endtimeest',   'end_time'],
                  cols = ['red', 'blue'],
                  ssns=ssns.loc[ssns.vw_id==v],
                  titl='Panel streaming and tuning sessions, {}'.format(v))
        #plot_ssn_rows(this_dvc, titl='Panel streaming and tuning sessions, {}'.format(v), ssn_id='swot_ssn')
        # if v == '17871700-1-xboxone-netflix':
        #     ax1.set_xticks(pd.date_range(dt.datetime(2017, 9, 1, 18, 30), dt.datetime(2017, 9, 1, 21), freq='30min'))
        f1.savefig('{}time_v_line_{}.png'.format(dr, v), dpi=300)
        plt.close(f1)

# -------------------------------------------------------------------------------------------------------------------- #
# -------------------------------------------------------------------------------------------------------------------- #
# -------------------------------------------------------------------------------------------------------------------- #
### Ignoring preceding SWOT for now?
ssns['tune_dur'] = ((ssns.tune_edtm - ssns.tune_sttm)/dt.timedelta(seconds=1))
if include_preceding:
    ssns['strm_dur'] = ((ssns.strm_edtm - ssns.strm_sttm)/dt.timedelta(seconds=1))
else:
    ssns['strm_dur'] = (ssns.edtm      - ssns.tune_sttm)/dt.timedelta(seconds=1)
if include_nontunes:
    ssns['tune_dur'] = ssns['tune_dur'].fillna(0)
    ssns['strm_dur'] = ssns['strm_dur'].fillna(0)
ssns['swot_dur'] = (ssns.strm_dur - ssns.tune_dur).clip(lower=0)
ssns['swot_frac'] = ssns.swot_dur / ssns.tune_dur
print('Tune mins {:0.0f}, strm mins {:0.0f}, SWOT mins {:0.0f}, Overall SWOT fraction = {:0.2f}%'\
      .format(ssns.tune_dur.sum(),
              ssns.strm_dur.sum(),
              ssns.swot_dur.sum(),
              100 * ssns.swot_dur.sum() / ssns.tune_dur.sum()))

### Record version effects
vers_track_fname ='{}/version_track.csv'.format(dr)
# version_track = pd.DataFrame([], ### for starting a new tracker
#                              columns=['gapfix', 'ceil_start', 'ceil_end', 'include_preceding', 'other_swot', 'include_nontunes',
#                                       'end_streaming_event_threshold',
#                                       'tune_mins','strm_mins','swot_mins','swot_frac'])
version_track = pd.read_csv(vers_track_fname, index_col=0) ### For adding to existing tracker
version_track.loc[dt.datetime.now()] = [gapfix, ceil_start, ceil_end, other_swot, include_preceding, include_nontunes,
                                        end_streaming_event_threshold,
                                        ssns.tune_dur.sum(), ssns.strm_dur.sum(), ssns.swot_dur.sum(),
                                        ssns.swot_dur.sum() / ssns.tune_dur.sum()]
version_track.sort_values(['gapfix', 'ceil_start', 'ceil_end', 'include_preceding', 'other_swot', 'include_nontunes',
                           'end_streaming_event_threshold'])\
             .to_csv(vers_track_fname)

assert 1==0, 'end of the line'




# ssn_aggs['tune_drtn'] = (ssn_aggs.tune_edtm - ssn_aggs.tune_sttm) / dt.timedelta(seconds=1)
# ssn_aggs['strm_drtn'] = (ssn_aggs.strm_edtm - ssn_aggs.strm_sttm) / dt.timedelta(seconds=1)
# ssn_aggs['swot'] = ssn_aggs.strm_drtn - ssn_aggs.tune_drtn

# -------------------------------------------------------------------------------------------------------------------- #
# used_lines = timeline.loc[( timeline.tune_ind.notnull()
#                           | timeline.final_streaming_event
#                           | timeline.is_last_strm)].copy()
# # -------------------------------------------------------------------------------------------------------------------- #
# ### Within each identified group, find the endtime of the first final_streaming_event
# ### and the endtime of the last event (why not latest endtime? sometimes there are multiple of same starttime, or overlap)
# ssn_aggs = used_lines.groupby('ssn_id').agg({'endtimeest':'last',
#                                              'change_endtime':'first'})
# ssn_aggs.rename(columns={'endtimeest':'last_event_endtime',
#                          'change_endtime': 'valid_group'}, inplace=True)
# ssn_aggs['first_finalevent_endtime'] = used_lines.loc[used_lines.final_streaming_event & used_lines.final_streaming_event.notnull()].groupby('ssn_id').endtimeest.first()
# ssn_aggs.drop(-1, inplace=True)
# ### If there is a final_streaming_event, assign that to the swot event
# ### otherwise assign the final time
# ssn_aggs['use_endtime'] = ssn_aggs.first_finalevent_endtime.copy()
# ssn_aggs.loc[ssn_aggs.first_finalevent_endtime.isnull(), 'use_endtime'] = ssn_aggs.last_event_endtime
# ssn_aggs.loc[~ssn_aggs.valid_group, 'use_endtime'] = np.nan
# # -------------------------------------------------------------------------------------------------------------------- #
# ### many of the new endtimes are after the next starttime -- was that true before?
# change_swot = timeline.loc[timeline.tune_ind.notnull() & timeline.change_endtime,
#                            ['tune_ind','ssn_id','end_time','next_tune_start','change_endtime']].copy()
# change_swot['new_endtime'] = list(ssn_aggs.loc[list(change_swot.ssn_id), 'use_endtime'])
# assert change_swot.new_endtime.isnull().sum() == 0, 'ERROR: missing endtime'
# # change_swot.loc[~change_swot.change_endtime, 'new_endtime'] = change_swot.end_time
# change_swot['new_endmin'] = change_swot.new_endtime.dt.round('min')
# change_swot['tune_ind'] = change_swot.tune_ind.astype('int')
# change_swot = change_swot.set_index('tune_ind')
# # -------------------------------------------------------------------------------------------------------------------- #
# ### Fill in streaming endtime in original table
# tune['streaming_endtime'] = tune.end_time.copy()
# tune.loc[change_swot.index, 'streaming_endtime'] = change_swot.new_endmin
# tune.loc[tune.streaming_endtime > tune.next_tune_start, 'streaming_endtime'] = tune.loc[tune.streaming_endtime > tune.next_tune_start, 'next_stream_start']
# assert tune.streaming_endtime.isnull().sum() == 0, 'ERROR: missing endtime'
#
# tune_checkpoint2 = tune.copy()    # for debugging/rapid analysis
# tune_checkpoint2.to_csv(dr + 'tune_final.csv')
#
########################################################################################################################
