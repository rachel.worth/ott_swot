### Compare data pulled using Eric's code (SWOT.py) vs mine (swot_data_pull.py)
### Make sure the dates are the same in each pull code, and gapfix=False
import pandas     as pd
import datetime as dt
import numpy as np
import matplotlib.pyplot as plt
import matplotlib
pd.set_option('display.width', 170)

### This is a pycharm command, has to be copy/pasted in maybe?
execfile('SWOT.py')
df1 = swot_master_table.copy()

### This is a pycharm command, has to be copy/pasted in maybe?
execfile('swot_data_pull.py')
df2 = tune.copy()

### Join both on device and start time
both = df1[['fsite','start_time','end_time','device','provider','duration','swotdur','swotstreamdur']].merge(
       df2[['fsite','start_time','end_time','device','provider','duration','swotdur','swotstreamdur']],
      on=['fsite','start_time'], how='outer').sort_values(['fsite','start_time','end_time_x'])
both['no1'] = both.end_time_x.isnull()
both['no2'] = both.end_time_y.isnull()
both['diff'] = ~both.no1 & ~both.no2 & (both.end_time_x != both.end_time_y)
both['swotfrac_x'] = both.swotdur_x / both.swotstreamdur_x
both['swotfrac_y'] = both.swotdur_y / both.swotstreamdur_y

### Cut back to only sessions entirely contained within the range
cropped = both.loc[both.start_time.between(*inner_trange) & both.end_time_x.between(*inner_trange)]

print(df1.shape, df2.shape)
print(cropped.isnull().mean())
print(cropped[['duration_x','duration_y','swotdur_x','swotdur_y',
            'swotstreamdur_x','swotstreamdur_y','swotfrac_x','swotfrac_y']].describe())
print((cropped.duration_x      == cropped.duration_y).mean(),
      (cropped.swotdur_x       == cropped.swotdur_y).mean(),
      (cropped.swotstreamdur_x == cropped.swotstreamdur_y).mean())


dvc_stats = both.groupby('fsite').agg({'start_time':'count',
                                       'no1':'mean',
                                       'no2':'mean',
                                       'diff':'mean'})

########################################################################################################################
########################################################################################################################
########################################################################################################################
### Old data missing:
f1 = dvc_stats.loc[dvc_stats.no1>0.1].sort_values('start_time', ascending=False).index[0]
trange = (dt.datetime(2017, 9, 30,  2,  4), dt.datetime(2017, 9, 30,  4,  49))
both.loc[ (both.fsite == f1)
        & (both.start_time.between(*trange)) ]

########################################################################################################################
### New data missing:
### Most is where df1 trailed over onto the next day
### Not this one:
f1 = '19042515-1' # both.loc[(both.start_time < dt.datetime(2017, 9, 29, 23, 59)) & both.no2]
trange = (dt.datetime(2017, 9, 29,  5,  0), dt.datetime(2017, 9, 29,  9,  16))

########################################################################################################################
### Different swotdurs, same start and end times -- resolved (probably by the next thing)
f1 = '17757599-1'
trange = (dt.datetime(2017, 9, 29, 18, 30), dt.datetime(2017, 9, 29, 19,  42))

########################################################################################################################
### New vers has negative swotdur??? -- resolved
### (my version was looking at all following streams, his only the ones between tunes)
f1 = '19776209-1'
trange = (dt.datetime(2017, 9, 29, 19, 27), dt.datetime(2017, 9, 29, 19, 48))

########################################################################################################################
### my swotdur is way higher (>10,000 vs. 10s in old vers)
### RESOLVED: above change meant new_dvc flag needed to be moved to after the drop in non-trailing rows
f1 = '20037881-1'
trange = (dt.datetime(2017, 9, 30, 16, 8), dt.datetime(2017, 9, 30, 17, 14))

########################################################################################################################
### original max durations are much higher than mine -- because it's at the end of the selection, mine cropped it off
f1 = '21107571-2'
trange = (dt.datetime(2017, 9, 29,  6, 44), dt.datetime(2017, 10, 1, 2,))

########################################################################################################################
### my max swotdur is higher -- RESOLVED only one of eric's DFs was converted to single roku
f1 = '21542532-1'
trange = (dt.datetime(2017, 9, 29, 16, 55), dt.datetime(2017, 9, 29, 18, 1))
trange = (dt.datetime(2017, 9, 29, 16, 55), dt.datetime(2017, 9, 29, 22, 40))

########################################################################################################################
### no df2 entry
f1 = '19042515-1'
trange = (dt.datetime(2017, 9, 29, 5, 0), dt.datetime(2017, 9, 29, 9, 31))

########################################################################################################################
### 21157480-1 at 9/30 1:51 missing in old vers because it's a tree_id eric didn't use, but it matches to amazon
### Is it reasonable to just go with it???
### Should it be broken out separately since the preceding/following events are a different amazon tree_id?
### What does it mean?
########################################################################################################################
both.loc[ (both.fsite == f1)
        & (both.start_time.between(*trange))]

tune1.loc[(tune1.fsite == f1)
        & (tune1.start_time.between(*trange))]

tune2.loc[(tune2.fsite == f1)
        & (tune2.start_time.between(*trange))]
### new vers lost the device name for 18922986-1 at 9/29 21:10 between these
tune3.loc[(tune3.fsite == f1)
        & (tune3.start_time.between(*trange))]

tune4.loc[(tune4.fsite == f1)
        & (tune4.start_time.between(*trange))]

tune.loc[(tune.fsite == f1)
       & (tune.start_time.between(*trange))]

strm.loc[(strm.fsite == f1)
       & (strm.starttimeest.between(trange[0], trange[1]+dt.timedelta(hours=1)))]

timeline.loc[(timeline.fsite == f1)
           & (timeline.both_times.between(trange[0], trange[1]+dt.timedelta(hours=1)))]

btwn_tunes.loc[(btwn_tunes.fsite == f1)
              & (btwn_tunes.both_times.between(trange[0], trange[1]+dt.timedelta(hours=1)))]
########################################################################################################################
credit_results.loc[ (credit_results.fsite == f1)
                  & (credit_results.start_time.between(*trange))]
### streaming at 19:14 lost between these
### credit_results.groupby(['fsite','start_time','end_time']).first().reset_index()
### Need to make sure that streaming event comes before nonstreaming in this groupby
credit_results_minwin.loc[ (credit_results_minwin.fsite == f1)
                         & (credit_results_minwin.start_time.between(*trange))]
### This aggregation takes first tree_id and last dist_sourceid
### Results in some things being mute vs other, is this valid??
### I'd already assigned activity by this point, so it goes with first
### 20372558-1, 9/30 2:04
credit_ranges.loc[ (credit_ranges.fsite == f1)
                 & (credit_ranges.start_time.between(*trange))]

swot1 = credit_ranges.groupby('groupID').agg(agg_dict)
swot1.loc[(swot1.fsite == f1)
        & (swot1.start_time.between(*trange))].merge(provider_names,'left')

swot_master_table.loc[ (swot_master_table.fsite == f1)
                     & (swot_master_table.start_time.between(*trange))]
########################################################################################################################
########################################################################################################################
########################################################################################################################
