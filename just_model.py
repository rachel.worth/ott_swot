### Manual model
import numpy as np
import pandas as pd
pd.set_option('display.width', 180)
import matplotlib.pyplot as plt
import seaborn as sb
import datetime as dt
import random as rd
import sys
import os
from swot_model_fns import *

### Import data
data_dir  = ''
data_tag  = '1701-1803_new_gapfix'
plot_path = 'ModelPlots/{}/'.format(data_tag)
if not os.path.exists(plot_path):
    os.makedirs(plot_path)

########################################################################################################################
# df = pd.read_csv('{0}swot_master_table_{1}.csv'.format(data_dir, data_tag), index_col=0)

### Read in 1701-1709 and 1706-1803, combine and drop duplicates
df1 = pd.read_csv('{0}swot_master_table_1701-1709_new_gapfix.csv'.format(data_dir, data_tag), index_col=0)
df2 = pd.read_csv('{0}swot_master_table_1706-1803_new_gapfix.csv'.format(data_dir, data_tag), index_col=0)

for c in ['start_time','end_time','next_tune_start','streaming_endtime']:
    df1[c] = pd.to_datetime(df1[c])
    df2[c] = pd.to_datetime(df2[c])

### Four months overlap: cut off the last month of the earlier one, and the first 2 months of the later one
### So that the ones near the boundaries are gone and remainder should be treated identically
df1 = df1.loc[df1.start_time <  dt.datetime(2017, 9, 1, 0, 0)].copy()
df2 = df2.loc[df2.start_time >= dt.datetime(2017, 8, 1, 0, 0)].copy()

### Merge to avoid duplication (groupID and next_tune_start are expected to differ)
df = df1.merge(df2, how='outer',
               on=[c for c in df1.columns if c not in ['groupID','next_tune_start']])

assert df.loc[df.groupID_x.isnull() | df.groupID_y.isnull()]\
                    .groupby(['fsite','provider','device','start_time','end_time'])\
                    .hh_id.count().sort_values(ascending=False).iloc[0] < 2, 'ERROR: duplicate sessions'

########################################################################################################################
### Convert durations to minutes, rounded, so get rid of precision difference between data sources
df[['duration','swotdur','swotstreamdur']] = (df[['duration','swotdur','swotstreamdur']]/60).round(0)

### Add some useful columns
df['vw_frac']    = 1 - df.swotdur / df.swotstreamdur
df['swot_frac']  = df.swotdur / df.swotstreamdur
df['swot_class'] = df.swot_frac > 0  # >= 0.05
df['hh']         = df.fsite.str.split('-', 1, expand=True).iloc[:, 0]
# df['month'] = pd.to_datetime(df.start_time).map(lambda t: t.month)
df['start_time'] = pd.to_datetime(df.start_time)
df['end_time']   = pd.to_datetime(df.end_time)

### Roll up roku devices into one group
df.loc[df.device.str.contains('Roku'), 'device'] = 'Roku'

### Combine census providers into one
### Reclassify census providers as one
df['orig_prv'] = df.provider.copy()
df.loc[df.provider.isin(['cw','crackle','directv','dtv','discovery']), 'provider'] = 'census'

### Drop youtube due to its much lower swot fraction
df = df.loc[df.provider != 'youtube']

########################################################################
### Identify days and hours in the data
df['date']  = pd.to_datetime(df.start_time - dt.timedelta(hours=3)).dt.strftime('%y-%m-%d')
df['hour']  = pd.to_datetime(df.start_time).dt.hour
df['date-hour']  = df.start_time.dt.floor('h')
df.groupby(['date','hour']).hh_id.count()

########################################################################
### Map dates to months
month_map = get_month_map(df)

### Apply month mapping and date skipping to dataset (cuts out 0.1% of the data)
if 'month' in df.columns:
    df = df.drop('month', axis=1)
df = df.merge(month_map, on='date', how='left')
df = df.loc[df.month.notnull()]

########################################################################
######################## Run model #####################################
########################################################################
### Set parameters for the model
n_train_months = 4
test_month = month_map.month.max()-1 # integer between 1 and month_map.month.max()
model_vers = 'drtn-dvc'
threshold=25
smoothing=5000
flatten_multiplier=1
do_flatten = True
tag = '_{}_m{}_n{}_t{}_s{}_x{}'.format(model_vers, test_month, n_train_months, threshold, smoothing, flatten_multiplier)

### Get train, test sets
modeldf, predictdf = get_dfs(df, test_month=test_month, n_train_months=n_train_months)

### Get a DF that defines the binning to use (matches each device-provider pair with a bin name)
bin_key = get_bin_key(modeldf, binmodel=model_vers.split('-')[1], threshold=threshold)

### DF of drtn vs. bin name. Each cell contains the SWOT fraction for that combination
### This is the table that gets applied to the census data, using bin_key to join the device to the bin name
pred_lookup = fit_model(modeldf, bin_key, smoothing=smoothing,
                        do_flatten=do_flatten, flatten_multiplier=flatten_multiplier)
### If there are higher durations in the test set than the training set had,
### extend the prediction DF and forward fill the last value from each column to the end
### (This assumes that things have pretty well flattened out at high durations, which is usually the case
### but wouldn't hurt to try and add a check that verifies this is the case first?)
if modeldf.swotstreamdur.max() < predictdf.swotstreamdur.max():
    pred_lookup = pred_lookup.reindex(
        range(int(pred_lookup.index.min()), int(predictdf.swotstreamdur.max()) + 1)).fillna(method='ffill')

### Export table, key
bin_key.to_csv('bins{}.csv'.format(tag))
pred_lookup.to_csv('swotfrac{}.csv'.format(tag))

### Generate the predictions on the test set
preds = predict_from_lookup_and_key(predictdf, pred_lookup, bin_key)

### Get some basic stats on the accuracy of the prediction, overall and by provider/device
### true_drtn: sum of true minutes tuned
### strm_drtn: sum of streamed minutes
### pred_drtn: sum of predicted minutes tuned -- want as close as possible to true_drtn
### delta: how many minutes too high/low was the prediction (pred_drtn - true_drtn) -- want close to 0
### reldiff: how large is that total error relative to the correct total (delta/true_drtn) --  want close to 0
stats_prv = calc_score(preds, truecol = 'duration', predcol='pred_drtn', strmcol='swotstreamdur', groupcol='provider')
stats_dvc = calc_score(preds, truecol = 'duration', predcol='pred_drtn', strmcol='swotstreamdur', groupcol='device')

########################################################################
### Make plots?

### number of sessions vs. duration
max_drtn = 60
ssns_v_drtn = df.groupby('swotstreamdur').hh_id.count()
f1, ax1 = plt.subplots(1, figsize=[6, 4])
ax1.plot(ssns_v_drtn.index, ssns_v_drtn)
ax1.set_xlim(0, max_drtn)
ax1.set_xticks(np.arange(0, max_drtn+1, int(max_drtn/10)));
ax1.set_xlabel('Stream duration (min)')
ax1.set_ylabel('# sessions')
ax1.set_title('Session duration distribution')
f1.savefig('{}ssns_per_drtn.png'.format(plot_path), dpi=300)
plt.close(f1)

### Roku duration curve for presentation
max_drtn = 60
f1, ax1 = plt.subplots(1, figsize=[6, 4])
ax1.plot(pred_lookup.index, pred_lookup['Roku'])
ax1.set_xlim(0, max_drtn)
ax1.set_xticks(np.arange(0, max_drtn+1, int(max_drtn/10)));
ax1.set_xlabel('Stream duration (min)')
ax1.set_ylabel('SWOT fraction')
ax1.set_title('Roku SWOT prediction')
f1.savefig('{}roku_drtn_curve.png'.format(plot_path), dpi=300)
plt.close(f1)

slope = pd.DataFrame({'slope_1min': pred_lookup['Roku']/pred_lookup['Roku'].shift(1),
                      'slope_2min': pred_lookup['Roku']/pred_lookup['Roku'].shift(2)})
print(slope.head(10))

