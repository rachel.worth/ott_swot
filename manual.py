### Manual model
import numpy as np
import pandas as pd
pd.set_option('display.width', 160)
import matplotlib.pyplot as plt
import seaborn as sb
import random
from manual_fns import *


### Import data
vers = 'v2_4'
dur  = '8mo'
df = pd.read_csv('{0}/swot_master_table_{1}.csv'.format(vers, dur), index_col=0)

### Convert durations to minutes, rounded, so get rid of precision difference between data sources
df[['duration','swotdur','swotstreamdur']] = (df[['duration','swotdur','swotstreamdur']]/60).round(0)

### Add some useful columns
df['vw_frac']    = 1 - df.swotdur / df.swotstreamdur
df['swot_frac']  = df.swotdur / df.swotstreamdur
df['swot_class'] = df.swot_frac > 0  # >= 0.05
df['hh']         = df.fsite.str.split('-', 1, expand=True).iloc[:, 0]
df['month'] = pd.to_datetime(df.start_time).map(lambda t: t.month)
### v2.4 stopped outputing make and model, so this is for backwards compatibility
if vers == 'v2_4':
    df['mk_mdl'] = df.device
    df[['make','model']] = df.device.str.split(' ', 1, expand=True)
else:
    df['mk_mdl'] = df.make.str.cat([df.model], sep='-')

########################################################################
########################################################################
########################################################################
### Count devices per model/provider cell
n_dvcs  = df.pivot_table(index='mk_mdl', columns='provider', values='fsite', aggfunc=pd.Series.nunique).fillna(0)
### Mean swot fraction per cell
swot_mn = df.pivot_table(index='mk_mdl', columns='provider', values='swot_frac', aggfunc='mean')

### Show just those from larger bins
# swot_mn.mask(n_dvcs < 30, other='')

########################################################################
###################### Grid Search #####################################
########################################################################
versions  = ['v0', 'v1', 'v4', 'drtn-no-v4', 'drtn-prv', 'drtn-dvc', 'drtn-v3', 'drtn-v4', 'drtn-mnl-v4', 'eqn']
n_months  = [1, 2, 3, 4]

dvc_list = list(df.device.value_counts().index)
prv_list = list(df.provider.value_counts().index)

binsize_list   = [10, 25, 50, 100, 150, 200, 100000]
smoothing_list = [100, 250, 500, 1000, 1500, 2000, 3000, 4000, 5000]

### Calculate model scores over full parameter grid (takes a long time)
# grid_scores = pd.DataFrame([], columns = ['version','n_months','month','binsize','smoothing','all']+prv_list+dvc_list)
# grid_scores = grid_scores.set_index(['version','n_months','month','binsize','smoothing'])
# for v in versions:
#     if any([vv in v for vv in ['v2','v3','v4','v5','v6','v7','v8','v9']]):
#         binsizes = binsize_list
#     else:
#         binsizes = [-1]
#     if 'drtn' in v:
#         smoothing = smoothing_list
#     else:
#         smoothing = [-1]
#     for n in n_months:
#         month_i = [2, 3, 4, 5, 6, 7, 8][(n - 1):]
#         # month_i = [8]
#         for m in month_i:
#             for b in binsizes:
#                 for s in smoothing:
#                     ind = (v, n, m, b, s)
#                     if (ind not in grid_scores.index):
#                         print('done:', ind)
#                         modelled = model_swot(*get_dfs(df, m, n),
#                                               model_vers=v, threshold=b, smoothing=s)
#                         grid_scores.loc[ind, ['all'] + prv_list] = calc_score(modelled, groupcol='provider')['reldiff']
#                         grid_scores.loc[ind,           dvc_list] = calc_score(modelled, groupcol='device')['reldiff']
#                     else:
#                         print('skip:', ind)
#
### grid_scores = grid_scores.sort_index()
### grid_scores.to_csv('ModelPlots/grid_scores.csv')

### Read in previous results
grid_scores = pd.read_csv('ModelPlots/grid_scores.csv', index_col=[0, 1, 2, 3, 4])

# -------------------------------------------------------------------------------- #
### RMS over months, 'all', score only
grid_scores_rms = grid_scores.groupby(['version','n_months','binsize','smoothing']).agg({'all': rms}).reset_index()

cmap = 'YlGnBu' #sb.diverging_palette(220, 10, as_cmap=True)
cmap_vmax, cmap_vmin = grid_scores_rms['all'].max(), grid_scores_rms['all'].min()
for v in versions:
    for n in n_months:
        f1, ax1 = plt.subplots(1)
        pane = grid_scores_rms.loc[(grid_scores_rms.version == v) & (grid_scores_rms.n_months == n)] \
            .pivot_table(index='binsize', columns='smoothing', values='all')
        sb.heatmap(pane, cmap=cmap, vmin=cmap_vmin, vmax=cmap_vmax, square=True, linewidths=0.2, ax=ax1)
        ax1.set_xticklabels([int(x) for x in pane.columns])
        ax1.set_yticklabels([int(y) for y in pane.index[::-1]], rotation=0)
        ### mark square(s) with the best value for this model
        best_val = pane.min().min()
        best_coords = np.array(pane[pane == best_val].stack().index.labels)
        if pane.shape[1] == 1:
            for i in best_coords[0]:
                ax1.scatter(0.5, -i + pane.shape[0] - 0.5, marker='x', c='black')
        elif pane.shape[0] == 1:
            for i in best_coords[1]:
                ax1.scatter(i + 0.5, 0.5, marker='x', c='black')
        else:
            for i in range(best_coords.shape[1]):
                ax1.scatter( best_coords[1, i] + 0.5,
                            -best_coords[0, i] + pane.shape[0]-0.5, marker='x', c='black')
        ax1.set_title('{0}, {1} months: min = {2:0.5f}'.format(v, n, best_val))
        f1.savefig('ModelPlots/GridScorePlots/grid_scores_{0}_m{1}.png'.format(v, n))
        plt.close(f1)

# -------------------------------------------------------------------------------- #
### RMS over months, 'all', score only
grid_scores_best = grid_scores_rms.pivot_table(index='n_months',columns='version', values='all', aggfunc='min')
grid_scores_best = grid_scores_best[versions]

f1, ax1 = plt.subplots(1)
sb.heatmap(grid_scores_best, cmap=cmap, square=True, linewidths=0.2, ax=ax1)
ax1.set_xticklabels([x for x in grid_scores_best.columns], rotation=30)
ax1.set_yticklabels([y for y in grid_scores_best.index[::-1]], rotation=0)
best_val = grid_scores_best.min().min()
best_coords = np.array(grid_scores_best[grid_scores_best == best_val].stack().index.labels)
for i in range(best_coords.shape[1]):
    ax1.scatter( best_coords[1, i] + 0.5,
                -best_coords[0, i] + grid_scores_best.shape[0] - 0.5, marker='x', c='black')
ax1.set_title('Best score from each model vs. # months')
f1.savefig('ModelPlots/GridScorePlots/grid_scores_best.png')
plt.close(f1)

all_months_i = [2, 3, 4, 5, 6, 7, 8]
all_months_n = ['Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug']

########################################################################
################### Plot drtn curve shapes #############################
########################################################################
binmodel  = 'v4'
n_months  = 3
binsize   = 150
smoothing = 3000
month_i   = all_months_i[(n_months-1):]
month_lab = all_months_n[(n_months-1):]

manual_bins = {
    'netflix': {
        1: ['Smart TV','Computer'],
        2: ['Bluray'],
        3: ['Wii U', 'Roku 2','Play Station 4','Roku 1','Wii','Google TV','Google Chromecast'],
        4: ['X Box One','Play Station 3','Amazon Fire Stick','Amazon Fire TV','X Box 360'],
        5: ['Apple TV','Other','Roku 3','Roku Streaming Stick']
    }
}
modeldf, predictdf = get_dfs(df, 7, n_months)
if 'mnl' in binmodel:
    bin_key = get_bin_key(modeldf, binmodel=binmodel[-2:], threshold=binsize, manual_bins=manual_bins)
else:
    bin_key = get_bin_key(modeldf, binmodel=binmodel,      threshold=binsize)

lookups = {}
for j in month_i:
    modeldf = get_dfs(df, j, n_months=n_months)[0]
    lookups[j] = drtn_model(modeldf, bin_key, smoothing=smoothing)
    # lookups[j] = eqn_model(modeldf, bin_key)

for p in ['netflix','youtube','hulu','hbo','amazon','crackle']:
    f1, ax1 = plt.subplots(5,4, figsize=[25, 14], sharex=True, sharey=True)
    ax2 = ax1.flatten()
    for i, c in enumerate(bin_key.loc[bin_key.provider == p, 'bin_name'].sort_values().unique()):
        for j in month_i:
            modeldf = get_dfs(df, j, n_months=n_months)[0]
            ax2[i].plot(lookups[j].index, lookups[j][c],
                        label='{0}: {1} events'.format(month_lab[j-n_months-1],
                                                       modeldf.loc[
                                           (modeldf.provider.isin(bin_key.loc[bin_key.bin_name==c].provider)) &
                                           (modeldf.device.isin(bin_key.loc[bin_key.bin_name==c].device))].shape[0]))
        ax2[i].legend(loc='upper right', frameon=True, framealpha=0.6)
        ax2[i].set_title(c)
        ax2[i].set_xscale('log')
    f1.savefig('ModelPlots/DurationCurves/drtn_bymonth_{0}_b{1}_s{2}_m{3}_{4}.png'.format(binmodel, binsize, smoothing, n_months, p))
    plt.close(f1)

########################################################################
################## Show where error is #################################
########################################################################
### Parameters to use for all versions
n_months = 3
test_binsize   = 150
test_smoothing = 3000

### Hardcoded list of bin edges
bin_drtns = list(np.arange(0, 15, 1)) + list(np.arange(15, 50, 5)) + list(np.arange(50, 100, 10)) + [100, 150, 200, 300, 400, 500, 1000, df.swotstreamdur.max()+1]
### Using this as the colormap maximum, for consistency (rerun all if adjusting)
vmax=25000

versions = ['v0','v1','v3','v4','drtn-v3','drtn-v4','drtn-mnl-v4','drtn-no-v4', 'eqn']
months   = all_months_i[(n_months-1):]
err_distrns_by_month = {}
for vers in versions:
    for month in months:
        modeldf, predictdf = get_dfs(df, month, n_months)
        preds = model_swot(modeldf, predictdf, model_vers = vers, threshold=test_binsize, smoothing=test_smoothing)
        preds['err'] = preds.pred_drtn - preds.duration
        preds['drtn_bin'] = pd.cut(preds.swotstreamdur, bins=bin_drtns)
        ### Get error by provider
        prv_err_mins = preds.pivot_table(index='provider', columns='drtn_bin', values='err', aggfunc='sum')
        prv_err_mins = prv_err_mins.loc[prv_list]
        ### Get error by device
        dvc_err_mins = preds.pivot_table(index='device', columns='drtn_bin', values='err', aggfunc='sum')
        dvc_err_mins = dvc_err_mins.loc[dvc_list]
        ### Combine into one df
        err_mins = pd.concat([prv_err_mins, dvc_err_mins])
        err_mins = err_mins.iloc[::-1]
        err_mins.columns = [i.right.astype('int') for i in err_mins.columns]
        err_mins['total']     = err_mins.sum(axis=1)
        err_mins.loc['total'] = err_mins.sum(axis=0)
        err_mins.loc['total','total'] = err_mins.loc[prv_list,'total'].sum()
        err_distrns_by_month[(vers, month)] = err_mins
        print('{0}: maximum error = {1}, color max = {2}'.format(vers, err_distrns_by_month[(vers, month)].abs().max().max(), vmax))

err_distrns = {}
for vers in versions:
    monthlies = [err_distrns_by_month[(vers, m)] for m in months]
    err_distrns[vers] = monthlies[0].fillna(0)
    for monthly in monthlies[1:]:
        err_distrns[vers] += monthly.fillna(0)
    for c in err_distrns[vers].columns:
        err_distrns[vers].loc[err_distrns[vers][c] == 0, c] = np.nan
    print(
        '{0}: maximum error = {1}, color max = {2}'.format(vers, err_distrns[vers].abs().max().max(), vmax))

total_drtn = df.loc[df.month.between(min(months), max(months)), 'duration'].sum()


# ---------------------------------------------------------------------- #
for vers in versions:
    f1, ax1 = plt.subplots(1, figsize=[20, 12])
    sb.heatmap(err_distrns[vers], cmap='bwr', center=0, vmax=vmax, vmin=-vmax, square=True, linewidths=0.1, ax=ax1)
    ax1.axhline(1, c='black')
    ax1.axhline(len(prv_list)+1, c='black')
    ax1.axvline(err_distrns[vers].shape[1]-1, c='black')
    ax1.set_xticklabels(err_distrns[vers].columns, rotation=30)
    ax1.set_yticklabels([y for y in err_distrns[vers].index[::-1]], rotation=0)
    ax1.set_xlabel('Duration (right edge of bin)', fontsize=14)
    ax1.set_ylabel('Category (provider or device)', fontsize=14)
    ax1.set_title('Absolute error distribution; total = ' +
                  '{0:0.1f} mins off ({1:0.2f}%)\n'.format(err_distrns[vers].loc['total','total'],
                                                           err_distrns[vers].loc['total', 'total']/total_drtn*100.) +
                  ' ({0}, b{1}, s{2}, m{3})'.format(vers, test_binsize, test_smoothing, n_months), fontsize=14)
    plt.tight_layout()
    f1.savefig('ModelPlots/ErrorDistrns/error_distributions_{0}_b{1}_s{2}_m{3}.png'.format(vers, test_binsize, test_smoothing, n_months))
    plt.close(f1)
# ---------------------------------------------------------------------- #
### Plot each month
vers='drtn-v4'
for month in months:
    f1, ax1 = plt.subplots(1, figsize=[20, 12])
    sb.heatmap(err_distrns_by_month[(vers, month)], cmap='bwr', center=0, vmax=vmax/4., vmin=-vmax/4., square=True, linewidths=0.1, ax=ax1)
    ax1.axhline(1, c='black')
    ax1.axhline(len(prv_list)+1, c='black')
    ax1.axvline(err_distrns_by_month[(vers, month)].shape[1]-1, c='black')
    ax1.set_xticklabels(err_distrns_by_month[(vers, month)].columns, rotation=30)
    ax1.set_yticklabels([y for y in err_distrns_by_month[(vers, month)].index[::-1]], rotation=0)
    ax1.set_xlabel('Duration (right edge of bin)', fontsize=14)
    ax1.set_ylabel('Category (provider or device)', fontsize=14)
    ax1.set_title('Absolute error distribution; total = ' +
                  '{0:0.1f} mins off ({1:0.2f}%)\n'.format(err_distrns_by_month[(vers, month)].loc['total', 'total'],
                                                           err_distrns_by_month[(vers, month)].loc['total', 'total']/total_drtn*100./4.) +
                  ' ({0}, b{1}, s{2}, m{3}-{4})'.format(vers, test_binsize, test_smoothing, n_months, month), fontsize=14)
    plt.tight_layout()
    f1.savefig('ModelPlots/ErrorDistrns/error_distributions_{0}_b{1}_s{2}_m{3}-{4}.png'.format(vers, test_binsize, test_smoothing, n_months, month))
    plt.close(f1)
# ---------------------------------------------------------------------- #
#### Compare errors from two models
# v1, v2 = 'v4', 'drtn-v4'
# err_dist_compare = err_distrns[v2] - err_distrns[v1]
#
# f1, ax1 = plt.subplots(1, figsize=[20, 12])
# sb.heatmap(err_dist_compare, cmap='bwr', center=0, square=True, linewidths=0.1, ax=ax1)
# ax1.axhline(len(prv_list), c='black')
# ax1.set_xticklabels([i.right.astype('int') for i in err_dist_compare.columns], rotation=30)
# ax1.set_yticklabels([y for y in err_dist_compare.index[::-1]], rotation=0)
# ax1.set_xlabel('Duration (right edge of bin)', fontsize=14)
# ax1.set_ylabel('Category (provider or device)', fontsize=14)
# ax1.set_title(
#     'Comparison of error distribution: {0} relative to {1}'.format(v2, v1) + \
#     ' (b{0}, s{1}, m{2}-{3})'.format(test_binsize, test_smoothing, n_months, month), fontsize=14)
# plt.tight_layout()
# f1.savefig(
#     'ModelPlots/ErrorDistrns/error_dist_comp_{0}_{1}_b{2}_s{3}_m{4}-{5}.png'.format(v1, v2, test_binsize, test_smoothing, n_months,
#                                                                        month))
# plt.close(f1)

########################################################################
################### Pred vs true swot curves ###########################
########################################################################
v = 'eqn'
n_months = 3
months   = all_months_i[(n_months-1):]

for m in months:
    modeldf, predictdf = get_dfs(df, m, n_months)
    preds = model_swot(modeldf, predictdf, model_vers=v)
    preds['err'] = preds.pred_drtn - preds.duration

    preds['strm_drtn'] = preds.swotstreamdur.copy()
    strm_tot = preds.pivot_table(index='swotstreamdur', columns='provider', values='strm_drtn', aggfunc='sum')
    tune_tot = preds.pivot_table(index='swotstreamdur', columns='provider', values='duration',  aggfunc='sum')
    pred_tot = preds.pivot_table(index='swotstreamdur', columns='provider', values='pred_drtn', aggfunc='sum')

    true_mn = 1. - tune_tot / strm_tot
    pred_mn = 1. - pred_tot / strm_tot

    f1, ax1 = plt.subplots(2, 3, figsize=[20, 12])
    ax2 = ax1.flatten()
    for i, p in enumerate(true_mn.columns):
        ax2[i].plot(true_mn.index, true_mn[p], 'bo',   alpha=0.5, label='_nolabel_')
        ax2[i].plot(true_mn.index, true_mn[p], 'blue', alpha=0.5, label='True')
        ax2[i].plot(pred_mn.index, pred_mn[p], 'ro',   alpha=0.5, label='_nolabel_')
        ax2[i].plot(pred_mn.index, pred_mn[p], 'red',  alpha=0.5, label='Predicted')
        ax2[i].set_title(p)
        ax2[i].set_xscale('log')
        ax2[i].set_xlim(1, df.swotstreamdur.max())
        ax2[i].set_ylim(0, 1)
    ax2[0].legend(loc='upper left')
    f1.savefig('ModelPlots/true_vs_pred_by_drtn_{0}_m{1}-{2}.png'.format(v, n_months, m))
    plt.close(f1)

########################################################################
################## Score plots by provider/device ######################
########################################################################
# versions  = ['v0', 'v1', 'v4', 'drtn-no', 'drtn-prv', 'drtn-dvc', 'drtn-v3', 'drtn-v4','drtn-mnl-v4']
versions = ['v0', 'v1', 'v4', 'drtn-prv', 'drtn-dvc', 'drtn-v4','drtn-mnl-v4', 'eqn']
n_months = 3
test_smoothing = 3000

agg_dict_prv = {}
for p in ['all'] + prv_list:
    agg_dict_prv[p] = rms
agg_dict_dvc = {}
for p in dvc_list:
    agg_dict_dvc[p] = rms

prv_v_bin = {}
dvc_v_bin = {}

all_mn_slice = slice(min(all_months_i), max(all_months_i))
grid_scores = grid_scores.sort_index()
for v in versions:
    prv_v_bin[v] = pd.concat([grid_scores.loc[(v, n_months, all_mn_slice, slice(-1, max(binsize_list)), test_smoothing), ['all'] + prv_list],
                              grid_scores.loc[(v, n_months, all_mn_slice, slice(-1, max(binsize_list)), -1            ), ['all'] + prv_list]])\
                                 .reset_index().groupby(['version','binsize']).agg(agg_dict_prv)
    prv_v_bin[v].index = prv_v_bin[v].index.droplevel(0)
    dvc_v_bin[v] = pd.concat([grid_scores.loc[(v, n_months, all_mn_slice, slice(-1, max(binsize_list)), test_smoothing), dvc_list],
                              grid_scores.loc[(v, n_months, all_mn_slice, slice(-1, max(binsize_list)), -1            ), dvc_list]])\
                                 .reset_index().groupby(['version','binsize']).agg(agg_dict_dvc)
    dvc_v_bin[v].index = dvc_v_bin[v].index.droplevel(0)

colors = sb.color_palette("Set1", len(versions), desat=0.7)
# ---------------------------------------------------------------------- #
f1, ax1 = plt.subplots(1)
p='all'
for j, v in enumerate(versions):
    if prv_v_bin[v].shape[0] > 1:
        x, y = prv_v_bin[v].index, 100. * prv_v_bin[v][p]
    else:
        x, y = binsize_list, [100. * prv_v_bin[v][p].iloc[0] for b in binsize_list]
    ax1.plot(x, y, c=colors[j],
             label='{0}: {1: 0.2f} at {2}'.format(v, 100. * prv_v_bin[v][p].min(),
                                                  prv_v_bin[v][p].idxmin()))
ax1.set_xscale('log')
ax1.set_xticks(binsize_list)
ax1.set_xticklabels(binsize_list, rotation=30)
ax1.set_xlim(.95*binsize_list[0], binsize_list[-2]*1.1)
ax1.legend(loc='upper right', frameon=True, framealpha=0.7)
ax1.set_xlabel('Bin threshold (# dvcs)')
ax1.set_ylabel('RMS error')
ax1.set_title('Error vs. window size, by model version (smoothing window = {0})'.format(test_smoothing))
plt.savefig('ModelPlots/model_v_binsize_{0}_s{1}_m{2}.png'.format(p, test_smoothing, n_months))
plt.close(f1)

# ---------------------------------------------------------------------- #
f1, ax1 = plt.subplots(3, 5, sharex=True, sharey=True, figsize = [21,10])
f1.subplots_adjust(hspace=0.1, wspace=0.05, right=.99, bottom=.07, top=.94, left=0.025)
ax = ax1.flatten()
for i, p in enumerate(['all'] + prv_list):
    ax[i].axhline(0, c='black', alpha=0.25)
    if p == 'all':
        ax[i].set_title('{0} ({1})'.format(p, int(n_dvcs.sum().sum())))
    else:
        ax[i].set_title('{0} ({1})'.format(p, int(n_dvcs[p].sum())))
    for j, v in enumerate(versions):
        if prv_v_bin[v].shape[0] > 1:
            x, y = prv_v_bin[v].index, 100. * prv_v_bin[v][p]
        else:
            x, y = binsize_list, [100. * prv_v_bin[v][p].iloc[0] for b in binsize_list]
        ax[i].plot(x, y, c=colors[j],
                   label='{0}: {1: 0.2f} at {2}'.format(v, 100. * prv_v_bin[v][p].min(),
                                                        prv_v_bin[v][p].idxmin()))
    ax[i].legend(loc='upper right', ncol=1, frameon=True, framealpha=0.7)
oldi = i
for j, d in enumerate(pd.Index(dvc_list)[[0, 1, 2, 3, 5, 10, 15, 18]]):
    i = j+oldi+1
    ax[i].axhline(0, c='black', alpha=0.25)
    ax[i].set_title('{0} ({1})'.format(d, int(n_dvcs.loc[d].sum())))
    for k, v in enumerate(versions):
        if dvc_v_bin[v].shape[0] > 1:
            x, y = dvc_v_bin[v].index, 100. * dvc_v_bin[v][d]
        else:
            x, y = binsize_list, [100. * dvc_v_bin[v][d].iloc[0] for b in binsize_list]
        ax[i].plot(x, y, c=colors[k],
                   label='{0}: {1: 0.2f} at {2}'.format(v, 100. * dvc_v_bin[v][d].min(),
                                                        dvc_v_bin[v][d].idxmin()))
    ax[i].legend(loc='upper right', ncol=1, frameon=True, framealpha=0.7)
ax[0].set_xlim(0, 1.2 * binsize_list[-2])
ax[0].set_ylim(0, 8)
f1.text(0.01, 0.5, 'Error (%)', ha='center', va='center', rotation='vertical', fontsize=14)
f1.text(0.5, 0.015, 'Bin size threshold (# dvcs)', ha='center', va='center', fontsize=14)
f1.text(0.5, 0.98, 'RMS monthly error in total viewed minutes, using previous month to predict SWOT %',
        ha='center', va='center', fontsize=16)
plt.savefig('ModelPlots/model_v_binsize_s{0}_m{1}.png'.format(test_smoothing, n_months))
plt.close(f1)

########################################################################
versions = ['v0', 'v1', 'v4', 'drtn-prv', 'drtn-dvc', 'drtn-v4','drtn-mnl-v4', 'eqn']
n_months = 3
test_binsize = 200

agg_dict_prv = {}
for p in ['all'] + prv_list:
    agg_dict_prv[p] = rms
agg_dict_dvc = {}
for p in dvc_list:
    agg_dict_dvc[p] = rms

prv_v_smth = {}
dvc_v_smth = {}
for v in versions:
    prv_v_smth[v] = pd.concat([grid_scores.loc[(v, n_months, all_mn_slice, test_binsize, slice(-1, max(smoothing_list))), ['all'] + prv_list],
                               grid_scores.loc[(v, n_months, all_mn_slice, -1,           slice(-1, max(smoothing_list))), ['all'] + prv_list]])\
                      .reset_index().groupby(['version','smoothing']).agg(agg_dict_prv)
    prv_v_smth[v].index = prv_v_smth[v].index.droplevel(0)
    dvc_v_smth[v] = pd.concat([grid_scores.loc[(v, n_months, all_mn_slice, test_binsize, slice(-1, max(smoothing_list))), dvc_list],
                               grid_scores.loc[(v, n_months, all_mn_slice, -1,           slice(-1, max(smoothing_list))), dvc_list]])\
                      .reset_index().groupby(['version','smoothing']).agg(agg_dict_dvc)
    dvc_v_smth[v].index = dvc_v_smth[v].index.droplevel(0)

colors = sb.color_palette("Set1", len(versions), desat=0.7)
# ---------------------------------------------------------------------- #
f1, ax1 = plt.subplots(1)
p='all'
for j, v in enumerate(versions):
    if prv_v_smth[v].shape[0] > 1:
        x, y = prv_v_smth[v].index, 100. * prv_v_smth[v][p]
    else:
        x, y = smoothing_list, [100. * prv_v_smth[v][p].iloc[0] for b in smoothing_list]
    ax1.plot(x, y, c=colors[j],
             label='{0}: {1: 0.2f} at {2}'.format(v, 100. * prv_v_smth[v][p].min(),
                                                  prv_v_smth[v][p].idxmin()))
ax1.set_xscale('log')
ax1.set_xticks(smoothing_list)
ax1.set_xticklabels(smoothing_list, rotation=30)
ax1.legend(loc='upper right', frameon=True, framealpha=0.7)
ax1.set_xlabel('Smoothing window (# events)')
ax1.set_ylabel('RMS error')
ax1.set_title('Error vs. window size, by model version (bin threshold = {0})'.format(test_binsize))
plt.savefig('ModelPlots/model_v_smoothing_all_t{0}_m{1}.png'.format(test_binsize, n_months))
plt.close(f1)

# ---------------------------------------------------------------------- #
f1, ax1 = plt.subplots(3, 5, sharex=True, sharey=True, figsize = [21,10])
f1.subplots_adjust(hspace=0.1, wspace=0.05, right=.99, bottom=.07, top=.94, left=0.025)
ax = ax1.flatten()
for i, p in enumerate(['all'] + prv_list):
    ax[i].axhline(0, c='black', alpha=0.25)
    if p == 'all':
        ax[i].set_title('{0} ({1})'.format(p, int(n_dvcs.sum().sum())))
    else:
        ax[i].set_title('{0} ({1})'.format(p, int(n_dvcs[p].sum())))
    for j, v in enumerate(versions):
        if prv_v_smth[v].shape[0] > 1:
            x, y = prv_v_smth[v].index, 100. * prv_v_smth[v][p]
        else:
            x, y = smoothing_list, [100. * prv_v_smth[v][p].iloc[0] for b in smoothing_list]
        ax[i].plot(x, y, c=colors[j],
                   label='{0}: {1: 0.2f} at {2}'.format(v, 100. * prv_v_smth[v][p].min(),
                                                        prv_v_smth[v][p].idxmin()))
    ax[i].legend(loc='upper right', ncol=1, frameon=True, framealpha=0.7)
oldi = i
for j, d in enumerate(pd.Index(dvc_list)[[0, 1, 2, 3, 5, 10, 15, 18]]):
    i = j+oldi+1
    ax[i].axhline(0, c='black', alpha=0.25)
    ax[i].set_title('{0} ({1})'.format(d, int(n_dvcs.loc[d].sum())))
    for k, v in enumerate(versions):
        if dvc_v_smth[v].shape[0] > 1:
            x, y = dvc_v_smth[v].index, 100. * dvc_v_smth[v][d]
        else:
            x, y = smoothing_list, [100. * dvc_v_smth[v][d].iloc[0] for b in smoothing_list]
        ax[i].plot(x, y, c=colors[k],
                   label='{0}: {1: 0.2f} at {2}'.format(v, 100. * dvc_v_smth[v][d].min(),
                                                        dvc_v_smth[v][d].idxmin()))
    ax[i].set_xlim(0.95 * smoothing_list[0], 1.2 * smoothing_list[-2])
    ax[i].set_xscale('log')
    ax[i].legend(loc='upper right', ncol=1, frameon=True, framealpha=0.7)
    ax[i].set_xticks(smoothing_list)
    ax[i].set_xticklabels(smoothing_list, rotation=30)
ax[0].set_ylim(0, 5)
f1.text(0.01, 0.5, 'Error (%)', ha='center', va='center', rotation='vertical', fontsize=14)
f1.text(0.5, 0.015, 'Smoothing window (# events)', ha='center', va='center', fontsize=14)
f1.text(0.5, 0.98, 'RMS monthly error in total viewed minutes, using previous month to predict SWOT %',
        ha='center', va='center', fontsize=16)
plt.savefig('ModelPlots/model_v_smoothing_s{0}_m{1}.png'.format(test_binsize, n_months))
plt.close(f1)

########################################################################
########################################################################
### how many minutes are being removed, vs. drtn
v = 'v0'
n = 3
m = 8
b = 150
s = 3000
# p = 'netflix'
# d = 'Smart TV'

predictdf, modeldf = get_dfs(df, m, n)
preds = model_swot(predictdf, modeldf, v, threshold=b, smoothing=s)

preds['true_swot'] = preds.swotstreamdur - preds.duration
preds['pred_swot'] = preds.swotstreamdur - preds.pred_drtn

preds.loc[preds.true_swot == 0, 'true_swot'] = 0.01

f1, ax1 = plt.subplots(2, 3, figsize=[20, 10])
ax2 = ax1.flatten()
for i, p in enumerate(preds.provider.unique()):
    ind = preds.provider == p
    ax2[i].scatter(preds.loc[ind, 'swotstreamdur'], preds.loc[ind, 'pred_swot'], c='blue', alpha=0.1, label='Predicted SWOT minutes')
    ax2[i].scatter(preds.loc[ind, 'swotstreamdur'], preds.loc[ind, 'true_swot'], c='red',  alpha=0.1, label='True SWOT minutes')
    ax2[i].set_xscale('log')
    ax2[i].set_yscale('log')
    ax2[i].set_ylim(0.001, max(preds.pred_swot.max(), preds.true_swot.max())+10)
    ax2[i].legend(loc='upper left', frameon=True, framealpha=0.3)
    ax2[i].set_title(p)
f1.savefig('ModelPlots/true_vs_pred_swotmins_{0}_m{1}-{2}_b{3}_s{4}.png'.format(v, n, m, b, s))
plt.close(f1)

########################################################################
########################################################################
########################################################################

########################################################################
########################################################################
### Test duration-based  model
# smoothing_list = [100,200, 300, 400, 500, 600, 700, 800, 900, 1000, 2000, 3000]
# drtn_model_v_smoothing = pd.DataFrame([], index=['all','netflix','youtube','hulu','amazon','hbo','crackle'], columns=smoothing_list)
# for s in smoothing_list:
#     print(s)
#     for i in range(2, 8):
#         drtn_model_v_month = pd.DataFrame([],
#                                           index=['all', 'netflix', 'youtube', 'hulu', 'amazon', 'hbo', 'crackle'],
#                                           columns=range(2, 8))
#         drtn_model_v_month[i] = calc_score(model_swot(*get_dfs(df, i), model_vers='drtn-prv', smoothing=s)).loc[:, 'reldiff']
#     drtn_model_v_smoothing[s] = rms(drtn_model_v_month, axis=1)
#
# smoothing=500
# f1, ax1 = plt.subplots(1)
# for p in drtn_model_v_smoothing.index:
#     ax1.plot(drtn_model_v_smoothing.columns, drtn_model_v_smoothing.loc[p], label=p)
# ax1.axvline(drtn_model_v_smoothing.loc['all'].idxmin(), c='gray', alpha=0.3)
# ax1.legend(loc='upper left')
# ax1.set_xlabel('Number of points to average over')
# ax1.set_ylabel('rms error')
# ax1.set_title('Drtn model error vs. amount of smoothing')
# f1.savefig('ModelPlots/drtn_error_v_smoothing.png'.format(smoothing))
# plt.close(f1)
#
# drtn_lookup = drtn_model(df, 500)
# f1, ax1 = plt.subplots()
# for p in drtn_lookup.columns:
#     ax1.plot(drtn_lookup.index, drtn_lookup[p], label=p)
# ax1.legend(loc='upper right')
# ax1.set_xscale('log')
# ax1.set_xlabel('swotstreamdur (min)')
# ax1.set_ylabel('Mean SWOT fraction')
# ax1.set_title('Tuning fraction vs. duration, by provider')
# f1.savefig('ModelPlots/duration_provider_model_{0}.png'.format(smoothing))
# plt.close(f1)
#
#
# drtn_model_v_month = pd.DataFrame([], index=['all','netflix','youtube','hulu','amazon','hbo','crackle'], columns=np.arange(2, 8))
# for i in range(2, 8):
#     print(i)
#     drtn_model_v_month[i] = calc_score(model_swot(*get_dfs(df, i), model_vers='drtn-prv', smoothing=smoothing)).loc[:, 'reldiff']
#
# f1, ax1 = plt.subplots(1)
# ax1.axhline(0, c='black', alpha=0.5)
# for p in drtn_model_v_month.index:
#     if p == 'all':
#         lw=3
#     else:
#         lw=1
#     ax1.plot(drtn_model_v_month.columns, 100.*drtn_model_v_month.loc[p], lw=lw, label=p)
# ax1.legend(loc='upper left')
# ax1.set_xticks(range(2,8))
# ax1.set_xticklabels(['Feb','Mar','Apr','May','Jun','Jul'])
# ax1.set_ylabel('Relative error (%)')
# ax1.set_title('Drtn model error by month (smoothing = {0})'.format(smoothing))
# f1.savefig('ModelPlots/drtn_error_v_month_s{0}.png'.format(smoothing))
# plt.close(f1)
#
# ### incorporate device model?
# threshold, smoothing = 50, 500
# score_cats = ['all', 'netflix', 'youtube', 'hulu', 'amazon', 'hbo', 'crackle']
# drtn_lookup = drtn_model(df, smoothing=smoothing, bin_key=get_bin_key(df))
# f1, ax1 = plt.subplots(3, 2, figsize=[25, 14], sharex=True)
# ax2 = ax1.flatten()
# for i, p in enumerate(score_cats[1:]):
#     for c in drtn_lookup.columns:
#         if p in c:
#             ax2[i].plot(drtn_lookup.index, drtn_lookup[c], label=c)
#     ax2[i].legend()
#     ax2[i].set_title(p)
# ax2[i].set_xscale('log')
# f1.savefig('ModelPlots/duration_provider_device_t{0}_s{1}.png'.format(threshold, smoothing))
# plt.close(f1)
#
# ########################################################################
# ### Categorize devices as similar?
# p='netflix'
# dvc_swotheight = pd.DataFrame(df.loc[(df.provider==p) & (df.swotstreamdur==8)].groupby('device').swot_frac.count())
# dvc_swotheight.columns = [p+'_n']
# dvc_swotheight[p+'_peak'] = df.loc[(df.provider==p) & (df.swotstreamdur==8 )].groupby('device').swot_frac.mean()
# dvc_swotheight[p+'_tail'] = df.loc[(df.provider==p) & (df.swotstreamdur>200)].groupby('device').swot_frac.mean()
# dvc_swotheight = dvc_swotheight.sort_values(p+'_peak')
#
# for p in ['youtube','hulu','hbo','amazon','crackle']:
#     dvc_swotheight[p+'_n'   ] = df.loc[(df.provider==p) & (df.swotstreamdur==8 )].groupby('device').swot_frac.count()
#     dvc_swotheight[p+'_peak'] = df.loc[(df.provider==p) & (df.swotstreamdur==8 )].groupby('device').swot_frac.mean()
#     dvc_swotheight[p+'_tail'] = df.loc[(df.provider==p) & (df.swotstreamdur>200)].groupby('device').swot_frac.mean()
#
# ########################################################################
# ###################### Bin devices #####################################
# ########################################################################
#
# predictions = model_swot(df, df, threshold=50)
# calc_score(predictions)
#
# ########################################################################
# ########################################################################
#
# model_versions = ['v0', 'v1', 'v4', 'drtn-prv', 'drtn-dvc', 'drtn-v3', 'drtn-v4','drtn-mnl-v4', 'drtn_no']
# test_threshold = 100
# test_smoothing = 500
# n_months=3
# month_i   = all_months_i[(n_months-1):]
# month_lab = all_months_n[(n_months-1):]
# # modelscores = test_model_version(df, score_cats, model_versions, threshold=test_threshold, smoothing=test_smoothing)
# ### Pick some example devices: top three, a middle one, and last one?
# n_dvcs = df.pivot_table(index='device', columns='provider', values='fsite', aggfunc=pd.Series.nunique).fillna(0)
# dvc_list = df.device.value_counts().index
# dvc_scores = {}
# prv_scores = {}
# for vers in model_versions:
#     print(vers)
#     dvc_scores[vers] = pd.DataFrame([], columns=month_i, index=dvc_list)
#     prv_scores[vers] = pd.DataFrame([], columns=month_i, index=score_cats)
#     for i in month_i:
#         modelled = model_swot(*get_dfs(df, i, n_months=n_months),
#                               model_vers=vers, threshold=test_threshold, smoothing=test_smoothing)
#         dvc_scores[vers][i] = calc_score(modelled, groupcol='device'  )['reldiff']
#         prv_scores[vers][i] = calc_score(modelled, groupcol='provider')['reldiff']
#
# print('model versions offset over how many categories (out of 26):')
# for v in model_versions:
#     print(pd.concat([(prv_scores[v] > 0).all(axis=1) | (prv_scores[v] < 0).all(axis=1), (dvc_scores[v]>0).all(axis=1) | (dvc_scores[v]<0).all(axis=1)]).sum())
#
#
# colors = sb.color_palette("Set1", len(model_versions), desat=0.7)
# f1, ax1 = plt.subplots(3, 5, sharex=True, sharey=True, figsize = [18,10])
# f1.subplots_adjust(hspace=0.1, wspace=0.05, right=.98, bottom=.06, top=.94, left=0.05)
# ax = ax1.flatten()
# for i, p in enumerate(score_cats):
#     ax[i].axhline(0, c='black', alpha=0.25)
#     if p == 'all':
#         ax[i].set_title('{0} ({1})'.format(p, int(n_dvcs.sum().sum())))
#     else:
#         ax[i].set_title('{0} ({1})'.format(p, int(n_dvcs[p].sum())))
#     for j, v in enumerate(model_versions):
#         ax[i].plot(prv_scores[v].columns, 100.*prv_scores[v].loc[p], c=colors[j],
#                    label='{0}: {1: 0.2f}'.format(v, 100.*rms(prv_scores[v].loc[p])))
#     ax[i].legend(loc='upper right', ncol=2)
# oldi = i
# for j, d in enumerate(dvc_list[[0,1,2,3, 5, 10, 15, 18]]):
#     i = j+oldi+1
#     ax[i].axhline(0, c='black', alpha=0.25)
#     ax[i].set_title('{0} ({1})'.format(d, int(n_dvcs.loc[d].sum())))
#     for k, v in enumerate(model_versions):
#         ax[i].plot(dvc_scores[v].columns, 100.*dvc_scores[v].loc[d], c=colors[k],
#                    label='{0}: {1: 0.2f}'.format(v, 100.*rms(dvc_scores[v].loc[d])))
#     ax[i].legend(loc='upper right', ncol=2)
#
# for col in [0, 1, 2, 3]:
#     ax1[2, col].set_xticks(month_i);
#     ax1[2, col].set_xticklabels(month_lab);
# ax[0].set_ylim(-10, 10)
# f1.text(0.02, 0.5, 'Error (%)', ha='center', va='center', rotation='vertical', fontsize=14)
# f1.text(0.5, 0.02, 'Month', ha='center', va='center', fontsize=14)
# f1.text(0.5, 0.98, 'Error in total viewed minutes, using previous month to predict SWOT %',
#         ha='center', va='center', fontsize=16)
# plt.savefig('ModelPlots/model_v_month_v_prvdr_t{0}_s{1}_m{2}.png'.format(test_threshold, test_smoothing, n_months))
# plt.close(f1)
#
# ########################################################################
#
# plt.clf()
# for i, d in enumerate(modelled.device.unique()):
#     plt.scatter(modelled.loc[(modelled.device == d) & (modelled.provider == 'netflix'), 'duration'],
#              modelled.loc[(modelled.device == d) & (modelled.provider == 'netflix'), 'pred_drtn'],
#              alpha=0.2, label=d)
# plt.xscale('log')
# plt.yscale('log')
# plt.legend()
#
#
#
#
# f1, ax1 = plt.subplots(3, 2, figsize=[15,10], sharex=True)
# ax2 = ax1.flatten()
# for i, p in enumerate(['netflix','youtube','hulu','hbo','amazon','crackle']):
#     prv_pred = modelled.loc[modelled.provider == p].copy().sort_values('swotstreamdur')
#     prv_pred['err'] = prv_pred.pred_drtn - prv_pred.duration
#     prv_pred['abs'] = prv_pred.err.abs()
#
#     prv_pred['true_swotfrac_avg'] = prv_pred.swot_frac.rolling(500, center=True, min_periods=10).mean()
#     prv_pred['pred_swotfrac_avg'] = prv_pred.pred_swotfrac.rolling(500, center=True, min_periods=10).mean()
#
#     err_v_drtn = prv_pred.groupby('swotstreamdur').err.sum()
#
#     ax2[i].fill_between(err_v_drtn.index, err_v_drtn, 0)
#     ax2[i].set_xlim(0, 200)
#
#     ax2[i].plot(prv_pred.swotstreamdur, 1000*prv_pred.true_swotfrac_avg, alpha=0.7, label='true')
#     ax2[i].plot(prv_pred.swotstreamdur, 1000*prv_pred.pred_swotfrac_avg, alpha=0.7, label='pred')
#     ax2[i].legend()
#     ax2[i].set_title(p)
# ax2[0].set_ylim(-300, 300)
#
# netflix_err = modelled.loc[modelled.provider=='netflix'].groupby('device').agg({'duration':'sum',
#                                                                                  'pred_drtn':'sum'}).fillna(0)
# netflix_err['err'] = netflix_err.pred_drtn - netflix_err.duration
# netflix_err['abs'] = netflix_err.err.abs()
# netflix_err = netflix_err.sort_values('abs')
# netflix_err['rel'] = netflix_err.err / netflix_err.duration


########################################################################
########################################################################
