import pyodbc     as db
import pandas     as pd
import sqlalchemy as sql
import tqdm
import winsound as ws
from v2_4 import swot_helper_functions as shf
pd.set_option('display.width', 160)

create_report = True
debug_report = True

dirpath = 'v2_4/'
opintel_streaming_event_query = dirpath+'streaming_events_sql_test.txt'
opintel_mime_event_query      = dirpath+'mime_events_sql_test.txt'
mw_query                      = dirpath+'mw_query_test.txt'
ottm_credit_query             = dirpath+'ottm_credit_results_wdev_test.txt'
mac_device                    = dirpath+'mac_device_map.csv'

mediaworks_user         = 'baroer01'
mediaworks_pass         = 'baroer01_changeme'
opintel_user            = 'OPINTEL_SPOTFIRE'
opintel_pass            = 'spotfire'

next_line = -1  #just to make the .shift lines that are used in this script more readable
prev_line =  1

date_range              = '2017-04-01','2017-04-08'

end_streaming_event_threshold = 240 # seconds
one_second = pd.Timedelta('1s')
one_minute = pd.Timedelta('1m')
two_months = pd.Timedelta(days=60)

hulu    = 1690781
amazon  = 777777,156698
youtube = 1431024
netflix = 3481638,214042
crackle = 1459202
hbo     = 56472,3213979

providers = [hulu, amazon, youtube, netflix, crackle, hbo]

pids = []
for x in providers:
    if type(x) == tuple:
        for j in x:
            pids.append(j)
    else:
        pids.append(x)
pids = tuple(pids)

mac_by_device = pd.read_csv(mac_device)

provider_names = pd.DataFrame({'tree_id': pids,
                               'provider': ['hulu', 'amazon', 'amazon', 'youtube', 'netflix', 'netflix', 'crackle', 'hbo', 'hbo']})
pid_map = provider_names.set_index('tree_id').provider
pid_map.loc[0] = 'other'

print('mw read')
mw_query = open(mw_query).read()
mw_connection = 'postgresql://{}:{}@TPARHELMPP057.enterprisenet.org:5432/MEDIAWORKS'.format(mediaworks_user,mediaworks_pass)
mw_engine     = sql.create_engine(mw_connection)
mw            = pd.read_sql_query(mw_query,mw_engine)
# mw.columns    = [str(str(x).lower()) for x in mw.columns.tolist()]
mw['fsite']   = mw.hh_id.iloc[:,0].astype('str') + '-' + mw.site.astype('str')
ws.PlaySound("*", ws.SND_ALIAS)

print('connecting...')
con = db.connect('DRIVER=NetezzaSQL;SERVER=nantz88.nielsen.com;PORT=5480;DATABASE=OPINTEL_PROD;UID=baroer01;PWD=Password@6;')
print('connected!')

streaming_events_query_text = open(opintel_streaming_event_query).read()
mime_events_query_text = open(opintel_mime_event_query).read()
ottm_credit_results_query_text = open(ottm_credit_query).read()

print('reading streaming_events')
streaming_events = pd.read_sql_query(streaming_events_query_text.format(pids,*date_range), con)
streaming_events.columns = [str.lower(x) for x in streaming_events.columns]

print('reading mime_events')
mime_events = pd.read_sql_query(mime_events_query_text.format(pids,*date_range), con)
mime_events.columns = [str.lower(x) for x in mime_events.columns]

print('reading dmx_events')
dmx_events = pd.concat([streaming_events[['starttimeest', 'endtimeest', 'macaddress', 'treeid']],
                        mime_events[['starttimeest', 'endtimeest', 'macaddress', 'treeid']]], ignore_index=True)

mw['macaddress'] = mw.mac_address
dmx_events = dmx_events.merge(mw[['macaddress','fsite']], on='macaddress', how='inner')
dmx_events = dmx_events.merge(mac_by_device, on='macaddress', how='inner')
cols = ['starttimeest', 'endtimeest', 'macaddress', 'treeid', 'fsite', 'device']
dmx_events = dmx_events[cols]
dmx_events.sort_values(['fsite', 'device', 'starttimeest'],inplace=True)
dmx_events.reset_index(drop=True,inplace=True)
ws.PlaySound("*", ws.SND_ALIAS)

print('reading credit_results')
credit_results = pd.read_sql_query(ottm_credit_results_query_text.format(tuple(dmx_events.fsite.dropna().unique().tolist()),pids,*date_range), con)
credit_results.columns = [str.lower(x) for x in credit_results.columns]
ws.PlaySound("*", ws.SND_ALIAS)

print('mw.shape, streaming_events.shape, mime_events.shape, dmx_events.shape, credit_results.shape:')
print(mw.shape, streaming_events.shape, mime_events.shape, dmx_events.shape, credit_results.shape)

print('---------------------------------- MW ----------------------------------')
print(mw.iloc[:3, :10])
print(mw.iloc[:3, 10:19])
print(mw.iloc[:3, 19:])

print('---------------------------------- STREAMING EVENTS ----------------------------------')
print(streaming_events.iloc[:3, :4])
print(streaming_events.iloc[:3, 4:10])
print(streaming_events.iloc[:3, 10:])

print('---------------------------------- MIME EVENTS ----------------------------------')
print(mime_events.iloc[:3, :8])
print(mime_events.iloc[:3, 8:])

# dmx_events

### appears to be minute-exploded (see _byminute cols:
# recorded_startest/end, recorded_startlocal/end,
# viewed_startest/end, viewed_startlocal/end
print('---------------------------------- CREDIT RESULTS ----------------------------------')
print(credit_results.iloc[:3, :7])
print(credit_results.iloc[:3, 7:14])
print(credit_results.iloc[:3, 14:20])
print(credit_results.iloc[:3, 20:31])
print(credit_results.iloc[:3, 31:44])
print(credit_results.iloc[:3, 44:52])
print(credit_results.iloc[:3, 52:])

#####################################################################################
### Try to follow the SWOT process?
### any pid not in the existing list == ?
credit_results.tree_id.unique()
for p in credit_results.tree_id.unique():
    if p not in pid_map.index:
        pid_map.loc[p] = 'misc'

# dmx_events['provider']     = [pid_map.loc[i] for i in dmx_events.treeid]
# credit_results['provider'] = [pid_map.loc[i] for i in credit_results.tree_id]

### Remove duplicate rows (i.e. collapse minute-exploding)
credit_results_minwin = credit_results.groupby(['fsite','viewed_startest','viewed_endest']).first().reset_index()
print('credit_results before/after dropping minute-exploded rows', credit_results.shape[0], credit_results_minwin.shape[0])

# setting the off moments to a different device than unknown device while the tv is on
credit_results_minwin.loc[(credit_results_minwin.dist_sourceid == 0) &
                          (credit_results_minwin.tree_id == 0),'ottmdevicekey'] = -1


example = credit_results_minwin.loc[credit_results_minwin.fsite == '16978374-1',
                                    ['fsite','viewed_startest','viewed_endest','dist_sourceid','tree_id']]
example['ottmdevicekey'] = credit_results_minwin.ottmdevicekey.iloc[:,0]
example['drtn'] = (example.viewed_endest - example.viewed_startest)/one_minute
example['gap']  = (example.viewed_startest - example.viewed_endest.shift(1))/one_minute

#########
example['range1class'] = None
example.loc[(example.dist_sourceid == 0),'range1class'] = 'off'
example.loc[(example.dist_sourceid == 10202) & (example.tree_id == 0),'range1class'] = 'mute'
example.loc[~(example.dist_sourceid.isin([0, 10202])) & (example.tree_id == 0),'range1class'] = 'other'
example.loc[(example.dist_sourceid != 0) & (example.tree_id != 0),'range1class'] = 'streaming'

example.loc[(example.ottmdevicekey != example.ottmdevicekey.shift(prev_line)) &
                  (example.ottmdevicekey != 3500001) &
                  (example.range1class.isin(['mute', 'other'])), 'range1class'] = 'sourcechange'

example.loc[(example.range1class == 'other') & 
            (example.range1class.shift(prev_line) == 'streaming')   &
            (example.range1class.shift(next_line) == 'streaming')   &
            (example.fsite == example.fsite.shift(prev_line)) &
            (example.fsite == example.fsite.shift(next_line)), 'range1class'] = 'streaming'

example['duration'] = (example.viewed_endest - example.viewed_startest) / one_second

example['groupID'] = 0
example['temp'] = 0
example['temp2'] = 0 # to differentiate between mutes in off ranges and mutes in stream ranges

#rule 1
example.loc[example.range1class == 'streaming', 'temp'] = 1

#rule 2
example.loc[example.range1class.isin(['off','sourcechange']), 'temp'] = 1

#rule 3
example.loc[(example.range1class == 'streaming') &
                  (example.range1class.shift(prev_line) == 'mute') & 
                  (example.range1class.shift(2*prev_line) == 'streaming'), 'temp'] = 0

#rule 4
example.loc[example.fsite != example.fsite.shift(prev_line), 'temp'] = 1

example['groupID'] = example.temp.cumsum()
