import numpy as np
import pandas as pd
pd.set_option('display.width', 160)
import matplotlib.pyplot as plt
import seaborn as sb
import random


# total minutes - duration, swotdur, and model
# random subsamples


########################################################################
def create_bins_v0(df, threshold=50):
    '''Average everything together -- no divisions'''
    n_dvcs = df.pivot_table(index='mk_mdl', columns='provider', values='fsite', aggfunc=pd.Series.nunique).fillna(0)
    ### Create df to assign bins to each provider/model combination
    bin_names = pd.DataFrame([], index=n_dvcs.index, columns=n_dvcs.columns).fillna(0)
    return bin_names

########################################################################
def create_bins_v1(df, threshold=50):
    '''bin every cell separately, regardless of threshold'''
    n_dvcs = df.pivot_table(index='mk_mdl', columns='provider', values='fsite', aggfunc=pd.Series.nunique).fillna(0)
    ### Create df to assign bins to each provider/model combination
    bin_names = pd.DataFrame([], index=n_dvcs.index, columns=n_dvcs.columns)
    ### first pass: identify individual cells that meet threshold
    for m in bin_names.index:
        for p in bin_names.columns:
            bin_names.loc[m, p] = m+'_'+p
    return bin_names

########################################################################
def create_bins_v2(df, threshold=50):
    '''Keep all providers separate, only bin within them'''
    n_dvcs = df.pivot_table(index='mk_mdl', columns='provider', values='fsite', aggfunc=pd.Series.nunique).fillna(0)
    ### Create df to assign bins to each provider/model combination
    bin_names = pd.DataFrame([], index=n_dvcs.index, columns=n_dvcs.columns)
    ### first pass: identify individual cells that meet threshold
    for m in bin_names.index:
        for p in bin_names.columns:
            if n_dvcs.loc[m, p] >= threshold:
                bin_names.loc[m, p] = m+'_'+p
    ### second pass: group remaining cells by provider, if they meet threshold
    for c in bin_names.columns:
        if n_dvcs.loc[bin_names[c].isnull(), c].sum() >= threshold:
            bin_names.loc[bin_names[c].isnull(), c] = 'misc_'+c
    ### Anything left? I guess just throw into one last bin?
    if bin_names.isnull().sum().sum() > 0:
        print('WARNING: {0} unassigned cells remaining! Coded as "other"'.format(bin_names.isnull().sum().sum()))
        bin_names = bin_names.fillna('other')
    return bin_names

########################################################################
def create_bins_v3(df, threshold=50):
    n_dvcs = df.pivot_table(index='mk_mdl', columns='provider', values='fsite', aggfunc=pd.Series.nunique).fillna(0)
    ### Create df to assign bins to each provider/model combination
    bin_names = pd.DataFrame([], index=n_dvcs.index, columns=n_dvcs.columns)
    ### first pass: identify individual cells that meet threshold
    for m in bin_names.index:
        for p in bin_names.columns:
            if n_dvcs.loc[m, p] >= threshold:
                bin_names.loc[m, p] = m+'_'+p
    ### second pass: group remaining cells by provider, ignore threshold
    for c in bin_names.columns:
        bin_names.loc[bin_names[c].isnull(), c] = 'misc_'+c
    return bin_names

########################################################################
def create_bins_v4(df, threshold=50, thresh2=1):
    '''all devices are kept separate for Netflix'''
    n_dvcs = df.pivot_table(index='mk_mdl', columns='provider',
                            values='fsite', aggfunc=pd.Series.nunique).fillna(0)
    ### Create df to assign bins to each provider/model combination
    bin_names = pd.DataFrame([], index=n_dvcs.index, columns=n_dvcs.columns)
    ### first pass: label all netflix bins as separate
    p = 'netflix'
    for m in bin_names.index:
        if n_dvcs.loc[m, p] >= min(threshold, thresh2):
            bin_names.loc[m, p] = m + '_' + p
    ### second pass: identify individual cells that meet threshold
    for m in bin_names.index:
        for p in bin_names.columns:
            if n_dvcs.loc[m, p] >= threshold:
                bin_names.loc[m, p] = m+'_'+p
    ### third pass: group remaining cells by provider, ignore threshold
    for p in bin_names.columns:
        bin_names.loc[bin_names[p].isnull(), p] = 'misc_'+p
    return bin_names

########################################################################
def create_bins_v5(df, threshold=50):
    '''bin providers rather than device types'''
    n_dvcs = df.pivot_table(index='mk_mdl', columns='provider',
                            values='fsite', aggfunc=pd.Series.nunique).fillna(0)
    ### Create df to assign bins to each provider/model combination
    bin_names = pd.DataFrame([], index=n_dvcs.index, columns=n_dvcs.columns)
    ### identify individual cells that meet threshold
    for m in bin_names.index:
        for p in bin_names.columns:
            if n_dvcs.loc[m, p] >= threshold:
                bin_names.loc[m, p] = m+'_'+p
    ### group remaining cells by device
    for m in bin_names.index:
        if n_dvcs.loc[m, bin_names.loc[m].isnull()].sum() >= threshold:
            bin_names.loc[m, bin_names.loc[m].isnull()] = 'misc_'+m
    ### Group remainder together as 'other'
    for m in bin_names.index:
        bin_names.loc[m, bin_names.loc[m].isnull()] = 'other'
    return bin_names
########################################################################
def create_bins_v6(df, threshold=50):
    '''bin providers rather than device types'''
    n_dvcs = df.pivot_table(index='mk_mdl', columns='provider',
                            values='fsite', aggfunc=pd.Series.nunique).fillna(0)
    ### Create df to assign bins to each provider/model combination
    bin_names = pd.DataFrame([], index=n_dvcs.index, columns=n_dvcs.columns)
    ### identify individual cells that meet threshold
    for m in bin_names.index:
        for p in bin_names.columns:
            if n_dvcs.loc[m, p] >= threshold:
                bin_names.loc[m, p] = m+'_'+p
    ### group remaining cells by device
    for m in bin_names.index:
        bin_names.loc[m, bin_names.loc[m].isnull()] = 'misc_'+m
    ### Group remainder together as 'other'
    # for m in bin_names.index:
    #     bin_names.loc[m, bin_names.loc[m].isnull()] = 'other'
    return bin_names
########################################################################
def create_bins_v7(df, threshold=50):
    '''bin providers rather than device types'''
    n_dvcs = df.pivot_table(index='mk_mdl', columns='provider',
                            values='fsite', aggfunc=pd.Series.nunique).fillna(0)
    ### Create df to assign bins to each provider/model combination
    bin_names = pd.DataFrame([], index=n_dvcs.index, columns=n_dvcs.columns)
    ### keep youtube separate from other providers when binning by device
    for m in bin_names.index:
        bin_names.loc[m, 'youtube'] = 'misc_youtube'
        bin_names.loc[m, 'netflix'] = 'misc_netflix'
    ### identify individual cells that meet threshold
    for m in bin_names.index:
        for p in bin_names.columns:
            if n_dvcs.loc[m, p] >= threshold:
                bin_names.loc[m, p] = m+'_'+p
    ### group remaining cells by device
    for m in bin_names.index:
        bin_names.loc[m, bin_names.loc[m].isnull()] = 'misc_'+m
    return bin_names
########################################################################
def create_bins_v8(df, threshold=50):
    '''customized'''
    n_dvcs = df.pivot_table(index='mk_mdl', columns='provider',
                            values='fsite', aggfunc=pd.Series.nunique).fillna(0)
    ### Create df to assign bins to each provider/model combination
    bin_names = pd.DataFrame([], index=n_dvcs.index, columns=n_dvcs.columns)
    ### keep youtube separate from other providers but binned with itself
    for m in bin_names.index:
        bin_names.loc[m, 'youtube'] = 'misc_youtube'
        bin_names.loc[m, 'crackle'] = 'misc_crackle'
    ### separate all netflix bins
    for m in bin_names.index:
        for p in ['netflix']:
            bin_names.loc[m, p] = m+'_'+p
    ### identify individual cells that meet threshold
    # for m in bin_names.index:
    #     for p in bin_names.columns:
    #         if n_dvcs.loc[m, p] >= threshold:
    #             bin_names.loc[m, p] = m+'_'+p
    ### group remaining cells by device
    for m in bin_names.index:
        bin_names.loc[m, bin_names.loc[m].isnull()] = 'misc_'+m
    return bin_names
########################################################################
def create_bins_v9(df, threshold=50):
    '''customized'''
    n_dvcs = df.pivot_table(index='mk_mdl', columns='provider',
                            values='fsite', aggfunc=pd.Series.nunique).fillna(0)
    ### Create df to assign bins to each provider/model combination
    bin_names = pd.DataFrame([], index=n_dvcs.index, columns=n_dvcs.columns)
    ### keep youtube separate from other providers but binned with itself
    for m in bin_names.index:
        bin_names.loc[m, 'youtube'] = 'misc_youtube'
        bin_names.loc[m, 'crackle'] = 'misc_crackle'
    ### separate all netflix bins
    for m in bin_names.index:
        for p in ['netflix']:
            bin_names.loc[m, p] = m+'_'+p
    ### identify individual cells that meet threshold
    for m in bin_names.index:
        for p in bin_names.columns:
            if n_dvcs.loc[m, p] >= threshold:
                bin_names.loc[m, p] = m+'_'+p
    ### group some cells by device
    for m in ['Smart TV','Amazon Fire Stick','Play Station 4']:
        bin_names.loc[m, bin_names.loc[m].isnull()] = 'misc_'+m
    ### group remaining cells by provider
    for p in bin_names.columns:
        bin_names.loc[bin_names[p].isnull(), p] = 'misc_'+p
    return bin_names
###################### Single function containing binmodels ############
def create_bins(modeldf, threshold=50, thresh2=1, binmodel='v7'):
    if binmodel == 'v0':
        return create_bins_v0(modeldf, threshold=threshold)
    elif binmodel == 'v1':
        return create_bins_v1(modeldf, threshold=threshold)
    elif binmodel == 'v2':
        return create_bins_v2(modeldf, threshold=threshold)
    elif binmodel == 'v3':
        return create_bins_v3(modeldf, threshold=threshold)
    elif binmodel == 'v4':
        return create_bins_v4(modeldf, threshold=threshold, thresh2=thresh2)
    elif binmodel == 'v5':
        return create_bins_v5(modeldf, threshold=threshold)
    elif binmodel == 'v6':
        return create_bins_v6(modeldf, threshold=threshold)
    elif binmodel == 'v7':
        return create_bins_v7(modeldf, threshold=threshold)
    elif binmodel == 'v8':
        return create_bins_v8(modeldf, threshold=threshold)
    elif binmodel == 'v9':
        return create_bins_v9(modeldf, threshold=threshold)
    else:
        assert 1==0, 'ERROR: invalid bin model version {0}'.format(binmodel)

###################### Calculate bin properties ########################
def bin_avgs(df, bin_names):
    ### Flatten into a list of models/providers
    bin_names['mdl'] = bin_names.index
    ### Key for looking up which bin each cell belongs to
    bin_key = pd.melt(bin_names, id_vars=['mdl'], value_name='bin_name')
    ### Number of devices in each cell
    n_dvcs = df.pivot_table(index='mk_mdl', columns='provider', values='fsite', aggfunc=pd.Series.nunique).fillna(0)
    ### Df with one row per bin, to calculate the overall swot fractions and such
    bin_info = pd.DataFrame(bin_key.bin_name.value_counts())
    bin_info.columns = ['n_cells']
    bin_info['n_dvcs']      = 0
    bin_info['strm_tot']    = 0
    bin_info['tune_tot']    = 0
    for cell in bin_info.index:
        cells = bin_key.loc[bin_key.bin_name == cell]
        strm_tot = 0
        tune_tot = 0
        for row in cells.index:
            m, p = cells.loc[row, 'mdl'], cells.loc[row, 'provider']
            bin_info.loc[cell, 'n_dvcs'] += n_dvcs.loc[m, p]
            strm_tot += df.loc[(df.device == m) & (df.provider == p), 'swotstreamdur'].sum()
            tune_tot += df.loc[(df.device == m) & (df.provider == p), 'duration'].sum()
        bin_info.loc[cell, ['strm_tot','tune_tot']] = strm_tot, tune_tot
    bin_info['mn_swotfrac']  = 1. - bin_info.tune_tot/bin_info.strm_tot
    bin_info['strm_per_dvc'] = bin_info.strm_tot/bin_info.n_dvcs
    return bin_info, bin_key

########################################################################
def bin_predict(df, bin_info, bin_key):
    preds = df[['device', 'provider', 'duration', 'swotstreamdur', 'swot_frac']].copy()
    ### Assign each session to a bin
    preds['bin_name'] = bin_key.set_index(['mdl', 'provider']).loc[
                           list(zip(*[preds.device, preds.provider])), 'bin_name'].tolist()
    ### Get the predicted swot fraction for that bin
    preds['pred_swotfrac'] = bin_info.loc[preds.bin_name, 'mn_swotfrac'].tolist()
    ### Predict duration by trimming swotstreamdur by predicted swot fraction
    preds['pred_drtn'] = preds.swotstreamdur * (1.-preds.pred_swotfrac)
    return preds

########################################################################
########################################################################
########################################################################
def get_bin_key(modeldf, binmodel='v4', threshold=50, manual_bins={}):
    if binmodel == 'prv':
        binmodel='provider'
    if binmodel == 'dvc':
        binmodel='device'
    if binmodel in ['provider','device']:
        bin_key = pd.DataFrame(list(zip(
            [p for p in modeldf.provider.unique() for d in modeldf.device.unique()],
            [d for p in modeldf.provider.unique() for d in modeldf.device.unique()])),
            columns=['provider','device'])
        bin_key['bin_name'] = bin_key[binmodel]
    else:
        bin_names = create_bins(modeldf, binmodel=binmodel, threshold=threshold, thresh2=1)
        ### Flatten into a list of models/providers
        bin_names['device'] = bin_names.index
        ### Key for looking up which bin each cell belongs to
        bin_key = pd.melt(bin_names, id_vars=['device'], value_name='bin_name')
    for p in manual_bins.keys():
        for i in manual_bins[p].keys():
            bin_key.loc[(bin_key.provider == p) &
                        (bin_key.device.isin(manual_bins[p][i])), 'bin_name'] = '{0:0>2}-{1}'.format(i, p)

    return bin_key
########################################################################
def drtn_model(modeldf, bin_key, smoothing=500, min_periods=100, do_flatten=True, flatten_small_thresh=2, flatten_start_thresh=0.05):
    ### Get duration model using each bin as a separate column
    pred_lookup = pd.DataFrame([], index=range(1, int(modeldf.swotstreamdur.max())+1), columns=bin_key.bin_name.unique())
    for c in pred_lookup.columns:
        ### device-provider pairs that are in this bin
        cells = bin_key.loc[bin_key.bin_name == c]
        ### subset of events that come from those device-provider pairs, sorted by total duration
        subset = modeldf.loc[modeldf.device.isin(cells.device) & modeldf.provider.isin(cells.provider),
                             ['swotstreamdur', 'duration', 'swot_frac']].sort_values('swotstreamdur')
        if do_flatten & (subset.shape[0] < smoothing*flatten_small_thresh):
            ### too few events => take bin total swot_frac
            pred_lookup[c] = 1. - subset.duration.sum() / subset.swotstreamdur.sum()
        else:
            ### rolling average of swot fraction
            subset['mn_swot_frac'] = subset.swot_frac.rolling(smoothing, center=True,
                                                              min_periods=min(min_periods, subset.shape[0])).mean()
            ### groupby to get a single average swot fraction for each duration
            pred_lookup[c] = subset.groupby('swotstreamdur').mn_swot_frac.mean()
            pred_lookup[c] = pred_lookup[c].fillna(method='ffill')
            if do_flatten & (pred_lookup[c].iloc[0] > flatten_start_thresh):
                pred_lookup[c] = 1. - subset.duration.sum() / subset.swotstreamdur.sum()
    return pred_lookup

########################################################################
### Construct df with curves using best smoothing window for each provider
def construct_drtn_eqns(df, prv_smoothing={'netflix': 1000,
                                           'youtube': 1000,
                                           'hulu':    1000,
                                           'amazon':   500,
                                           'hbo':      500,
                                           'crackle':   60},
                        peak_mins=[7,8,9,10], fname='ModelPlots/drtn_norm.csv'):
    drtn_curves = pd.DataFrame([], index=range(1, int(df.swotstreamdur.max())))
    for p in prv_smoothing.keys():
        drtn_curves[p] = drtn_model(df, get_bin_key(df, binmodel='provider'),
                                    smoothing=prv_smoothing[p], do_flatten=False, min_periods=int(prv_smoothing[p]/2))[p]
    ### Force the first point to zero
    drtn_curves.loc[1] = [0]*drtn_curves.shape[1]
    ### Normalize how?
    ### divide by provider total swot_frac near peak
    prv_aggs = df.loc[df.swotstreamdur.isin(peak_mins)]\
                 .groupby('provider').agg({'fsite':        'count',
                                           'swotstreamdur':'sum',
                                           'duration':     'sum'})
    prv_aggs['swot_frac'] = (prv_aggs.swotstreamdur - prv_aggs.duration) / prv_aggs.swotstreamdur
    drtn_norm = drtn_curves / prv_aggs.swot_frac
    ### If crackle isn't sampled enough to justify using, try averaging the
    ### other curves together?
    # drtn_norm['crackle'] = drtn_norm[[c for c in drtn_norm.columns if c != 'crackle']].mean(axis=1)
    # drtn_norm['crackle'] = drtn_norm.crackle / drtn_norm.crackle.loc[1:100].max()
    ### Save to file
    drtn_norm.to_csv(fname)
    return drtn_norm

########################################################################
### Predict swot fraction for particular device and provider at each possible duration
def eqn_model(modeldf, bin_key, peak_mins = [7, 8, 9, 10]):
    ### Read established provider-duration equation templates
    drtn_norm = pd.read_csv('ModelPlots/drtn_norm.csv', index_col=0)
    ### Calculate stream/tune rates per provider/device
    prv_aggs = modeldf.loc[modeldf.swotstreamdur.isin(peak_mins)]\
                 .groupby('provider').agg({'fsite':        'count',
                                           'swotstreamdur':'sum',
                                           'duration':     'sum'})
    prv_aggs['swot_frac'] = (prv_aggs.swotstreamdur - prv_aggs.duration) / prv_aggs.swotstreamdur
    strm_tot = modeldf.loc[modeldf.swotstreamdur.isin(peak_mins)].pivot_table(index='device', columns='provider', values='swotstreamdur', aggfunc='sum')
    tune_tot = modeldf.loc[modeldf.swotstreamdur.isin(peak_mins)].pivot_table(index='device', columns='provider', values='duration', aggfunc='sum')
    swot_tot = strm_tot - tune_tot
    swot_mn = swot_tot / strm_tot
    for c in swot_mn.columns:
        prov_mn_swot = prv_aggs.loc[c, 'swot_frac']
        swot_mn.loc[swot_mn[c] == 0, c] = prov_mn_swot
        swot_mn[c] = swot_mn[c].fillna(prov_mn_swot)
    ### Multiple read-in provider-drtn equations by calculated prv-dvc swot fracs to get predictions
    pred_lookup = pd.DataFrame([], index=range(1, int(modeldf.swotstreamdur.max()) + 1), columns=bin_key.bin_name.unique())
    for c in pred_lookup.columns:
        d, p = c.split('_')
        pred_lookup[c] = drtn_norm[p] * swot_mn.loc[d, p]
    return pred_lookup

########################################################################
########################################################################
def get_dfs(df, i, n_months=1):
    modeldf   = df.loc[df.month.isin(list(range( (i-n_months), i)))]
    predictdf = df.loc[df.month == i]
    return modeldf, predictdf
########################################################################
def get_lookup(bin_info, bin_key):
    pred_lookup = pd.DataFrame([], index=bin_key.mdl.unique(), columns=bin_key.provider.unique())
    for m in pred_lookup.index:
        for p in pred_lookup.columns:
            pred_lookup.loc[m, p] = bin_info.loc[bin_key.loc[(bin_key.mdl==m) & (bin_key.provider==p), 'bin_name'].values[0], 'mn_swotfrac']
    return pred_lookup
########################################################################
def predict_from_lookup(predictdf, pred_lookup, ind='device', col='provider'):
    preds = predictdf[list(set([ind, col, 'device', 'provider', 'duration', 'swotstreamdur', 'swot_frac']))].copy()
    preds['pred_swotfrac'] = np.nan
    for p in pred_lookup.columns:
        preds.loc[preds[col]==p, 'pred_swotfrac'] = list(pred_lookup.loc[preds.loc[preds[col]==p, ind], p])
    preds['pred_drtn'] = preds.swotstreamdur * (1. - preds.pred_swotfrac)
    return preds
########################################################################
def predict_from_lookup_and_key(predictdf, pred_lookup, bin_key):
    preds = predictdf[['device', 'provider', 'duration', 'swotstreamdur', 'swot_frac']].copy()
    preds['pred_swotfrac'] = np.nan
    for c in pred_lookup.columns:
        ### device-provider pairs that are in this bin
        cells = bin_key.loc[bin_key.bin_name == c]
        ### subset of events that come from those device-provider pairs, sorted by total duration
        binmembers = predictdf.device.isin(cells.device) & predictdf.provider.isin(cells.provider)
        ### assign correct swotfrac for duration to binmembers
        preds.loc[binmembers, 'pred_swotfrac'] = list(pred_lookup.loc[preds.loc[binmembers, 'swotstreamdur'], c])
    preds['pred_drtn'] = preds.swotstreamdur * (1. - preds.pred_swotfrac)
    return preds
########################################################################
def model_swot(modeldf, predictdf, model_vers = 'v4', threshold=150, smoothing=3000):
    if 'drtn' in model_vers:
        do_flatten = True
        if 'drtn-mnl-' in model_vers:
            manual_bins = {
                'netflix': {
                    1: ['Smart TV', 'Computer'],
                    2: ['Bluray'],
                    3: ['Wii U', 'Roku 2', 'Play Station 4', 'Roku 1', 'Wii', 'Google TV', 'Google Chromecast'],
                    4: ['X Box One', 'Play Station 3', 'Amazon Fire Stick', 'Amazon Fire TV', 'X Box 360'],
                    5: ['Apple TV', 'Other', 'Roku 3', 'Roku Streaming Stick']
                }
            }
            bin_key = get_bin_key(modeldf, binmodel=model_vers[-2:], threshold=threshold, manual_bins=manual_bins)
        elif 'drtn-no' in model_vers:
            do_flatten, bin_key = False, get_bin_key(modeldf, binmodel=model_vers[-2:], threshold=threshold)
        else:
            assert len(model_vers.split('-')) == 2, 'ERROR: invalid model name "{0}"'.format(model_vers)
            bin_key = get_bin_key(modeldf, binmodel=model_vers.split('-')[1], threshold=threshold)
        pred_lookup = drtn_model(modeldf, bin_key, smoothing=smoothing, do_flatten=do_flatten)
        if modeldf.swotstreamdur.max() < predictdf.swotstreamdur.max():
            pred_lookup = pred_lookup.reindex(
                range(int(pred_lookup.index.min()), int(predictdf.swotstreamdur.max()) + 1)).fillna(method='ffill')
        preds = predict_from_lookup_and_key(predictdf, pred_lookup, bin_key)
    elif 'eqn' in model_vers:
        bin_key = get_bin_key(modeldf, binmodel='v1')
        pred_lookup = eqn_model(modeldf, bin_key)
        preds = predict_from_lookup_and_key(predictdf, pred_lookup, bin_key)
    else:
        bin_names = create_bins(modeldf, threshold=threshold, binmodel=model_vers)
        bin_info, bin_key = bin_avgs(modeldf, bin_names)
        pred_lookup = get_lookup(bin_info, bin_key)
        preds = predict_from_lookup(predictdf, pred_lookup, ind='device')
    return preds
########################################################################
########################################################################
########################################################################
def calc_score(df, truecol = 'duration', predcol='pred_drtn', groupcol='provider'):
    d = df[[truecol, predcol, groupcol]].copy()
    d['delta'] = d[predcol] - d[truecol]
    aggs = pd.DataFrame(d.groupby(groupcol)['duration'].sum())#.astype('int')
    aggs.columns = ['true_drtn']
    aggs['pred_drtn'] = d.groupby(groupcol)['pred_drtn'].sum()#.astype('int')
    # aggs['delta'] = d.groupby(groupcol)['delta'].sum()
    aggs['delta'] = aggs.pred_drtn - aggs.true_drtn
    aggs['reldiff'] = aggs.delta/aggs.true_drtn
    aggs.loc['all'] = [df[truecol].sum(),
                       df[predcol].sum(),
                       df[predcol].sum() - df[truecol].sum(),
                      (df[predcol].sum() - df[truecol].sum())/df[truecol].sum()]
    return aggs
########################################################################
def rms(x, axis=0):
    return (x**2).mean(axis=axis)**0.5
########################################################################
########################################################################
def test_model_version(df, score_cats, model_versions, threshold=50, smoothing=500):
    scores = {}
    for model in model_versions:
        testscores = pd.DataFrame([], index=score_cats,
                                  columns=range(2,8))
        # iterate over 6 months
        for i in range(1, 7):
            testscores[i+1] = calc_score(model_swot(df.loc[df.month==i],
                                                    df.loc[df.month==(i+1)],
                                                    model_vers=model, threshold=threshold, smoothing=smoothing))['reldiff']
        scores[model] = testscores
    return scores

# -------------------------------------------------------------------- #
# -------------------------------------------------------------------- #
# -------------------------------------------------------------------- #
def test_model_threshold(df, thresholds, score_cats, version='v6'):
    scores = {}
    for t in thresholds:
        testscores = pd.DataFrame([], index=score_cats,
                                  columns=range(2,8))
        for i in range(1, 7):
            testscores[i+1] = calc_score(model_swot(df.loc[df.month==i],
                                                    df.loc[df.month==(i+1)],
                                                    threshold=t, binmodel=version))['reldiff']
        scores[t] = testscores
    return scores

########################################################################
########################################################################
########################################################################
