# -*- coding: utf-8 -*-
"""
Created on Tue Aug 15 10:26:34 2017

@author: adamel01
"""

# -*- coding: utf-8 -*-
"""
Created on Tue Aug 15 10:08:36 2017

@author: adamel01
"""

# -*- coding: utf-8 -*-
"""
Created on Fri Aug 11 10:28:46 2017

@author: adamel01
"""

import numpy as np
import pandas as pd
pd.set_option('display.width', 160)
import matplotlib.pyplot as plt
import seaborn as sb
import random


# total minutes - duration, swotdur, and model
# random subsamples


########################################################################
def rms(x, axis=0):
    return (x**2).mean(axis=axis)**0.5
########################################################################
########################################################################
########################################################################
def calc_score(df, truecol = 'duration', predcol='pred_drtn', groupcol='provider'):
    d = df[[truecol, predcol, groupcol]].copy()
    d['delta'] = d[predcol] - d[truecol]
    aggs = pd.DataFrame(d.groupby(groupcol)['duration'].sum())#.astype('int')
    aggs.columns = ['true_drtn']
    aggs['pred_drtn'] = d.groupby(groupcol)['pred_drtn'].sum()#.astype('int')
    # aggs['delta'] = d.groupby(groupcol)['delta'].sum()
    aggs['delta'] = aggs.pred_drtn - aggs.true_drtn
    aggs['reldiff'] = aggs.delta/aggs.true_drtn
    aggs.loc['all'] = [df[truecol].sum(),
                       df[predcol].sum(),
                       df[predcol].sum() - df[truecol].sum(),
                       (df[predcol].sum() - df[truecol].sum())/df[truecol].sum()]
    return aggs
########################################################################
def create_bins_v0(df, threshold=50):
    '''Average everything together -- no divisions'''
    n_dvcs = df.pivot_table(index='mk_mdl', columns='provider', values='fsite', aggfunc=pd.Series.nunique).fillna(0)
    ### Create df to assign bins to each provider/model combination
    bin_names = pd.DataFrame([], index=n_dvcs.index, columns=n_dvcs.columns).fillna(0)
    return bin_names

########################################################################
def create_bins_v1(df, threshold=50):
    '''bin every cell separately, regardless of threshold'''
    n_dvcs = df.pivot_table(index='mk_mdl', columns='provider', values='fsite', aggfunc=pd.Series.nunique).fillna(0)
    ### Create df to assign bins to each provider/model combination
    bin_names = pd.DataFrame([], index=n_dvcs.index, columns=n_dvcs.columns)
    ### first pass: identify individual cells that meet threshold
    for m in bin_names.index:
        for p in bin_names.columns:
            bin_names.loc[m, p] = m+'_'+p
    return bin_names

########################################################################
def create_bins_v2(df, threshold=50):
    '''Keep all providers separate, only bin within them'''
    n_dvcs = df.pivot_table(index='mk_mdl', columns='provider', values='fsite', aggfunc=pd.Series.nunique).fillna(0)
    ### Create df to assign bins to each provider/model combination
    bin_names = pd.DataFrame([], index=n_dvcs.index, columns=n_dvcs.columns)
    ### first pass: identify individual cells that meet threshold
    for m in bin_names.index:
        for p in bin_names.columns:
            if n_dvcs.loc[m, p] >= threshold:
                bin_names.loc[m, p] = m+'_'+p
    ### second pass: group remaining cells by provider, if they meet threshold
    for c in bin_names.columns:
        if n_dvcs.loc[bin_names[c].isnull(), c].sum() >= threshold:
            bin_names.loc[bin_names[c].isnull(), c] = 'misc_'+c
    ### Anything left? I guess just throw into one last bin?
    if bin_names.isnull().sum().sum() > 0:
        print('WARNING: {0} unassigned cells remaining! Coded as "other"'.format(bin_names.isnull().sum().sum()))
        bin_names = bin_names.fillna('other')
    return bin_names

########################################################################
def create_bins_v3(df, threshold=50):
    n_dvcs = df.pivot_table(index='mk_mdl', columns='provider', values='fsite', aggfunc=pd.Series.nunique).fillna(0)
    ### Create df to assign bins to each provider/model combination
    bin_names = pd.DataFrame([], index=n_dvcs.index, columns=n_dvcs.columns)
    ### first pass: identify individual cells that meet threshold
    for m in bin_names.index:
        for p in bin_names.columns:
            if n_dvcs.loc[m, p] >= threshold:
                bin_names.loc[m, p] = m+'_'+p
    ### second pass: group remaining cells by provider, ignore threshold
    for c in bin_names.columns:
        bin_names.loc[bin_names[c].isnull(), c] = 'misc_'+c
    return bin_names

########################################################################
def create_bins_v4(df, threshold=50, thresh2=1):
    '''all devices are kept separate for Netflix'''
    n_dvcs = df.pivot_table(index='mk_mdl', columns='provider',
                            values='fsite', aggfunc=pd.Series.nunique).fillna(0)
    ### Create df to assign bins to each provider/model combination
    bin_names = pd.DataFrame([], index=n_dvcs.index, columns=n_dvcs.columns)
    ### first pass: label all netflix bins as separate
    p = 'netflix'
    for m in bin_names.index:
        if n_dvcs.loc[m, p] >= min(threshold, thresh2):
            bin_names.loc[m, p] = m + '_' + p
    ### second pass: identify individual cells that meet threshold
    for m in bin_names.index:
        for p in bin_names.columns:
            if n_dvcs.loc[m, p] >= threshold:
                bin_names.loc[m, p] = m+'_'+p
    ### third pass: group remaining cells by provider, ignore threshold
    for p in bin_names.columns:
        bin_names.loc[bin_names[p].isnull(), p] = 'misc_'+p
    return bin_names

########################################################################
def create_bins_v5(df, threshold=50):
    '''bin providers rather than device types'''
    n_dvcs = df.pivot_table(index='mk_mdl', columns='provider',
                            values='fsite', aggfunc=pd.Series.nunique).fillna(0)
    ### Create df to assign bins to each provider/model combination
    bin_names = pd.DataFrame([], index=n_dvcs.index, columns=n_dvcs.columns)
    ### identify individual cells that meet threshold
    for m in bin_names.index:
        for p in bin_names.columns:
            if n_dvcs.loc[m, p] >= threshold:
                bin_names.loc[m, p] = m+'_'+p
    ### group remaining cells by device
    for m in bin_names.index:
        if n_dvcs.loc[m, bin_names.loc[m].isnull()].sum() >= threshold:
            bin_names.loc[m, bin_names.loc[m].isnull()] = 'misc_'+m
    ### Group remainder together as 'other'
    for m in bin_names.index:
        bin_names.loc[m, bin_names.loc[m].isnull()] = 'other'
    return bin_names
########################################################################
def create_bins_v6(df, threshold=50):
    '''bin providers rather than device types'''
    n_dvcs = df.pivot_table(index='mk_mdl', columns='provider',
                            values='fsite', aggfunc=pd.Series.nunique).fillna(0)
    ### Create df to assign bins to each provider/model combination
    bin_names = pd.DataFrame([], index=n_dvcs.index, columns=n_dvcs.columns)
    ### identify individual cells that meet threshold
    for m in bin_names.index:
        for p in bin_names.columns:
            if n_dvcs.loc[m, p] >= threshold:
                bin_names.loc[m, p] = m+'_'+p
    ### group remaining cells by device
    for m in bin_names.index:
        bin_names.loc[m, bin_names.loc[m].isnull()] = 'misc_'+m
    ### Group remainder together as 'other'
    # for m in bin_names.index:
    #     bin_names.loc[m, bin_names.loc[m].isnull()] = 'other'
    return bin_names
########################################################################
def create_bins_v7(df, threshold=50):
    '''bin providers rather than device types'''
    n_dvcs = df.pivot_table(index='mk_mdl', columns='provider',
                            values='fsite', aggfunc=pd.Series.nunique).fillna(0)
    ### Create df to assign bins to each provider/model combination
    bin_names = pd.DataFrame([], index=n_dvcs.index, columns=n_dvcs.columns)
    ### keep youtube separate from other providers when binning by device
    for m in bin_names.index:
        bin_names.loc[m, 'youtube'] = 'misc_youtube'
        bin_names.loc[m, 'netflix'] = 'misc_netflix'
    ### identify individual cells that meet threshold
    for m in bin_names.index:
        for p in bin_names.columns:
            if n_dvcs.loc[m, p] >= threshold:
                bin_names.loc[m, p] = m+'_'+p
    ### group remaining cells by device
    for m in bin_names.index:
        bin_names.loc[m, bin_names.loc[m].isnull()] = 'misc_'+m
    return bin_names
########################################################################
def create_bins_v8(df, threshold=50):
    '''customized'''
    n_dvcs = df.pivot_table(index='mk_mdl', columns='provider',
                            values='fsite', aggfunc=pd.Series.nunique).fillna(0)
    ### Create df to assign bins to each provider/model combination
    bin_names = pd.DataFrame([], index=n_dvcs.index, columns=n_dvcs.columns)
    ### keep youtube separate from other providers but binned with itself
    for m in bin_names.index:
        bin_names.loc[m, 'youtube'] = 'misc_youtube'
        bin_names.loc[m, 'crackle'] = 'misc_crackle'
    ### separate all netflix bins
    for m in bin_names.index:
        for p in ['netflix']:
            bin_names.loc[m, p] = m+'_'+p
    ### identify individual cells that meet threshold
    # for m in bin_names.index:
    #     for p in bin_names.columns:
    #         if n_dvcs.loc[m, p] >= threshold:
    #             bin_names.loc[m, p] = m+'_'+p
    ### group remaining cells by device
    for m in bin_names.index:
        bin_names.loc[m, bin_names.loc[m].isnull()] = 'misc_'+m
    return bin_names
########################################################################
def create_bins_v9(df, threshold=50):
    '''customized'''
    n_dvcs = df.pivot_table(index='mk_mdl', columns='provider',
                            values='fsite', aggfunc=pd.Series.nunique).fillna(0)
    ### Create df to assign bins to each provider/model combination
    bin_names = pd.DataFrame([], index=n_dvcs.index, columns=n_dvcs.columns)
    ### keep youtube separate from other providers but binned with itself
    for m in bin_names.index:
        bin_names.loc[m, 'youtube'] = 'misc_youtube'
        bin_names.loc[m, 'crackle'] = 'misc_crackle'
    ### separate all netflix bins
    for m in bin_names.index:
        for p in ['netflix']:
            bin_names.loc[m, p] = m+'_'+p
    ### identify individual cells that meet threshold
    for m in bin_names.index:
        for p in bin_names.columns:
            if n_dvcs.loc[m, p] >= threshold:
                bin_names.loc[m, p] = m+'_'+p
    ### group some cells by device
    for m in ['Smart TV','Amazon Fire Stick','Play Station 4']:
        bin_names.loc[m, bin_names.loc[m].isnull()] = 'misc_'+m
    ### group remaining cells by provider
    for p in bin_names.columns:
        bin_names.loc[bin_names[p].isnull(), p] = 'misc_'+p
    return bin_names
###################### Single function containing binmodels ############
def create_bins(modeldf, threshold=50, thresh2=1, binmodel='v7'):
    if binmodel == 'v0':
        return create_bins_v0(modeldf, threshold=threshold)
    elif binmodel == 'v1':
        return create_bins_v1(modeldf, threshold=threshold)
    elif binmodel == 'v2':
        return create_bins_v2(modeldf, threshold=threshold)
    elif binmodel == 'v3':
        return create_bins_v3(modeldf, threshold=threshold)
    elif binmodel == 'v4':
        return create_bins_v4(modeldf, threshold=threshold, thresh2=thresh2)
    elif binmodel == 'v5':
        return create_bins_v5(modeldf, threshold=threshold)
    elif binmodel == 'v6':
        return create_bins_v6(modeldf, threshold=threshold)
    elif binmodel == 'v7':
        return create_bins_v7(modeldf, threshold=threshold)
    elif binmodel == 'v8':
        return create_bins_v8(modeldf, threshold=threshold)
    elif binmodel == 'v9':
        return create_bins_v9(modeldf, threshold=threshold)
    else:
        assert 1==0, 'ERROR: invalid bin model version {0}'.format(binmodel)

###################### Calculate bin properties ########################
def bin_avgs(df, bin_names):
    ### Flatten into a list of models/providers
    bin_names['mdl'] = bin_names.index
    ### Key for looking up which bin each cell belongs to
    bin_key = pd.melt(bin_names, id_vars=['mdl'], value_name='bin_name')
    ### Number of devices in each cell
    n_dvcs = df.pivot_table(index='mk_mdl', columns='provider', values='fsite', aggfunc=pd.Series.nunique).fillna(0)
    ### Df with one row per bin, to calculate the overall swot fractions and such
    bin_info = pd.DataFrame(bin_key.bin_name.value_counts())
    bin_info.columns = ['n_cells']
    bin_info['n_dvcs']      = 0
    bin_info['strm_tot']    = 0
    bin_info['tune_tot']    = 0
    for cell in bin_info.index:
        cells = bin_key.loc[bin_key.bin_name == cell]
        strm_tot = 0
        tune_tot = 0
        for row in cells.index:
            m, p = cells.loc[row, 'mdl'], cells.loc[row, 'provider']
            bin_info.loc[cell, 'n_dvcs'] += n_dvcs.loc[m, p]
            strm_tot += df.loc[(df.device == m) & (df.provider == p), 'swotstreamdur'].sum()
            tune_tot += df.loc[(df.device == m) & (df.provider == p), 'duration'].sum()
        bin_info.loc[cell, ['strm_tot','tune_tot']] = strm_tot, tune_tot
    bin_info['mn_swotfrac']  = 1. - bin_info.tune_tot/bin_info.strm_tot
    bin_info['strm_per_dvc'] = bin_info.strm_tot/bin_info.n_dvcs
    return bin_info, bin_key

########################################################################
def bin_predict(df, bin_info, bin_key):
    preds = df[['device', 'provider', 'duration', 'swotstreamdur', 'swot_frac']].copy()
    ### Assign each session to a bin
    preds['bin_name'] = bin_key.set_index(['mdl', 'provider']).loc[
                           list(zip(*[preds.device, preds.provider])), 'bin_name'].tolist()
    ### Get the predicted swot fraction for that bin
    preds['pred_swotfrac'] = bin_info.loc[preds.bin_name, 'mn_swotfrac'].tolist()
    ### Predict duration by trimming swotstreamdur by predicted swot fraction
    preds['pred_drtn'] = preds.swotstreamdur * (1.-preds.pred_swotfrac)
    return preds

########################################################################
def get_dfs(df, i):
    modeldf   = df.loc[df.month == i]
    predictdf = df.loc[df.month == (i+1)]
    return modeldf, predictdf

########################################################################
def model_swot(modeldf, predictdf, threshold=50, binmodel = 'v7'):
    bin_names = create_bins(modeldf, threshold=threshold, binmodel=binmodel)
    bin_info, bin_key = bin_avgs(modeldf, bin_names)
    preds = bin_predict(predictdf, bin_info, bin_key)
    return preds

########################################################################
def test_model_version(df, score_cats, model_versions, threshold=50):
    scores = {}
    for model in model_versions:
        testscores = pd.DataFrame([], index=score_cats,
                                  columns=range(2,7))
        # iterate over 6 months
        for i in range(1, 6):
            testscores[i+1] = calc_score(model_swot(df.loc[df.month==i],
                                                    df.loc[df.month==(i+1)],
                                                    threshold=threshold, binmodel=model))['reldiff']
        scores[model] = testscores
    return scores

#EW#######################################################################
##Run model on 3 months of data, apply to the following month
##ex: create model on Jan, Feb, Mar --> appply to April

def test_model_version_3mo(df, score_cats, model_versions, threshold=50):
    scores = {}
    for model in model_versions:
        testscores = pd.DataFrame([], index=score_cats,
                                  columns=range(4,7))
        # iterate over 6 months
        for i in range(1, 4):
            testscores[i+3] = calc_score(model_swot(df.loc[df.month<(i+3)],
                                                    df.loc[df.month==(i+3)],
                                                    threshold=threshold, binmodel=model))['reldiff']
        scores[model] = testscores
    return scores
   
# -------------------------------------------------------------------- #
# -------------------------------------------------------------------- #
# -------------------------------------------------------------------- #
def test_model_threshold(df, thresholds, score_cats, version):
    scores = {}
    for t in thresholds:
        testscores = pd.DataFrame([], index=score_cats,
                                  columns=range(2,7))
        for i in range(1, 6):
            testscores[i+1] = calc_score(model_swot(df.loc[df.month==i],
                                                    df.loc[df.month==(i+1)],
                                                    threshold=t, binmodel=version))['reldiff']
        scores[t] = testscores
    return scores

########################################################################
########################################################################
########################################################################