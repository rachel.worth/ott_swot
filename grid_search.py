### Manual model
import numpy as np
import pandas as pd
pd.set_option('display.width', 180)
import matplotlib.pyplot as plt
import seaborn as sb
import datetime as dt
import random as rd
import sys
import os

### Import data
data_dir  = ''
data_tag  = '1701-1803_new_gapfix'
plot_path = 'ModelPlots/{}/'.format(data_tag)
if not os.path.exists(plot_path):
    os.makedirs(plot_path)

########################################################################################################################
# df = pd.read_csv('{0}swot_master_table_{1}.csv'.format(data_dir, data_tag), index_col=0)

### Read in 1701-1709 and 1706-1803, combine and drop duplicates
df1 = pd.read_csv('{0}swot_master_table_1701-1709_new_gapfix.csv'.format(data_dir, data_tag), index_col=0)
df2 = pd.read_csv('{0}swot_master_table_1706-1803_new_gapfix.csv'.format(data_dir, data_tag), index_col=0)

for c in ['start_time','end_time','next_tune_start','streaming_endtime']:
    df1[c] = pd.to_datetime(df1[c])
    df2[c] = pd.to_datetime(df2[c])

### Four months overlap: cut off the last month of the earlier one, and the first 2 months of the later one
### So that the ones near the boundaries are gone and remainder should be treated identically
df1 = df1.loc[df1.start_time <  dt.datetime(2017, 9, 1, 0, 0)].copy()
df2 = df2.loc[df2.start_time >= dt.datetime(2017, 8, 1, 0, 0)].copy()

### Merge to avoid duplication (groupID and next_tune_start are expected to differ)
df = df1.merge(df2, how='outer',
               on=[c for c in df1.columns if c not in ['groupID','next_tune_start']])

assert df.loc[df.groupID_x.isnull() | df.groupID_y.isnull()]\
                    .groupby(['fsite','provider','device','start_time','end_time'])\
                    .hh_id.count().sort_values(ascending=False).iloc[0] < 2, 'ERROR: duplicate sessions'

########################################################################################################################
### Convert durations to minutes, rounded, so get rid of precision difference between data sources
df[['duration','swotdur','swotstreamdur']] = (df[['duration','swotdur','swotstreamdur']]/60).round(0)

### Add some useful columns
df['vw_frac']    = 1 - df.swotdur / df.swotstreamdur
df['swot_frac']  = df.swotdur / df.swotstreamdur
df['swot_class'] = df.swot_frac > 0  # >= 0.05
df['hh']         = df.fsite.str.split('-', 1, expand=True).iloc[:, 0]
# df['month'] = pd.to_datetime(df.start_time).map(lambda t: t.month)
df['start_time'] = pd.to_datetime(df.start_time)
df['end_time']   = pd.to_datetime(df.end_time)
# df['month_n']      = df.start_time.dt.strftime('%y-%m')
# month_key = pd.Series(df.month_n.sort_values().unique())

### Roll up roku devices into one group
df.loc[df.device.str.contains('Roku'), 'device'] = 'Roku'

### Combine census providers into one
### Reclassify census providers as one
df['orig_prv'] = df.provider.copy()
df.loc[df.provider.isin(['cw','crackle','directv','dtv','discovery']), 'provider'] = 'census'

### Drop youtube due to its much lower swot fraction
df = df.loc[df.provider != 'youtube']

########################################################################################################################
########################################################################################################################
########################################################################################################################
### Want a list of dates, skipping those which are missing data
df['date']  = pd.to_datetime(df.start_time - dt.timedelta(hours=3)).dt.strftime('%y-%m-%d')
df['hour']  = pd.to_datetime(df.start_time).dt.hour
df['date-hour']  = df.start_time.dt.floor('h')
df.groupby(['date','hour']).hh_id.count()

########################################################################
### Create "months", each containing 30 days of complete data (skipping missing days)
month_map = get_month_map(df)
if 'month' in df.columns:
    df = df.drop('month', axis=1)
df = df.merge(month_map, on='date', how='left')
df = df.loc[df.month.notnull()]

########################################################################################################################
########################################################################################################################
########################################################################################################################
# ### Count devices per model/provider cell
# n_dvcs = df.pivot_table(index='device', columns='provider', values='fsite', aggfunc=pd.Series.nunique).fillna(0)
#
# all_minutes = df.duration.sum()
# ### Panel provider market shares
# prov_frac = df.groupby('provider').duration.sum().sort_values(ascending=False)/all_minutes*100
# orig_prv_sum = df.loc[df.provider=='census'].groupby('orig_prv').duration.sum().sort_values(ascending=False)
# orig_prv_frac = orig_prv_sum/orig_prv_sum.sum()*100
#
# pal = sb.color_palette("Set1", df.orig_prv.nunique()+1, desat=0.7)
# # pal = sb.color_palette("Set1", 2, desat=0.7)*(int(df.orig_prv.nunique()/2)+1)
# f1, ax1 = plt.subplots(1, figsize=[20, 10])
# for i, p in enumerate(prov_frac.index):
#     if i == 0:
#         left = 0
#     else:
#         left = prov_frac.iloc[:i].sum()
#     ax1.barh(1, prov_frac.loc[p], color=pal[i], left=left, label=p)
#     ax1.text(s='{0}: {1:0.1f}%'.format(p, prov_frac.loc[p]),
#              x=left + prov_frac.loc[p]/2., y=1, rotation=90, va='center', ha='center')
# for j, p in enumerate(orig_prv_frac.index):
#     if j == 0:
#         left = 0
#     else:
#         left = orig_prv_frac.iloc[:j].sum()
#     ax1.barh(0, orig_prv_frac.loc[p], color=pal[i+j+1], left=left, label=p)
#     ax1.text(s='{0}: {1:0.1f}%'.format(p, orig_prv_frac.loc[p]),
#              x=left + orig_prv_frac.loc[p]/2, y=0, rotation=90, va='center', ha='center')
# # ax1.legend(loc='upper right', frameon=True, framealpha=0.5)
# ax1.plot([0,prov_frac.drop('census').sum()], [.4, .6], c='black', lw=1)
# ax1.plot([100,100], [.4, .6], c='black', lw=1)
# ax1.set_title('Provider market share in OTT panel vs. census')
# ax1.set_yticks([0, 1]);
# ax1.set_yticklabels(['Panel: census providers', 'Panel: all providers']);
# plt.tight_layout()
# f1.savefig('ModelPlots/CensusModel/prov_marketshare.png')
# plt.close(f1)

########################################################################
###################### Grid Search #####################################
########################################################################
versions  = ['v0', 'dvc', 'dvcbin', 'drtn-v0', 'drtn-dvc', 'drtn-dvcbin']
# versions = ['drtn-dvcbin','dvcbin']
n_months  = [1, 2, 3, 4, 5, 6]
months_possible = month_map.month.unique()[max(n_months):]

# all_months_i = list(month_key.index)
# all_months_n = list(month_key)

dvc_list = list(df.device.value_counts().index)
prv_list = list(df.provider.value_counts().index)

binsize_list   = [25, 50, 100, 150, 100000]
smoothing_list = [100, 1000, 5000, 10000]
flatten_list   = [1, 2, 3, 4, 5]

# -------------------------------------------------------------------- #
### Calculate model scores over full parameter grid (takes a long time)
### Add to existing
# grid_scores = pd.read_csv('ModelPlots/CensusModel/grid_scores.csv', index_col=[0, 1, 2, 3, 4, 5])
### Start from scratch
grid_scores = pd.DataFrame([], columns = ['version','n_months','month','binsize','smoothing','flatten','all']+prv_list+dvc_list)
grid_scores = grid_scores.set_index(['version','n_months','month','binsize','smoothing','flatten'])

for v in versions:
    if any([vv in v for vv in ['v2','v3','v4','v5','v6','v7','v8','v9']]) | ('dvcbin' in v):
        binsizes = binsize_list
    else:
        binsizes = [-1]
    if 'drtn' in v:
        smoothing = smoothing_list
        flatten   = flatten_list
    else:
        smoothing = [-1]
        flatten   = [-1]
    for n in n_months:
        # months_possible = month_map.month.unique()[n:]
        for m in months_possible:
            for b in binsizes:
                for s in smoothing:
                    for x in flatten:
                        ind = (v, n, m, b, s, x)
                        if (ind not in grid_scores.index):
                            print('done:', ind)
                            modelled = model_swot(*get_dfs(df, test_month=m, n_train_months=n),
                                                  model_vers=v, threshold=b, smoothing=s, flatten_multiplier=x)
                            ### Store the rms error per device and per provider
                            grid_scores.loc[ind, ['all'] + prv_list] = calc_score(modelled, groupcol='provider')['reldiff']
                            grid_scores.loc[ind,           dvc_list] = calc_score(modelled, groupcol='device')['reldiff']
                        # else:
                        #     print('skip:', ind)

grid_scores = grid_scores.sort_index()
grid_scores.to_csv('{}grid_scores.csv'.format(plot_path))

########################################################################
######################     Plots   #####################################
########################################################################
### RMS over months, 'all', score only
### Want same number of samples from each, so only take the months that are available for all?
months_possible = month_map.month.unique()[max(n_months):]
idx = pd.IndexSlice
grid_scores_rms = grid_scores.loc[idx[:,:, months_possible, :, :, :], :]\
                             .groupby(['version','n_months','smoothing','flatten']).agg({'all': rms}).reset_index()

cmap = 'YlGnBu' #sb.diverging_palette(220, 10, as_cmap=True)
cmap_vmax, cmap_vmin = grid_scores_rms['all'].max(), grid_scores_rms['all'].min()
for v in versions:
    for n in n_months:
        f1, ax1 = plt.subplots(1)
        pane = grid_scores_rms.loc[(grid_scores_rms.version == v) & (grid_scores_rms.n_months == n)] \
                              .pivot_table(index='flatten', columns='smoothing', values='all')
        sb.heatmap(pane, cmap=cmap, vmin=cmap_vmin, vmax=cmap_vmax, square=True, linewidths=0.2, ax=ax1)
        ax1.set_xticklabels([int(x) for x in pane.columns])
        ax1.set_yticklabels([int(y) for y in pane.index[::-1]], rotation=0)
        ### mark square(s) with the best value for this model
        best_val = pane.min().min()
        best_coords = np.array(pane[pane == best_val].stack().index.labels)
        if pane.shape[1] == 1:
            for i in best_coords[0]:
                ax1.scatter(0.5, -i + pane.shape[0] - 0.5, marker='x', c='black')
        elif pane.shape[0] == 1:
            for i in best_coords[1]:
                ax1.scatter(i + 0.5, 0.5, marker='x', c='black')
        else:
            for i in range(best_coords.shape[1]):
                ax1.scatter( best_coords[1, i] + 0.5,
                            -best_coords[0, i] + pane.shape[0]-0.5, marker='x', c='black')
        ax1.set_title('{0}, {1} months: min = {2:0.5f}'.format(v, n, best_val))
        f1.savefig('{}grid_scores_{}_n{}.png'.format(plot_path, v, n))
        plt.close(f1)

# -------------------------------------------------------------------------------- #
### RMS over months, 'all', score only
grid_scores_best = grid_scores_rms.pivot_table(index='n_months',columns='version', values='all', aggfunc='min')
grid_scores_best = grid_scores_best[versions]

f1, ax1 = plt.subplots(1)
sb.heatmap(grid_scores_best, cmap=cmap, square=True, linewidths=0.2, ax=ax1)
ax1.set_xticklabels([x for x in grid_scores_best.columns], rotation=30)
ax1.set_yticklabels([y for y in grid_scores_best.index[::-1]], rotation=0)
best_val = grid_scores_best.min().min()
best_coords = np.array(grid_scores_best[grid_scores_best == best_val].stack().index.labels)
for i in range(best_coords.shape[1]):
    ax1.scatter( best_coords[1, i] + 0.5,
                -best_coords[0, i] + grid_scores_best.shape[0] - 0.5, marker='x', c='black')
ax1.set_title('Best score from each model vs. # months')
f1.savefig('{}grid_scores_best.png'.format(plot_path))
plt.close(f1)
# ########################################################################
# ### See values for specific sections?
# # ind = v, n, m, b, s, x
# print(pd.DataFrame({
#     'n2s50':  grid_scores.loc[('drtn-dvc', 2, slice(1, 10), 25, 50,  slice(1,5)), ['all']].reset_index().groupby('flatten').agg({'all':rms})['all'],
#     'n3s50':  grid_scores.loc[('drtn-dvc', 3, slice(1, 10), 25, 50,  slice(1,5)), ['all']].reset_index().groupby('flatten').agg({'all':rms})['all'],
#     'n2s500': grid_scores.loc[('drtn-dvc', 2, slice(1, 10), 25, 500, slice(1,5)), ['all']].reset_index().groupby('flatten').agg({'all':rms})['all'],
#     'n3s500': grid_scores.loc[('drtn-dvc', 3, slice(1, 10), 25, 500, slice(1,5)), ['all']].reset_index().groupby('flatten').agg({'all':rms})['all']}))
#
# ########################################################################
# ### Error vs. amount of smoothing
# n_mn = 3
# test_binsize = 25
# test_flatten = 2
# all_mn_slice = slice(min(all_months_i), max(all_months_i))
#
# # if test_binsize not in grid_scores.reset_index().binsize.unique():
# #     grid_scores.loc[('fake',-1, -1, 150, -1)] = np.nan
# #     grid_scores = grid_scores.sort_index()
#
# agg_dict_prv = {}
# for p in ['all'] + prv_list:
#     agg_dict_prv[p] = rms
# agg_dict_dvc = {}
# for p in dvc_list:
#     agg_dict_dvc[p] = rms
#
# prv_v_smth = {}
# dvc_v_smth = {}
# for v in versions:
#     prv_v_smth[v] = pd.concat([grid_scores.loc[(v, n_mn, all_mn_slice, test_binsize, slice(-1, max(smoothing_list)), test_flatten), ['all'] + prv_list],
#                                grid_scores.loc[(v, n_mn, all_mn_slice, test_binsize, slice(-1, max(smoothing_list)),           -1), ['all'] + prv_list]])\
#                       .reset_index().groupby(['version','smoothing']).agg(agg_dict_prv)
#     prv_v_smth[v].index = prv_v_smth[v].index.droplevel(0)
#     # prv_v_smth[v] = grid_scores.loc[(v, n_mn, all_mn_slice, test_binsize, slice(-1, max(smoothing_list)), test_flatten), ['all'] + prv_list]\
#     #                   .reset_index().groupby(['version','smoothing']).agg(agg_dict_prv)
#     dvc_v_smth[v] = pd.concat([grid_scores.loc[(v, n_mn, all_mn_slice, test_binsize, slice(-1, max(smoothing_list)), test_flatten), dvc_list],
#                                grid_scores.loc[(v, n_mn, all_mn_slice, test_binsize, slice(-1, max(smoothing_list)),           -1), dvc_list]])\
#                       .reset_index().groupby(['version','smoothing']).agg(agg_dict_dvc)
#     dvc_v_smth[v].index = dvc_v_smth[v].index.droplevel(0)
#     # dvc_v_smth[v] = grid_scores.loc[(v, n_mn, all_mn_slice, test_binsize, slice(-1, max(smoothing_list)), test_flatten), dvc_list]\
#     #                   .reset_index().groupby(['version','smoothing']).agg(agg_dict_dvc)
#
# colors = sb.color_palette("Set1", len(versions), desat=0.7)
# # ---------------------------------------------------------------------- #
# for p in ['all','census']:
#     f1, ax1 = plt.subplots(1)
#     for j, v in enumerate(versions):
#         if prv_v_smth[v].shape[0] > 1:
#             x, y = prv_v_smth[v].index, 100. * prv_v_smth[v][p]
#         else:
#             x, y = smoothing_list, [100. * prv_v_smth[v][p].iloc[0] for b in smoothing_list]
#         ax1.plot(x, y, c=colors[j],
#                  label='{0}: {1: 0.2f} at {2}'.format(v, 100. * prv_v_smth[v][p].min(),
#                                                       prv_v_smth[v][p].idxmin()))
#     ax1.set_xscale('log')
#     ax1.set_xticks(smoothing_list)
#     ax1.set_xticklabels(smoothing_list, rotation=30)
#     ax1.legend(loc='upper right', frameon=True, framealpha=0.7)
#     ax1.set_xlabel('Smoothing window (# events)')
#     ax1.set_ylabel('RMS error')
#     ax1.set_title('Error vs. window size ({0}), by model version (bin threshold = {1}, smoothing multiplier = {2})'.format(p, test_binsize, test_flatten))
#     plt.savefig('ModelPlots/CensusModel/model_v_smoothing_{0}_b{1}_m{2}_x{3}.png'.format(p, test_binsize, n_mn, test_flatten))
#     plt.close(f1)
#
# # ---------------------------------------------------------------------- #
# f1, ax1 = plt.subplots(4, 5, sharex=True, sharey=True, figsize = [21,10])
# f1.subplots_adjust(hspace=0.1, wspace=0.05, right=.99, bottom=.07, top=.94, left=0.025)
# ax = ax1.flatten()
# for i, p in enumerate(['all'] + prv_list):
#     ax[i].axhline(0, c='black', alpha=0.25)
#     if p == 'all':
#         ax[i].set_title('{0} ({1})'.format(p, int(n_dvcs.sum().sum())))
#     else:
#         ax[i].set_title('{0} ({1})'.format(p, int(n_dvcs[p].sum())))
#     for j, v in enumerate(versions):
#         if prv_v_smth[v].shape[0] > 1:
#             x, y = prv_v_smth[v].index, 100. * prv_v_smth[v][p]
#         else:
#             x, y = smoothing_list, [100. * prv_v_smth[v][p].iloc[0] for b in smoothing_list]
#         ax[i].plot(x, y, c=colors[j],
#                    label='{0}: {1: 0.2f} at {2}'.format(v, 100. * prv_v_smth[v][p].min(),
#                                                         prv_v_smth[v][p].idxmin()))
#     ax[i].legend(loc='upper right', ncol=1, frameon=True, framealpha=0.7)
# oldi = i
# for j, d in enumerate(pd.Index(dvc_list)[[0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 12, 15]]):
#     i = j+oldi+1
#     ax[i].axhline(0, c='black', alpha=0.25)
#     ax[i].set_title('{0} ({1})'.format(d, int(n_dvcs.loc[d].sum())))
#     for k, v in enumerate(versions):
#         if dvc_v_smth[v].shape[0] > 1:
#             x, y = dvc_v_smth[v].index, 100. * dvc_v_smth[v][d]
#         else:
#             x, y = smoothing_list, [100. * dvc_v_smth[v][d].iloc[0] for b in smoothing_list]
#         ax[i].plot(x, y, c=colors[k],
#                    label='{0}: {1: 0.2f} at {2}'.format(v, 100. * dvc_v_smth[v][d].min(),
#                                                         dvc_v_smth[v][d].idxmin()))
#     ax[i].set_xlim(0.95 * smoothing_list[0], 1.2 * smoothing_list[-2])
#     ax[i].set_xscale('log')
#     ax[i].legend(loc='upper right', ncol=1, frameon=True, framealpha=0.7)
#     ax[i].set_xticks(smoothing_list)
#     ax[i].set_xticklabels(smoothing_list, rotation=30)
# ax[0].set_ylim(0, 5)
# f1.text(0.01, 0.5, 'Error (%)', ha='center', va='center', rotation='vertical', fontsize=14)
# f1.text(0.5, 0.015, 'Smoothing window (# events)', ha='center', va='center', fontsize=14)
# f1.text(0.5, 0.98, 'RMS monthly error in total viewed minutes, using previous month to predict SWOT %',
#         ha='center', va='center', fontsize=16)
# plt.savefig('ModelPlots/CensusModel/model_v_smoothing_b{0}_m{1}_x{2}.png'.format(test_binsize, n_mn, test_flatten))
# plt.close(f1)
#
# ########################################################################
# ########################################################################
# ### Why are census rms scores *so* bad?
#
# ind = grid_scores['census'].idxmax()
# v, n, m, b, s, x = ind
# modelled = model_swot(*get_dfs(df, m, n),
#                       model_vers=v, threshold=b, smoothing=s, flatten_multiplier=x)
#
# df['end_time'] = pd.to_datetime(df.end_time)
# df['next_stream_start'] = pd.to_datetime(df.next_stream_start)
# df['gap'] = (df.next_stream_start - df.end_time)/dt.timedelta(seconds=1)
#
# worst_census = modelled.loc[modelled.provider=='census'].copy()
# worst_census['err'] = worst_census.pred_drtn - worst_census.duration
# worst_census['err_abs'] = worst_census.err.abs()
# worst_census = worst_census.sort_values('err_abs')
#
# ########################################################################
# ########################################################################
# ### Google Chromecast for modeldf in predicting June-Aug has really high swot fraction,
# ### and it's higher when flattened than not. How?
# ### the very last session is really long and almost all swot
# testdf = get_dfs(df, 6, 3)[0]
# testdf = testdf.loc[testdf.device == 'Google Chromecast'].sort_values('swotstreamdur')
#
# testdf['cm_strm'] = testdf.swotstreamdur.cumsum()
# testdf['cm_tune'] = testdf.duration.cumsum()
# testdf['cm_swtfrac'] = testdf.cm_tune/testdf.cm_strm
# ########################################################################
# ################## Show where error is #################################
# ########################################################################
# ### Parameters to use for all versions
# n_mn        = 3
# test_binsize    = 25
# test_smoothing  = 500
# test_multiplier = 2
#
# ### Hardcoded list of bin edges
# bin_drtns = list(np.arange(0, 15, 1)) + list(np.arange(15, 50, 5)) + list(np.arange(50, 100, 10)) + [100, 150, 200, 300, 400, 500, 1000, df.swotstreamdur.max()+1]
# ### Using this as the colormap maximum, for consistency (rerun all if adjusting)
# vmax=25000
#
# months   = all_months_i[(n_mn-1):]
# err_distrns_by_month = {}
# for v in versions:
#     for month in months:
#         modeldf, predictdf = get_dfs(df, month, n_mn)
#         preds = model_swot(modeldf, predictdf, model_vers = v, threshold=test_binsize, smoothing=test_smoothing, flatten_multiplier=test_multiplier)
#         preds['err'] = preds.pred_drtn - preds.duration
#         preds['drtn_bin'] = pd.cut(preds.swotstreamdur, bins=bin_drtns)
#         ### Get error by provider
#         prv_err_mins = preds.pivot_table(index='provider', columns='drtn_bin', values='err', aggfunc='sum')
#         prv_err_mins = prv_err_mins.loc[prv_list]
#         ### Get error by device
#         dvc_err_mins = preds.pivot_table(index='device', columns='drtn_bin', values='err', aggfunc='sum')
#         dvc_err_mins = dvc_err_mins.loc[dvc_list]
#         ### Combine into one df
#         err_mins = pd.concat([prv_err_mins, dvc_err_mins])
#         err_mins = err_mins.iloc[::-1]
#         err_mins.columns = [i.right.astype('int') for i in err_mins.columns]
#         err_mins['total']     = err_mins.sum(axis=1)
#         err_mins.loc['total'] = err_mins.sum(axis=0)
#         err_mins.loc['total','total'] = err_mins.loc[prv_list,'total'].sum()
#         err_distrns_by_month[(v, month)] = err_mins
#         print('{0}: maximum error = {1}, color max = {2}'.format(v, err_distrns_by_month[(v, month)].abs().max().max(), vmax))
#
# err_distrns = {}
# for v in versions:
#     monthlies = [err_distrns_by_month[(v, m)] for m in months]
#     err_distrns[v] = monthlies[0].fillna(0)
#     for monthly in monthlies[1:]:
#         err_distrns[v] += monthly.fillna(0)
#     for c in err_distrns[v].columns:
#         err_distrns[v].loc[err_distrns[v][c] == 0, c] = np.nan
#     print(
#         '{0}: maximum error = {1}, color max = {2}'.format(v, err_distrns[v].abs().max().max(), vmax))
#
# total_drtn = df.loc[df.month.between(min(months), max(months)), 'duration'].sum()
#
# # ---------------------------------------------------------------------- #
# for v in versions:
#     f1, ax1 = plt.subplots(1, figsize=[20, 12])
#     sb.heatmap(err_distrns[v], cmap='bwr', center=0, vmax=vmax, vmin=-vmax, square=True, linewidths=0.1, ax=ax1)
#     ax1.axhline(1, c='black')
#     ax1.axhline(len(prv_list)+1, c='black')
#     ax1.axvline(err_distrns[v].shape[1]-1, c='black')
#     ax1.set_xticklabels(err_distrns[v].columns, rotation=30)
#     ax1.set_yticklabels([y for y in err_distrns[v].index[::-1]], rotation=0)
#     ax1.set_xlabel('Duration (right edge of bin)', fontsize=14)
#     ax1.set_ylabel('Category (provider or device)', fontsize=14)
#     ax1.set_title('Absolute error distribution; total = ' +
#                   '{0:0.1f} mins off ({1:0.2f}%)\n'.format(err_distrns[v].loc['total','total'],
#                                                            err_distrns[v].loc['total', 'total']/total_drtn*100.) +
#                   ' ({0}, b{1}, s{2}, m{3})'.format(v, test_binsize, test_smoothing, n_mn), fontsize=14)
#     plt.tight_layout()
#     f1.savefig('ModelPlots/CensusModel/error_distributions_{0}_b{1}_s{2}_m{3}.png'.format(v, test_binsize, test_smoothing, n_mn))
#     plt.close(f1)
# # ---------------------------------------------------------------------- #
# ### Plot each month
# # vers='v4'
# # for month in months:
# #     f1, ax1 = plt.subplots(1, figsize=[20, 12])
# #     sb.heatmap(err_distrns_by_month[(vers, month)], cmap='bwr', center=0, vmax=vmax/4., vmin=-vmax/4., square=True, linewidths=0.1, ax=ax1)
# #     ax1.axhline(1, c='black')
# #     ax1.axhline(len(prv_list)+1, c='black')
# #     ax1.axvline(err_distrns_by_month[(vers, month)].shape[1]-1, c='black')
# #     ax1.set_xticklabels(err_distrns_by_month[(vers, month)].columns, rotation=30)
# #     ax1.set_yticklabels([y for y in err_distrns_by_month[(vers, month)].index[::-1]], rotation=0)
# #     ax1.set_xlabel('Duration (right edge of bin)', fontsize=14)
# #     ax1.set_ylabel('Category (provider or device)', fontsize=14)
# #     ax1.set_title('Absolute error distribution; total = ' +
# #                   '{0:0.1f} mins off ({1:0.2f}%)\n'.format(err_distrns_by_month[(vers, month)].loc['total', 'total'],
# #                                                            err_distrns_by_month[(vers, month)].loc['total', 'total']/total_drtn*100./4.) +
# #                   ' ({0}, b{1}, s{2}, m{3}-{4})'.format(vers, test_binsize, test_smoothing, n_mn, month), fontsize=14)
# #     plt.tight_layout()
# #     f1.savefig('ModelPlots/CensusModel/error_distributions_{0}_b{1}_s{2}_m{3}-{4}.png'.format(vers, test_binsize, test_smoothing, n_mn, month))
# #     plt.close(f1)
# #
########################################################################
################### Plot drtn curve shapes #############################
########################################################################
binmodel  = 'dvc'
# n_mn      = 3
binsize   = -1
# months_possible = month_map.month.unique()[n_mn:]
months_possible = month_map.month.unique()[max(n_months):]
# month_i   = all_months_i[(n_mn-1):]
# month_lab = all_months_n[(n_mn-1):]

### Plot for each flattening multiplier
for n_mn in [3, 4]:
    ### Use same bins for all months
    modeldf, predictdf = get_dfs(df, months_possible[-1], n_mn)
    bin_key = get_bin_key(modeldf, binmodel=binmodel, threshold=binsize)
    for flatten, smoothing in [(1, 5000), (2, 5000)]:#[(1, 50), (4, 50), (1, 1000), (2, 500), (4, 250), (4, 500)]:
        print(flatten, smoothing)
        ### Generate duration curves for all months
        lookups = {}
        for k in months_possible:
            modeldf = get_dfs(df, k, n_train_months=n_mn)[0]
            lookups[k] = fit_model(modeldf, bin_key, smoothing=smoothing, flatten_multiplier=flatten)
        f1, ax1 = plt.subplots(*layout(bin_key.bin_name.sort_values().nunique()), figsize=[18, 10], sharex=True, sharey=True)
        ax2 = ax1.flatten()
        for i, c in enumerate(bin_key.bin_name.sort_values().unique()):
            for j in months_possible:
                modeldf = get_dfs(df, j, n_train_months=n_mn)[0]
                ax2[i].plot(lookups[j].index, lookups[j][c],
                            label='mn {0}: {1} events'.format(j,
                                                           modeldf.loc[
                                               (modeldf.provider.isin(bin_key.loc[bin_key.bin_name==c].provider)) &
                                               (modeldf.device.isin(  bin_key.loc[bin_key.bin_name==c].device  ))].shape[0]))
            ax2[i].legend(loc='upper right', frameon=True, framealpha=0.6)
            ax2[i].set_title(c)
            ax2[i].set_xscale('log')
        f1.text(0.5, 0.01, 'Streaming Duration (min)', ha='center', va='center')
        f1.text(0.01, 0.5, 'SWOT Fraction', ha='center', va='center', rotation=90)
        plt.tight_layout()
        f1.savefig('{0}drtn_curves_{1}_b{2}_s{3}x{4}_m{5}.png'.format(plot_path, binmodel, binsize, smoothing, flatten, n_mn))
        plt.close(f1)

########################################################################




########################################################################
################### Pred vs true swot curves ###########################
########################################################################
v = 'drtn-dvc'
binsize=50
smoothing=250
n_mn = 3
months_possible = month_map.month.unique()[n_mn:]

for m in months_possible:
    modeldf, predictdf = get_dfs(df, m, n_mn)
    preds = model_swot(modeldf, predictdf, model_vers=v, threshold=binsize, smoothing=smoothing)
    preds['err'] = preds.pred_drtn - preds.duration

    preds['strm_drtn'] = preds.swotstreamdur.copy()
    strm_tot = preds.pivot_table(index='swotstreamdur', columns='device', values='strm_drtn', aggfunc='sum')
    tune_tot = preds.pivot_table(index='swotstreamdur', columns='device', values='duration',  aggfunc='sum')
    pred_tot = preds.pivot_table(index='swotstreamdur', columns='device', values='pred_drtn', aggfunc='sum')

    true_mn = 1. - tune_tot / strm_tot
    pred_mn = 1. - pred_tot / strm_tot

    f1, ax1 = plt.subplots(2, 3, figsize=[20, 12])
    ax2 = ax1.flatten()
    for i, d in enumerate(dvc_list[:6]):
        ax2[i].plot(true_mn.index, true_mn[d], 'bo',   alpha=0.5, label='_nolabel_')
        ax2[i].plot(true_mn.index, true_mn[d], 'blue', alpha=0.5, label='True')
        ax2[i].plot(pred_mn.index, pred_mn[d], 'ro',   alpha=0.5, label='_nolabel_')
        ax2[i].plot(pred_mn.index, pred_mn[d], 'red',  alpha=0.5, label='Predicted')
        ax2[i].set_title(d)
        ax2[i].set_xscale('log')
        ax2[i].set_xlim(1, df.swotstreamdur.max())
        ax2[i].set_ylim(0, 1)
    ax2[0].legend(loc='upper left')
    f1.savefig('{0}true_vs_pred_by_drtn_{1}_m{2}-{3}_b{4}_s{5}.png'.format(plot_path, v, n_mn, m, binsize, smoothing))
    plt.close(f1)

########################################################################
### True vs. pred duration curves for census only
v = 'drtn-dvc'
binsize=50
smoothing=250
n_mn = 3
months_possible = month_map.month.unique()[n_mn:]

# months     = all_months_i[(n_mn-1):]
# month_labs = all_months_n[(n_mn-1):]

true_mn = {}
pred_mn = {}
for i, m in enumerate(months_possible):
    modeldf, predictdf = get_dfs(df, m, n_mn)
    preds = model_swot(modeldf, predictdf, model_vers=v, threshold=binsize, smoothing=smoothing)
    preds['err'] = preds.pred_drtn - preds.duration

    preds['strm_drtn'] = preds.swotstreamdur.copy()
    strm_tot = preds.pivot_table(index='swotstreamdur', columns='provider', values='strm_drtn', aggfunc='sum')
    tune_tot = preds.pivot_table(index='swotstreamdur', columns='provider', values='duration',  aggfunc='sum')
    pred_tot = preds.pivot_table(index='swotstreamdur', columns='provider', values='pred_drtn', aggfunc='sum')

    true_mn[m] = 1. - tune_tot / strm_tot
    pred_mn[m] = 1. - pred_tot / strm_tot

for p in prv_list:
    f1, ax1 = plt.subplots(*layout(len(months_possible)), figsize=[20, 12], sharex=True, sharey=True)
    ax2 = ax1.flatten()
    for i, m in enumerate(months_possible):
        ax2[i].plot(true_mn[m].index, true_mn[m][p], 'bo',   alpha=0.5, label='_nolabel_')
        ax2[i].plot(true_mn[m].index, true_mn[m][p], 'blue', alpha=0.5, label='True')
        ax2[i].plot(pred_mn[m].index, pred_mn[m][p], 'ro',   alpha=0.5, label='_nolabel_')
        ax2[i].plot(pred_mn[m].index, pred_mn[m][p], 'red',  alpha=0.5, label='Predicted')
        ax2[i].set_title('Month {}'.format(i))
        ax2[i].set_xscale('log')
        ax2[i].set_xlim(1, df.swotstreamdur.max())
        ax2[i].set_ylim(0, 1)
    ax2[0].legend(loc='upper left')
    f1.text(0.5, 0.97, p, fontsize=14, ha='center')
    f1.savefig('{0}true_vs_pred_{1}_m{2}_b{4}_s{5}_{6}.png'.format(plot_path, v, n_mn, m, binsize,
                                                                                           smoothing, p))
    plt.close(f1)

########################################################################
### True vs. pred duration curves for census only - all months combined
v = 'drtn-dvc'
binsize = 50
smoothing = 250
n_mn = 3
months_possible = month_map.month.unique()[n_mn:]
# months = all_months_i[(n_mn - 1):]
# month_labs = all_months_n[(n_mn - 1):]

for i, m in enumerate(months_possible):
    modeldf, predictdf = get_dfs(df, m, n_mn)
    preds_this_month = model_swot(modeldf, predictdf, model_vers=v, threshold=binsize, smoothing=smoothing)
    preds_this_month['err'] = preds_this_month.pred_drtn - preds_this_month.duration
    preds_this_month['strm_drtn'] = preds_this_month.swotstreamdur.copy()
    preds_this_month['month'] = m
    if i == 0:
        all_preds = preds_this_month.copy()
    else:
        all_preds = pd.concat([all_preds, preds_this_month])

strm_tot = all_preds.pivot_table(index='swotstreamdur', columns='provider', values='strm_drtn', aggfunc='sum')
tune_tot = all_preds.pivot_table(index='swotstreamdur', columns='provider', values='duration',  aggfunc='sum')
pred_tot = all_preds.pivot_table(index='swotstreamdur', columns='provider', values='pred_drtn', aggfunc='sum')

true_mn = 1. - tune_tot / strm_tot
pred_mn = 1. - pred_tot / strm_tot

f1, ax1 = plt.subplots(2, 3, figsize=[20, 12], sharex=True, sharey=True)
ax2 = ax1.flatten()
for i, p in enumerate(prv_list):
    ax2[i].plot(true_mn.index, true_mn[p], 'bo',   alpha=0.5, label='_nolabel_')
    ax2[i].plot(true_mn.index, true_mn[p], 'blue', alpha=0.5, label='True')
    ax2[i].plot(pred_mn.index, pred_mn[p], 'ro',   alpha=0.5, label='_nolabel_')
    ax2[i].plot(pred_mn.index, pred_mn[p], 'red',  alpha=0.5, label='Predicted')
    ax2[i].set_title(p)
    ax2[i].set_xscale('log')
    ax2[i].set_xlim(1, df.swotstreamdur.max())
    ax2[i].set_ylim(0, 1)
ax2[0].legend(loc='upper left')
f1.text(0.5, 0.97, 'All months combined', fontsize=14, ha='center')
f1.savefig('{0}true_vs_pred_{1}_m{2}_b{3}_s{4}.png'.format(plot_path, v, n_mn, binsize, smoothing))
plt.close(f1)

########################################################################
########################################################################