import datetime as dt
import pandas     as pd
import numpy as np
import random as rd
import matplotlib.pyplot as plt
import matplotlib as mpl
import seaborn as sb
pd.set_option('display.width', 175)

def plot_ssn_rows_old(this_dvc,
                  ssn_sttm1_col = 'starttimeest', ssn_edtm1_col = 'endtimeest',
                  ssn_sttm2_col = 'start_time', ssn_edtm2_col = 'end_time',
                  ssn_id='strm_ssn_id',
                  titl=''):
    if ssn_id:
        ssns = this_dvc.groupby(ssn_id).agg({ssn_sttm1_col: 'min',
                                             ssn_edtm1_col: 'max',
                                             ssn_sttm2_col: 'min',
                                             ssn_edtm2_col: 'max'})
        ssns['sttm'] = ssns[[ssn_sttm1_col, ssn_sttm2_col]].min(axis=1)
        ssns['edtm'] = ssns[[ssn_edtm1_col, ssn_edtm2_col]].max(axis=1)

    ssn_sttm1 = this_dvc[ssn_sttm1_col]
    ssn_edtm1 = this_dvc[ssn_edtm1_col]
    ssn_sttm2 = this_dvc[ssn_sttm2_col]
    ssn_edtm2 = this_dvc[ssn_edtm2_col]

    x_range = (min(ssn_sttm1.min(), ssn_sttm2.min()) - dt.timedelta(seconds=1),
               max(ssn_edtm1.max(), ssn_edtm2.max()) + dt.timedelta(seconds=1))
    y_range = (this_dvc.index.min()-0.5, this_dvc.index.max()+0.5)

    pal = sb.color_palette("deep")

    f1, ax1 = plt.subplots(1, figsize=[10, 10])
    f1.autofmt_xdate()
    ax1.set_xlim(x_range)
    ax1.set_ylim(y_range)
    ### streaming
    for i in this_dvc.index:
        ax1.plot([ssn_sttm1.loc[i], ssn_edtm1.loc[i]], [i, i],
                 alpha=0.5, lw=1, color='red')
        ax1.plot([ssn_sttm2.loc[i], ssn_edtm2.loc[i]], [i, i],
                 alpha=0.5, lw=1, color='blue')
        ### very small scatter points in case the line is too short to plot anything
        ax1.scatter([ssn_sttm1.loc[i]], [i],
                    alpha=0.5, s=1, color='red')
        ax1.scatter([ssn_sttm2.loc[i]], [i],
                    alpha=0.5, s=1, color='blue')
    if ssn_id:
        for it, i in enumerate(ssns.index):
            ax1.axvspan(ssns.loc[i, 'sttm'], ssns.loc[i, 'edtm'],
                        alpha=0.15, facecolor=pal[it%3], label='__nolegend__')
    ax1.xaxis.set_major_formatter(mpl.dates.DateFormatter('%y-%m-%d %H:%M'));
    # ax1.set_xticks([t for t in pd.date_range(*x_range, freq='s') if (t.minute == 0) & (t.second==0)]);
    ax1.set_title(titl)
    # ax1.set_xlabel('Start and end times')
    ax1.set_ylabel('Line number')
    plt.tight_layout()
    return f1, ax1

def plot_ssn_rows(this_dvc,
                  ssn_sttms = ['starttimeest', 'start_time'],
                  ssn_edtms = ['endtimeest',   'end_time'],
                  cols = ['red', 'blue'],
                  ssns=None, ssn_id=None,
                  titl=''):
    ### If ssn id col provided, but not ssn DF, create ssn DF
    if (ssns is None) and (ssn_id is not None):
        ssn_aggs = {}
        for t in ssn_sttms:
            ssn_aggs[t] = 'min'
        for t in ssn_edtms:
            ssn_aggs[t] = 'max'
        ssns = this_dvc.groupby(ssn_id).agg(ssn_aggs)
        ssns['sttm'] = ssns[ssn_sttms].min(axis=1)
        ssns['edtm'] = ssns[ssn_edtms].max(axis=1)
    ### Get color palette to use in plotting shaded regions
    pal = sb.color_palette("deep")
    ### Get axis limits based on the data
    x_range = (this_dvc[ssn_sttms].min().min() - dt.timedelta(seconds=1),
               this_dvc[ssn_sttms].max().max() + dt.timedelta(seconds=1))
    y_range = (this_dvc.index.min()-0.5, this_dvc.index.max()+0.5)
    ### Create plot
    f1, ax1 = plt.subplots(1, figsize=[6, 6])
    f1.autofmt_xdate()
    ax1.set_xlim(x_range)
    ax1.set_ylim(y_range)
    ### Plot a line for each row in the data, in the appropriate color for that datasource
    for i in this_dvc.index:
        for st, ed, col in zip(ssn_sttms, ssn_edtms, cols):
            ### Horizontal line from st to ed, at height i
            ax1.plot([this_dvc.loc[i, st], this_dvc.loc[i, ed]], [i, i],
                     alpha=0.5, lw=1, color=col)
            ### very small scatter points in case the line is too short to plot anything
            ax1.scatter([this_dvc.loc[i, st]], [i],
                        alpha=0.5, s=1, color=col)
    ### Plot background shaded regions to indicate session boundaries
    if ssns is not None:
        for it, i in enumerate(ssns.index):
            ax1.axvspan(ssns.loc[i, 'sttm'], ssns.loc[i, 'edtm'],
                        alpha=0.15, facecolor=pal[it%3], label='__nolegend__')
    ax1.xaxis.set_major_formatter(mpl.dates.DateFormatter('%y-%m-%d %H:%M'));
    ax1.set_title(titl)
    ax1.set_ylabel('Line number')
    plt.tight_layout()
    return f1, ax1
