import pandas     as pd
pd.set_option('display.width', 180)
import numpy as np
import datetime as dt
import matplotlib
import matplotlib.pyplot as plt
import seaborn as sb
import random
import os

vers = '' # 'v2_4/'  #'DCR_data/NewPanelData'
dir  = '1706-1803_new_gapfix' #'9mo'
plot_path = 'DataPlots/{}/'.format(dir)  #'DCR_data/NewPanelData/'
if not os.path.exists(plot_path):
    os.makedirs(plot_path)
print('Reading {0}swot_master_table_{1}.csv'.format(vers, dir))
print('Saving plots to {0}'.format(plot_path))
df = pd.read_csv('{0}swot_master_table_{1}.csv'.format(vers, dir), index_col = 0)
if vers in ['v2.1/','v2.3/']:
    df['mk_mdl'] = df.make.str.cat([df.model], sep='-')
else:
    df['mk_mdl'] = df.device
    df[['make', 'model']] = df.device.str.split(' ', 1, expand=True)
df['vw_frac']    = 1 - df.swotdur / df.swotstreamdur
df['swot_frac' ] = df.swotdur / df.swotstreamdur
df['swot_class'] = df.swot_frac >= 0.05
df['hh']         = df.fsite.str.split('-', 1, expand=True).iloc[:,0]
df['start_hour'] = pd.DatetimeIndex(df.start_time).hour
df['month'] = pd.to_datetime(df.start_time).map(lambda t: t.month)
### Convert durations to minutes, rounded, so get rid of precision difference between data sources
df[['duration','swotdur','swotstreamdur']] = (df[['duration','swotdur','swotstreamdur']]/60).round(0)

### For census anaylsis only:
### Roll up roku devices into one group
df['census_dvc'] = df.device.copy()
df.loc[df.device.str.contains('Roku'), 'census_dvc'] = 'Roku'



# RPD for comparison
events_rpd = pd.read_csv('../RPDOnOff/New/onoff_events.csv', index_col = 0)

########################################################################################################################
### for SWOT Model presentation

### four example devices
pres_dvcs = ['Smart TV','Roku','Google Chromecast','Google TV']

### provider share in panel, stacked bar
df['new_prv'] = df.provider.copy()
df.loc[df.provider.isin(['cw','crackle','directv','discovery']), 'new_prv'] = 'census'

all_minutes  = df.duration.sum()
new_prv_frac = df.groupby('new_prv').duration.sum().sort_values(ascending=False)/all_minutes*100
old_prv_sum  = df.loc[df.new_prv=='census'].groupby('provider').duration.sum().sort_values(ascending=False)
old_prv_frac = old_prv_sum/old_prv_sum.sum()*100

pal = sb.color_palette("Set1", df.new_prv.nunique()+1, desat=0.7)
temp = pal[5]
pal[5] = pal[4]
pal[4] = temp
f1, ax1 = plt.subplots(1, figsize=[10, 3])
for i, p in enumerate(new_prv_frac.index):
    if i == 0:
        left = 0
    else:
        left = new_prv_frac.iloc[:i].sum()
    ax1.barh(1, new_prv_frac.loc[p], color=pal[i], left=left, label=p)
    print('{0}: {1:0.1f}%'.format(p, new_prv_frac.loc[p]))
ax1.set_title('Provider market share in OTT panel')
f1.savefig('DataPlots/pres_prv_marketshare.png', dpi=800)
plt.close(f1)

### number of devices, barchart
n_dvcs = df[['fsite','census_dvc']].drop_duplicates().census_dvc.value_counts()
f1, ax1 = plt.subplots(1, figsize=[10, 3])
ax1.bar(range(n_dvcs.shape[0]), n_dvcs)
ax1.set_xticks(range(n_dvcs.shape[0]));
ax1.set_xticklabels(n_dvcs.index, rotation=20, ha='right');
ax1.set_title('Number of unique devices in panel')
plt.tight_layout()
f1.savefig('DataPlots/pres_dvc_count.png', dpi=800)
plt.close(f1)

### tuning from devices, barchart
n_mins = df.groupby('census_dvc').swotstreamdur.sum().sort_values(ascending=False)
f1, ax1 = plt.subplots(1, figsize=[10, 3])
ax1.bar(range(n_mins.shape[0]), n_mins)
ax1.set_xticks(range(n_mins.shape[0]));
ax1.set_xticklabels(n_mins.index, rotation=20, ha='right');
ax1.set_title('Number of streamed minutes from each device type in panel')
plt.tight_layout()
f1.savefig('DataPlots/pres_dvc_min_count.png', dpi=800)
plt.close(f1)

### swot frac by device, barchart
dvc_swot = df[['census_dvc','swotstreamdur','duration']].groupby('census_dvc').sum()
dvc_swot['swot_frac'] = 1. - dvc_swot.duration / dvc_swot.swotstreamdur
f1, ax1 = plt.subplots(1, figsize=[10, 3])
ax1.barh(range(len(pres_dvcs)), dvc_swot.loc[pres_dvcs[::-1], 'swot_frac'])
ax1.set_yticks(range(len(pres_dvcs)));
ax1.set_yticklabels(pres_dvcs[::-1]);
ax1.set_xlabel('Overall SWOT fraction')
plt.tight_layout()
f1.savefig('DataPlots/pres_dvc_swot.png', dpi=800)
plt.close(f1)

########################################################################################################################
### cumulative unique hh count
df['date'] = pd.DatetimeIndex(df.start_time).normalize()
date_counts = df[['hh','date']].groupby('date').count().sort_index()
date_hh_counts = df[['hh','date']].sort_values(['hh','date']).groupby('hh').agg({'date':'min'})\
                   .reset_index()\
                   .groupby('date').agg({'hh': pd.Series.nunique}).sort_index().cumsum()

from pandas.tseries import converter
converter.register()

f1, ax1 = plt.subplots(2, 1, figsize=[20, 10], sharex=True)
f1.autofmt_xdate();
ax1[0].plot(date_counts.index, date_counts)
ax1[0].set_ylabel('# events per day')
ax1[1].plot(date_hh_counts.index, date_hh_counts)
ax1[1].set_ylabel('Cumulative unique HHs')
f1.savefig('{0}cmhhs_by_date.png'.format(plot_path))
plt.close(f1)

########################################################################################################################
### How does accuracy compare to n_dvcs?
mdl_info = events_rpd.loc[events_rpd.tunr_id==0,['glbl_hh_dvc_tnr_id','model_type']].drop_duplicates()\
                     .groupby('model_type').count().sort_values('glbl_hh_dvc_tnr_id')
mdl_info.columns = ['n_dvcs']
mdl_info['mn_frac'] = events_rpd.loc[events_rpd.tunr_id==0,['glbl_hh_dvc_tnr_id','model_type', 'on_frac']].drop_duplicates()\
                     .groupby('model_type').on_frac.mean()
mdl_info['sd_frac'] = events_rpd.loc[events_rpd.tunr_id==0,['glbl_hh_dvc_tnr_id','model_type', 'on_frac']].drop_duplicates()\
                     .groupby('model_type').on_frac.std()

########################################################################################################################
########################################################################################################################
def calc_by_property(df, prop, dvc_col = 'fsite'):
    by_ = pd.concat([df[prop+[dvc_col]].drop_duplicates().groupby(prop).count(),
                     df[prop+[dvc_col]].drop_duplicates().groupby(prop).count()*2,
                     df[prop+[dvc_col]].drop_duplicates().groupby(prop).count()*3,
                     df[prop+[dvc_col]].drop_duplicates().groupby(prop).count()*4], axis=1)
    by_.columns = ['n500','n1000','n1500','n2000']
    return by_

def calc_n_incl(by_, n):
    n_incl = pd.DataFrame([], index = ['n_incl','n_excl'], columns = by_.columns)
    for c in by_.columns:
        n_incl.loc['n_incl', c] = by_.loc[by_[c] >= n, c].sum()
        n_incl.loc['n_excl', c] = by_.loc[by_[c] <  n, c].sum()
    n_incl.loc['fraction'] = n_incl.loc['n_incl'] / n_incl.sum()
    return n_incl

##############################################################################################
### calculate dimensions for a grid of n subplots
def layout(n):
    n1 = int(np.ceil(n**0.5))
    if n1*(n1-1) >= n:
        n2 = n1-1
    else:
        n2 = n1
    return n1, n2

########################################################################################################################
### Provider info
df['start_time'] = pd.to_datetime(df.start_time)
df[  'end_time'] = pd.to_datetime(df.end_time)
df['drtn_min'] = (df.end_time - df.start_time)/dt.timedelta(minutes=1)

swot_aggs = {'fsite':    {'n_dvcs'  : 'nunique',
                          'n_ssns'  : 'count'},
             'drtn_min': {'n_mins'  : 'sum',
                          'med_drtn': 'median',
                          'mn_drtn' : 'mean',
                          'max_drtn': 'max'},
             'swot_frac': {'mn_swot_frac': 'mean',
                           'md_swot_frac': 'median'}
             }
swot_agg_cols = ['n_dvcs','n_ssns','n_mins','med_drtn','mn_drtn','max_drtn','mn_swot_frac','md_swot_frac']
prvdr_info = df.groupby('provider').agg(swot_aggs)
prvdr_info.columns = prvdr_info.columns.droplevel()
prvdr_info = prvdr_info[swot_agg_cols]

make_info = df.groupby('make').agg(swot_aggs)
make_info.columns = make_info.columns.droplevel()
make_info = make_info[swot_agg_cols]

prvdr_make_swot_md = df.pivot_table(index = 'provider',columns = ['make','model'],values='swot_frac',aggfunc = 'median')
prvdr_make_swot_mn = df.pivot_table(index = 'provider',columns = ['make','model'],values='swot_frac',aggfunc = 'mean')
prvdr_make_n_hhs   = df.pivot_table(index = 'provider', columns = ['make','model'],
                                                   values='hh', aggfunc = pd.Series.nunique)

rpd_dvc = events_rpd.glbl_hh_dvc_tnr_id.str.split('-', expand=True)
events_rpd['glbl_hh_dvc_id'] = rpd_dvc[[0,1]].apply(lambda x: '-'.join(x), axis=1)

### Unique OTT devices
# df['dvc'] = df.fsite.str.cat([df.macaddress, df.make, df.model], sep='-')
df['dvc'] = df.fsite.str.cat([df.hh, df.device], sep='-')

### devices per household
n_dvcs_hh_ott = df.groupby('hh').fsite.nunique().mean()
n_dvcs_hh_rpd = events_rpd.groupby('glbl_hh_id').glbl_hh_dvc_id.nunique().mean()

# minutes tuning per device per day
df['st_date'] = pd.Index(pd.to_datetime(df.start_time)).date
n_mins_dvc_ott = (df.pivot_table(index='fsite',  columns = 'st_date', values='duration', aggfunc='sum').fillna(0)).mean().median()
n_mins_hh_ott  = (df.pivot_table(index='hh', columns = 'st_date', values='duration', aggfunc='sum').fillna(0)).mean().median()

events_rpd['st_date'] = pd.Index(pd.to_datetime(events_rpd.sttm_rpd)).date
n_mins_dvc_rpd = events_rpd.pivot_table(index='glbl_hh_dvc_id',columns = 'st_date', values='drtn', aggfunc='sum').fillna(0).mean().median()
n_mins_hh_rpd  = events_rpd.pivot_table(index='glbl_hh_id',    columns = 'st_date', values='drtn', aggfunc='sum').fillna(0).mean().median()

with open('{0}stats.txt'.format(plot_path), 'w') as f:
    f.write('OTT: {0: 8.2f} dvcs/hh, {1: 8.2f} mins/dvc, {2: 8.2f} mins/hh, {3} hhs, {4} dvcs\n'.format(
        n_dvcs_hh_ott, n_mins_dvc_ott, n_mins_hh_ott, df.hh.nunique(), df.fsite.nunique()))
    f.write('RPD: {0: 8.2f} dvcs/hh, {1: 8.2f} mins/dvc, {2: 8.2f} mins/hh, {3} hhs, {4} dvcs\n'.format(
        n_dvcs_hh_rpd, n_mins_dvc_rpd, n_mins_hh_rpd, events_rpd.glbl_hh_id.nunique(), events_rpd.glbl_hh_dvc_id.nunique()))
    f.write('{0: 6.2f} times more data per hh in RPD than OTT\n\n'.format(n_mins_hh_rpd/n_mins_hh_ott))
    f.write(df[['fsite','make','model']].drop_duplicates()\
            .groupby(['make','model']).fsite.count().sort_values(ascending=False).to_string())

##############################################################################################
########################################################################################################################
### How many devices per make/model?
by_make  = calc_by_property(df, ['make'])
by_model = calc_by_property(df, ['make','model'])
by_prvdr = calc_by_property(df, ['provider'])
by_prvdr_model = calc_by_property(df, ['provider','make','model'])

### RPD for comparison
dvcs_rpd = events_rpd[['glbl_hh_dvc_tnr_id','glbl_hh_id','tunr_id','model_type']].drop_duplicates()
dvcs_rpd = dvcs_rpd.loc[dvcs_rpd.tunr_id == 0]
by_model_rpd = calc_by_property(dvcs_rpd, ['model_type'], 'glbl_hh_dvc_tnr_id')
########################################################################################################################
incl_frac = pd.DataFrame([], columns = ['n500_make',  'n1000_make',  'n1500_make',  'n2000_make',
                                        'n500_model', 'n1000_model', 'n1500_model', 'n2000_model',
                                        'n500_prvdr', 'n1000_prvdr', 'n1500_prvdr', 'n2000_prvdr',
                                        'n500_prvdr_model', 'n1000_prvdr_model', 'n1500_prvdr_model', 'n2000_prvdr_model',
                                        'n800_rpd'], index = [])
for n in [10, 20, 30, 40, 50, 60, 70, 80, 90, 100]:
    incl_frac.loc[n] = list(calc_n_incl(by_make,  n).loc['fraction']) + \
                       list(calc_n_incl(by_model, n).loc['fraction']) + \
                       list(calc_n_incl(by_prvdr, n).loc['fraction']) + \
                       list(calc_n_incl(by_prvdr_model, n).loc['fraction']) + \
                       list(calc_n_incl(by_model_rpd[['n500']], n).loc['fraction'])
### Plot
f1, ax1 = plt.subplots(2,2, figsize=[12, 10], sharex=True, sharey=True)
ax1[0,0].plot(incl_frac.index, incl_frac.n800_rpd,   '--', label='RPD,  n=800', color='black')
ax1[0,0].plot(incl_frac.index, incl_frac.n500_make,  label='make, n=500')
ax1[0,0].plot(incl_frac.index, incl_frac.n1000_make, label='make, n=1000')
ax1[0,0].plot(incl_frac.index, incl_frac.n1500_make, label='make, n=1500')
ax1[0,0].plot(incl_frac.index, incl_frac.n2000_make, label='make, n=2000')
# ax1[0,0].set_xlabel('N (# dvcs per category to be included)')
ax1[0,0].set_ylabel('Fraction of dvcs included')
ax1[0,0].set_title('Make')
ax1[0,0].legend(loc='best')
ax1[0,0].set_xlim(10, 100)
ax1[0,0].set_ylim(0, 1)

ax1[0,1].plot(incl_frac.index, incl_frac.n800_rpd,   '--', label='RPD,  n=800', color='black')
ax1[0,1].plot(incl_frac.index, incl_frac.n500_model,  label='model, n=500')
ax1[0,1].plot(incl_frac.index, incl_frac.n1000_model, label='model, n=1000')
ax1[0,1].plot(incl_frac.index, incl_frac.n1500_model, label='model, n=1500')
ax1[0,1].plot(incl_frac.index, incl_frac.n2000_model, label='model, n=2000')
# ax1[0,1].set_xlabel('N (# dvcs per category to be included)')
# ax1[0,1].set_ylabel('Fraction of dvcs included')
ax1[0,1].set_title('Make & Model')
ax1[0,1].legend(loc='best')

ax1[1,0].plot(incl_frac.index, incl_frac.n800_rpd,   '--', label='RPD,  n=800', color='black')
ax1[1,0].plot(incl_frac.index, incl_frac.n500_prvdr,  label='provider, n=500')
ax1[1,0].plot(incl_frac.index, incl_frac.n1000_prvdr, label='provider, n=1000')
ax1[1,0].plot(incl_frac.index, incl_frac.n1500_prvdr, label='provider, n=1500')
ax1[1,0].plot(incl_frac.index, incl_frac.n2000_prvdr, label='provider, n=2000')
ax1[1,0].set_xlabel('N (# dvcs per category to be included)')
ax1[1,0].set_ylabel('Fraction of dvcs included')
ax1[1,0].set_title('Provider')
ax1[1,0].legend(loc='best')

ax1[1,1].plot(incl_frac.index, incl_frac.n800_rpd,   '--', label='RPD,  n=800', color='black')
ax1[1,1].plot(incl_frac.index, incl_frac.n500_prvdr_model,  label='provider/model, n=500')
ax1[1,1].plot(incl_frac.index, incl_frac.n1000_prvdr_model, label='provider/model, n=1000')
ax1[1,1].plot(incl_frac.index, incl_frac.n1500_prvdr_model, label='provider/model, n=1500')
ax1[1,1].plot(incl_frac.index, incl_frac.n2000_prvdr_model, label='provider/model, n=2000')
ax1[1,1].set_xlabel('N (# dvcs per category to be included)')
# ax1[1,1].set_ylabel('Fraction of dvcs included')
ax1[1,1].set_title('Provider, Make, and Model')
ax1[1,1].legend(loc='best')

plt.tight_layout()
f1.savefig('{0}incl_frac_vs_n.png'.format(plot_path))
plt.close(f1)

########################################################################################################################
f1, ax1 = plt.subplots(1, figsize=[6, 4])

ax1.plot(incl_frac.index, incl_frac.n800_rpd,   '--', label='TV', color='black')
ax1.plot(incl_frac.index, incl_frac.n2000_prvdr_model, label='4x')
ax1.plot(incl_frac.index, incl_frac.n1500_prvdr_model, label='3x')
ax1.plot(incl_frac.index, incl_frac.n1000_prvdr_model, label='2x')
ax1.plot(incl_frac.index, incl_frac.n500_prvdr_model,  label='Current size')
ax1.set_ylim(0,1)
ax1.set_xlabel('N (# dvcs required per bin)')
ax1.set_ylabel('Fraction of devices included')
ax1.set_title('Included data fraction')
ax1.legend(loc='best')

plt.tight_layout()
f1.savefig('{0}incl_frac_model_provider.png'.format(plot_path), dpi=800)
plt.close(f1)

########################################################################################################################
##############################################################################################
### Distribution of SWT/SWOT per provider over swotstreamdur
ssd_bins = np.arange(df['swotstreamdur'].min(), df['swotstreamdur'].quantile(0.9), 5)
prvdr_list = ['netflix','youtube','hulu','amazon','hbo','crackle','cw','directv','discovery']
prvdr_list = [p for p in prvdr_list if p in df.provider.unique()]
assert len(prvdr_list) <= len(df.provider.unique()), 'ERROR: missing a provider?'

if len(prvdr_list) == 6:
    f1, ax1 = plt.subplots(2, 3, figsize=[12,8])
else:
    f1, ax1 = plt.subplots(2, 5, figsize=[20, 10])
axlist = ax1.flatten()
for i, p in enumerate(prvdr_list):
    for x0, x1 in [[18, 21], [39, 42]]:
        axlist[i].axvspan(x0, x1, alpha=0.2, color='green')
    batch = df.loc[df.provider == p]
    for j, cond in enumerate([~df.swot_class, df.swot_class]):
        subset = batch.loc[cond]
        n = subset.shape[0]
        while subset.shape[0] < 2:
            subset = subset.append(df.loc[df.swotstreamdur == df.swotstreamdur.max()])
        sb.distplot(subset['swotstreamdur'],
                    label='SW{0} {1}'.format(['T: ','OT:'][j], n),
                    bins=ssd_bins, color=['blue','red'][j], kde=False, norm_hist=False, ax=axlist[i])
    axlist[i].legend(loc='upper right')
    axlist[i].set_title(p)
plt.tight_layout()
f1.savefig('{0}swotstreamdur_prvdr_distn.png'.format(plot_path))
plt.close(f1)
#--------------------------------------------------------------------------------------------#
mdl_list = df[['fsite','mk_mdl']].drop_duplicates().mk_mdl.value_counts()

f1, ax1 = plt.subplots(layout(df.mk_mdl.nunique())[0], layout(df.mk_mdl.nunique())[1],figsize=[15,15])
axlist = ax1.flatten()
for i, m in enumerate(mdl_list.index):
    for x0, x1 in [[18, 21], [39, 42]]:
        axlist[i].axvspan(x0, x1, alpha=0.2, color='green')
    batch = df.loc[df.mk_mdl == m]
    axlist[i].hist( [ batch.loc[ df.swot_class, 'swotstreamdur'],
                      batch.loc[~df.swot_class, 'swotstreamdur']],
                    bins=ssd_bins, stacked=True, rwidth=1., alpha=0.75,
                    color=['#e50000','#0504aa'],
                    label=['SWOT: {0}'.format( batch.loc[ df.swot_class].shape[0] ),
                           'SWT:  {0}'.format( batch.loc[~df.swot_class].shape[0] )])
    # if False in batch.swot_class.value_counts().index: #loc[False] > 1:
    #     if batch.swot_class.value_counts().loc[False] > 1:
    #         sb.distplot(batch.loc[~df.swot_class, 'swotstreamdur'],
    #                     label='SWT:  {0}'.format( batch.loc[~df.swot_class].shape[0] ),
    #                     bins=ssd_bins, color='blue', kde=False, norm_hist=False, ax=axlist[i])
    # if True in batch.swot_class.value_counts().index:
    #     if batch.swot_class.value_counts().loc[True] > 1:
    #         sb.distplot(batch.loc[ df.swot_class, 'swotstreamdur'],
    #                     label='SWOT: {0}'.format( batch.loc[ df.swot_class].shape[0] ),
    #                     bins=ssd_bins, color='red',  kde=False, norm_hist=False, ax=axlist[i])
    axlist[i].legend(loc='upper right')
    axlist[i].set_title('{0}, n={1}'.format(m, mdl_list.loc[m]))

plt.tight_layout()
f1.savefig('{0}swotstreamdur_mdl_distn.png'.format(plot_path))
plt.close(f1)

#--------------------------------------------------------------------------------------------#
select_mdl_list = ['smarttv-none','bluray-none','playstation-3','wii-none']
namelist = ['SmartTV', 'Blu-ray', 'PlayStation 3', 'Wii']

f1, ax1 = plt.subplots(layout(len(select_mdl_list))[0], layout(len(select_mdl_list))[1], figsize=[8,8])
axlist = ax1.flatten()
for i, m in enumerate(select_mdl_list):
    if m in df.mk_mdl.unique():
        batch = df.loc[df.mk_mdl == m]
        if False in batch.swot_class.value_counts().index:
            if batch.swot_class.value_counts().loc[False] > 1:
                sb.distplot(batch.loc[~df.swot_class, 'swotstreamdur'],
                            label='SWT:   {0}'.format( batch.loc[~df.swot_class].shape[0] ),
                            bins=ssd_bins, color='blue', kde=False, norm_hist=False, ax=axlist[i])
        if True in batch.swot_class.value_counts().index:
            if batch.swot_class.value_counts().loc[True] > 1:
                sb.distplot(batch.loc[ df.swot_class, 'swotstreamdur'],
                            label='SWOT: {0}'.format( batch.loc[ df.swot_class].shape[0] ),
                            bins=ssd_bins, color='red',  kde=False, norm_hist=False, ax=axlist[i])
        axlist[i].legend(loc='upper right')
        axlist[i].set_xlabel('Streaming Duration (min)')
        # axlist[i].set_ylabel('Density')
        axlist[i].set_title('{0}, n={1}'.format(namelist[i], mdl_list.loc[m]))

plt.tight_layout()
f1.savefig('{0}swotstreamdur_mdl_select.png'.format(plot_path))
plt.close(f1)

print(df.loc[df.mk_mdl.isin(select_mdl_list), ['fsite','mk_mdl']].drop_duplicates().groupby('mk_mdl').fsite.count())
########################################################################################################################
### Viewed vs. streamed durations
f1, ax1 = plt.subplots(1, 2, figsize=[12,6])

x = np.arange(0, 10001, 10)
ax1[0].plot(x,   1*x, alpha = 0.3, color='red')
ax1[1].plot(x,   1*x, alpha = 0.3, color='red')

for i in np.arange(1, 10*60+1, 60):
  ax1[0].axvline(i, alpha = 0.2, color='red')
  ax1[0].axhline(i, alpha = 0.2, color='red')
ax1[0].plot(df.swotstreamdur, df.duration, 'ko',alpha=0.05);
ax1[0].set_title('Distribution of streamed vs. viewed durations')
ax1[0].set_xlabel('Streamed duration (minutes)')
ax1[0].set_ylabel('Viewed duration (minutes)')
ax1[0].set_xlim(0,500)
ax1[0].set_ylim(0,500)

ax1[1].plot(df.swotstreamdur, df.duration, 'ko',alpha=0.05);
ax1[1].set_title('Log-distribution of streamed vs. viewed durations')
ax1[1].set_xlabel('Streamed duration (minutes)')
ax1[1].set_ylabel('Viewed duration (minutes)')
ax1[1].set_xscale('log')
ax1[1].set_yscale('log')

plt.tight_layout()
f1.savefig('{0}vwd_strm_drtns.png'.format(plot_path))
plt.close(f1)
##############################################################################################
### Histograms of fraction distributions
f1, ax1 = plt.subplots(1, 2, figsize=[12,6])

ax1[0].hist(df['swot_frac'], bins = 50);
ax1[0].set_title('Distribution of SWOT Fraction');
ax1[0].set_xlabel('Fraction of session which is SWOT');
ax1[0].set_ylabel('Number of events');
ax1[0].set_yscale('log');

for i in range(1, 5):
  ax1[1].axvline(i * 60, alpha = 0.2, color='red')

ax1[1].plot(df.swotstreamdur, df.swot_frac, 'bo',alpha=0.1);

ax1[1].set_title('Distribution of SWOT fraction')
ax1[1].set_xlabel('Length of session (minutes)')
ax1[1].set_ylabel('Fraction of session which is SWOT')
# ax1[1].set_xlim(0, 600)
ax1[1].set_xscale('log')

plt.tight_layout()
f1.savefig('{0}swot_frac.png'.format(plot_path))
plt.close(f1)
##############################################################################################
### Time distribution of full/empty events
# Excluding off genres?
df['start_hour'] = pd.DatetimeIndex(df.start_time).to_period('h').to_datetime()
sthr_counts = pd.concat([df.loc[(df.swot_frac ==0)].groupby('start_hour').fsite.count(),
                         df.loc[(df.swot_frac > 0)].groupby('start_hour').fsite.count()],
                        axis=1)
sthr_counts.columns = ['no_swot','some_swot']
sthr_counts = sthr_counts.reindex(pd.date_range(sthr_counts.index.min(), sthr_counts.index.max(), freq='h')).fillna(0)
sthr_counts[  'no_swot_avg'] = sthr_counts.no_swot.rolling(  3, center=True, min_periods = 1).mean()
sthr_counts['some_swot_avg'] = sthr_counts.some_swot.rolling(3, center=True, min_periods = 1).mean()

sttm_counts = pd.concat([df.loc[(df.swot_frac ==0)].groupby('start_time').fsite.count(),
                         df.loc[(df.swot_frac > 0)].groupby('start_time').fsite.count()],
                        axis=1)
sttm_counts.columns = ['no_swot','some_swot']
# sttm_counts.index = sttm_counts.index.to_datetime()
sttm_counts = sttm_counts.reindex(pd.date_range(sttm_counts.index.min(), sttm_counts.index.max(), freq='min')).fillna(0)
sttm_counts[  'no_swot_avg'] = sttm_counts.no_swot.rolling(  120, center=True, min_periods = 1).mean()
sttm_counts['some_swot_avg'] = sttm_counts.some_swot.rolling(120, center=True, min_periods = 1).mean()

sttm_stack_day = sttm_counts.iloc[0:1440, 0:2].copy()
for i in range(1440):
    sttm_stack_day.iloc[i] = sttm_counts.iloc[(sttm_counts.index.hour   == sttm_stack_day.index[i].hour  ) &
                                              (sttm_counts.index.minute == sttm_stack_day.index[i].minute)].sum()

sttm_stack_hr = sttm_counts.iloc[0:60, 0:2].copy()
for i in range(60):
    sttm_stack_hr.iloc[i] = sttm_counts.iloc[(sttm_counts.index.minute == sttm_stack_hr.index[i].minute)].sum()

f1, ax1 = plt.subplots(3, 2, figsize=[20,12]);
f1.autofmt_xdate();

ax1[0, 0].plot(sthr_counts.index, sthr_counts.no_swot,       color='red',  alpha = 0.3, label='Sessions w/o SWOT');
ax1[0, 0].plot(sthr_counts.index, sthr_counts.some_swot,     color='blue', alpha = 0.3, label='Sessions w/ SWOT');
ax1[0, 0].plot(sthr_counts.index, sthr_counts.no_swot_avg,   color='red',  alpha = 1., label='w/o, 2-hr avg');
ax1[0, 0].plot(sthr_counts.index, sthr_counts.some_swot_avg, color='blue', alpha = 1., label='w/,  2-hr avg');
ax1[0, 0].set_title('Time Distribution of sessions');
ax1[0, 0].set_ylabel('Number of events');
ax1[0, 0].legend();
plt.setp(ax1[0, 0].get_xticklabels(), visible=True, rotation=30);

ax1[0, 1].plot(sthr_counts.index, sthr_counts.some_swot/(sthr_counts.some_swot+sthr_counts.no_swot),
               color='black',  alpha = 0.3, label='SWOT fraction');
ax1[0, 1].plot(sthr_counts.index, sthr_counts.some_swot_avg/(sthr_counts.some_swot_avg+sthr_counts.no_swot_avg),
               color='black',  alpha = 1., label='SWOT fraction, 2-hr avg');
ax1[0, 1].set_title('Time Distribution of sessions');
ax1[0, 1].set_ylabel('SWOT fraction');
ax1[0, 1].legend();
plt.setp(ax1[0, 1].get_xticklabels(), visible=True, rotation=30);

ax1[1, 0].plot(sttm_stack_day.index, sttm_stack_day.no_swot,   color='red',  alpha = 0.7, label='Sessions w/o SWOT');
ax1[1, 0].plot(sttm_stack_day.index, sttm_stack_day.some_swot, color='blue', alpha = 0.7, label='Sessions w/ SWOT');
ax1[1, 0].set_title('Time distribution, daily');
ax1[1, 0].set_ylabel('Number of events');
ax1[1, 0].xaxis.set_major_formatter(matplotlib.dates.DateFormatter('%H:%M'))
plt.setp(ax1[1, 0].get_xticklabels(), visible=True, rotation=30);

ax1[1, 1].plot(sttm_stack_day.index,
               (sttm_stack_day.some_swot/(sttm_stack_day.some_swot+sttm_stack_day.no_swot)),
               color='black', alpha=0.3, label='SWOT fraction');
ax1[1, 1].plot(sttm_stack_day.index,
               (sttm_stack_day.some_swot/(sttm_stack_day.some_swot+sttm_stack_day.no_swot)).rolling(60, center=True, min_periods = 1).mean(),
               color='black', label='SWOT fraction, 1-hr avg');
ax1[1, 1].set_ylabel('SWOT fraction');
ax1[1, 1].legend()
ax1[1, 1].xaxis.set_major_formatter(matplotlib.dates.DateFormatter('%H:%M'))
plt.setp(ax1[1, 1].get_xticklabels(), visible=True, rotation=30);

ax1[2, 0].plot(sttm_stack_hr.index, sttm_stack_hr.no_swot,   color='red',  alpha = 0.7, label='Sessions w/o SWOT');
ax1[2, 0].plot(sttm_stack_hr.index, sttm_stack_hr.some_swot, color='blue', alpha = 0.7, label='Sessions w/ SWOT');
ax1[2, 0].set_ylabel('Number of events');
ax1[1, 0].set_title('Time distribution, hourly');
ax1[2, 0].xaxis.set_major_formatter(matplotlib.dates.DateFormatter('--:%M:%S'))
plt.setp(ax1[2, 0].get_xticklabels(), visible=True, rotation=30);

ax1[2, 1].plot(sttm_stack_hr.index, sttm_stack_hr.some_swot/(sttm_stack_hr.some_swot+sttm_stack_hr.no_swot),
               color='black', label='SWOT fraction');
ax1[2, 1].set_ylabel('SWOT fraction');
ax1[2, 1].set_ylim(0, ax1[2, 1].get_ylim()[1])
ax1[2, 1].xaxis.set_major_formatter(matplotlib.dates.DateFormatter('--:%M:%S'))
plt.setp(ax1[2, 1].get_xticklabels(), visible=True, rotation=30);

plt.tight_layout()
f1.savefig('{0}sttm_counts.png'.format(plot_path));
plt.close(f1);

##############################################################################################
### Dvc history parameters?
df['sttm'] = pd.DatetimeIndex(df.start_time)
first_min = df.sttm.min()
df['mins_in_data'] = round((df.sttm - first_min).astype('timedelta64[m]'))

df['dvc_mn_drtn' ]      = np.nan
df['dvc_med_drtn']      = np.nan
df['dvc_sum_drtn']      = np.nan
for dvc in df.fsite.unique():
    subset  = df.fsite == dvc
    usethis = df.loc[subset].sort_values('sttm')
    df.loc[subset, 'dvc_mn_drtn' ] = usethis.swotstreamdur.expanding().mean().shift(1)
    df.loc[subset, 'dvc_med_drtn'] = usethis.swotstreamdur.expanding().median().shift(1)
    df.loc[subset, 'dvc_sum_drtn'] = usethis.swotstreamdur.expanding().sum().shift(1)

df.fillna({'dvc_sum_drtn': 0,
           'dvc_mn_drtn' : df['dvc_mn_drtn'].fillna(df.dvc_mn_drtn.dropna().median()),
           'dvc_med_drtn': df['dvc_med_drtn'].fillna(df.dvc_med_drtn.dropna().median())},
        inplace=True);
df['dvc_frac_indata']   = (df.dvc_sum_drtn     / df.mins_in_data).fillna(1)

##############################################################################################
### Correlation matrix of parameters
# Compute the correlation matrix
corr = df[['swotstreamdur','start_hour','mins_in_data','dvc_sum_drtn','dvc_mn_drtn','dvc_med_drtn','dvc_frac_indata',
           'swotdur','swot_frac']].corr()
# Generate a mask for the upper triangle
mask = np.zeros_like(corr, dtype=np.bool)
mask[np.triu_indices_from(mask)] = True
# Generate a custom diverging colormap
cmap = sb.diverging_palette(220, 10, as_cmap=True)

# Draw the heatmap with the mask and correct aspect ratio
f, ax = plt.subplots(figsize=(11, 9))
sb.heatmap(corr, mask=mask, cmap=cmap, vmax=.5,
            square=True, xticklabels=True, yticklabels=True,
            linewidths=.5, cbar_kws={"shrink": .5}, ax=ax)
ax.set_xticklabels(ax.xaxis.get_majorticklabels(), rotation=30)
ax.set_yticklabels(ax.yaxis.get_majorticklabels(), rotation=30)
ax.set_title('Correlation matrix')
plt.tight_layout()
f.savefig('{0}correlation_matrix.png'.format(plot_path))
plt.close(f)

##############################################################################################
### See whether the bins converge as binsize increases?
### Take mean swot frac per bin for random subsets of the hhs, for different # of hhs
### See how large of sample size it takes for different samples to have similar means?
n_dvcs  = df.pivot_table(index='mk_mdl',columns='provider',values='fsite', aggfunc=pd.Series.nunique).fillna(0)
swot_mn = df.pivot_table(index='mk_mdl',columns='provider',values='swot_frac', aggfunc='mean')

# test = pd.DataFrame([[0, '','', np.nan]], index=[0], columns = ['n', 'mk_mdl', 'provider','swot_mn'])
# for p in ['netflix','hulu','youtube']:
#     for m in ['Smart TV', 'X Box One', 'Play Station 4']:
#         print(p, m)
#         for n in np.arange(10, df.loc[(df.provider==p) & (df.mk_mdl==m), 'fsite'].nunique(), 1):
#             for i in range(5):
#                 ind = test.index.max() + 1
#                 sample_hhs = random.sample( list(df.loc[(df.provider==p) & (df.mk_mdl==m), 'fsite'].unique()), n)
#                 test.loc[ind] = [n, m, p, df.loc[df.fsite.isin(sample_hhs), 'swot_frac'].mean()]
#
# ### Plot test results
# cols = sb.color_palette("Paired", n_colors=test[['mk_mdl','provider']].drop_duplicates().shape[0]-1)
# i = 0
# f1, ax1 = plt.subplots(1)
# for p in ['netflix','hulu','youtube']:
#     for m in ['Smart TV', 'X Box One', 'Play Station 4']:
#         ax1.scatter(test.loc[(test.mk_mdl==m) & (test.provider==p), 'n'],
#                     test.loc[(test.mk_mdl==m) & (test.provider==p), 'swot_mn'],
#                     c=cols[i],
#                     label = m+', '+p)
#         i+=1
# ax1.legend()
# ax1.set_xlim(0, n_dvcs.max().max())
# ax1.set_ylim(0, 0.2)
# ax1.set_xlabel('# HHs sampled')
# ax1.set_ylabel('Mean SWOT fraction')
# f1.savefig('{0}samplesize_convergence.png'.format(plot_path))
# plt.close(f1)
##############################################################################################
### fraction of minutes that are SWOT, per mk/mdl/prv, per 5-10 min duration bin

df['mk_mdl_prv'] = df.mk_mdl.str.cat([df.provider], sep='-')
# bins = list(np.arange(0, df['swotstreamdur'].quantile(0.99), 5)) + [df.swotstreamdur.max()]
# bins = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 50, 100, 500, 1000, 10000]
bins = [1, 5, 10, 15, 600, 10000]
df['drtn_bin']   = pd.cut(df.swotstreamdur,
                          bins=bins, #labels = bins[:(len(bins)-1)],
                          right=False)
swot_sum    = df.pivot_table(index='mk_mdl_prv', columns='drtn_bin', values='swotdur',       aggfunc='sum').fillna(0)
tot_sum     = df.pivot_table(index='mk_mdl_prv', columns='drtn_bin', values='swotstreamdur', aggfunc='sum').fillna(0)
n_dvcs_drtn = df.pivot_table(index='mk_mdl_prv', columns='drtn_bin', values='fsite', aggfunc=pd.Series.nunique).fillna(0)

swot_frac = swot_sum/tot_sum

### Display larger bins
swot_frac.mask(n_dvcs_drtn<30, other='')
swot_frac.mask(n_dvcs_drtn<10, other='')

swot_frac.columns = [(bins[i], bins[i+1]) for i in range(len(bins)-1)]
swot_frac['n_unq'] = df[['fsite','mk_mdl_prv']].drop_duplicates().mk_mdl_prv.value_counts()

################################\
pal = sb.color_palette("Paired", n_colors=len(swot_frac.loc[swot_frac.n_unq>=30].index))
i=0
f1, ax1 = plt.subplots(1)
for r in swot_frac.loc[swot_frac.n_unq>=30].index:
    ax1.plot(range(len(bins)-1),
             swot_frac.loc[r, [c for c in swot_frac if c is not 'n_unq']],
             c=pal[i], label = r)
    i+=1
ax1.legend()
ax1.set_xlabel('Duration bin (min)')
ax1.set_ylabel('Fraction of minutes which are SWOT')
ax1.set_title('Bins with at least 30 devices in sample')
ax1.set_xticks(range(len(bins)-1));
ax1.set_xticklabels([c for c in swot_frac if c is not 'n_unq'], rotation=30);
f1.subplots_adjust(bottom=.17)
f1.savefig('{0}swotfrac_breakout.png'.format(plot_path))
plt.close(f1)


### do provider and device interact?
### sort swot_mn by global means for columns/rows
# dvc_sort = df.groupby('mk_mdl').swot_frac.mean().sort_values()
# prv_sort = swot_filt.mean(axis=0).dropna().sort_values()
# dvc_sort = swot_filt.mean(axis=1).dropna().sort_values()
swot_filt = swot_mn.mask(n_dvcs<10, other=np.nan)
### Take most populous provider and model for benchmark values
prv_sort = swot_filt.loc['Smart TV'].sort_values().dropna()
dvc_sort = swot_filt.netflix.sort_values().dropna()
swot_sorted = swot_filt.loc[dvc_sort.index, prv_sort.index]

### Plot each column, see if the overall trends are consistent across devices and across providers
### Create grid of predictions based on provider/device benchmarks to compare
mnswot_pred = pd.DataFrame([], index = swot_sorted.index, columns = swot_sorted.columns)
for c in mnswot_pred:
    mnswot_pred[c] = dvc_sort/dvc_sort.loc['Smart TV'] * prv_sort.loc[c]

### See variation from only bins with at least 30 members
# ((swot_sorted - mnswot_pred)/mnswot_pred).mask(n_dvcs<30, other='')
### Plot predictions
f1, ax1 = plt.subplots(1,2, figsize=[16,6], sharex=True, sharey=True)
# ax1[0].plot(range(swot_sorted.shape[0]), dvc_sort, c='black', label='all')
for i, r in enumerate(prv_sort.index):
    ax1[0].axhline(prv_sort.loc[r], c=sb.color_palette()[i], alpha=0.5)
for i, c in enumerate(swot_sorted.columns):
    ax1[0].plot(range(swot_sorted.shape[0]), swot_sorted[c], c=sb.color_palette()[i], label=c)
ax1[0].legend(loc='upper left')
ax1[0].set_xticks(range(swot_sorted.shape[0]))
ax1[0].set_xticklabels(swot_sorted.index, rotation=30, ha='right')
ax1[0].set_ylabel('Mean SWOT fraction')
ax1[0].set_title('Device/provider SWOT variation (n >= 10 dvcs)')
### dots, size = n_dvcs
for i, c in enumerate(swot_sorted.columns):
    ax1[0].scatter(range(swot_sorted.shape[0]), swot_sorted[c],
                   s=n_dvcs.loc[swot_sorted.index, c]+5,
                   c=sb.color_palette()[i], label=c)
### predictions
# ax1[1].plot(range(mnswot_pred.shape[0]), dvc_sort, c='black', label='all')
for i, r in enumerate(prv_sort.index):
    ax1[1].axhline(prv_sort.loc[r], c=sb.color_palette()[i], alpha=0.5)
for i, c in enumerate(mnswot_pred.columns):
    ax1[1].plot(range(mnswot_pred.shape[0]), mnswot_pred[c], c=sb.color_palette()[i], label=c)
### dots, size = n_dvcs
for i, c in enumerate(swot_sorted.columns):
    ax1[1].scatter(range(swot_sorted.shape[0]), swot_sorted[c],
                   s=n_dvcs.loc[swot_sorted.index, c]+5,
                   c=sb.color_palette()[i], label=c)
# ax1[1].legend(loc='best')
ax1[1].set_xticks(range(mnswot_pred.shape[0]))
ax1[1].set_xticklabels(mnswot_pred.index, rotation=30, ha='right')
ax1[1].set_ylabel('Predicted mean SWOT fraction')
ax1[1].set_title('Device/provider predicted SWOT variation')
ax1[1].set_xlim(-1, ax1[1].get_xlim()[1])
ax1[1].set_ylim(0, ax1[1].get_ylim()[1])
plt.tight_layout()
f1.savefig('{0}mnswot_predicted.png'.format(plot_path), dpi=300)
plt.close(f1)

##############################################################################################
### Mean swot fraction vs. duration
plist = list(df.provider.unique())
swot_vs_dur   = {}
swot_avgs_120 = {}
swot_avgs_10  = {}
breakpoint = 150
for p in ['all'] + plist:
    if p == 'all':
        d = df
    else:
        d = df.loc[df.provider == p]
    swot_vs_dur[p] = d.groupby('swotstreamdur').agg({'duration': {'n_ssns':'count', 'sum_dur':'sum'},
                                              'swotstreamdur': {'sum_swotdur':'sum'},
                                              'swot_frac': {'mn_swotfrac':'mean'}})\
                    .reindex(range(1, int(d.swotstreamdur.max(0)+1))).fillna(0)
    swot_vs_dur[p].columns = swot_vs_dur[p].columns.droplevel(0)
    swot_vs_dur[p] = swot_vs_dur[p][['n_ssns', 'sum_dur', 'sum_swotdur', 'mn_swotfrac']]
    swot_vs_dur[p]['sum_swot'] = swot_vs_dur[p].sum_swotdur - swot_vs_dur[p].sum_dur
    swot_vs_dur[p]['mn_frac']  = swot_vs_dur[p].sum_dur / swot_vs_dur[p].sum_swotdur

    swot_avgs_120[p] = swot_vs_dur[p][['sum_dur','sum_swotdur','sum_swot']].rolling(120, center=True).mean().dropna()
    swot_avgs_120[p]['mn_frac'] = swot_avgs_120[p].sum_dur / swot_avgs_120[p].sum_swotdur

    swot_avgs_10[p]  = swot_vs_dur[p][['sum_dur','sum_swotdur','sum_swot']].rolling( 10, center=True).mean().dropna()
    swot_avgs_10[p]['mn_frac'] = swot_avgs_10[p].sum_dur / swot_avgs_10[p].sum_swotdur

    f1, ax1 = plt.subplots(2,2, figsize=[20,10])
    ax2 = ax1.flatten()
    for i in [0,1,2,3]:
        ax2[i].axvline(21, c='black', alpha=0.1)
        ax2[i].axvline(42, c='black', alpha=0.1)
    for i in [0,1]:
        ax2[i].plot( swot_vs_dur[p].index,  swot_vs_dur[p].sum_swotdur, 'blue',  alpha=0.3, linewidth=0.5, label='_nolegend_')
        ax2[i].plot( swot_vs_dur[p].index,  swot_vs_dur[p].sum_dur,     'green', alpha=0.3, linewidth=0.5, label='_nolegend_')
        ax2[i].plot( swot_vs_dur[p].index,  swot_vs_dur[p].sum_swot,    'red',   alpha=0.3, linewidth=0.5, label='_nolegend_')

    ax2[0].plot(swot_avgs_10[p].index, swot_avgs_10[p].sum_swotdur, 'blue',  alpha=1.0, label='streamed')
    ax2[0].plot(swot_avgs_10[p].index, swot_avgs_10[p].sum_dur,     'green', alpha=1.0, label='tuned')
    ax2[0].plot(swot_avgs_10[p].index, swot_avgs_10[p].sum_swot,    'red',   alpha=1.0, label='swot')
    ax2[1].plot(swot_avgs_120[p].index, swot_avgs_120[p].sum_swotdur, 'blue',  alpha=1.0, label='streamed')
    ax2[1].plot(swot_avgs_120[p].index, swot_avgs_120[p].sum_dur,     'green', alpha=1.0, label='tuned')
    ax2[1].plot(swot_avgs_120[p].index, swot_avgs_120[p].sum_swot,    'red',   alpha=1.0, label='swot')
    ax2[0].legend(loc='upper right')
    ax2[1].legend(loc='upper right')
    ax2[0].set_xlim(0,breakpoint)
    ax2[0].set_ylim(0, swot_vs_dur[p].loc[swot_vs_dur[p].index <= breakpoint, 'sum_swotdur'].max())
    ax2[1].set_xlim(breakpoint, 1000)
    ax2[1].set_ylim(0, swot_vs_dur[p].loc[swot_vs_dur[p].index >= breakpoint, 'sum_swotdur'].max())

    for i in [2,3]:
        ax2[i].plot(swot_vs_dur[p].index, swot_vs_dur[p]['mn_frac'], 'green', alpha=0.2, label='per minute')
        ax2[i].plot(swot_avgs_10[p].loc[30:].index, swot_avgs_10[p].loc[30:, 'mn_frac'], 'blue', alpha=0.4, label='10 min avg')
        ax2[i].plot(swot_avgs_120[p].loc[100:].index, swot_avgs_120[p].loc[100:].mn_frac,  'blue', label='2hr avg')
        ax2[i].set_ylim(0.84, 1.)
        ax2[i].legend(loc='lower right')
    ax2[2].set_xlim(0, breakpoint)
    ax2[3].set_xlim(breakpoint, 1000)

    ax2[0].set_title('Summed minutes, 10-min avg')
    ax2[1].set_title('Summed minutes, 2-hr avg')
    ax2[2].set_title('Fraction viewed')
    ax2[3].set_title('Fraction viewed')
    f1.text(0.5, 0.04, 'Streamed duration (min)', ha='center')
    f1.text(0.5, 0.95, 'SWOT Minutes vs duration ({0})'.format(p), ha='center')
    ax2[0].set_ylabel('Total minutes')
    ax2[2].set_ylabel('Fraction viewed')

    f1.savefig('{0}swot_vs_dur-{1}.png'.format(plot_path, p), dpi=300)
    plt.close(f1)

##############################################################################################
### Duration -- all, for presentation
breakpoint = 200
f1, ax1 = plt.subplots(1, figsize=[10,5])
p = 'all'
ax1.axvline(21, c='black', alpha=0.1)
ax1.axvline(42, c='black', alpha=0.1)
ax1.plot( swot_vs_dur[p].index,  swot_vs_dur[p].sum_swotdur, 'blue',  label='Streaming')
ax1.plot( swot_vs_dur[p].index,  swot_vs_dur[p].sum_dur,     'green', label='Tuning')
ax1.plot( swot_vs_dur[p].index,  swot_vs_dur[p].sum_swot,    'red',   label='SWOT')
ax1.legend(loc='upper right')
ax1.set_xlabel('Streaming duration (min)')
ax1.set_ylabel('Total minutes streamed')
ax1.set_xlim(0,breakpoint)
ax1.set_ylim(0, swot_vs_dur[p].loc[swot_vs_dur[p].index <= breakpoint, 'sum_swotdur'].max())
plt.tight_layout()
f1.savefig('{0}swot_vs_dur_presentation.png'.format(plot_path, p), dpi=300)
plt.close(f1)
##############################################################################################
### Count # of sessions per total duration
ssn_count = pd.DataFrame(swot_vs_dur['all']['n_ssns'])
ssn_count.to_csv('{}ssn_count.csv'.format(plot_path))

f1, ax1 = plt.subplots(1, figsize=[10,5])
ax1.axvline(21, c='black', alpha=0.1)
ax1.axvline(42, c='black', alpha=0.1)
ax1.plot( ssn_count.index,  ssn_count.n_ssns/df.shape[0], 'blue',  label='Total')
# ax1.legend(loc='upper right')
ax1.set_xlabel('Streaming duration (min)')
ax1.set_ylabel('# sessions')
ax1.set_xlim(0, breakpoint)
ax1.set_ylim(0, (ssn_count.loc[ssn_count.index <= breakpoint, 'n_ssns']/df.shape[0]).max())
plt.tight_layout()
f1.savefig('{0}ssns_vs_dur.png'.format(plot_path, p), dpi=300)
plt.close(f1)
# swot_avgs_120[p] = swot_vs_dur[p][['sum_dur', 'sum_swotdur', 'sum_swot']].rolling(120, center=True).mean().dropna()
# swot_avgs_120[p]['mn_frac'] = swot_avgs_120[p].sum_dur / swot_avgs_120[p].sum_swotdur
#
# swot_avgs_10[p] = swot_vs_dur[p][['sum_dur', 'sum_swotdur', 'sum_swot']].rolling(10, center=True).mean().dropna()
# swot_avgs_10[p]['mn_frac'] = swot_avgs_10[p].sum_dur / swot_avgs_10[p].sum_swotdur
##############################################################################################
### Calculate mn swotfrac above vs below breakpoints for each provider
tlist = [0, 5, 10, 14, 18, 22, 30, 45, 60, 80, 100, 200, 500, 10000]
prv_brk = pd.DataFrame([], columns=df.provider.unique(), index=tlist[:(len(tlist)-1)])
for t in range(len(tlist)-1):
    for p in prv_brk.columns:
        try:
            prv_brk.loc[tlist[t], p] = (df.loc[(df.provider==p) & df.swotstreamdur.between(tlist[t], tlist[t+1]), 'duration'].sum() /
                                        df.loc[(df.provider==p) & df.swotstreamdur.between(tlist[t], tlist[t+1]), 'swotstreamdur'].sum())
        except:
            prv_brk.loc[tlist[t], p] = np.nan

pal = sb.color_palette("Paired", n_colors=prv_brk.shape[1])
f1, ax1 = plt.subplots(1)
for i, p in enumerate(prv_brk.columns):
    ax1.plot(range(prv_brk.shape[0]), 1.-prv_brk[p], c=pal[i], label=p)
ax1.legend(loc='upper right', frameon=True, framealpha=0.5)
ax1.set_xticks(np.arange(prv_brk.shape[0]+1)-0.5);
ax1.set_xticklabels(tlist);
ax1.set_title('Typical SWOT by provider/duration')
ax1.set_xlabel('Duration range (min')
ax1.set_ylabel('Mean SWOT fraction')
ax1.set_ylim(0., 0.4)
f1.savefig('{0}provider_duration_bins.png'.format(plot_path))
plt.close(f1)


##############################################################################################
x = np.arange(0, 500)
def gaussian(x, a, b, c):
    return a * np.exp(-(x - b)**2./(2.*c**2.))

coefs = pd.DataFrame([[2., 1., 0., 21., 2., 0., 42., 2.]]*len(plist), index=plist,
                     columns=['c', 'p', 'a1', 'b1', 'c1', 'a2', 'b2', 'c2'])
coefs.loc['netflix',   ['a1','a2']] = [.06, .025]
coefs.loc['hulu', ['c', 'a1', 'c1', 'a2']] = [1.5, .06, 3, .025]

coefs.loc['amazon',  ['c','p']] = [2., 1.1]
coefs.loc['crackle', 'c'] = .25
coefs.loc['youtube', 'c'] = 0.25
coefs.loc['hbo',     'c'] = 0.3

coefs.loc['discovery', 'c'] = .25
coefs.loc['directv',   'c'] = .25
coefs.loc['cw',        'c'] = .25

f1, ax2 = plt.subplots(2, 5, figsize=[20, 10])
ax1 = ax2.flatten()
for i in range(len(plist)):
    p = plist[i]
    ax1[i].plot(swot_vs_dur[p].index, swot_vs_dur[p]['mn_frac'], 'green', alpha=0.8, label='per minute')
    ax1[i].set_xlim(0, 200)
    ax1[i].set_ylim(0.7, 1.0)
    ax1[i].plot(x, 1.-coefs.loc[p, 'c']/x**coefs.loc[p, 'p'] + gaussian(x, *coefs.loc[p, ['a1','b1','c1']]) + gaussian(x, *coefs.loc[p, ['a2','b2','c2']]), 'black', alpha = 0.8)
    ax1[i].set_title(p)
f1.text(0.5, 0.03, 'Streaming duration (min)')
f1.text(0.03, 0.5, 'SWOT fraction', rotation=90)
f1.savefig('{0}provider_duration_trend.png'.format(plot_path))
plt.close(f1)

##############################################################################################
sttm = pd.DatetimeIndex(df.start_time)
edtm = pd.DatetimeIndex(pd.Series(sttm) + pd.Series([dt.timedelta(minutes=m) for m in df.swotstreamdur]))
edtm_tune = pd.DatetimeIndex(df.end_time)
df['st_min'] = sttm.hour * 60 + sttm.minute
df['ed_min'] = edtm.hour * 60 + edtm.minute
df['ed_min_tune'] = edtm_tune.hour * 60 + edtm_tune.minute
times = ['st','ed']
tod = {}
for i in [0,1]:
    t = times[i]
    tod[t] = df.groupby(t+'_min').agg({'duration':  {'sm_tune':'sum'},
                                    'swotstreamdur':{'sm_strm':'sum'}})
    tod[t].columns = tod[t].columns.droplevel(0)
    tod[t]['sm_swot']  =  tod[t].sm_strm - tod[t].sm_tune
    tod[t]['swot_frac'] = tod[t].sm_swot / tod[t].sm_strm
    tod[t]['tune_frac'] = tod[t].sm_tune / tod[t].sm_strm
    tod[t]['swot_frac_10']  = tod[t].swot_frac.rolling( 10, min_periods=1, center=True).mean()
    tod[t]['swot_frac_120'] = tod[t].swot_frac.rolling(120, min_periods=1, center=True).mean()

f1, ax1 = plt.subplots(2,2, sharex=True)
ax2 = ax1.flatten()
for i in [0,1]:
    t = times[i]
    ax1[0,i].plot(tod[t].index, tod[t].sm_strm, 'blue',  linewidth=0.5, label='streaming')
    ax1[0,i].plot(tod[t].index, tod[t].sm_tune, 'green', linewidth=0.5, label='tuning')
    ax1[0,i].plot(tod[t].index, tod[t].sm_swot, 'red',   linewidth=0.5, label='SWOT')
    ax1[0,i].set_title('SWOT fraction vs. {0} time'.format(['start','end'][i]))
    ax1[0,i].legend(loc='upper right')

    ax1[1,i].plot(tod[t].index, tod[t].swot_frac, 'red', alpha=0.2, label='SWOT')
    ax1[1,i].plot(tod[t].index, tod[t].swot_frac_10, 'red', alpha=0.4, label='SWOT (10min avg)')
    ax1[1,i].plot(tod[t].index, tod[t].swot_frac_120, 'red', label='SWOT (120min avg)')

    ax1[1,i].set_xlabel(['Start','End'][i] + ' minute')
    ax1[1,i].legend(loc='upper right')
ax1[0,0].set_xlim(0,1440)
ax1[1,0].set_ylabel('SWOT fraction')
ax1[0,0].set_ylabel('Summed minutes')
f1.savefig('{0}daily_trend.png'.format(plot_path), dpi=300)
plt.close(f1)
##############################################################################################
# sb.pairplot(data=df, vars=['st_min','ed_min','ed_min_tune'], hue='provider')
##############################################################################################
### Data rates per day
date_counts.rename(columns={'hh':'n_events'}, inplace=True)
date_counts['missing'] = date_counts.n_events < 400
date_counts['drop_missing'] = date_counts.n_events.copy()
date_counts.loc[date_counts.missing, 'drop_missing'] = np.nan
date_counts['mn_all' ] = date_counts.n_events.rolling(7, center=True).mean()
date_counts['mn_no_m'] = date_counts.drop_missing.rolling(7, center=True, min_periods=1).mean()

date_counts['sm_drtn'] = df.groupby('date').duration.sum()
date_counts['drtn_drop_missing'] = date_counts.sm_drtn.copy()
date_counts.loc[date_counts.missing, 'drtn_drop_missing'] = np.nan
date_counts['mn_drtn_no_m'] = date_counts.drtn_drop_missing.rolling(7, center=True, min_periods=1).mean()

f1, ax1 = plt.subplots(3, 1, figsize=[20, 10], sharex=True)
f1.autofmt_xdate();
ax1[0].plot(date_counts.index, date_counts.n_events, c=pal[1], alpha=0.5, label='Day sum')
ax1[0].plot(date_counts.index, date_counts.mn_no_m,  c=pal[1], alpha=1.0, label='1-week mean (drop missing)')
ax1[0].set_ylabel('Events per day')
ax1[0].set_xlim(date_counts.index.min(), date_counts.index.max())
ax1[0].legend();
###
ax1[1].plot(date_counts.index, date_counts.sm_drtn,      c=pal[1], alpha=0.5, label='Day sum')
ax1[1].plot(date_counts.index, date_counts.mn_drtn_no_m, c=pal[1], alpha=1.0, label='1-week mean (drop missing)')
ax1[1].set_ylabel('Tuned minutes per day')
ax1[1].legend();
###
ax1[2].plot(sthr_counts.index, sthr_counts.no_swot,       color=pal[1],  alpha = 0.3, label='Sessions w/o SWOT');
ax1[2].plot(sthr_counts.index, sthr_counts.some_swot,     color=pal[5], alpha = 0.3, label='Sessions w/ SWOT');
ax1[2].plot(sthr_counts.index, sthr_counts.no_swot_avg,   color=pal[1],  alpha = 1., label='w/o, 2-hr avg');
ax1[2].plot(sthr_counts.index, sthr_counts.some_swot_avg, color=pal[5], alpha = 1., label='w/,  2-hr avg');
ax1[2].set_ylabel('Events per minute');
ax1[2].legend();
plt.setp(ax1[1].get_xticklabels(), visible=True, rotation=30);
plt.tight_layout()
f1.savefig('{0}day_analysis.png'.format(plot_path))
plt.close(f1)

##############################################################################################

##############################################################################################

##############################################################################################

##############################################################################################

##############################################################################################
