
import timeit
import time
import numpy as np
import pandas as pd
pd.set_option('display.width', 160)
import matplotlib.pyplot as plt
import seaborn as sb
import random
from sklearn.model_selection import train_test_split, KFold, GroupKFold, GridSearchCV
from sklearn.ensemble import RandomForestRegressor, RandomForestClassifier, GradientBoostingRegressor
from sklearn.linear_model import LinearRegression, LogisticRegression
from sklearn.svm import SVC
from sklearn.metrics import r2_score, confusion_matrix, accuracy_score, recall_score, precision_score, f1_score


### Choose which version to run
### Fit on time
# pre, suf = 'ModelPlots/', '_dt'
# fit_on = 'duration'
#
# pre, suf = 'ModelPlots/', '_swotdur'
# fit_on = 'swotdur'

# ## Fit on fraction that is viewing
# pre, suf = 'ModelPlots/', '_frac'
# fit_on = 'vw_frac'

# pre, suf = 'ModelPlots/', '_swotfrac'
# fit_on = 'swot_frac'

# ### Fit on class (at least 5% swot vs. not)
pre, suf1 = 'ModelPlots/v2.1/', '_class'
fit_on = 'swot_class'

###################################################################################
df = pd.read_csv('v2.1/swot_master_table.csv', index_col = 0)

### Convert durations to minutes, rounded, so get rid of precision difference between data sources
df[['duration','swotdur','swotstreamdur']] = (df[['duration','swotdur','swotstreamdur']]/60).round(0)

df['vw_frac']    = 1 - df.swotdur / df.swotstreamdur
df['swot_frac' ] = df.swotdur / df.swotstreamdur
df['swot_class'] = df.swot_frac>0 #>= 0.05
df['hh']         = df.fsite.str.split('-', 1, expand=True).iloc[:,0]

### Start hour as one-hot categoricals (could subdivide further? half/quarter hour?)
df['start_hour'] = pd.DatetimeIndex(df.start_time).hour
start_hour = df.start_hour
df = pd.get_dummies(df, 'start_hour', columns=['start_hour'])
df['start_hour'] = start_hour
start_hour_cols = [c for c in df.columns if 'start_hour_' in c]

### Provider
prvdr = df.provider
df = pd.get_dummies(df, 'prvdr', columns=['provider'])
df['prvdr'] = prvdr
prvdr_cols = [c for c in df.columns if 'prvdr_' in c]

### Model type
mk_mdl = df.make.str.cat([df.model], sep='-')
df['mk_mdl'] = mk_mdl
df = pd.get_dummies(df, 'mk_mdl', columns=['mk_mdl'])
df['mk_mdl'] = mk_mdl
model_cols = [c for c in df.columns if 'mk_mdl_' in c]

### Model/Provider
mk_mdl_prvdr = df.mk_mdl.str.cat([df.prvdr], sep='-')
df['mk_mdl_prvdr'] = mk_mdl_prvdr
df = pd.get_dummies(df, 'mk_mdl_prvdr', columns=['mk_mdl_prvdr'])
df['mk_mdl_prvdr'] = mk_mdl_prvdr
mdl_prvdr_cols = [c for c in df.columns if 'mk_mdl_prvdr_' in c]

### Dvc history parameters?
df['sttm'] = pd.DatetimeIndex(df.start_time)
first_min = df.sttm.min()
df['mins_in_data'] = round((df.sttm - first_min).astype('timedelta64[m]'))

df['dvc_mn_drtn' ]      = np.nan
df['dvc_med_drtn']      = np.nan
df['dvc_sum_drtn']      = np.nan
for dvc in df.fsite.unique():
    subset  = df.fsite == dvc
    usethis = df.loc[subset].sort_values('sttm')
    df.loc[subset, 'dvc_mn_drtn' ] = usethis.swotstreamdur.expanding().mean().shift(1)
    df.loc[subset, 'dvc_med_drtn'] = usethis.swotstreamdur.expanding().median().shift(1)
    df.loc[subset, 'dvc_sum_drtn'] = usethis.swotstreamdur.expanding().sum().shift(1)

df.fillna({'dvc_sum_drtn': 0,
           'dvc_mn_drtn' : df['dvc_mn_drtn'].fillna(df.dvc_mn_drtn.dropna().median()),
           'dvc_med_drtn': df['dvc_med_drtn'].fillna(df.dvc_med_drtn.dropna().median())},
        inplace=True);
df['dvc_frac_indata']   = (df.dvc_sum_drtn     / df.mins_in_data).fillna(1)

###################################################################################
### Split train/test files
hh_list = df.hh.unique()
test_hhs = random.sample(list(hh_list), round(len(hh_list)*0.3))
df['test'] = df.hh.isin(test_hhs)

fit_cols = ['swotstreamdur','dvc_frac_indata','dvc_sum_drtn','dvc_mn_drtn','dvc_med_drtn'] + \
           start_hour_cols + model_cols + prvdr_cols + mdl_prvdr_cols
# fit_cols = ['swotstreamdur'] + start_hour_cols

data = df[['test', 'hh'] + fit_cols + ['duration', 'swotdur', 'vw_frac', 'swot_frac', 'swot_class']]
X_tr = data.loc[~data.test, fit_cols]
y_tr = data.loc[~data.test, fit_on]

X_ts = data.loc[ data.test, fit_cols]
y_ts = data.loc[ data.test, fit_on]

# ----------------------------------------------------------------------------- #
### Set up model parameters
if fit_on == 'swot_class':
    # md    = LogisticRegression()
    # suf2 = '_lr'
    md    = RandomForestClassifier()
    suf2 = '_rf'
    # md    = SVC()
    # suf2 = '_svc'
    scr   = 'f1'
else:
    md    = RandomForestRegressor()
    suf2 = '_rf'
    # md    = LinearRegression()
    # suf2 = '_lr'
    scr   = 'r2'
suf = suf1+suf2
gkf       = list(GroupKFold(n_splits=6).split(X_tr, y_tr, data.loc[~data.test,'hh']))
if isinstance(md, LinearRegression) | isinstance(md, LogisticRegression):
    param_grid = {}
elif isinstance(md, SVC):
        param_grid = {'C': [10.]}
else:
    param_grid = {'n_estimators': [ 50, 100, 200],
                 'max_depth':    [ 8, 12, 15, 20]}
grid = GridSearchCV(estimator=md, param_grid=param_grid, cv=gkf,
                    scoring=scr, n_jobs=6)

# ----------------------------------------------------------------------------- #
### Fit the model
print('Fitting model (start time {})'.format(time.ctime()))
print('{0} models to run'.format( ' x '.join( [str(len(gkf))]+[str(len(param_grid[k])) for k in param_grid.keys()] ) ))
start_time = timeit.default_timer()
grid.fit(X_tr, y_tr)
# rf.fit(X_tr, y_tr)
elapsed = timeit.default_timer() - start_time
print(elapsed)

#################################################################################
### Evaluate the model
print('Evaluating model (start time {})'.format(time.ctime()))
model = grid.best_estimator_
# model = rf

#################################################################################

### Feature importances
if isinstance(md, LinearRegression) | isinstance(md, RandomForestClassifier) | isinstance(md, RandomForestRegressor):
    if isinstance(md, LinearRegression):
        importances = pd.DataFrame({'importance': model.coef_}, index = X_tr.columns)
    elif isinstance(md, RandomForestClassifier) | isinstance(md, RandomForestRegressor):
        importances = pd.DataFrame({'importance': model.feature_importances_}, index = X_tr.columns)
    imp_cols1 = [c for c in ['swotstreamdur','dvc_frac_indata','dvc_sum_drtn','dvc_mn_drtn','dvc_med_drtn'] if c in fit_cols]
    imp_cols2 = [[], ['start_hour']][len([c for c in fit_cols if 'start_hour' in c]) > 0] + \
                [[], ['mk_mdl'    ]][len([c for c in fit_cols if 'mk_mdl'     in c]) > 0] + \
                [[], ['prvdr'     ]][len([c for c in fit_cols if 'prvdr'      in c]) > 0] + \
                [[], ['mdl_prvdr' ]][len([c for c in fit_cols if 'mdl_prvdr'  in c]) > 0]
    imp_cols = imp_cols1 + imp_cols2
    for s in imp_cols2:
        importances.loc[s] = importances.loc[[c for c in fit_cols if s+'_' in c]].sum()
    print(importances.loc[imp_cols])
    importances.to_csv('{0}importances{1}.csv'.format(pre, suf))

#################################################################################################
### scores from each individual run in the grid search
grid_results = grid.cv_results_
tests = [c for c in grid_results.keys() if ('_test_score' in c) & ('split' in c)]
indv_scores = {}
for t in tests:
    indv_scores[t] = grid_results[t]
indv_scores = pd.DataFrame(indv_scores)

scores_dict = {}
for k in param_grid.keys():
    scores_dict[k] = grid_results['param_'+k]
scores_dict['score'] = grid_results['mean_test_score']
raw_scores = pd.DataFrame(scores_dict)
raw_scores.to_csv('{0}raw_scores{1}.csv'.format(pre, suf))
n_vals = pd.Series([])
for c in raw_scores:
    if c != 'score':
        n_vals.loc[c] = raw_scores[c].nunique()
if n_vals.shape[0] >= 2:
    pvt_ind  = n_vals.idxmax()
    pvt_cols = n_vals.drop(pvt_ind).idxmax()
    scores = raw_scores.pivot_table(index=pvt_ind, columns=pvt_cols, values='score', aggfunc='mean')
if (isinstance(md, RandomForestClassifier) |
    isinstance(md, RandomForestRegressor)):
    ### Plot all model scores
    pal_choices = {5: ['red','orange','green','cyan','blue'],
                   4: ['red','green','cyan','blue'],
                   3: ['red','green','blue'],
                   2: ['red','blue'],
                   1: ['blue']}
    pal = pal_choices[scores.shape[1]]
    tests = [c for c in indv_scores.keys() if ('_test_score' in c) & ('split' in c)]
    n = scores.shape[0]
    plt.clf()
    for i, c in enumerate(scores):
        plt.plot(scores.index, scores[c], color=pal[i], label='{0} = {1}'.format(scores.columns.name, c))
        for j, t in enumerate(tests):
            plt.plot(scores.index, indv_scores[t][(n*i):(n*(i+1))],
                     color=pal[i], marker='o', linestyle='', label='_nolegend_')
    plt.legend(loc='best')
    plt.xlabel(scores.index.name)
    plt.ylabel('Score')
    plt.title('Grid search scores for {0} model'.format(y_tr.name))
    plt.savefig('{0}indv_scores{1}.png'.format(pre, suf))
    plt.close('all')
# -----------------------------------------------------------------------------
### Get predictions
### Make dataframe with test set predictions (and some of X and y)
predictions = pd.DataFrame(X_ts[['swotstreamdur']].copy())
predictions['start_hour'] = df.start_hour #X.loc[X.test, 't_start']
predictions['mk_mdl']     = df.mk_mdl #X.loc[X.test, 't_start']
predictions['dt_actual'] = data.loc[data.test, 'duration']
predictions['f_actual'] = data.loc[data.test, 'vw_frac']
predictions['class_actual'] = data.loc[data.test, 'swot_class']
if (y_tr.name in ['swotdur','swot_frac']):
    predictions['dt_actual'] = data.loc[data.test, 'swotdur']
    predictions['f_actual']  = data.loc[data.test, 'swot_frac']

if (y_tr.name == 'vw_frac') | (y_tr.name == 'swot_frac'):
    predictions['f_pred']   = model.predict(X_ts)
    ### Recover minute durations from fractions
    predictions['dt_pred']   = predictions.f_pred   * predictions.swotstreamdur
elif (y_tr.name == 'duration') | (y_tr.name == 'swotdur'):
    predictions['dt_pred']   = model.predict(X_ts)
    ### Recover fraction from duration
    predictions['f_pred']   = predictions.dt_pred   / predictions.swotstreamdur
elif y_tr.name == 'swot_class':
    predictions['class_pred']   = model.predict(X_ts)

#################################################################################
#################################################################################
### Plot prediction grid
if fit_on == 'swot_class':
    x_col = 'swotstreamdur'
    y_col = 'prvdr'
    x_grid = np.arange(df[x_col].min(), df[x_col].max()*1.0001, (df[x_col].max()-df[x_col].min())/1000. )
    if y_col in ['mk_mdl','prvdr']:
        ### rank make/model by how much SWOT frac each has, and assign a number for plotting
        dvc_info = pd.DataFrame(df.groupby(y_col).fsite.nunique())
        dvc_info.columns = ['n_dvcs']
        dvc_info['mn_swotfrac'] = df.groupby(y_col).swot_frac.mean()
        dvc_info = dvc_info.sort_values('mn_swotfrac')
        dvc_info['num'] = range(dvc_info.shape[0])
        ### Add the mk/mdl number to the full data
        df[y_col+'_num']   = list(dvc_info.loc[df[y_col], 'num'])
        y_grid = dvc_info.num
        y_plot = y_col+'_num'
    elif y_col == 'start_hour':
        y_grid = df.start_hour.unique()
        y_grid.sort()
        y_plot = 'start_hour'
    else:
        y_grid = np.arange(df[y_col].min(), df[y_col].max()*1.0001, (df[y_col].max()-df[y_col].min())/1000. )
        y_plot = y_col
    ### create a mesh over the model features, to get a grid of the model's output
    xx, yy = np.meshgrid(x_grid, y_grid)
    svc_grid = pd.DataFrame(np.c_[xx.ravel(), yy.ravel()])
    svc_grid.columns = [x_col, y_plot]
    if y_col in ['mk_mdl','prvdr']:
        svc_grid[y_col] = list(dvc_info.reset_index().set_index('num').loc[svc_grid[y_col+'_num'], y_col])
    if y_col == 'start_hour':
        svc_grid[y_col] = svc_grid[y_col].astype('int')
    if y_col in ['mk_mdl','prvdr','start_hour']:
        save_col = svc_grid[y_col]
        svc_grid = pd.get_dummies(svc_grid, y_col, columns=[y_col])
        svc_grid[y_col] = save_col
    ### Add static columns for the other fit_cols, filled with most common value or set manually
    for k in fit_cols:
        if k not in svc_grid.columns:
            svc_grid[k] = [df[k].median()]*svc_grid.shape[0]
    ### One of the more likely device types to have swot
    if y_col != 'mk_mdl':
        svc_grid['mk_mdl_fire-tv'] = True
    ### Get model predictions at each point in the grid
    print('making prediction grid')
    Z = model.predict(svc_grid[fit_cols])
    Z2 = Z.reshape(xx.shape)
    # Zmap = Z2.astype('int')#np.array(['red' if z else 'blue' for z in Z]).reshape(xx.shape)

    ### Create figure
    f1, ax1 = plt.subplots(1,figsize=[10,5])
    ### Plot the model's prediction with different colors
    ax1.contourf(xx, yy, Z2, cmap=plt.cm.coolwarm, alpha=0.5)
    ### Plot the test data points, with vertical jitter added to the mk/mdl for visibility
    for i in range(2):
        if y_col in ['mk_mdl', 'prvdr', 'start_hour']:
            noise = np.random.normal(0, 0.1, df.loc[(df.test) & (df.swot_class == i)].shape[0])
        else:
            noise = [0] * df.loc[(df.test) & (df.swot_class == i)].shape[0]
        ax1.scatter(df.loc[(df.test) & (df.swot_class == i), x_col],
                    df.loc[(df.test) & (df.swot_class == i), y_plot] + \
                    noise,
                    c=['blue','red'][i], s = 2, alpha = 0.5, label = ['SWT','SWOT'][i])
    ax1.set_xlabel(x_col);
    ax1.set_ylabel(y_col);
    if y_col in ['mk_mdl','prvdr','start_hour']:
        ax1.set_yticks( y_grid);
        if y_col in ['mk_mdl','prvdr']:
            ax1.set_yticklabels(dvc_info.index);
    ax1.legend(loc='best', frameon=True, framealpha=0.5)
    # if x_col == 'swotstreamdur':
    ax1.set_xlim(0, df[x_col].quantile(.999))
    # else:
    #     ax1.set_xlim(x_grid[0] - (x_grid[1] - x_grid[0]) * 0.5, x_grid[-1] + (x_grid[1] - x_grid[0]) * 0.5)
    if y_col not in ['mk_mdl','prvdr','start_hour']:
        ax1.set_ylim(0, df[y_plot].quantile(.999))
    else:
        ax1.set_ylim(y_grid[0] - (y_grid[1] - y_grid[0])*0.5, y_grid[-1] + (y_grid[1] - y_grid[0])*0.5)
    ax1.set_title('Recall = {0:.3f}'.format(recall_score(predictions.class_actual, predictions.class_pred)))

    plt.tight_layout()
    f1.savefig('{0}prediction_grid_{1}-{2}BY{3}.pdf'.format(pre, suf, x_col, y_col), format='pdf')
    plt.close(f1)

##############################################################################################
### Visualize predictions
if y_tr.name == 'swot_class':
    ### Classification version
    conf = pd.DataFrame(confusion_matrix(predictions.class_actual, predictions.class_pred),
                 index   = ['is_tuning','is_swot'],
                 columns = ['pred_tuning','pred_swot'])
    classes = ['tuning','swot']
    f1, ax1 = plt.subplots(1, figsize=[6,6])
    with sb.axes_style("whitegrid"):
        plt.imshow(conf, interpolation='nearest', cmap=plt.cm.Blues)
    plt.title('swot_class Confusion Matrix')
    plt.colorbar()
    tick_marks = np.arange(len(classes))
    plt.xticks(tick_marks, classes, rotation=45)
    plt.yticks(tick_marks, classes)

    thresh = conf.max().max() / 2.
    for i in range(conf.shape[0]):
        for j in range(conf.shape[1]):
            plt.text(j, i, conf.iloc[i, j],
                     horizontalalignment="center",
                     color="white" if conf.iloc[i, j] > thresh else "black")

    plt.tight_layout()
    plt.ylabel('True label')
    plt.xlabel('Predicted label')
    plt.savefig('{0}conf_matrix{1}.png'.format(pre,suf))
    plt.close('all')
    conf['total'] = conf.sum(axis=1)
    conf.loc['total'] = conf.sum()
    print(conf)
    ##############################################################################################
    ### Plot predictions and actual
    f1, ax1 = plt.subplots(1, 2, figsize=[18, 6], sharex=True, sharey=True)

    for i in [0,1]:
        ax1[0].plot(predictions.loc[predictions.class_actual==i,'swotstreamdur'],
                    predictions.loc[predictions.class_actual==i,'dt_actual'],
                    ['ko','ro'][i], alpha=0.1, ms = 5, label=['Not SWOT','SWOT'][i])
    # ax1[0].plot(predictions.swotstreamdur, predictions.dt_pred, 'bo', alpha=0.05, label='Predicted')
    leg = ax1[0].legend(loc='upper left')
    for lh in leg.legendHandles:
        lh._legmarker.set_alpha(1)

    ax1[0].set_title('Streamed vs. viewed durations and class')
    ax1[0].set_xlabel('Length of streamed session')
    ax1[0].set_ylabel('Length of viewed session')
    ax1[0].set_xlim(0, 800)
    ax1[0].set_ylim(0, 800)

    for i in [0,1]:
        ax1[1].plot(predictions.loc[predictions.class_pred==i,'swotstreamdur'],
                    predictions.loc[predictions.class_pred==i,'dt_actual'],
                    ['ko','ro'][i], alpha=0.1, ms = 5, label=['Not SWOT','SWOT'][i])
    ax1[1].set_title('Predicted class (recall = {0:.2f})'.format(recall_score(predictions.class_actual, predictions.class_pred)))
    ax1[1].set_xlabel('Length of streamed session')
    ax1[1].set_ylabel('Length of viewed session')

    plt.tight_layout()
    f1.savefig('{0}predictions{1}.png'.format(pre, suf))
    plt.close(f1)
##############################################################################################
else:
    ### Regression version
    ### Plot predictions and actual
    f1, ax1 = plt.subplots(1, 3, figsize=[18,6])

    ax1[0].plot(predictions.swotstreamdur, predictions.dt_actual, 'ko',alpha=0.05,   label = 'Actual')
    ax1[0].plot(predictions.swotstreamdur, predictions.dt_pred,   'bo',alpha=0.05,   label = 'Predicted')
    leg=ax1[0].legend(loc='upper left')
    for lh in leg.legendHandles:
        lh._legmarker.set_alpha(1)

    ax1[0].set_title('Streamed vs. viewed durations (R2 = {0:.2f})'.format(r2_score(predictions.dt_actual, predictions.dt_pred)))
    ax1[0].set_xlabel('Streamed duration')
    if 'swot' in y_tr.name:
        ax1[0].set_ylabel('SWOT duration')
        ax1[0].set_xlim(0, 800)
        ax1[0].set_ylim(0, 800)
    else:
        ax1[0].set_ylabel('Viewed duration')
        ax1[0].set_xlim(0, 800)
        ax1[0].set_ylim(0, 800)

    ax1[1].plot(predictions.swotstreamdur, predictions.dt_actual, 'ko',alpha=0.05,   label = 'Actual')
    ax1[1].plot(predictions.swotstreamdur, predictions.dt_pred,   'bo',alpha=0.05,   label = 'Predicted')
    ax1[1].set_xscale('log');
    ax1[1].set_yscale('log');
    ax1[1].set_title('Log view')
    ax1[1].set_xlabel('Streamed duration')
    if 'swot' in y_tr.name:
        ax1[1].set_ylabel('SWOT duration')
    else:
        ax1[1].set_ylabel('Viewed duration')

    ax1[2].plot(predictions.swotstreamdur, predictions.f_actual, 'ko',alpha=0.05,   label = 'Actual');
    ax1[2].plot(predictions.swotstreamdur, predictions.f_pred,   'bo',alpha=0.05,   label = 'Predicted');
    ax1[2].set_title('Stream drtn vs viewed fraction (R2 = {0:.2f})'.format(r2_score(predictions.f_actual, predictions.f_pred)))
    ax1[2].set_xlabel('Streamed duration')
    if 'swot' in y_tr.name:
        ax1[2].set_ylabel('SWOT duration')
    else:
        ax1[2].set_ylabel('Viewed duration')
    ax1[2].set_xscale('log');

    plt.tight_layout()
    f1.savefig('{0}predictions{1}.png'.format(pre,suf))
    plt.close(f1)
    ##############################################################################################
    ### RPD vs NPM minute counts, and mean/median per minute
    f1, ax1 = plt.subplots(2, 2, figsize=[10,10])

    max_x = max(pd.concat([y_tr, pd.Series(model.predict(X_tr)), y_ts, pd.Series(model.predict(X_ts))]))

    x = np.arange(0, 1e6+1, 10)
    ax1[0,0].plot(x,   1*x, alpha = 0.3, color='red')
    ax1[0,0].plot(y_tr, model.predict(X_tr), 'ko',alpha=0.05,   label = 'Train')
    ax1[0,0].set_title('Actual vs. Predicted, train set (R2 = {0:.2f})'.format(model.score(X_tr, y_tr)))
    ax1[0,0].set_xlabel('Actual duration')
    ax1[0,0].set_ylabel('Predicted duration')

    ax1[0,1].plot(x,   1*x, alpha = 0.3, color='red')
    ax1[0,1].plot(y_ts, model.predict(X_ts), 'bo',alpha=0.05,   label = 'Test');
    ax1[0,1].set_title('Actual vs. Predicted, test set (R2 = {0:.2f})'.format(model.score(X_ts, y_ts)))
    ax1[0,1].set_xlabel('Actual duration')
    ax1[0,1].set_ylabel('Predicted duration')

    ax1[0, 0].set_xlim(0, max_x)
    ax1[0, 0].set_ylim(0, max_x)
    ax1[0, 1].set_xlim(0, max_x)
    ax1[0, 1].set_ylim(0, max_x)
    if  y_tr.name == 'duration':
        ax1[1, 0].plot(x, 1 * x, alpha=0.3, color='red')
        ax1[1, 0].plot(y_tr, model.predict(X_tr), 'ko', alpha=0.05, label='Train')
        # ax1[1,0].set_title('Actual vs. Predicted, train set (R2 = {0:.2f})'.format(model.score(X_tr, y_tr)))
        ax1[1, 0].set_xlabel('Actual viewed duration')
        ax1[1, 0].set_ylabel('Predicted viewed duration')

        ax1[1, 1].plot(x, 1 * x, alpha=0.3, color='red')
        ax1[1, 1].plot(y_ts, model.predict(X_ts), 'bo', alpha=0.05, label='Test');
        # ax1[1,1].set_title('Actual vs. Predicted, test set (R2 = {0:.2f})'.format(model.score(X_ts, y_ts)))
        ax1[1, 1].set_xlabel('Actual viewed duration')
        ax1[1, 1].set_ylabel('Predicted viewed duration')

        ax1[1,0].set_xscale('log');
        ax1[1,0].set_yscale('log');
        ax1[1,0].set_xlim(2e1,max_x)
        ax1[1,0].set_ylim(1e2,max_x)
        ax1[1,1].set_xscale('log');
        ax1[1,1].set_yscale('log');
        ax1[1,1].set_xlim(2e1,max_x)
        ax1[1,1].set_ylim(1e2,max_x)

    plt.tight_layout()
    f1.savefig('{0}train_v_test{1}.png'.format(pre,suf))
    plt.close(f1)



