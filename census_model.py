### Manual model
import numpy as np
import pandas as pd
pd.set_option('display.width', 180)
import matplotlib.pyplot as plt
import seaborn as sb
import datetime as dt
import random as rd
import sys
# from manual_fns import *


### Import data
# vers = 'DCR_data/NewPanelData'
vers = 'v2_4'
dur  = '9mo'
all_months_i = [2, 3, 4, 5, 6, 7, 8, 9]
all_months_n = ['Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep']

df = pd.read_csv('{0}/swot_master_table_{1}.csv'.format(vers, dur), index_col=0)

### Convert durations to minutes, rounded, so get rid of precision difference between data sources
df[['duration','swotdur','swotstreamdur']] = (df[['duration','swotdur','swotstreamdur']]/60).round(0)

### Add some useful columns
df['vw_frac']    = 1 - df.swotdur / df.swotstreamdur
df['swot_frac']  = df.swotdur / df.swotstreamdur
df['swot_class'] = df.swot_frac > 0  # >= 0.05
df['hh']         = df.fsite.str.split('-', 1, expand=True).iloc[:, 0]
df['month'] = pd.to_datetime(df.start_time).map(lambda t: t.month)
### v2.4 stopped outputing make and model, so this is for backwards compatibility
if vers in ['v2.1', 'v2.2', '2.3']:
    df['device'] = df.make.str.cat([df.model], sep='-')
else:
    df[['make','model']] = df.device.str.split(' ', 1, expand=True)

### Roll up roku devices into one group
df.loc[df.device.str.contains('Roku'), 'device'] = 'Roku'

### Combine census providers into one
### Reclassify census providers as one
df['orig_prv'] = df.provider.copy()
df.loc[df.provider.isin(['cw','crackle','directv','discovery']), 'provider'] = 'census'

### Drop youtube due to its much lower swot fraction
df = df.loc[df.provider != 'youtube']

########################################################################
########################################################################
########################################################################
### Count devices per model/provider cell
n_dvcs = df.pivot_table(index='device', columns='provider', values='fsite', aggfunc=pd.Series.nunique).fillna(0)

all_minutes = df.duration.sum()
### Panel provider market shares
prov_frac = df.groupby('provider').duration.sum().sort_values(ascending=False)/all_minutes*100
orig_prv_sum = df.loc[df.provider=='census'].groupby('orig_prv').duration.sum().sort_values(ascending=False)
orig_prv_frac = orig_prv_sum/orig_prv_sum.sum()*100

pal = sb.color_palette("Set1", df.orig_prv.nunique()+1, desat=0.7)
# pal = sb.color_palette("Set1", 2, desat=0.7)*(int(df.orig_prv.nunique()/2)+1)
f1, ax1 = plt.subplots(1, figsize=[20, 10])
for i, p in enumerate(prov_frac.index):
    if i == 0:
        left = 0
    else:
        left = prov_frac.iloc[:i].sum()
    ax1.barh(1, prov_frac.loc[p], color=pal[i], left=left, label=p)
    ax1.text(s='{0}: {1:0.1f}%'.format(p, prov_frac.loc[p]),
             x=left + prov_frac.loc[p]/2., y=1, rotation=90, va='center', ha='center')
for j, p in enumerate(orig_prv_frac.index):
    if j == 0:
        left = 0
    else:
        left = orig_prv_frac.iloc[:j].sum()
    ax1.barh(0, orig_prv_frac.loc[p], color=pal[i+j+1], left=left, label=p)
    ax1.text(s='{0}: {1:0.1f}%'.format(p, orig_prv_frac.loc[p]),
             x=left + orig_prv_frac.loc[p]/2, y=0, rotation=90, va='center', ha='center')
# ax1.legend(loc='upper right', frameon=True, framealpha=0.5)
ax1.plot([0,prov_frac.drop('census').sum()], [.4, .6], c='black', lw=1)
ax1.plot([100,100], [.4, .6], c='black', lw=1)
ax1.set_title('Provider market share in OTT panel vs. census')
ax1.set_yticks([0, 1]);
ax1.set_yticklabels(['Panel: census providers', 'Panel: all providers']);
plt.tight_layout()
f1.savefig('ModelPlots/CensusModel/prov_marketshare.png')
plt.close(f1)

##############################################################################################
### calculate dimensions for a grid of n subplots
def layout(n):
    n1 = int(np.ceil(n**0.5))
    if n1*(n1-1) >= n:
        n2 = n1-1
    else:
        n2 = n1
    return n1, n2

########################################################################
def get_dfs(df, i, n_months=1):
    modeldf   = df.loc[df.month.isin(list(range( (i-n_months), i)))]
    predictdf = df.loc[df.month == i]
    return modeldf, predictdf
########################################################################
def create_bins_v0(df, threshold=50):
    '''Average everything together -- no divisions'''
    n_dvcs = df.pivot_table(index='device', columns='provider', values='fsite', aggfunc=pd.Series.nunique).fillna(0)
    ### Create df to assign bins to each provider/model combination
    bin_names = pd.DataFrame([], index=n_dvcs.index, columns=n_dvcs.columns).fillna(0)
    return bin_names
########################################################################
def create_bins_v4(df, threshold=50, thresh2=1):
    '''all devices are kept separate for Netflix'''
    n_dvcs = df.pivot_table(index='device', columns='provider',
                            values='fsite', aggfunc=pd.Series.nunique).fillna(0)
    ### Create df to assign bins to each provider/model combination
    bin_names = pd.DataFrame([], index=n_dvcs.index, columns=n_dvcs.columns)
    ### first pass: label all netflix bins as separate
    p = 'netflix'
    for m in bin_names.index:
        if n_dvcs.loc[m, p] >= min(threshold, thresh2):
            bin_names.loc[m, p] = m + '_' + p
    ### second pass: identify individual cells that meet threshold
    for m in bin_names.index:
        for p in bin_names.columns:
            if n_dvcs.loc[m, p] >= threshold:
                bin_names.loc[m, p] = m+'_'+p
    ### third pass: group remaining cells by provider, ignore threshold
    for p in bin_names.columns:
        bin_names.loc[bin_names[p].isnull(), p] = 'misc_'+p
    return bin_names

########################################################################
def create_bins_dvcbin(df, threshold=50):
    '''Average everything together -- no divisions'''
    n_dvcs = df.groupby('device').agg({'fsite':pd.Series.nunique})
    ### Create df to assign bins to each provider/model combination
    bin_names = pd.DataFrame([], index=n_dvcs.index,
                             columns=df.pivot_table(index='device', columns='provider', values='fsite', aggfunc=pd.Series.nunique).columns)
    for d in n_dvcs.index:
        if n_dvcs.loc[d, 'fsite'] >= threshold:
            bin_names.loc[d] = d
        else:
            bin_names.loc[d] = 'Other'
    return bin_names
###################### Single function containing binmodels ############
def create_bins(modeldf, threshold=50, thresh2=1, binmodel='v7'):
    if binmodel == 'v0':
        return create_bins_v0(modeldf)
    elif binmodel == 'v4':
        return create_bins_v4(modeldf)
    elif binmodel == 'dvcbin':
        return create_bins_dvcbin(modeldf, threshold=threshold)
    else:
        assert 1==0, 'ERROR: invalid bin model version {0}'.format(binmodel)
########################################################################
def get_bin_key(modeldf, binmodel='v4', threshold=50, manual_bins={}):
    if binmodel == 'prv':
        binmodel='provider'
    if binmodel == 'dvc':
        binmodel='device'
    if binmodel in ['provider','device']:
        bin_key = pd.DataFrame(list(zip(
            [p for p in modeldf.provider.unique() for d in modeldf.device.unique()],
            [d for p in modeldf.provider.unique() for d in modeldf.device.unique()])),
            columns=['provider','device'])
        bin_key['bin_name'] = bin_key[binmodel]
    else:
        bin_names = create_bins(modeldf, binmodel=binmodel, threshold=threshold, thresh2=1)
        ### Flatten into a list of models/providers
        bin_names['device'] = bin_names.index
        ### Key for looking up which bin each cell belongs to
        bin_key = pd.melt(bin_names, id_vars=['device'], value_name='bin_name')
    for p in manual_bins.keys():
        for i in manual_bins[p].keys():
            bin_key.loc[(bin_key.provider == p) &
                        (bin_key.device.isin(manual_bins[p][i])),
                        'bin_name'] = '{0:0>2}-{1}'.format(i, p)
    return bin_key
########################################################################
def fit_model(modeldf, bin_key, smoothing=500, min_periods=100, do_flatten=True,
              flatten_multiplier=2, flatten_start_thresh=0.1):
    ### Get duration model using each bin as a separate column
    pred_lookup = pd.DataFrame([], index=range(1, int(modeldf.swotstreamdur.max())+1), columns=bin_key.bin_name.unique())
    for c in pred_lookup.columns:
        ### device-provider pairs that are in this bin
        cells = bin_key.loc[bin_key.bin_name == c]
        ### subset of events that come from those device-provider pairs, sorted by total duration
        subset = modeldf.loc[modeldf.device.isin(cells.device) & modeldf.provider.isin(cells.provider),
                             ['swotstreamdur', 'duration', 'swot_frac']].sort_values('swotstreamdur')
        if do_flatten & (subset.shape[0] < smoothing*flatten_multiplier):
            ### too few events => take bin total swot_frac
            pred_lookup[c] = 1. - subset.duration.sum() / subset.swotstreamdur.sum()
        else:
            ### rolling average of swot fraction
            subset['mn_swot_frac'] = subset.swot_frac.rolling(smoothing, center=True,
                                                              min_periods=min(smoothing, min_periods, subset.shape[0])).mean()
            ### groupby to get a single average swot fraction for each durationd
            pred_lookup[c] = subset.groupby('swotstreamdur').mn_swot_frac.mean()
            pred_lookup[c] = pred_lookup[c].fillna(method='ffill')
            ### If not enough points at short durations, patch with means at each point, then smooth
            if np.isnan(pred_lookup.loc[1, c]):
                dr=1
                while np.isnan(pred_lookup.loc[dr, c]):
                    pred_lookup.loc[dr, c] = subset.loc[subset.swotstreamdur == dr, 'swot_frac'].mean()
                    dr += 1
                pred_lookup.loc[0, c] = 0
                pred_lookup.sort_index(inplace=True)
                pred_lookup.loc[1:(dr-1), c] = pred_lookup.loc[:(dr-1), c].rolling(3, center=True, min_periods=1).mean()
                pred_lookup = pred_lookup.drop(0, axis=0)
            ### If first point value is too high, assume fit is bad and flatten the curve
            if do_flatten & (pred_lookup[c].iloc[0] > flatten_start_thresh):
                pred_lookup[c] = 1. - subset.duration.sum() / subset.swotstreamdur.sum()
    return pred_lookup

########################################################################
def predict_from_lookup_and_key(predictdf, pred_lookup, bin_key):
    preds = predictdf[['device', 'provider', 'duration', 'swotstreamdur', 'swot_frac']].copy()
    preds['pred_swotfrac'] = np.nan
    for c in pred_lookup.columns:
        ### device-provider pairs that are in this bin
        cells = bin_key.loc[bin_key.bin_name == c]
        ### subset of events that come from those device-provider pairs, sorted by total duration
        binmembers = predictdf.device.isin(cells.device) & predictdf.provider.isin(cells.provider)
        ### assign correct swotfrac for duration to binmembers
        preds.loc[binmembers, 'pred_swotfrac'] = list(pred_lookup.loc[preds.loc[binmembers, 'swotstreamdur'], c])
    preds['pred_drtn'] = preds.swotstreamdur * (1. - preds.pred_swotfrac)
    return preds
########################################################################
def model_swot(modeldf, predictdf, model_vers = 'v4', threshold=150, smoothing=3000, flatten_multiplier=2):
    do_flatten = True
    if 'drtn' not in model_vers:
        smoothing = sys.maxsize
        flatten_multiplier=1
    if 'drtn-no-' in model_vers:
        do_flatten = False
        bin_key = get_bin_key(modeldf, binmodel=model_vers.split('-')[-1], threshold=threshold)
    else:
        if len(model_vers.split('-')) == 1:
            bin_key = get_bin_key(modeldf, binmodel=model_vers, threshold=threshold)
        elif len(model_vers.split('-')) == 2:
            bin_key = get_bin_key(modeldf, binmodel=model_vers.split('-')[1], threshold=threshold)
        else:
            assert 1==0, 'ERROR: invalid bin model version {0}'.format(model_vers)
    pred_lookup = fit_model(modeldf, bin_key, smoothing=smoothing,
                            do_flatten=do_flatten, flatten_multiplier=flatten_multiplier)
    if modeldf.swotstreamdur.max() < predictdf.swotstreamdur.max():
        pred_lookup = pred_lookup.reindex(
            range(int(pred_lookup.index.min()), int(predictdf.swotstreamdur.max()) + 1)).fillna(method='ffill')
    preds = predict_from_lookup_and_key(predictdf, pred_lookup, bin_key)
    return preds

########################################################################
def calc_score(df, truecol = 'duration', predcol='pred_drtn', groupcol='provider'):
    d = df[[truecol, predcol, groupcol]].copy()
    d['delta'] = d[predcol] - d[truecol]
    aggs = pd.DataFrame(d.groupby(groupcol)['duration'].sum())#.astype('int')
    aggs.columns = ['true_drtn']
    aggs['pred_drtn'] = d.groupby(groupcol)['pred_drtn'].sum()#.astype('int')
    # aggs['delta'] = d.groupby(groupcol)['delta'].sum()
    aggs['delta'] = aggs.pred_drtn - aggs.true_drtn
    aggs['reldiff'] = aggs.delta/aggs.true_drtn
    aggs.loc['all'] = [df[truecol].sum(),
                       df[predcol].sum(),
                       df[predcol].sum() - df[truecol].sum(),
                      (df[predcol].sum() - df[truecol].sum())/df[truecol].sum()]
    return aggs
########################################################################
def rms(x, axis=0):
    return (x**2).mean(axis=axis)**0.5

########################################################################
### Presentation plots
pres_dvcs = ['Smart TV','Roku','Google Chromecast','Google TV']

### Duration curves for presentation
binmodel  = 'dvc'
n_mn      = 3
binsize   = 25
month_i   = all_months_i[-3:]
month_lab = all_months_n[-3:]

### Use same bins for all months
modeldf, predictdf = get_dfs(df, 7, n_mn)
bin_key = get_bin_key(modeldf, binmodel=binmodel, threshold=binsize)

### Plot for each flattening multiplier
for flatten, smoothing in [(1, 1000), (2, 500), (4, 250), (5, 250)]:
    print(flatten, smoothing)
    ### Generate duration curves for all months
    lookups = {}
    for m in month_i:
        modeldf = get_dfs(df, m, n_months=n_mn)[0]
        lookups[m] = fit_model(modeldf, bin_key, smoothing=smoothing, flatten_multiplier=flatten)
    f1, ax1 = plt.subplots(*layout(len(pres_dvcs)), figsize=[6, 6], sharex=True, sharey=True)
    ax2 = ax1.flatten()
    for i, c in enumerate(pres_dvcs):
        for j, m in enumerate(month_i):
            modeldf = get_dfs(df, m, n_months=n_mn)[0]
            ax2[i].plot(lookups[m].index, lookups[m][c],
                        label='{0}'.format(month_lab[j],
                                           modeldf.loc[
                                           (modeldf.provider.isin(bin_key.loc[bin_key.bin_name==c].provider)) &
                                           (modeldf.device.isin(  bin_key.loc[bin_key.bin_name==c].device  ))].shape[0]))
        ax2[i].legend(loc='upper right', frameon=True, framealpha=0.6)
        ax2[i].set_title(c)
        ax2[i].set_xscale('log')
    f1.text(0.5, 0.02, 'Streaming Duration (min)', ha='center', va='center')
    f1.text(0.03, 0.5, 'SWOT Fraction', ha='center', va='center', rotation=90)
    # plt.tight_layout()
    f1.subplots_adjust(hspace=0.15, wspace=0.05, right=.99, top=.94, left=0.11, bottom=.1)
    f1.savefig('DataPlots/drtn_curves_{0}_b{1}_s{2}x{3}_m{4}.png'.format(binmodel, binsize, smoothing, flatten, n_mn), dpi=800)
    plt.close(f1)

flatten, smoothing, c = 1, 1000, 'Roku'
lookups = {}
for m in month_i:
    modeldf = get_dfs(df, m, n_months=n_mn)[0]
    lookups[m] = fit_model(modeldf, bin_key, smoothing=smoothing, flatten_multiplier=flatten)
f1, ax1 = plt.subplots(1, figsize=[4, 4])
for j, m in enumerate(month_i):
    modeldf = get_dfs(df, m, n_months=n_mn)[0]
    ax1.plot(lookups[m].index, lookups[m][c],
                label='{0}'.format(month_lab[j],
                                   modeldf.loc[
                                       (modeldf.provider.isin(bin_key.loc[bin_key.bin_name == c].provider)) &
                                       (modeldf.device.isin(bin_key.loc[bin_key.bin_name == c].device))].shape[0]))
ax1.legend(loc='upper right', frameon=True, framealpha=0.6)
ax1.set_title(c)
ax1.set_xscale('log')
ax1.set_xlabel('Streaming Duration (min)')
ax1.set_ylabel('SWOT Fraction')
plt.tight_layout()
f1.savefig('DataPlots/drtn_curves_onedvc_{0}_b{1}_s{2}x{3}_m{4}.png'.format(binmodel, binsize, smoothing, flatten, n_mn),
           dpi=800)
plt.close(f1)

########################################################################
### Actual minutes viewed vs. predicted
n=3
v='drtn-dvc'
b=25
s=500
x=2
month_i   = all_months_i[(n - 1):]
month_lab = all_months_n[(n-1):]
monthly_strm = pd.DataFrame([], index=month_i, columns=n_dvcs.index)
monthly_true = pd.DataFrame([], index=month_i, columns=n_dvcs.index)
monthly_pred = pd.DataFrame([], index=month_i, columns=n_dvcs.index)
for m in month_i:
    modeldf, predict_df = get_dfs(df, m, n_months=n_mn)
    modelled = model_swot(*get_dfs(df, m, n), model_vers=v, threshold=b, smoothing=s, flatten_multiplier=x)
    row = modelled.groupby('device').agg({'pred_drtn':'sum','duration':'sum','swotstreamdur':'sum'})
    monthly_strm.loc[m] = row.swotstreamdur
    monthly_true.loc[m] = row.duration
    monthly_pred.loc[m] = row.pred_drtn

cols = [sb.color_palette("Paired")[1], sb.color_palette("Paired")[2], sb.color_palette("Paired")[3]]

f1, ax1 = plt.subplots(*layout(len(pres_dvcs)), figsize=[7, 6])
ax2 = ax1.flatten()
for i, c in enumerate(pres_dvcs):
    ax2[i].plot(monthly_strm.index, monthly_strm[c], c=cols[0], label='Streaming')
    ax2[i].plot(monthly_true.index, monthly_true[c], c=cols[1],  label='True tuning')
    ax2[i].plot(monthly_true.index, monthly_pred[c], c=cols[2],  label='Predicted tuning', linestyle='--')
    ax2[i].set_title(c)
    ax2[i].set_xticks(month_i)
    ax2[i].set_xticklabels(month_lab, rotation=20, ha='right')
    ax2[i].set_ylim(0, monthly_strm[c].max()*1.1)
ax2[0].legend(loc='upper left')
f1.text(0.02, 0.5, 'Total minutes', ha='center', va='center', rotation=90)
f1.subplots_adjust(hspace=0.25, wspace=0.22, right=.99, top=.96, left=0.115, bottom=.06)
f1.savefig('DataPlots/pres_minutes_predicted.png', dpi=700)
plt.close(f1)

### RMS error %
print(rms((monthly_pred - monthly_true)/monthly_true).loc[pres_dvcs]*100)
print('Total rms: ', rms((monthly_pred.sum(axis=1) - monthly_true.sum(axis=1))/monthly_true.sum(axis=1))*100)
print((monthly_pred.sum(axis=1) - monthly_true.sum(axis=1))/monthly_true.sum(axis=1)*100)

########################################################################
### Total impact
impact = pd.DataFrame({'swotfrac_true': 1 - monthly_true.sum(axis=1)/monthly_strm.sum(axis=1),
                       'swotfrac_pred': 1 - monthly_pred.sum(axis=1)/monthly_strm.sum(axis=1),
                       'perc_error':    100*(monthly_pred.sum(axis=1)-monthly_true.sum(axis=1))/monthly_true.sum(axis=1),
                       'tuned_true': monthly_true.sum(axis=1),
                       'tuned_pred': monthly_pred.sum(axis=1)})
print(impact)

# with sb.set_style('ticks'):
f1, ax1 = plt.subplots(1, figsize=[4, 3])
ax1.barh([0, 1, 2],
     [monthly_pred.sum(axis=1).loc[9], monthly_true.sum(axis=1).loc[9], monthly_strm.sum(axis=1).loc[9]],
     color=[cols[2], cols[1], cols[0]])
# ax1.set_facecolor('white')
# for y, x in enumerate([monthly_pred.sum(axis=1).loc[9], monthly_true.sum(axis=1).loc[9], monthly_strm.sum(axis=1).loc[9]]):
#   ax1.text(x-1e5, y, ''.format(x), color='white', fontsize=14, va='center', ha='right')
ax1.set_yticks([0, 1, 2]);
ax1.set_yticklabels(['Predicted tuning', 'True tuning', 'Streaming']);
ax1.set_xticklabels([], rotation=20, ha='right', va='center');
plt.tight_layout()
f1.savefig('DataPlots/pres_impact.png', dpi=800)
plt.close(f1)

########################################################################
###################### Grid Search #####################################
########################################################################
# versions  = ['v0', 'dvc', 'dvcbin', 'drtn-dvc', 'drtn-dvcbin', 'v4', 'drtn-v4']
versions  = ['v0', 'dvc', 'dvcbin', 'drtn-v0', 'drtn-dvc', 'drtn-dvcbin']
n_months  = [1, 2, 3, 4, 5]
all_months_i = [2, 3, 4, 5, 6, 7, 8, 9]
all_months_n = ['Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep']

dvc_list = list(df.device.value_counts().index)
prv_list = list(df.provider.value_counts().index)

binsize_list   = [25, 50, 100, 150, 100000]
smoothing_list = [50, 100, 250, 500, 750, 1000, 2500, 5000]
flatten_list   = [1, 2, 3, 4, 5]

# -------------------------------------------------------------------- #
### Calculate model scores over full parameter grid (takes a long time)
### Add to existing
# grid_scores = pd.read_csv('ModelPlots/CensusModel/grid_scores.csv', index_col=[0, 1, 2, 3, 4, 5])
### Start from scratch
grid_scores = pd.DataFrame([], columns = ['version','n_months','month','binsize','smoothing','flatten','all']+prv_list+dvc_list)
grid_scores = grid_scores.set_index(['version','n_months','month','binsize','smoothing','flatten'])

for v in versions:
    # if any([vv in v for vv in ['v2','v3','v4','v5','v6','v7','v8','v9']]) | ('dvcbin' in v):
    binsizes = binsize_list
    # else:
    #     binsizes = [-1]
    if 'drtn' in v:
        smoothing = smoothing_list
        flatten      = flatten_list
    else:
        smoothing = [-1]
        flatten      = [-1]
    for n in n_months:
        month_i = all_months_i[(n - 1):]
        for m in month_i:
            for b in binsizes:
                for s in smoothing:
                    for x in flatten:
                        ind = (v, n, m, b, s, x)
                        if (ind not in grid_scores.index):
                            print('done:', ind)
                            modelled = model_swot(*get_dfs(df, m, n),
                                                  model_vers=v, threshold=b, smoothing=s, flatten_multiplier=x)
                            grid_scores.loc[ind, ['all'] + prv_list] = calc_score(modelled, groupcol='provider')['reldiff']
                            grid_scores.loc[ind,           dvc_list] = calc_score(modelled, groupcol='device')['reldiff']
                        else:
                            print('skip:', ind)

grid_scores = grid_scores.sort_index()
grid_scores.to_csv('ModelPlots/CensusModel/grid_scores.csv')

########################################################################
######################     Plots   #####################################
########################################################################
### RMS over months, 'all', score only
grid_scores_rms = grid_scores.groupby(['version','n_months','smoothing','flatten']).agg({'all': rms}).reset_index()

cmap = 'YlGnBu' #sb.diverging_palette(220, 10, as_cmap=True)
cmap_vmax, cmap_vmin = grid_scores_rms['all'].max(), grid_scores_rms['all'].min()
for v in versions:
    for n in n_months:
        f1, ax1 = plt.subplots(1)
        pane = grid_scores_rms.loc[(grid_scores_rms.version == v) & (grid_scores_rms.n_months == n)] \
                              .pivot_table(index='flatten', columns='smoothing', values='all')
        sb.heatmap(pane, cmap=cmap, vmin=cmap_vmin, vmax=cmap_vmax, square=True, linewidths=0.2, ax=ax1)
        ax1.set_xticklabels([int(x) for x in pane.columns])
        ax1.set_yticklabels([int(y) for y in pane.index[::-1]], rotation=0)
        ### mark square(s) with the best value for this model
        best_val = pane.min().min()
        best_coords = np.array(pane[pane == best_val].stack().index.labels)
        if pane.shape[1] == 1:
            for i in best_coords[0]:
                ax1.scatter(0.5, -i + pane.shape[0] - 0.5, marker='x', c='black')
        elif pane.shape[0] == 1:
            for i in best_coords[1]:
                ax1.scatter(i + 0.5, 0.5, marker='x', c='black')
        else:
            for i in range(best_coords.shape[1]):
                ax1.scatter( best_coords[1, i] + 0.5,
                            -best_coords[0, i] + pane.shape[0]-0.5, marker='x', c='black')
        ax1.set_title('{0}, {1} months: min = {2:0.5f}'.format(v, n, best_val))
        f1.savefig('ModelPlots/CensusModel/grid_scores_{0}_n{1}.png'.format(v, n))
        plt.close(f1)

# -------------------------------------------------------------------------------- #
### RMS over months, 'all', score only
grid_scores_best = grid_scores_rms.pivot_table(index='n_months',columns='version', values='all', aggfunc='min')
grid_scores_best = grid_scores_best[versions]

f1, ax1 = plt.subplots(1)
sb.heatmap(grid_scores_best, cmap=cmap, square=True, linewidths=0.2, ax=ax1)
ax1.set_xticklabels([x for x in grid_scores_best.columns], rotation=30)
ax1.set_yticklabels([y for y in grid_scores_best.index[::-1]], rotation=0)
best_val = grid_scores_best.min().min()
best_coords = np.array(grid_scores_best[grid_scores_best == best_val].stack().index.labels)
for i in range(best_coords.shape[1]):
    ax1.scatter( best_coords[1, i] + 0.5,
                -best_coords[0, i] + grid_scores_best.shape[0] - 0.5, marker='x', c='black')
ax1.set_title('Best score from each model vs. # months')
f1.savefig('ModelPlots/CensusModel/grid_scores_best.png')
plt.close(f1)
########################################################################
### See values for specific sections?
# ind = v, n, m, b, s, x
print(pd.DataFrame({
    'n2s50':  grid_scores.loc[('drtn-dvc', 2, slice(1, 10), 25, 50,  slice(1,5)), ['all']].reset_index().groupby('flatten').agg({'all':rms})['all'],
    'n3s50':  grid_scores.loc[('drtn-dvc', 3, slice(1, 10), 25, 50,  slice(1,5)), ['all']].reset_index().groupby('flatten').agg({'all':rms})['all'],
    'n2s500': grid_scores.loc[('drtn-dvc', 2, slice(1, 10), 25, 500, slice(1,5)), ['all']].reset_index().groupby('flatten').agg({'all':rms})['all'],
    'n3s500': grid_scores.loc[('drtn-dvc', 3, slice(1, 10), 25, 500, slice(1,5)), ['all']].reset_index().groupby('flatten').agg({'all':rms})['all']}))

########################################################################
################## Score plots by provider/device ######################
########################################################################
### Error vs. binsize
# n_mn = 3
# test_smoothing = 500
#
# agg_dict_prv = {}
# for p in ['all'] + prv_list:
#     agg_dict_prv[p] = rms
# agg_dict_dvc = {}
# for p in dvc_list:
#     agg_dict_dvc[p] = rms
#
# prv_v_bin = {}
# dvc_v_bin = {}
#
# all_mn_slice = slice(min(all_months_i), max(all_months_i))
# grid_scores = grid_scores.sort_index()
# for v in versions:
#     prv_v_bin[v] = pd.concat([grid_scores.loc[(v, n_mn, all_mn_slice, slice(-1, max(binsize_list)), test_smoothing), ['all'] + prv_list],
#                               grid_scores.loc[(v, n_mn, all_mn_slice, slice(-1, max(binsize_list)), -1            ), ['all'] + prv_list]])\
#                                  .reset_index().groupby(['version','binsize']).agg(agg_dict_prv)
#     prv_v_bin[v].index = prv_v_bin[v].index.droplevel(0)
#     dvc_v_bin[v] = pd.concat([grid_scores.loc[(v, n_mn, all_mn_slice, slice(-1, max(binsize_list)), test_smoothing), dvc_list],
#                               grid_scores.loc[(v, n_mn, all_mn_slice, slice(-1, max(binsize_list)), -1            ), dvc_list]])\
#                                  .reset_index().groupby(['version','binsize']).agg(agg_dict_dvc)
#     dvc_v_bin[v].index = dvc_v_bin[v].index.droplevel(0)
#
# colors = sb.color_palette("Set1", len(versions), desat=0.7)
# # ---------------------------------------------------------------------- #
# f1, ax1 = plt.subplots(1)
# p='all'
# for j, v in enumerate(versions):
#     if prv_v_bin[v].shape[0] > 1:
#         x, y = prv_v_bin[v].index, 100. * prv_v_bin[v][p]
#     else:
#         x, y = binsize_list, [100. * prv_v_bin[v][p].iloc[0] for b in binsize_list]
#     ax1.plot(x, y, c=colors[j],
#              label='{0}: {1: 0.2f} at {2}'.format(v, 100. * prv_v_bin[v][p].min(),
#                                                   prv_v_bin[v][p].idxmin()))
# ax1.set_xscale('log')
# ax1.set_xticks(binsize_list)
# ax1.set_xticklabels(binsize_list, rotation=30)
# ax1.set_xlim(.95*binsize_list[0], binsize_list[-2]*1.1)
# ax1.legend(loc='upper right', frameon=True, framealpha=0.7)
# ax1.set_xlabel('Bin threshold (# dvcs)')
# ax1.set_ylabel('RMS error')
# ax1.set_title('Error vs. window size, by model version (smoothing window = {0})'.format(test_smoothing))
# plt.savefig('ModelPlots/CensusModel/model_v_binsize_{0}_s{1}_m{2}.png'.format(p, test_smoothing, n_mn))
# plt.close(f1)
#
# # ---------------------------------------------------------------------- #
# f1, ax1 = plt.subplots(4, 5, sharex=True, sharey=True, figsize = [21,10])
# f1.subplots_adjust(hspace=0.1, wspace=0.05, right=.99, bottom=.07, top=.94, left=0.025)
# ax = ax1.flatten()
# for i, p in enumerate(['all'] + prv_list):
#     ax[i].axhline(0, c='black', alpha=0.25)
#     if p == 'all':
#         ax[i].set_title('{0} ({1})'.format(p, int(n_dvcs.sum().sum())))
#     else:
#         ax[i].set_title('{0} ({1})'.format(p, int(n_dvcs[p].sum())))
#     for j, v in enumerate(versions):
#         if prv_v_bin[v].shape[0] > 1:
#             x, y = prv_v_bin[v].index, 100. * prv_v_bin[v][p]
#         else:
#             x, y = binsize_list, [100. * prv_v_bin[v][p].iloc[0] for b in binsize_list]
#         ax[i].plot(x, y, c=colors[j],
#                    label='{0}: {1: 0.2f} at {2}'.format(v, 100. * prv_v_bin[v][p].min(),
#                                                         prv_v_bin[v][p].idxmin()))
#     ax[i].legend(loc='upper right', ncol=1, frameon=True, framealpha=0.7)
# oldi = i
# for j, d in enumerate(pd.Index(dvc_list)[[0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 12, 15]]):
# # for j, d in enumerate(pd.Index(dvc_list)[[0, 1, 2, 3, 4, 5, 6, 8, 10, 12, 14, 16, 18]]:
#     i = j+oldi+1
#     ax[i].axhline(0, c='black', alpha=0.25)
#     ax[i].set_title('{0} ({1})'.format(d, int(n_dvcs.loc[d].sum())))
#     for k, v in enumerate(versions):
#         if dvc_v_bin[v].shape[0] > 1:
#             x, y = dvc_v_bin[v].index, 100. * dvc_v_bin[v][d]
#         else:
#             x, y = binsize_list, [100. * dvc_v_bin[v][d].iloc[0] for b in binsize_list]
#         ax[i].plot(x, y, c=colors[k],
#                    label='{0}: {1: 0.2f} at {2}'.format(v, 100. * dvc_v_bin[v][d].min(),
#                                                         dvc_v_bin[v][d].idxmin()))
#     ax[i].legend(loc='upper right', ncol=1, frameon=True, framealpha=0.7)
# ax[0].set_xlim(0, 1.2 * binsize_list[-2])
# ax[0].set_ylim(0, 8)
# f1.text(0.01, 0.5, 'Error (%)', ha='center', va='center', rotation='vertical', fontsize=14)
# f1.text(0.5, 0.015, 'Bin size threshold (# dvcs)', ha='center', va='center', fontsize=14)
# f1.text(0.5, 0.98, 'RMS monthly error in total viewed minutes, using previous month to predict SWOT %',
#         ha='center', va='center', fontsize=16)
# plt.savefig('ModelPlots/CensusModel/model_v_binsize_s{0}_m{1}.png'.format(test_smoothing, n_mn))
# plt.close(f1)
#
########################################################################
### Error vs. amount of smoothing
n_mn = 3
test_binsize = 25
test_flatten = 2
all_mn_slice = slice(min(all_months_i), max(all_months_i))

# if test_binsize not in grid_scores.reset_index().binsize.unique():
#     grid_scores.loc[('fake',-1, -1, 150, -1)] = np.nan
#     grid_scores = grid_scores.sort_index()

agg_dict_prv = {}
for p in ['all'] + prv_list:
    agg_dict_prv[p] = rms
agg_dict_dvc = {}
for p in dvc_list:
    agg_dict_dvc[p] = rms

prv_v_smth = {}
dvc_v_smth = {}
for v in versions:
    prv_v_smth[v] = pd.concat([grid_scores.loc[(v, n_mn, all_mn_slice, test_binsize, slice(-1, max(smoothing_list)), test_flatten), ['all'] + prv_list],
                               grid_scores.loc[(v, n_mn, all_mn_slice, test_binsize, slice(-1, max(smoothing_list)),           -1), ['all'] + prv_list]])\
                      .reset_index().groupby(['version','smoothing']).agg(agg_dict_prv)
    prv_v_smth[v].index = prv_v_smth[v].index.droplevel(0)
    # prv_v_smth[v] = grid_scores.loc[(v, n_mn, all_mn_slice, test_binsize, slice(-1, max(smoothing_list)), test_flatten), ['all'] + prv_list]\
    #                   .reset_index().groupby(['version','smoothing']).agg(agg_dict_prv)
    dvc_v_smth[v] = pd.concat([grid_scores.loc[(v, n_mn, all_mn_slice, test_binsize, slice(-1, max(smoothing_list)), test_flatten), dvc_list],
                               grid_scores.loc[(v, n_mn, all_mn_slice, test_binsize, slice(-1, max(smoothing_list)),           -1), dvc_list]])\
                      .reset_index().groupby(['version','smoothing']).agg(agg_dict_dvc)
    dvc_v_smth[v].index = dvc_v_smth[v].index.droplevel(0)
    # dvc_v_smth[v] = grid_scores.loc[(v, n_mn, all_mn_slice, test_binsize, slice(-1, max(smoothing_list)), test_flatten), dvc_list]\
    #                   .reset_index().groupby(['version','smoothing']).agg(agg_dict_dvc)

colors = sb.color_palette("Set1", len(versions), desat=0.7)
# ---------------------------------------------------------------------- #
for p in ['all','census']:
    f1, ax1 = plt.subplots(1)
    for j, v in enumerate(versions):
        if prv_v_smth[v].shape[0] > 1:
            x, y = prv_v_smth[v].index, 100. * prv_v_smth[v][p]
        else:
            x, y = smoothing_list, [100. * prv_v_smth[v][p].iloc[0] for b in smoothing_list]
        ax1.plot(x, y, c=colors[j],
                 label='{0}: {1: 0.2f} at {2}'.format(v, 100. * prv_v_smth[v][p].min(),
                                                      prv_v_smth[v][p].idxmin()))
    ax1.set_xscale('log')
    ax1.set_xticks(smoothing_list)
    ax1.set_xticklabels(smoothing_list, rotation=30)
    ax1.legend(loc='upper right', frameon=True, framealpha=0.7)
    ax1.set_xlabel('Smoothing window (# events)')
    ax1.set_ylabel('RMS error')
    ax1.set_title('Error vs. window size ({0}), by model version (bin threshold = {1}, smoothing multiplier = {2})'.format(p, test_binsize, test_flatten))
    plt.savefig('ModelPlots/CensusModel/model_v_smoothing_{0}_b{1}_m{2}_x{3}.png'.format(p, test_binsize, n_mn, test_flatten))
    plt.close(f1)

# ---------------------------------------------------------------------- #
f1, ax1 = plt.subplots(4, 5, sharex=True, sharey=True, figsize = [21,10])
f1.subplots_adjust(hspace=0.1, wspace=0.05, right=.99, bottom=.07, top=.94, left=0.025)
ax = ax1.flatten()
for i, p in enumerate(['all'] + prv_list):
    ax[i].axhline(0, c='black', alpha=0.25)
    if p == 'all':
        ax[i].set_title('{0} ({1})'.format(p, int(n_dvcs.sum().sum())))
    else:
        ax[i].set_title('{0} ({1})'.format(p, int(n_dvcs[p].sum())))
    for j, v in enumerate(versions):
        if prv_v_smth[v].shape[0] > 1:
            x, y = prv_v_smth[v].index, 100. * prv_v_smth[v][p]
        else:
            x, y = smoothing_list, [100. * prv_v_smth[v][p].iloc[0] for b in smoothing_list]
        ax[i].plot(x, y, c=colors[j],
                   label='{0}: {1: 0.2f} at {2}'.format(v, 100. * prv_v_smth[v][p].min(),
                                                        prv_v_smth[v][p].idxmin()))
    ax[i].legend(loc='upper right', ncol=1, frameon=True, framealpha=0.7)
oldi = i
for j, d in enumerate(pd.Index(dvc_list)[[0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 12, 15]]):
    i = j+oldi+1
    ax[i].axhline(0, c='black', alpha=0.25)
    ax[i].set_title('{0} ({1})'.format(d, int(n_dvcs.loc[d].sum())))
    for k, v in enumerate(versions):
        if dvc_v_smth[v].shape[0] > 1:
            x, y = dvc_v_smth[v].index, 100. * dvc_v_smth[v][d]
        else:
            x, y = smoothing_list, [100. * dvc_v_smth[v][d].iloc[0] for b in smoothing_list]
        ax[i].plot(x, y, c=colors[k],
                   label='{0}: {1: 0.2f} at {2}'.format(v, 100. * dvc_v_smth[v][d].min(),
                                                        dvc_v_smth[v][d].idxmin()))
    ax[i].set_xlim(0.95 * smoothing_list[0], 1.2 * smoothing_list[-2])
    ax[i].set_xscale('log')
    ax[i].legend(loc='upper right', ncol=1, frameon=True, framealpha=0.7)
    ax[i].set_xticks(smoothing_list)
    ax[i].set_xticklabels(smoothing_list, rotation=30)
ax[0].set_ylim(0, 5)
f1.text(0.01, 0.5, 'Error (%)', ha='center', va='center', rotation='vertical', fontsize=14)
f1.text(0.5, 0.015, 'Smoothing window (# events)', ha='center', va='center', fontsize=14)
f1.text(0.5, 0.98, 'RMS monthly error in total viewed minutes, using previous month to predict SWOT %',
        ha='center', va='center', fontsize=16)
plt.savefig('ModelPlots/CensusModel/model_v_smoothing_b{0}_m{1}_x{2}.png'.format(test_binsize, n_mn, test_flatten))
plt.close(f1)

########################################################################
########################################################################
### Why are census rms scores *so* bad?

ind = grid_scores['census'].idxmax()
v, n, m, b, s, x = ind
modelled = model_swot(*get_dfs(df, m, n),
                      model_vers=v, threshold=b, smoothing=s, flatten_multiplier=x)

df['end_time'] = pd.to_datetime(df.end_time)
df['next_stream_start'] = pd.to_datetime(df.next_stream_start)
df['gap'] = (df.next_stream_start - df.end_time)/dt.timedelta(seconds=1)

worst_census = modelled.loc[modelled.provider=='census'].copy()
worst_census['err'] = worst_census.pred_drtn - worst_census.duration
worst_census['err_abs'] = worst_census.err.abs()
worst_census = worst_census.sort_values('err_abs')

########################################################################
########################################################################
### Google Chromecast for modeldf in predicting June-Aug has really high swot fraction,
### and it's higher when flattened than not. How?
### the very last session is really long and almost all swot
testdf = get_dfs(df, 6, 3)[0]
testdf = testdf.loc[testdf.device == 'Google Chromecast'].sort_values('swotstreamdur')

testdf['cm_strm'] = testdf.swotstreamdur.cumsum()
testdf['cm_tune'] = testdf.duration.cumsum()
testdf['cm_swtfrac'] = testdf.cm_tune/testdf.cm_strm
########################################################################
################## Show where error is #################################
########################################################################
### Parameters to use for all versions
n_mn        = 3
test_binsize    = 25
test_smoothing  = 500
test_multiplier = 2

### Hardcoded list of bin edges
bin_drtns = list(np.arange(0, 15, 1)) + list(np.arange(15, 50, 5)) + list(np.arange(50, 100, 10)) + [100, 150, 200, 300, 400, 500, 1000, df.swotstreamdur.max()+1]
### Using this as the colormap maximum, for consistency (rerun all if adjusting)
vmax=25000

months   = all_months_i[(n_mn-1):]
err_distrns_by_month = {}
for v in versions:
    for month in months:
        modeldf, predictdf = get_dfs(df, month, n_mn)
        preds = model_swot(modeldf, predictdf, model_vers = v, threshold=test_binsize, smoothing=test_smoothing, flatten_multiplier=test_multiplier)
        preds['err'] = preds.pred_drtn - preds.duration
        preds['drtn_bin'] = pd.cut(preds.swotstreamdur, bins=bin_drtns)
        ### Get error by provider
        prv_err_mins = preds.pivot_table(index='provider', columns='drtn_bin', values='err', aggfunc='sum')
        prv_err_mins = prv_err_mins.loc[prv_list]
        ### Get error by device
        dvc_err_mins = preds.pivot_table(index='device', columns='drtn_bin', values='err', aggfunc='sum')
        dvc_err_mins = dvc_err_mins.loc[dvc_list]
        ### Combine into one df
        err_mins = pd.concat([prv_err_mins, dvc_err_mins])
        err_mins = err_mins.iloc[::-1]
        err_mins.columns = [i.right.astype('int') for i in err_mins.columns]
        err_mins['total']     = err_mins.sum(axis=1)
        err_mins.loc['total'] = err_mins.sum(axis=0)
        err_mins.loc['total','total'] = err_mins.loc[prv_list,'total'].sum()
        err_distrns_by_month[(v, month)] = err_mins
        print('{0}: maximum error = {1}, color max = {2}'.format(v, err_distrns_by_month[(v, month)].abs().max().max(), vmax))

err_distrns = {}
for v in versions:
    monthlies = [err_distrns_by_month[(v, m)] for m in months]
    err_distrns[v] = monthlies[0].fillna(0)
    for monthly in monthlies[1:]:
        err_distrns[v] += monthly.fillna(0)
    for c in err_distrns[v].columns:
        err_distrns[v].loc[err_distrns[v][c] == 0, c] = np.nan
    print(
        '{0}: maximum error = {1}, color max = {2}'.format(v, err_distrns[v].abs().max().max(), vmax))

total_drtn = df.loc[df.month.between(min(months), max(months)), 'duration'].sum()

# ---------------------------------------------------------------------- #
for v in versions:
    f1, ax1 = plt.subplots(1, figsize=[20, 12])
    sb.heatmap(err_distrns[v], cmap='bwr', center=0, vmax=vmax, vmin=-vmax, square=True, linewidths=0.1, ax=ax1)
    ax1.axhline(1, c='black')
    ax1.axhline(len(prv_list)+1, c='black')
    ax1.axvline(err_distrns[v].shape[1]-1, c='black')
    ax1.set_xticklabels(err_distrns[v].columns, rotation=30)
    ax1.set_yticklabels([y for y in err_distrns[v].index[::-1]], rotation=0)
    ax1.set_xlabel('Duration (right edge of bin)', fontsize=14)
    ax1.set_ylabel('Category (provider or device)', fontsize=14)
    ax1.set_title('Absolute error distribution; total = ' +
                  '{0:0.1f} mins off ({1:0.2f}%)\n'.format(err_distrns[v].loc['total','total'],
                                                           err_distrns[v].loc['total', 'total']/total_drtn*100.) +
                  ' ({0}, b{1}, s{2}, m{3})'.format(v, test_binsize, test_smoothing, n_mn), fontsize=14)
    plt.tight_layout()
    f1.savefig('ModelPlots/CensusModel/error_distributions_{0}_b{1}_s{2}_m{3}.png'.format(v, test_binsize, test_smoothing, n_mn))
    plt.close(f1)
# ---------------------------------------------------------------------- #
### Plot each month
# vers='v4'
# for month in months:
#     f1, ax1 = plt.subplots(1, figsize=[20, 12])
#     sb.heatmap(err_distrns_by_month[(vers, month)], cmap='bwr', center=0, vmax=vmax/4., vmin=-vmax/4., square=True, linewidths=0.1, ax=ax1)
#     ax1.axhline(1, c='black')
#     ax1.axhline(len(prv_list)+1, c='black')
#     ax1.axvline(err_distrns_by_month[(vers, month)].shape[1]-1, c='black')
#     ax1.set_xticklabels(err_distrns_by_month[(vers, month)].columns, rotation=30)
#     ax1.set_yticklabels([y for y in err_distrns_by_month[(vers, month)].index[::-1]], rotation=0)
#     ax1.set_xlabel('Duration (right edge of bin)', fontsize=14)
#     ax1.set_ylabel('Category (provider or device)', fontsize=14)
#     ax1.set_title('Absolute error distribution; total = ' +
#                   '{0:0.1f} mins off ({1:0.2f}%)\n'.format(err_distrns_by_month[(vers, month)].loc['total', 'total'],
#                                                            err_distrns_by_month[(vers, month)].loc['total', 'total']/total_drtn*100./4.) +
#                   ' ({0}, b{1}, s{2}, m{3}-{4})'.format(vers, test_binsize, test_smoothing, n_mn, month), fontsize=14)
#     plt.tight_layout()
#     f1.savefig('ModelPlots/CensusModel/error_distributions_{0}_b{1}_s{2}_m{3}-{4}.png'.format(vers, test_binsize, test_smoothing, n_mn, month))
#     plt.close(f1)
#
########################################################################
################### Plot drtn curve shapes #############################
########################################################################
binmodel  = 'dvc'
n_mn      = 3
binsize   = 25
month_i   = all_months_i[(n_mn-1):]
month_lab = all_months_n[(n_mn-1):]

### Use same bins for all months
modeldf, predictdf = get_dfs(df, 7, n_mn)
bin_key = get_bin_key(modeldf, binmodel=binmodel, threshold=binsize)

### Plot for each flattening multiplier
for flatten, smoothing in [(1, 50), (4, 50), (1, 1000), (2, 500), (4, 250), (4, 500)]:
    print(flatten, smoothing)
    ### Generate duration curves for all months
    lookups = {}
    for k in month_i:
        modeldf = get_dfs(df, k, n_months=n_mn)[0]
        lookups[k] = fit_model(modeldf, bin_key, smoothing=smoothing, flatten_multiplier=flatten)
    f1, ax1 = plt.subplots(*layout(bin_key.bin_name.sort_values().nunique()), figsize=[18, 10], sharex=True, sharey=True)
    ax2 = ax1.flatten()
    for i, c in enumerate(bin_key.bin_name.sort_values().unique()):
        for j in month_i:
            modeldf = get_dfs(df, j, n_months=n_mn)[0]
            ax2[i].plot(lookups[j].index, lookups[j][c],
                        label='{0}: {1} events'.format(month_lab[j-n_mn-1],
                                                       modeldf.loc[
                                           (modeldf.provider.isin(bin_key.loc[bin_key.bin_name==c].provider)) &
                                           (modeldf.device.isin(  bin_key.loc[bin_key.bin_name==c].device  ))].shape[0]))
        ax2[i].legend(loc='upper right', frameon=True, framealpha=0.6)
        ax2[i].set_title(c)
        ax2[i].set_xscale('log')
    f1.text(0.5, 0.01, 'Streaming Duration (min)', ha='center', va='center')
    f1.text(0.01, 0.5, 'SWOT Fraction', ha='center', va='center', rotation=90)
    plt.tight_layout()
    f1.savefig('ModelPlots/CensusModel/drtn_curves_{0}_b{1}_s{2}x{3}_m{4}.png'.format(binmodel, binsize, smoothing, flatten, n_mn))
    plt.close(f1)

########################################################################
################### Pred vs true swot curves ###########################
########################################################################
v = 'drtn-dvc'
binsize=50
smoothing=250
n_mn = 3
months   = all_months_i[(n_mn-1):]

for m in months:
    modeldf, predictdf = get_dfs(df, m, n_mn)
    preds = model_swot(modeldf, predictdf, model_vers=v, threshold=binsize, smoothing=smoothing)
    preds['err'] = preds.pred_drtn - preds.duration

    preds['strm_drtn'] = preds.swotstreamdur.copy()
    strm_tot = preds.pivot_table(index='swotstreamdur', columns='device', values='strm_drtn', aggfunc='sum')
    tune_tot = preds.pivot_table(index='swotstreamdur', columns='device', values='duration',  aggfunc='sum')
    pred_tot = preds.pivot_table(index='swotstreamdur', columns='device', values='pred_drtn', aggfunc='sum')

    true_mn = 1. - tune_tot / strm_tot
    pred_mn = 1. - pred_tot / strm_tot

    f1, ax1 = plt.subplots(2, 3, figsize=[20, 12])
    ax2 = ax1.flatten()
    for i, d in enumerate(dvc_list[:6]):
        ax2[i].plot(true_mn.index, true_mn[d], 'bo',   alpha=0.5, label='_nolabel_')
        ax2[i].plot(true_mn.index, true_mn[d], 'blue', alpha=0.5, label='True')
        ax2[i].plot(pred_mn.index, pred_mn[d], 'ro',   alpha=0.5, label='_nolabel_')
        ax2[i].plot(pred_mn.index, pred_mn[d], 'red',  alpha=0.5, label='Predicted')
        ax2[i].set_title(d)
        ax2[i].set_xscale('log')
        ax2[i].set_xlim(1, df.swotstreamdur.max())
        ax2[i].set_ylim(0, 1)
    ax2[0].legend(loc='upper left')
    f1.savefig('ModelPlots/CensusModel/true_vs_pred_by_drtn_{0}_m{1}-{2}_b{3}_s{4}.png'.format(v, n_mn, m, binsize, smoothing))
    plt.close(f1)

########################################################################
### True vs. pred duration curves for census only
v = 'drtn-dvc'
binsize=50
smoothing=250
n_mn = 3
months     = all_months_i[(n_mn-1):]
month_labs = all_months_n[(n_mn-1):]

true_mn = {}
pred_mn = {}
for i, m in enumerate(months):
    modeldf, predictdf = get_dfs(df, m, n_mn)
    preds = model_swot(modeldf, predictdf, model_vers=v, threshold=binsize, smoothing=smoothing)
    preds['err'] = preds.pred_drtn - preds.duration

    preds['strm_drtn'] = preds.swotstreamdur.copy()
    strm_tot = preds.pivot_table(index='swotstreamdur', columns='provider', values='strm_drtn', aggfunc='sum')
    tune_tot = preds.pivot_table(index='swotstreamdur', columns='provider', values='duration',  aggfunc='sum')
    pred_tot = preds.pivot_table(index='swotstreamdur', columns='provider', values='pred_drtn', aggfunc='sum')

    true_mn[m] = 1. - tune_tot / strm_tot
    pred_mn[m] = 1. - pred_tot / strm_tot

for p in prv_list:
    f1, ax1 = plt.subplots(2, 3, figsize=[20, 12], sharex=True, sharey=True)
    ax2 = ax1.flatten()
    for i, m in enumerate(months):
        ax2[i].plot(true_mn[m].index, true_mn[m][p], 'bo',   alpha=0.5, label='_nolabel_')
        ax2[i].plot(true_mn[m].index, true_mn[m][p], 'blue', alpha=0.5, label='True')
        ax2[i].plot(pred_mn[m].index, pred_mn[m][p], 'ro',   alpha=0.5, label='_nolabel_')
        ax2[i].plot(pred_mn[m].index, pred_mn[m][p], 'red',  alpha=0.5, label='Predicted')
        ax2[i].set_title(month_labs[i])
        ax2[i].set_xscale('log')
        ax2[i].set_xlim(1, df.swotstreamdur.max())
        ax2[i].set_ylim(0, 1)
    ax2[0].legend(loc='upper left')
    f1.text(0.5, 0.97, p, fontsize=14, ha='center')
    f1.savefig('ModelPlots/CensusModel/true_vs_pred_{0}_m{1}_b{3}_s{4}_{5}.png'.format(v, n_mn, m, binsize,
                                                                                           smoothing, p))
    plt.close(f1)

########################################################################
### True vs. pred duration curves for census only - all months combined
v = 'drtn-dvc'
binsize = 50
smoothing = 250
n_mn = 3
months = all_months_i[(n_mn - 1):]
month_labs = all_months_n[(n_mn - 1):]

for i, m in enumerate(months):
    modeldf, predictdf = get_dfs(df, m, n_mn)
    preds_this_month = model_swot(modeldf, predictdf, model_vers=v, threshold=binsize, smoothing=smoothing)
    preds_this_month['err'] = preds_this_month.pred_drtn - preds_this_month.duration
    preds_this_month['strm_drtn'] = preds_this_month.swotstreamdur.copy()
    preds_this_month['month'] = m
    if i == 0:
        all_preds = preds_this_month.copy()
    else:
        all_preds = pd.concat([all_preds, preds_this_month])

strm_tot = all_preds.pivot_table(index='swotstreamdur', columns='provider', values='strm_drtn', aggfunc='sum')
tune_tot = all_preds.pivot_table(index='swotstreamdur', columns='provider', values='duration',  aggfunc='sum')
pred_tot = all_preds.pivot_table(index='swotstreamdur', columns='provider', values='pred_drtn', aggfunc='sum')

true_mn = 1. - tune_tot / strm_tot
pred_mn = 1. - pred_tot / strm_tot

f1, ax1 = plt.subplots(2, 3, figsize=[20, 12], sharex=True, sharey=True)
ax2 = ax1.flatten()
for i, p in enumerate(prv_list):
    ax2[i].plot(true_mn.index, true_mn[p], 'bo',   alpha=0.5, label='_nolabel_')
    ax2[i].plot(true_mn.index, true_mn[p], 'blue', alpha=0.5, label='True')
    ax2[i].plot(pred_mn.index, pred_mn[p], 'ro',   alpha=0.5, label='_nolabel_')
    ax2[i].plot(pred_mn.index, pred_mn[p], 'red',  alpha=0.5, label='Predicted')
    ax2[i].set_title(p)
    ax2[i].set_xscale('log')
    ax2[i].set_xlim(1, df.swotstreamdur.max())
    ax2[i].set_ylim(0, 1)
ax2[0].legend(loc='upper left')
f1.text(0.5, 0.97, 'All months combined', fontsize=14, ha='center')
f1.savefig('ModelPlots/CensusModel/true_vs_pred_{0}_m{1}_b{2}_s{3}.png'.format(v, n_mn, binsize, smoothing))
plt.close(f1)

########################################################################
########################################################################