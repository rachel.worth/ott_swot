### Manual model
import numpy as np
import pandas as pd
import datetime as dt
pd.set_option('display.width', 160)
import matplotlib
import matplotlib.pyplot as plt
import seaborn as sb
import random as rd

date = '17-08-10'

######################################################################################
print('Read census ping data')
census_pings = pd.read_csv('DCR_data/dcr_events_{0}.csv'.format(date)).sort_values(['ipaddress','visitor','time'])

print('Prep data')
### Time columns
# census_pings['dt']      = pd.to_datetime(census_pings.datadate)
# census_pings['time_dt'] = pd.to_datetime(census_pings.time*1e9)
# census_pings['sttm']   = census_pings.dt
# census_pings['edtm']   = census_pings.dt + pd.to_timedelta(census_pings.content_length, unit='s')
# census_pings['gap']    = (census_pings.dt.astype('int64')/1e9 - census_pings.dt.shift(1).astype('int64')/1e9)/60.
# census_pings['consec'] = census_pings.gap.abs() <= 60

census_pings['drtn_min'] = census_pings.content_length/60.
######################################################################################
print('Read census session(?) data')
census_ssns = pd.read_csv('DCR_data/etl_events_{0}.csv'.format(date)).sort_values(['ipaddress','cookie','start_time_stamp'])

print('Prep data')
### Time columns
# census_ssns['dt']      = pd.to_datetime(census_ssns.datadate)
# census_ssns['time_dt'] = pd.to_datetime(census_ssns.time*1e9)
# census_ssns['sttm']   = census_ssns.dt
# census_ssns['edtm']   = census_ssns.dt + pd.to_timedelta(census_ssns.content_length, unit='s')
# census_ssns['gap']    = (census_ssns.dt.astype('int64')/1e9 - census_ssns.dt.shift(1).astype('int64')/1e9)/60.
# census_ssns['consec'] = census_ssns.gap.abs() <= 60

census_ssns['drtn_min'] = census_ssns.duration/60.

######################################################################################
### OTT panel
vers = 'DCR_data/NewPanelData'
dur  = '8mo'
print('Reading {0}/swot_master_table_{1}.csv'.format(vers, dur))
panel = pd.read_csv('{0}/swot_master_table_{1}.csv'.format(vers, dur), index_col = 0)

# panel[['make', 'model']] = panel.device.str.split(' ', 1, expand=True)
# panel['vw_frac']    = 1 - panel.swotdur / panel.swotstreamdur
# panel['swot_frac' ] = panel.swotdur / panel.swotstreamdur
# panel['swot_class'] = panel.swot_frac >= 0.05
# panel['hh']         = panel.fsite.str.split('-', 1, expand=True).iloc[:,0]
# panel['start_hour'] = pd.DatetimeIndex(panel.start_time).hour
### Convert durations to minutes, rounded, so get rid of precision difference between data sources

# panel[['duration','swotdur','swotstreamdur']] = (panel[['duration','swotdur','swotstreamdur']]/60).round(0)

panel['drtn_min'] = panel.swotstreamdur/60

######################################################################################
# RPD for comparison
rpd = pd.read_csv('../RPDOnOff/New/onoff_events.csv', index_col = 0)

rpd['drtn_min'] = rpd.drtn

######################################################################################
######################################################################################
######################################################################################
names = ['RPD','Panel OTT', 'Census sessions', 'Census pings']
data  = [  rpd,      panel,       census_ssns,   census_pings]

max_t = max([df.drtn_min.max() for df in data])

### Log
bins = np.arange(0, np.ceil((max_t+1)/1000)*1000, 50)
f1, ax1 = plt.subplots(4, 1, sharex=True, sharey=True, figsize=[20, 10])
for i in range(len(data)):
    sb.distplot(data[i].drtn_min, bins=bins, kde=False, norm_hist=True, ax=ax1[i])
    ax1[i].set_title(names[i])
    ax1[i].set_xlabel('')
    ax1[i].set_yscale('log')
ax1[i].set_xlim(0, bins[-1])
ax1[i].set_ylim(10**(-7.5), 10**(-1.5))
ax1[i].set_xlabel('Duration (minutes)')
plt.tight_layout()
f1.savefig('DCR_data/ssn_drtn_comparison_log.png')
plt.close(f1)

### Linear - short
max_t2 = 100
bins = np.arange(0, max_t2+1, 1)
f1, ax1 = plt.subplots(4, 1, sharex=True, sharey=True, figsize=[20, 10])
for i in range(len(data)):
    sb.distplot(data[i].drtn_min, bins=bins, kde=False, norm_hist=True, ax=ax1[i])
    ax1[i].set_title(names[i])
    ax1[i].set_xlabel('')
ax1[i].set_xlim(0, max_t2)
# ax1[i].set_ylim(0, 0.4)
ax1[i].set_xlabel('Duration (minutes)')
plt.tight_layout()
f1.savefig('DCR_data/ssn_drtn_comparison_lin.png')
plt.close(f1)

