import pandas     as pd
import numpy as np
import datetime as dt
from difflib import SequenceMatcher
pd.set_option('display.width', 170)

########################################################################################################################
### Matrix of similarities
def panel_common_device_mapping(strm_dvcs, tune_dvcs, rare_dvc_thresh=10, verbose=False, group_roku=True):
    '''This function takes two lists of device counts (tuning and streaming data from the OTTM panel)
    and creates a mapping from each of them to one common set of device names. Uncommon devices
    (those with fewer than rare_dvc_thresh unique devices in the data) will be lumped together
    with uncategorizable ones in the 'other' category.

    Each device count input should be of the form output by the pandas value_counts() method called
    on the device name column.

    The format for the streaming device names is
    appliance_type-make-model
    and all device names should be converted to lower case and have spaces stripped.'''
    if verbose:
        print('panel_common_device_mapping')
    ####################################################################################################################
    ### Get matrix of similarity between each streaming and tuning model name
    st_tn_sims = pd.DataFrame([], index=strm_dvcs.index, columns=[])
    st_tn_sims.index.name   = 'st'
    st_tn_sims.columns.name = 'tn'
    for c in tune_dvcs.index:
        st_tn_sims[c] = [SequenceMatcher(None, c, x.split('-')[-1]).ratio() for x in st_tn_sims.index]
    ####################################################################################################################
    ### A DF with the best-matching tune dvc for each stream dvc
    st_tn_match = pd.DataFrame({'tn':  st_tn_sims.idxmax(axis=1),
                                'sim': st_tn_sims.max(   axis=1)})\
                    .sort_values('sim', ascending=False).reset_index()
    ### Impose hard rules: tv, dvd, computer appliance types == smarttv, dvd, computer (and only those)
    for st, tn in [('tv-',       'smarttv'),
                   ('dvd-',      'dvd'),
                   ('computer-', 'computer')]:
        st_tn_match.loc[st_tn_match.st.str.startswith(st), 'tn'] = tn
        st_tn_match.loc[~st_tn_match.st.str.startswith(st) & (st_tn_match.tn == tn), 'tn'] = 'other'
    st_tn_match.loc[~st_tn_match.tn.isin(['smarttv','dvd','computer'])
                   & (st_tn_match.sim < 0.5), 'tn'] = 'other'
    ### See things with multiple matches
    if verbose:
        print('- panel mapping: find devices with multiple matches')
    for dvc in tune_dvcs.index:
        subset = st_tn_match.loc[st_tn_match.tn == dvc]
        if (dvc not in ['dvd', 'computer', 'smarttv', 'other']) & (subset.shape[0] > 1):
            if verbose:
                print(subset)
            if (subset.sim==1).any() & ~(subset.sim==1).all():
                if verbose:
                    print('--- only one perfect match ==> change imperfect to other')
                st_tn_match.loc[subset.loc[subset.sim<1].index, 'tn'] = 'other'
            else:
                if verbose:
                    print('--- leave as is')
    ####################################################################################################################
    ### Address rare devices: if fewer than rare_dvc_thresh of a device type, group it together with others
    ### Either with others from the same make if there's a clear single option,
    ### or "Other" if not
    if verbose:
        print('- uncommon devices')
    st_tn_match['assign'] = st_tn_match.tn.copy()
    for dvc in tune_dvcs.loc[tune_dvcs<rare_dvc_thresh].index:
        if dvc != 'other':
            ### Find tuning device in the streaming devices
            st_dvc = st_tn_match.loc[st_tn_match.tn == dvc, 'st']
            if st_dvc.shape[0] > 0:
                st_dvc_make = st_tn_match.loc[st_tn_match.tn == dvc, 'st'].str.split('-', expand=True).iloc[:, 1]
                ### Find other devices of same make; if only one other, combine them
                ### If more than that... which to put it with? Or just make it 'other'?
                same_make = st_tn_match.loc[st_tn_match.st.str.split('-', expand=True).iloc[:, 1].isin(st_dvc_make)]
                if verbose:
                    print('--- {0}: # same make = {1}, # same make/diff devices = {2}'.format(dvc, same_make.shape[0],
                                                                         (same_make.loc[same_make.tn != dvc].shape[0])))
                if (same_make.shape[0] == 2) & (same_make.loc[same_make.tn != dvc].shape[0] > 0):
                    if verbose:
                        print(same_make)
                    st_tn_match.loc[st_tn_match.tn == dvc, 'assign'] = same_make.loc[same_make.tn!=dvc, 'assign'].iloc[0]
                else:
                    if verbose:
                        print('---   ==> other')
                    st_tn_match.loc[st_tn_match.tn == dvc, 'assign'] = 'other'
            # else:
                # If tune dvc has no stream match, it wasn't included originally and must be added (in new loop below)
                # these should be non-streaming devices (e.g. game systems)
                # pass

    ####################################################################################################################
    ### Add non-streaming devices
    if verbose:
        print('- add devices with no streaming to mapping file')
    no_strm_val = 'none'
    assert no_strm_val not in strm_dvcs.index.unique(), 'ERROR: "none" already in streaming devices. Cannot use for tune-only rows'
    for dvc in tune_dvcs.index:
        if dvc not in st_tn_match.tn.unique():
            ind = st_tn_match.index.max() + 1
            if tune_dvcs.loc[dvc] < rare_dvc_thresh:
                if verbose:
                    print('--- adding row: {0} -> other'.format(dvc))
                st_tn_match.loc[ind, 'st']     = np.nan
                st_tn_match.loc[ind, 'sim']    = 0
                st_tn_match.loc[ind, 'tn']     = dvc
                st_tn_match.loc[ind, 'assign'] = 'other'
            else:
                if verbose:
                    print('--- adding row: {0} -> {1}'.format(dvc, dvc))
                st_tn_match.loc[ind, 'st']     = np.nan
                st_tn_match.loc[ind, 'sim']    = 0
                st_tn_match.loc[ind, 'tn']     = dvc
                st_tn_match.loc[ind, 'assign'] = dvc

    ####################################################################################################################
    ### Map 'unknown' devices to 'unknown' category
    if verbose:
        print('- "unknown" devices ==> "other"')
    unkn_ind = st_tn_match['assign'].str.contains('unknown')
    zero_ind = (st_tn_match['assign'] == '0') | (st_tn_match['assign'] == '')
    for ind in [unkn_ind, zero_ind]:
        st_tn_match.loc[ind, 'assign'] = 'unknown'
        if verbose:
            print(st_tn_match.loc[ind])

    ####################################################################################################################
    ### Roku: group all together (as long as census continues to not include roku model info)
    if group_roku:
        if verbose:
            print('- group all rokus together')
        roku_ind = ( ( (st_tn_match.st.notnull() & st_tn_match['st'].str.contains('roku'))
                     | (st_tn_match.tn.notnull() & st_tn_match['tn'].str.contains('roku')))
                   &~( (st_tn_match.st.notnull()) & st_tn_match.st.str.startswith('tv-') ) )
        st_tn_match.loc[roku_ind & (st_tn_match.tn=='other'), 'tn'] = 'roku'
        st_tn_match.loc[roku_ind, 'assign'] = 'roku'
        if verbose:
            print(st_tn_match.loc[roku_ind])

    ####################################################################################################################
    return st_tn_match

########################################################################################################################
########################################################################################################################
########################################################################################################################
def old_panel_common_device_mapping(strm_dvc_list, tune_dvc_list, pnl_dvc_names=None):
    tune_key = tune_dvc_list.reset_index().copy()
    strm_key = strm_dvc_list.reset_index().copy()
    tune_key.columns = ['tn','n_dvcs']
    strm_key.columns = ['st','n_dvcs']
    ### List of static categories
    if not pnl_dvc_names:
        pnl_dvc_names = ['Smart TV', 'Play Station 4', 'X Box One', 'Amazon Fire Stick', 'Bluray', 'Play Station 3',
                         'X Box 360', 'Roku', 'Apple TV',
                         'Amazon Fire TV', 'Google Chromecast', 'Computer', 'Wii U', 'Other', 'Wii', 'Google TV']
    pnl_dvc_names = [s.lower().replace(' ','') for s in pnl_dvc_names]
    ### strm names
    strm_key['assign'] = 'other'
    strm_key.loc[strm_key.st.str.startswith('computer'), 'assign'] = 'computer'
    for d in pnl_dvc_names:
        strm_key.loc[strm_key.st.str.contains(d), 'assign'] = d
        # mw.loc[mw.model_cln.str.contains(d.lower().replace(' ', '')), 'device'] = d
    strm_key.loc[strm_key.st.str.startswith('tv-')  & (strm_key['assign'] == 'other'), 'assign'] = 'smarttv'
    strm_key.loc[strm_key.st.str.startswith('dvd-') & (strm_key['assign'] == 'other'), 'assign'] = 'bluray'
    strm_key.loc[strm_key.st.str.contains('wiiu-')  & (strm_key['assign'] == 'wii'),   'assign'] = 'wiiu'
    ### tune names
    tune_key['assign'] = 'other'
    for d in pnl_dvc_names:
        tune_key.loc[tune_key.tn.str.contains(d), 'assign'] = d
    # mw.loc[(mw.appliance_type=='TV')  & (mw.device=='Other'), 'device'] = 'Smart TV'
    tune_key.loc[tune_key.tn == 'dvd',  'assign'] = 'bluray'
    tune_key.loc[tune_key.tn == 'wiiu', 'assign'] = 'wiiu'
    ### Combine
    st_tn_match = pd.concat([strm_key[['st','assign']], tune_key[['tn','assign']]])
    return st_tn_match

########################################################################################################################
########################################################################################################################
########################################################################################################################
def panel_census_common_device_mapping(st_tn_match, dcr_dvcs, verbose=False):
    '''This function takes the output from the above panel common mapping function and adds a mapping
    for DCR data using a device count list from that census data.

    As with the above, each device count input should be of the form output by the pandas value_counts() method called
    on the device name column.

    The format for the streaming device names is "hardware_type-device_type-device_name"
    with all device names should be converted to lower case and have spaces stripped.'''
    if verbose:
        print('panel_census_common_device_mapping')
    ### Get dataframe of census similarity with streaming model
    st_cn_sims = pd.DataFrame([], index=st_tn_match.st.dropna().unique(), columns=[])
    for c in dcr_dvcs.index:
        st_cn_sims[c] = [SequenceMatcher(None, c, i.split('-')[-1]).ratio() for i in st_cn_sims.index]
    ### Match non-TV census to strm devices (get name and ratio)
    st_tn_cn_match = st_tn_match[['assign','st','tn']]
    st_tn_cn_match = st_tn_cn_match.merge(st_cn_sims.loc[~st_cn_sims.index.str.startswith('tv-'),
                                                         ~st_cn_sims.columns.str.startswith('tv-')]\
                                                    .idxmax(axis=0).reset_index().rename(columns={'index':'cn',0:'st'}),
                                          on='st', how='left')
    st_tn_cn_match = st_tn_cn_match.merge(st_cn_sims.loc[~st_cn_sims.index.str.startswith('tv-'),
                                                         ~st_cn_sims.columns.str.startswith('tv-')]\
                                                    .max(axis=0).reset_index().rename(columns={'index':'cn',0:'cn_score'}),
                                          on='cn', how='left')

    ### Hard rule: census hardware type of TV ==> smart tv
    ### If well matched to a panel smart tv, use that, otherwise make a new row
    if verbose:
        print('- tv maps to existing tv or general smarttv category')
    tv_sim = st_cn_sims.loc[st_cn_sims.index.str.startswith('tv-'),
                            st_cn_sims.columns.str.startswith('tv-')]
    for c in tv_sim.columns:
        if tv_sim[c].max() >= 0.4:
            i = st_tn_cn_match.loc[st_tn_cn_match.st == tv_sim[c].idxmax()].index[0]
            if st_tn_cn_match.cn.isnull().loc[i]:
                st_tn_cn_match.loc[i, ['cn','cn_score']] = [c, tv_sim[c].max()]
            else:
                new_i = st_tn_cn_match.index.max() + 1
                st_tn_cn_match.loc[new_i] = st_tn_cn_match.loc[i]
                st_tn_cn_match.loc[i, ['cn', 'cn_score']] = [c, tv_sim[c].max()]
            if verbose:
                print('--- {} max {:.2f} ==> {}'.format(c, tv_sim[c].max(), tv_sim[c].idxmax()))
                print(st_tn_cn_match.loc[st_tn_cn_match.st == tv_sim[c].idxmax()] )
        else:
            new_i = st_tn_cn_match.index.max()+1
            st_tn_cn_match.loc[new_i, 'assign'] = 'smarttv'
            st_tn_cn_match.loc[new_i, 'cn']     = c
            if verbose:
                print('--- {} max {:.2f} ==> add new row'.format(c, tv_sim[c].max()))
        st_tn_cn_match = st_tn_cn_match.drop_duplicates()
    return st_tn_cn_match

########################################################################################################################
########################################################################################################################
########################################################################################################################
### Extract mappings for each data source
### strm
def get_device_key(st_tn_cn_match, input_map_col, output_map_col='assign',
                   input_data_col='dvc_cln', output_data_col='device',
                   skip_val=None, n_input_dvcs=None):
    dvc_key = st_tn_cn_match[[input_map_col, output_map_col]].dropna().drop_duplicates()\
                                 .rename(columns={input_map_col:  input_data_col,
                                                  output_map_col: output_data_col})
    if n_input_dvcs:
        assert dvc_key.loc[dvc_key.dvc_cln!=skip_val].shape[0] == n_input_dvcs, \
           'ERROR: {} {} devices at start, {} at end'.format(n_input_dvcs, input_map_col,
                                                             dvc_key.loc[dvc_key.dvc_cln!=skip_val].shape[0])
    return dvc_key

########################################################################################################################
########################################################################################################################
########################################################################################################################
if __name__ == '__main__':
    panel_vers = '1709-1709_new/'
    dcr_vers = '_2018-04-20'
    # tune = pd.read_csv('{}checkpoint_tune1.csv'.format(panel_vers), index_col=0)
    # tune.columns = [s.lower() for s in tune.columns]
    # tune['tn_dvc'] = tune.tn_dvc.str.lower().str.replace(' ','')
    # mw   = pd.read_csv('{}checkpoint_mw1.csv'.format(panel_vers), index_col=0)
    # mw['fsite'] = ( mw.hh_id.astype('str') + '-'
    #               + mw.site.astype('str') )
    # mw['st_dvc'] = ( mw.appliance_type.fillna('') + '-'
    #                 + mw.make.fillna('')           + '-'
    #                 + mw.model.fillna('')).str.lower().str.replace(' ','')
    # strm = pd.read_csv('{}checkpoint_strm1.csv'.format(panel_vers), index_col=0)
    # strm = strm.merge(mw[['fsite','mac_address','st_dvc']],
    #                   how='left', on=['mac_address'])[['fsite','mac_address','st_dvc',
    #                                                   'tree_id','starttimeest','endtimeest']]
    #
    # ### Find co-occurring device names
    # occurrence = tune[['fsite','tn_dvc']].drop_duplicates().merge(
    #              strm[['fsite','st_dvc']].drop_duplicates(), on='fsite',how='outer')\
    #                  .groupby(['tn_dvc','st_dvc']).count().reset_index()\
    #                  .pivot_table(index='st_dvc', columns='tn_dvc', values='fsite').fillna(0)
    #
    # occurrence = occurrence[[c for c in occurrence.columns if c not in ['0','unknown','game-unknown','icd-unknown']]]
    #
    # ### Normalized by total # matches per tune/strm dvc
    # cohab_weight1 = occurrence.divide(occurrence.sum(axis=0), axis=1)
    # cohab_weight2 = occurrence.divide(occurrence.sum(axis=1), axis=0)

    ### Read panel data
    tune_dvcs = pd.read_csv('{}tune_dvcs.csv'.format(panel_vers), index_col=0, header=None)[1]
    strm_dvcs = pd.read_csv('{}strm_dvcs.csv'.format(panel_vers), index_col=0, header=None)[1]

    # ### Mappings?
    # string_map = {}
    # for k in string_map.keys():
    #     tune_dvcs.index = tune_dvcs.index.str.replace(k, string_map[k])
    #     strm_dvcs.index = strm_dvcs.index.str.replace(k, string_map[k])

    ### Read census data and pull device count
    dcr_dvcs = pd.read_csv('DCR_data/dcr_dvcs{}.csv'.format(dcr_vers), index_col=0)

    ### Is there still only the one roku group?
    assert (dcr_dvcs.index.str.contains('roku')).sum() == 1, 'ERROR: new roku group present:\n{}'.format(
                                                                       dcr_dvcs.loc[dcr_dvcs.index.str.contains('roku')])
    group_roku = True
    tn_skip = 'roku'

    ### Get mappings
    st_tn_cn_match     = panel_census_common_device_mapping(    panel_common_device_mapping(strm_dvcs, tune_dvcs,
                                                                                            group_roku=group_roku),
                                                            dcr_dvcs)
    st_tn_cn_match_old = panel_census_common_device_mapping(old_panel_common_device_mapping(strm_dvcs, tune_dvcs),
                                                            dcr_dvcs)

    ### Get device keys for each dataset
    ### By including n_input_dvcs, it will check that the number of device types in == out (ignoring skip_val)
    tune_dvc_key = get_device_key(st_tn_cn_match, 'tn', n_input_dvcs=tune_dvcs.shape[0], skip_val=tn_skip)
    strm_dvc_key = get_device_key(st_tn_cn_match, 'st', n_input_dvcs=strm_dvcs.shape[0])
    dcr_dvc_key  = get_device_key(st_tn_cn_match, 'cn', n_input_dvcs=dcr_dvcs.shape[0])

    ### Compare old/new versions
    for col, dvcs, skip in [('tn', tune_dvcs, tn_skip),
                            ('st', strm_dvcs, None),
                            ('cn', dcr_dvcs,  None)]:
        dvc_key1 = get_device_key(st_tn_cn_match_old, col, n_input_dvcs=dvcs.shape[0], skip_val=skip)\
                            .rename(columns={'device':'dvc_old'})
        dvc_key2 = get_device_key(st_tn_cn_match, col, n_input_dvcs=dvcs.shape[0], skip_val=skip)\
                            .rename(columns={'device':'dvc_new'})
        key_both = dvc_key1.merge(dvc_key2, on='dvc_cln', how='outer').sort_values('dvc_cln')
        print('------- {} -------'.format(col))
        print(key_both.loc[key_both.dvc_old!=key_both.dvc_new])

    ### View the matches
    line = '-'*25
    center_str = '{:^25}'
    print(line + center_str.format('census mapping') + line)
    print(dcr_dvc_key.sort_values(['device','dvc_cln']).merge(dcr_dvcs.reset_index(),
                                                              how='left', on='dvc_cln'))
    print(line + center_str.format('tune mapping') + line)
    print(tune_dvc_key.sort_values(['device','dvc_cln'])\
                      .merge(tune_dvcs.reset_index()\
                                      .rename(columns={0:'dvc_cln',1:'n_dvcs'}),
                             how='left', on='dvc_cln'))
    for d in strm_dvc_key.device.unique():
        print(line + center_str.format('strm '+d) + line)
        print(strm_dvc_key.loc[strm_dvc_key.device==d].sort_values(['device','dvc_cln'])\
                          .merge(strm_dvcs.reset_index()\
                                          .rename(columns={'st':'dvc_cln',1:'n_dvcs'}),
                                 how='left', on='dvc_cln'))

### diagnose an error in the matches:
# input_map_col='tn'
# output_map_col='assign'
# input_data_col='dvc_cln'
# output_data_col='device'
# skip_val='roku'
# n_input_dvcs=tune_dvcs.shape[0]
#
# dvc_key = st_tn_cn_match[[input_map_col, output_map_col]].dropna().drop_duplicates() \
#     .rename(columns={input_map_col: input_data_col,
#                      output_map_col: output_data_col})
#
# dvc_key.loc[dvc_key.dvc_cln != skip_val].shape[0], n_input_dvcs
#
# print(' '.join(dvc_key.dvc_cln.sort_values()))
# print(' '.join(tune_dvcs.sort_index().index))

########################################################################################################################
########################################################################################################################
########################################################################################################################
# ### Map back to the streaming events
# if 'device' in tune.columns:
#     tune = tune.drop('device', axis=1).merge(tune_dvc_key, on='dvc_cln', how='left')
# else:
#     tune = tune.merge(tune_dvc_key, on='dvc_cln', how='left')
# if 'device' in strm.columns:
#     strm = strm.drop('device', axis=1).merge(strm_dvc_key, on='dvc_cln', how='left')
# else:
#     strm = strm.merge(strm_dvc_key, on='dvc_cln', how='left')
#
# assert tune.device.isnull().sum() == 0, 'ERROR: unassigned devices in tune'
# assert strm.device.isnull().sum() == 0, 'ERROR: unassigned devices in strm'


