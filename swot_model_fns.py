import numpy as np
import pandas as pd
import datetime as dt
import sys

########################################################################
def get_month_map(df):
    '''Determine which dates are present in the data, and construct "months"
    which each contain 30 complete days'''
    ### Count events per hour
    hour_list = df.groupby('date-hour')[['hh_id']].count().rename(columns={'hh_id': 'n_ssns'})
    hour_list['date-hour'] = pd.to_datetime(hour_list.index)
    hour_list = hour_list.reindex(pd.date_range(hour_list['date-hour'].dt.floor('h').min(),
                                                hour_list['date-hour'].dt.floor('h').max(), freq='h')).fillna(0)
    hour_list['date-hour'] = pd.to_datetime(hour_list.index)
    hour_list['date'] = pd.to_datetime(hour_list['date-hour'] - dt.timedelta(hours=3)).dt.strftime('%y-%m-%d')
    hour_list['is_zero'] = hour_list.n_ssns == 0
    ### Then count number of hours per day with no entries
    ### Consider any day with data in 12 or fewer of the hours (of 24) to be invalid and skipped
    date_list = hour_list.groupby('date').agg({'is_zero': 'mean'})
    date_list['skip'] = date_list.is_zero > 0.5
    ### to simulate refreshing the data once a month, divide remaining days into 30-day segments ignoring skipped days
    good_dates = date_list.loc[~date_list.skip].copy()
    good_dates['from_end'] = good_dates.shape[0] - np.arange(good_dates.shape[0]) - 1
    n_months = np.ceil(good_dates.shape[0] / 30)
    good_dates['month'] = (n_months - np.ceil((good_dates['from_end'] + 1) / 30)).astype('int') + 1
    ### Map dates to months
    month_map = good_dates.reset_index()[['date', 'month']].drop_duplicates()
    return month_map

##############################################################################################
### calculate dimensions for a grid of n subplots
def layout(n):
    n1 = int(np.ceil(n**0.5))
    if n1*(n1-1) >= n:
        n2 = n1-1
    else:
        n2 = n1
    return n1, n2

########################################################################
def get_dfs(df, test_month, n_train_months=3):
    """Pull a training and test dataset from df where the test set contains test_month
    and the training set contains the n_train_months preceding months"""
    predictdf = df.loc[df.month == test_month]
    modeldf   = df.loc[df.month.between(test_month - n_train_months, test_month - 1)]
    return modeldf, predictdf
########################################################################
def create_bins_v0(df, threshold=50):
    '''Average everything together -- no divisions'''
    n_dvcs = df.pivot_table(index='device', columns='provider', values='fsite', aggfunc=pd.Series.nunique).fillna(0)
    ### Create df to assign bins to each provider/model combination
    bin_names = pd.DataFrame([], index=n_dvcs.index, columns=n_dvcs.columns).fillna(0)
    return bin_names
########################################################################
def create_bins_v4(df, threshold=50, thresh2=1):
    '''all devices are kept separate for Netflix'''
    n_dvcs = df.pivot_table(index='device', columns='provider',
                            values='fsite', aggfunc=pd.Series.nunique).fillna(0)
    ### Create df to assign bins to each provider/model combination
    bin_names = pd.DataFrame([], index=n_dvcs.index, columns=n_dvcs.columns)
    ### first pass: label all netflix bins as separate
    p = 'netflix'
    for m in bin_names.index:
        if n_dvcs.loc[m, p] >= min(threshold, thresh2):
            bin_names.loc[m, p] = m + '_' + p
    ### second pass: identify individual cells that meet threshold
    for m in bin_names.index:
        for p in bin_names.columns:
            if n_dvcs.loc[m, p] >= threshold:
                bin_names.loc[m, p] = m+'_'+p
    ### third pass: group remaining cells by provider, ignore threshold
    for p in bin_names.columns:
        bin_names.loc[bin_names[p].isnull(), p] = 'misc_'+p
    return bin_names

########################################################################
def create_bins_dvcbin(df, threshold=50):
    """For each device with sufficient unique members, create a separate bin.
    Group the rest together with "Other"."""
    n_dvcs = df.groupby('device').agg({'fsite':pd.Series.nunique})
    ### Create df to assign bins to each provider/model combination
    bin_names = pd.DataFrame([], index=n_dvcs.index,
                             columns=df.pivot_table(index='device', columns='provider', values='fsite', aggfunc=pd.Series.nunique).columns)
    for d in n_dvcs.index:
        if n_dvcs.loc[d, 'fsite'] >= threshold:
            bin_names.loc[d] = d
        else:
            bin_names.loc[d] = 'Other'
    return bin_names
###################### Single function containing binmodels ############
def create_bins(modeldf, threshold=50, thresh2=1, binmodel='v7'):
    """Call whichever create_bins version is specified by "binmodel"."""
    if binmodel == 'v0':
        return create_bins_v0(modeldf)
    elif binmodel == 'v4':
        return create_bins_v4(modeldf)
    elif binmodel == 'dvcbin':
        return create_bins_dvcbin(modeldf, threshold=threshold)
    else:
        assert 1==0, 'ERROR: invalid bin model version {0}'.format(binmodel)
########################################################################
def get_bin_key(modeldf, binmodel='v4', threshold=50, manual_bins={}):
    """Convert the bin DF into a key showing the bin name assigned to each unique device-provider pair."""
    if binmodel == 'prv':
        binmodel='provider'
    if binmodel == 'dvc':
        binmodel='device'
    if binmodel in ['provider','device']:
        bin_key = pd.DataFrame(list(zip(
            [p for p in modeldf.provider.unique() for d in modeldf.device.unique()],
            [d for p in modeldf.provider.unique() for d in modeldf.device.unique()])),
            columns=['provider','device'])
        bin_key['bin_name'] = bin_key[binmodel]
    else:
        bin_names = create_bins(modeldf, binmodel=binmodel, threshold=threshold, thresh2=1)
        ### Flatten into a list of models/providers
        bin_names['device'] = bin_names.index
        ### Key for looking up which bin each cell belongs to
        bin_key = pd.melt(bin_names, id_vars=['device'], value_name='bin_name')
    for p in manual_bins.keys():
        for i in manual_bins[p].keys():
            bin_key.loc[(bin_key.provider == p) &
                        (bin_key.device.isin(manual_bins[p][i])),
                        'bin_name'] = '{0:0>2}-{1}'.format(i, p)
    return bin_key
########################################################################
def fit_model(modeldf, bin_key, smoothing=500, min_periods=100, do_flatten=True,
              flatten_multiplier=2, flatten_start_thresh=0.1):
    """Generate prediction table, which shows the SWOT fraction predicted for each bin-duration combo
    drtn  SmartTV Roku etc
    1     0.001   0.01 ...
    2     0.004   0.02 ...
    3     0.006   0.04 ...
    ...   ...     ...
    """
    ### Get duration model using each bin as a separate column
    pred_lookup = pd.DataFrame([], index=range(1, int(modeldf.swotstreamdur.max())+1), columns=bin_key.bin_name.unique())
    for c in pred_lookup.columns:
        ### device-provider pairs that are in this bin
        cells = bin_key.loc[bin_key.bin_name == c]
        ### subset of events that come from those device-provider pairs, sorted by total duration
        subset = modeldf.loc[modeldf.device.isin(cells.device) & modeldf.provider.isin(cells.provider),
                             ['swotstreamdur', 'duration', 'swot_frac']].sort_values('swotstreamdur')
        if do_flatten & (subset.shape[0] < smoothing*flatten_multiplier):
            ### too few events => take bin total swot_frac
            pred_lookup[c] = 1. - subset.duration.sum() / subset.swotstreamdur.sum()
        else:
            ### rolling average of swot fraction
            subset['mn_swot_frac'] = subset.swot_frac.rolling(smoothing, center=True,
                                                              min_periods=min(smoothing, min_periods, subset.shape[0])).mean()
            ### groupby to get a single average swot fraction for each durationd
            pred_lookup[c] = subset.groupby('swotstreamdur').mn_swot_frac.mean()
            pred_lookup[c] = pred_lookup[c].fillna(method='ffill')
            ### If not enough points at short durations, patch with means at each point, then smooth
            if np.isnan(pred_lookup.loc[1, c]):
                dr=1
                while np.isnan(pred_lookup.loc[dr, c]):
                    pred_lookup.loc[dr, c] = subset.loc[subset.swotstreamdur == dr, 'swot_frac'].mean()
                    dr += 1
                pred_lookup.loc[0, c] = 0
                pred_lookup.sort_index(inplace=True)
                pred_lookup.loc[1:(dr-1), c] = pred_lookup.loc[:(dr-1), c].rolling(3, center=True, min_periods=1).mean()
                pred_lookup = pred_lookup.drop(0, axis=0)
            ### If first point value is too high, assume fit is bad and flatten the curve
            if do_flatten & (pred_lookup[c].iloc[0] > flatten_start_thresh):
                pred_lookup[c] = 1. - subset.duration.sum() / subset.swotstreamdur.sum()
    return pred_lookup

########################################################################
def predict_from_lookup_and_key(predictdf, pred_lookup, bin_key):
    """Apply the prediction table to the test data to get predicted SWOT fraction per session."""
    preds = predictdf[['device', 'provider', 'duration', 'swotstreamdur', 'swot_frac']].copy()
    preds['pred_swotfrac'] = np.nan
    for c in pred_lookup.columns:
        ### device-provider pairs that are in this bin
        cells = bin_key.loc[bin_key.bin_name == c]
        ### subset of events that come from those device-provider pairs, sorted by total duration
        binmembers = predictdf.device.isin(cells.device) & predictdf.provider.isin(cells.provider)
        ### assign correct swotfrac for duration to binmembers
        preds.loc[binmembers, 'pred_swotfrac'] = list(pred_lookup.loc[preds.loc[binmembers, 'swotstreamdur'], c])
    preds['pred_drtn'] = preds.swotstreamdur * (1. - preds.pred_swotfrac)
    return preds
########################################################################
def model_swot(modeldf, predictdf, model_vers = 'v4', threshold=150, smoothing=3000, flatten_multiplier=2):
    """Does the whole model process:
    1. Take the train and test data
    2. Create the bins for provider/device
    3. Create the prediction table from the training data
    4. Apply prediction table to the test data, and return the predictions."""
    do_flatten = True
    if 'drtn' not in model_vers:
        smoothing = sys.maxsize
        flatten_multiplier=1
    if 'drtn-no-' in model_vers:
        do_flatten = False
        bin_key = get_bin_key(modeldf, binmodel=model_vers.split('-')[-1], threshold=threshold)
    else:
        if len(model_vers.split('-')) == 1:
            bin_key = get_bin_key(modeldf, binmodel=model_vers, threshold=threshold)
        elif len(model_vers.split('-')) == 2:
            bin_key = get_bin_key(modeldf, binmodel=model_vers.split('-')[1], threshold=threshold)
        else:
            assert 1==0, 'ERROR: invalid bin model version {0}'.format(model_vers)
    pred_lookup = fit_model(modeldf, bin_key, smoothing=smoothing,
                            do_flatten=do_flatten, flatten_multiplier=flatten_multiplier)
    if modeldf.swotstreamdur.max() < predictdf.swotstreamdur.max():
        pred_lookup = pred_lookup.reindex(
            range(int(pred_lookup.index.min()), int(predictdf.swotstreamdur.max()) + 1)).fillna(method='ffill')
    preds = predict_from_lookup_and_key(predictdf, pred_lookup, bin_key)
    return preds

########################################################################
def calc_score(df, truecol = 'duration', predcol='pred_drtn', groupcol='provider'):
    """Calculate metrics of model performance from a DF of predictions.
    Stats are provided in total, as well as per device or per provider depending on groupcol.
    truecol is the name of the true session duration, and predcol of the predicted session duration."""
    d = df[[truecol, predcol, groupcol]].copy()
    d['delta'] = d[predcol] - d[truecol]
    aggs = pd.DataFrame(d.groupby(groupcol)['duration'].sum())#.astype('int')
    aggs.columns = ['true_drtn']
    aggs['pred_drtn'] = d.groupby(groupcol)['pred_drtn'].sum()#.astype('int')
    # aggs['delta'] = d.groupby(groupcol)['delta'].sum()
    aggs['delta'] = aggs.pred_drtn - aggs.true_drtn
    aggs['reldiff'] = aggs.delta/aggs.true_drtn
    aggs.loc['all'] = [df[truecol].sum(),
                       df[predcol].sum(),
                       df[predcol].sum() - df[truecol].sum(),
                      (df[predcol].sum() - df[truecol].sum())/df[truecol].sum()]
    return aggs
########################################################################
def calc_score(pred, truecol = 'duration', strmcol='swotstreamdur', predcol='pred_drtn', groupcol='provider'):
    """Calculate metrics of model performance from a DF of predictions.
    Stats are provided in total, as well as per device or per provider depending on groupcol.
    truecol = true session duration
    strmcol = streaming duraion
    predcol = predicted session duration."""
    d = pred[[truecol, strmcol, predcol, groupcol]].copy()
    # d['delta'] = d[predcol] - d[truecol]
    aggs = pd.DataFrame(d.groupby(groupcol)[truecol].sum())#.astype('int')
    aggs.columns = ['true_drtn']
    aggs['strm_drtn'] = d.groupby(groupcol)[strmcol].sum()
    aggs['pred_drtn'] = d.groupby(groupcol)[predcol].sum()#.astype('int')
    # aggs['delta'] = d.groupby(groupcol)['delta'].sum()
    aggs['delta'] = aggs.pred_drtn - aggs.true_drtn
    aggs['reldiff'] = aggs.delta/aggs.true_drtn
    aggs.loc['all'] = [d[truecol].sum(),
                       d[strmcol].sum(),
                       d[predcol].sum(),
                       d[predcol].sum() - d[truecol].sum(),
                      (d[predcol].sum() - d[truecol].sum())/d[truecol].sum()]
    return aggs
########################################################################
def rms(x, axis=0):
    """Calculate root mean squared"""
    return (x**2).mean(axis=axis)**0.5

########################################################################
