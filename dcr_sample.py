### Manual model
import numpy as np
import pandas as pd
import datetime as dt
pd.set_option('display.width', 180)
import matplotlib
import matplotlib.pyplot as plt
import seaborn as sb
import random as rd
import os

# vers = '17-09-20'
vers = 'parama-all'

floc = 'Census_ott_timestamp_check'
plot_path = '{}/{}/'.format(floc, vers)
if not os.path.exists(plot_path):
    os.makedirs(plot_path)

print('Read: {0}\nWrite to: {1}'.format(vers, plot_path))
######################################################################################
### Sample of all columns
# test = pd.read_csv('DCR_data/sample.csv')
# print(test.iloc[:3, :10])
# print(test.iloc[:3, 10:17])
# print(test.iloc[:3, 17:29])
# print(test.iloc[:3, 29:37])
# print(test.iloc[:3, 37:45])
# print(test.iloc[:3, 45:52])
# print(test.iloc[:3, 52:62])
# print(test.iloc[:3, 62:])

######################################################################################
print('read panel device fractions')
pnl_dvc_fracs = pd.read_csv('dvc_fraction_panel.csv', index_col=0)

### Manually alias certain device names
dvc_alias = {'Amazon Fire TV':    ['Amazon 4k Fire TV'],
             'Amazon Fire Stick': ['Amazon Fire TV Stick'],
             'Play Station 3':    ['PlayStation 3'],
             'Play Station 4':    ['PlayStation 4'],
             'X Box One':         ['XBOX One'],
             'X Box 360':         ['XBOX 360'],
             'Wii U':             ['Wii U'],
             'Bluray':            ['Blu-Ray'],
             'Roku':              ['Roku'],
             }

######################################################################################
######################################################################################
######################################################################################
print('Read census ping data')
if vers == 'both':
    dcr_pingsA = pd.read_csv('DCR_data/dcr_events_17-06-01.csv').sort_values(['ipaddress','visitor','time'])
    dcr_pingsB = pd.read_csv('DCR_data/dcr_events_17-06-02.csv').sort_values(['ipaddress','visitor','time'])
    dcr_pings = pd.concat([dcr_pingsA, dcr_pingsB]).drop_duplicates()
    del dcr_pingsA, dcr_pingsB
elif vers == '2017_11_07':
    header = list(pd.read_csv('{0}/ott_census_data_columns.txt'.format(floc), header=None)[0])
    dcr_pings = pd.read_csv('{0}/{1}.tar'.format(floc, vers),
                            sep='|', skiprows=[0, 2770295, 2770310], header=None, names=header)#.sort_values(['ipaddress','visitor','time'])
    dcr_pings = dcr_pings.loc[ (dcr_pings['product']   == 'dcr')
                             & (dcr_pings.platform     == 'OTT')
                             & (dcr_pings.content_type == 'video')
                             & (dcr_pings.country_id   == 'usa')
                             & (dcr_pings.robotic_flag == 0)
                             & (dcr_pings.optout_flag  == 0)
                             ].copy()
elif vers == '2day_check':
    header = list(pd.read_csv('{0}/ott_census_data_columns.txt'.format(floc), header=None)[0])
    dcr_pingsA = pd.read_csv('{0}/2017_11_07.tar'.format(floc),
                            sep='|', skiprows=[0, 2770295, 2770310], header=None,
                            names=header)
    dcr_pingsB = pd.read_csv('{0}/2017_11_08.tar'.format(floc),
                            sep='|', skiprows=[], header=None,
                            names=header)
    filter_cols = ['product','platform','content_type','country_id','robotic_flag','optout_flag']
    use_cols = ['datadate','time','collection_timestamp','device_timestamp','reporting_datadate',
                'client_id','app_id','useragent',
                'device_name','visitor','ipaddress','device_type','physical_hardware_type',
                'content_length','content_duration','actiontype','program_name','episode_name','pageurl'
                ]
    dcr_pings = pd.concat([dcr_pingsA[filter_cols+use_cols], dcr_pingsB[filter_cols+use_cols]])
    dcr_pings = dcr_pings.loc[(dcr_pings['product'] == 'dcr')
                              & (dcr_pings.platform == 'OTT')
                              & (dcr_pings.content_type == 'video')
                              & (dcr_pings.country_id == 'usa')
                              & (dcr_pings.robotic_flag == 0)
                              & (dcr_pings.optout_flag == 0)
                              ].copy()
    dcr_pings = dcr_pings[use_cols].drop_duplicates().sort_values(['visitor','app_id','collection_timestamp'])
    # del dcr_pingsA, dcr_pingsB
elif 'parama' in vers:
    # List of column names in the raw census data table
    col_names = ['time', 'datadate', 'reporting_datadate', 'product', 'client_id', 'vcid', 'actiontype', 'content_type',
                 'subresourcetype', 'nuid_rnid', 'resource_group', 'content_id', 'channel_asset', 'placement_id',
                 'segment_code',
                 'app_id', 'platform', 'device_type', 'program_name', 'episode_name', 'asset_name', 'segment_a',
                 'segment_b',
                 'segment_c', 'country_id', 'dma', 'segment_3', 'adobe_suit_id', 'adobe_offset', 'cross_refernece_id1',
                 'cross_reference_id2', 'ipaddress', 'useragent', 'robotua', 'robotactivity', 'robotic_flag',
                 'content_length',
                 'content_duration', 'ad_duration', 'number_of_ads', 'app_crash_count', 'visitor', 'ad_load_flag',
                 'is_full_episode_flag', 'original_air_date_time', 'cross_reference_id3', 'video_format_code',
                 'pageurl', 'count_pings',
                 'count_app_launch_pings', 'adobe_id', 'player_id', 'physical_hardware_type', 'device_name',
                 'adb_session_id',
                 'ad_support_flag', 'optout_flag', 'genre', 'traf_type', 'household_id', 'ip_source',
                 'generic_provider',
                 'generic_provider_id', 'generic_publisher', 'generic_publisher_id', 'devid', 'sesid',
                 'device_timestamp',
                 'collection_timestamp', 'creation_datetime', 'load_id', 'pdate', 'phour']
    if any([str in vers for str in ['sdk','all']]):
        sdk_pings = pd.read_csv('census_ott_timestamp_check/sdk_ott_census.txt', sep='/t', header=None, names=col_names)\
                      .sort_values(['visitor','app_id','collection_timestamp'])
    if any([str in vers for str in ['cloud', 'all']]):
        cld_pings = pd.read_csv('census_ott_timestamp_check/unifiedping.txt', sep=',', header=None, names=col_names) \
            .sort_values(['visitor', 'app_id', 'collection_timestamp'])
        cld_pings = cld_pings.loc[cld_pings.visitor.notnull()].copy()
    if vers == 'parama-all':
        dcr_pings = pd.concat([cld_pings, sdk_pings])
    elif vers == 'parama-sdk':
        dcr_pings = cld_pings.copy()
    elif vers == 'parama-cloud':
        dcr_pings = sdk_pings.copy()
    else:
        assert 1==0, 'ERROR: invalid version {}'.format(vers)
else:
    dcr_pings = pd.read_csv('DCR_data/dcr_events_{0}.csv'.format(vers)).sort_values(['visitor','app_id','collection_timestamp'])

print('Prep data')
### Identify source
dcr_pings['source'] = 'sdk'
dcr_pings.loc[dcr_pings.actiontype=='unified', 'source'] = 'cloud'
### Time columns
# dcr_pings['dt1']     = pd.to_datetime(dcr_pings.time*1e9)
# dcr_pings['dt2']     = pd.to_datetime(dcr_pings.datadate)
dcr_pings['dt_cl']   = pd.to_datetime(dcr_pings.collection_timestamp*1e9)
dcr_pings['dt_dv']   = pd.to_datetime(dcr_pings.device_timestamp*1e9)
dcr_pings['dt_cr']   = pd.to_datetime(dcr_pings.creation_datetime)
# dcr_pings['time_dt'] = pd.to_datetime(dcr_pings.time*1e9)
# dcr_pings['sttm']   = dcr_pings.dt
# dcr_pings['edtm']   = dcr_pings.dt + pd.to_timedelta(dcr_pings.content_length, unit='s')
### Get elapsed times
dcr_pings['sort_action'] = 1
dcr_pings.loc[dcr_pings.actiontype == 'view', 'sort_action'] = 0
t_types = ['dv','cl']
for t_type in t_types:
    dt_col = 'dt_{}'.format(t_type)
    dcr_pings = dcr_pings.sort_values(['visitor','app_id',dt_col] +
                                      ['dt_'+t for t in t_types if t!=t_type] +
                                      ['sort_action'])
    ### Time elapsed before/after
    for x, name in [(1, 'prev'), (-1, 'next')]:
        gap_col = 'gap_{}_{}'.format(t_type, name)
        dcr_pings[gap_col] = x*(dcr_pings[dt_col] - dcr_pings[dt_col].shift(x))/dt.timedelta(minutes=1)
        dcr_pings.loc[ (dcr_pings.visitor != dcr_pings.visitor.shift(x))
                     | (dcr_pings.app_id  != dcr_pings.app_id.shift(x) ), gap_col] = np.nan
### Relevant elapsed time for each api:
dcr_pings['drtn'] = dcr_pings.gap_cl_prev
dcr_pings.loc[dcr_pings.source == 'cloud', 'drtn'] = dcr_pings.loc[dcr_pings.source == 'cloud', 'gap_cl_next']
### Content length in minutes
dcr_pings['cont_min'] = dcr_pings.content_length/60
# dcr_pings['extra_content'] = dcr_pings.cont_min - dcr_pings.gap_cl
# dcr_pings.loc[dcr_pings.extra_content<0, 'extra_content'] = 0
### Lag between device (send) time and collecion (received) time
### Should indicate how many seconds behind Nielsen time this device is (plus signal transit time)
dcr_pings['lag_dv']   = (dcr_pings.dt_cl - dcr_pings.dt_dv)/dt.timedelta(seconds=1)
### Lag from creation to send time (both on device clock, should always be positive, usually is negative
dcr_pings['lag_send'] = (dcr_pings.dt_dv - dcr_pings.dt_cr)/dt.timedelta(seconds=1)

dcr_pings['view'] = dcr_pings['actiontype'] == 'view'
# dcr_pings['consec'] = dcr_pings.gap.abs() <= 60
# -------------------------------------------------------------------------------------------------------------------- #
### Write list of device names and their frequency
dvcs = dcr_pings.groupby(['physical_hardware_type','device_type','device_name'])\
                .agg({'visitor': pd.Series.nunique}).reset_index().rename(columns={'visitor':'n_visitors'})
dvcs.to_csv('dcr_dvcs.csv', index=False)

# -------------------------------------------------------------------------------------------------------------------- #
### Pings per minute per app
dcr_pings['min_cl'] = dcr_pings.dt_cl.dt.floor('min')
pings_per_min = dcr_pings.pivot_table(index='min_cl', columns='app_id', values='time', aggfunc='count')
pings_per_min = pings_per_min.reindex(pd.date_range(pings_per_min.index.min(), pings_per_min.index.max(), freq='min')).fillna(0)

f1, ax1 = plt.subplots(1)
for app in pings_per_min.columns:
    ax1.plot(pings_per_min.index, pings_per_min[app], label=app)
ax1.legend()
f1.savefig('{}pings_per_min.png'.format(plot_path))
plt.close(f1)
# -------------------------------------------------------------------------------------------------------------------- #
sample_cols = ['visitor','app_id','device_name',
               'segment_code','actiontype','program_name','episode_name',
               'collection_timestamp','device_timestamp','dt_cl','dt_dv','drtn','cont_min']
sample_names = {'dt_cl': 'collection_time',
                'dt_dv': 'device_time',
                'drtn':  'elapsed_time',
                'cont_min': 'content_length_min',
                'gap_cl_prev': 'gap_to_prev',
                'gap_cl_next': 'gap_to_next'}
# -------------------------------------------------------------------------------------------------------------------- #
### Pick a random cloud visitor for a sample to show prev vs next gap
v = '000708174c5106dfd2caea79fea288a8ff86737b6b21aab285a5fcb550aabcd5'
dcr_pings.loc[ (dcr_pings.source=='cloud')
             & (dcr_pings.visitor == v),
               sample_cols + ['gap_cl_prev','gap_cl_next']]\
         .rename(columns=sample_names).to_csv('{}cloud_visitor_{}.csv'.format(plot_path, v))

# -------------------------------------------------------------------------------------------------------------------- #
### content per minute (rounded) per person
### SDK only, so shouldn't much exceed 5
crd_per_min = dcr_pings.loc[dcr_pings.source=='sdk']\
                       .groupby(['visitor','app_id','min_cl']).cont_min.sum().sort_values(ascending=False)
print(crd_per_min.head(10))

n=6
f1, ax1 = plt.subplots(2, figsize=[10, 5])
f1.autofmt_xdate()
for v in crd_per_min.reset_index().visitor.drop_duplicates()[:n]:
    sample1 = dcr_pings.sort_values(['visitor','app_id','dt_cl','dt_dv','sort_action'])\
                       .loc[dcr_pings.visitor == v,
                            sample_cols]
    print(v, sample1.shape)
    sample1.rename(columns={'dt_cl':    'collection_datetime',
                            'dt_dv':    'device_datetime',
                            'drtn':     'elapsed_min',
                            'cont_min': 'content_length_min'})\
           .to_csv('{}visitor_{}.csv'.format(plot_path, v), index=False)
    sample2 = crd_per_min.loc[v].reset_index().set_index('min_cl')
    sample2 = sample2.reindex(pd.date_range(sample2.index.min(), sample2.index.max(), freq='min')).fillna(0)
    ax1[0].plot(sample2.index, sample2.cont_min, lw=1)
ax1[0].axhline(5, lw=1, alpha=0.3, c='black')
ax1[0].xaxis.set_major_formatter(matplotlib.dates.DateFormatter('%H:%M'))
for v in crd_per_min.loc[crd_per_min>7].sort_values().reset_index().visitor[:n]:
    sample2 = crd_per_min.loc[v].reset_index().set_index('min_cl')
    sample2 = sample2.reindex(pd.date_range(sample2.index.min(), sample2.index.max(), freq='min')).fillna(0)
    ax1[1].plot(sample2.index, sample2.cont_min, lw=1)
ax1[1].axhline(5, lw=1, alpha=0.3, c='black')
ax1[1].xaxis.set_major_formatter(matplotlib.dates.DateFormatter('%H:%M'))
f1.savefig('{}content_per_min.png'.format(plot_path))
plt.close(f1)

# -------------------------------------------------------------------------------------------------------------------- #
### How many viewed minutes per minute count as offline viewing?

n_viewers_offline = pd.DataFrame({'threshold':np.arange(0, 10, 0.1)})
n_viewers_offline['n_viewers_above'] = [crd_per_min.loc[crd_per_min>=t].reset_index().visitor.nunique()
                                        for t in n_viewers_offline.threshold]
n_viewers_offline['frac_viewers_above'] = n_viewers_offline['n_viewers_above']/crd_per_min.reset_index().visitor.nunique()

f1, ax1 = plt.subplots(1, figsize=[5,4])
ax1.plot(n_viewers_offline.threshold, n_viewers_offline.frac_viewers_above)
ax1.set_xlabel('Offline viewing threshold (min viewed per minute)')
ax1.set_ylabel('Fraction of visitors with offline viewing')
ax1.axvline(5, lw=1, alpha=0.3, c='black')
ax1.axvline(7.5, lw=1, alpha=0.3, c='black')
f1.savefig('{}offline_threshold.png'.format(plot_path))
plt.close(f1)

# -------------------------------------------------------------------------------------------------------------------- #
### Visualize timestamp relationship for one visitor
pal = sb.color_palette('deep')
time_names = {'dt_cl': 'collection time',
              'dt_dv': 'device time',
              'dt_cr': 'creation time'}
time_clrs = {'dt_cl': pal[2],
             'dt_dv': pal[1],
             'dt_cr': pal[0]}
### Get list of visitors to plot
ppl_per_group = 3
visitor_app_count = dcr_pings.groupby(['visitor','app_id']).dt_cl.count()\
                             .sort_values(ascending=False).reset_index().set_index('visitor')
visitors_per_app = visitor_app_count.reset_index().groupby('app_id').visitor.count().sort_values(ascending=False)
visitor_list = [visitor_app_count.loc[visitor_app_count.app_id==app].index[i]
                for app in visitors_per_app.loc[visitors_per_app>=ppl_per_group].index
                for i in range(ppl_per_group)]
### People with multiple apps?
apps_per_visitor = visitor_app_count.reset_index().visitor.value_counts()
top_multiapp_visitors = visitor_app_count.reset_index().groupby('visitor').dt_cl.sum()\
                          .loc[apps_per_visitor.loc[apps_per_visitor>1].index]\
                          .sort_values(ascending=False).index[:ppl_per_group]
visitor_list += [v for v in top_multiapp_visitors if v not in visitor_list]
### Manually add some that contain offline viewing
visitor_list += list(crd_per_min.reset_index().visitor[:6])
### Plot ping times of each visitor in the list
for v in visitor_list:
    sample1 = dcr_pings.loc[dcr_pings.visitor == v]
    f1, ax1 = plt.subplots(1, figsize=[10,10], sharex=True, sharey=True)
    f1.autofmt_xdate()
    ### plot each time vs each time
    for ax, x, y1, y2 in [(ax1, 'dt_cl', 'dt_cr', 'dt_dv')]:
                         # (ax1[1], 'dt_dv', 'dt_cr', 'dt_cl'),
                         # (ax1[2], 'dt_cr', 'dt_cl', 'dt_dv')]:
        other_col = [c for c in ['dt_cl','dt_dv'] if c != x][0]
        ### Plot view pings
        ax.plot(sample1.loc[sample1['actiontype'] == 'view', x],
                    sample1.loc[sample1['actiontype'] == 'view', other_col],
                    '+', c=pal[4], mew=0.5, ms=40, label='view pings ({} vs {})'.format(x, other_col))
        ### Plot episode shading
        eps = sample1[['dt_cl','program_name','episode_name']].drop_duplicates(['program_name','episode_name'])
        for i in range(eps.shape[0]-1):
            ax.axvspan(eps.iloc[i].dt_cl, eps.iloc[i+1].dt_cl,
                       alpha=0.2, facecolor=pal[i%len(pal)], lw=0, label='__nolegend__')
        #### Plot all pings
        for ycol in [x, y1, y2]:
            ax.plot(pd.Index(sample1[x]), sample1[ycol], c=time_clrs[ycol], lw=0.5, marker='.', label=time_names[ycol])
        ax.legend(loc='upper left', frameon=True, framealpha=0.5)
        ax.set_xlabel(time_names[x])
        ax.xaxis.set_major_formatter(matplotlib.dates.DateFormatter('%y-%m-%d %H:%M'))
        ax.yaxis.set_major_formatter(matplotlib.dates.DateFormatter('%y-%m-%d %H:%M'))
    ### all
    ax.set_xlim(dcr_pings.dt_cl.min(), dcr_pings.dt_cl.max())
    ax.set_ylim(dcr_pings.dt_cl.min(), dcr_pings.dt_cl.max())
    visitor_uses = sample1[['source','app_id']].drop_duplicates()
    ax.set_title('\n'.join(['{}: {}'.format(visitor_uses.loc[i, 'source'], visitor_uses.loc[i, 'app_id']) for i in visitor_uses.index]))
    plt.tight_layout()
    f1.savefig('{}visitor_{}.png'.format(plot_path, v), dpi=300)
    plt.close(f1)
# -------------------------------------------------------------------------------------------------------------------- #
### Example of device_time flattening
v = 'b84045db-c129-4dd6-99fc-eacfacc55448'
sample1 = dcr_pings.loc[ (dcr_pings.visitor == v),
                        ['visitor','app_id','device_name','segment_code','actiontype',
                         'program_name','episode_name','dt_cl','dt_dv','drtn','cont_min']]
sample1.rename(columns={'dt_cl':'collection_time',
                        'dt_dv':'device_time'})\
       .to_csv('{}device_freeze_visitor{}.csv'.format(plot_path, v), index=False)
# -------------------------------------------------------------------------------------------------------------------- #
### Presentation visitor plots
select_visitor_list = ['4418843d6a717c0d886469a44d61d1ad','a492edc0fe7347e4535491b955f4c035'] + \
                      [v for v in visitor_list if any([v.startswith(s) for s in ['b8404','7c34','dcd','2dbf']])]
for v in select_visitor_list:
    sample1 = dcr_pings.loc[dcr_pings.visitor == v]
    f1, ax1 = plt.subplots(1, figsize=[5,5], sharex=True, sharey=True)
    f1.autofmt_xdate()
    ### plot each time vs each time
    for ax, x, y2 in [(ax1, 'dt_cl', 'dt_dv')]:
        other_col = [c for c in ['dt_cl','dt_dv'] if c != x][0]
        ### Plot view pings
        ax.plot(sample1.loc[sample1['actiontype'] == 'view', x],
                sample1.loc[sample1['actiontype'] == 'view', other_col],
                '|', c=pal[1], mew=0.5, ms=20, label='view pings)'.format(x, other_col))
        ### Plot episode shading
        eps = sample1[['dt_cl','program_name','episode_name']].drop_duplicates(['program_name','episode_name'])
        eps.loc[eps.index.max()+1, 'dt_cl'] = sample1.dt_cl.max()
        for i in range(eps.shape[0]-1):
            ax.axvspan(eps.iloc[i].dt_cl, eps.iloc[i+1].dt_cl,
                       alpha=0.2, facecolor=pal[i%len(pal)], lw=0, label='__nolegend__')
        #### Plot all pings
        for ycol in [x, y2]:
            ax.plot(pd.Index(sample1[x]), sample1[ycol], c=time_clrs[ycol], lw=0.5, marker='.', label=time_names[ycol])
        ### Labels
        ax.legend(loc='upper left', frameon=True, framealpha=0.5)
        ax.set_xlabel(time_names[x])
        ax.xaxis.set_major_formatter(matplotlib.dates.DateFormatter('%H:%M'))
        ax.yaxis.set_major_formatter(matplotlib.dates.DateFormatter('%H:%M'))
    ### all
    if v == '4418843d6a717c0d886469a44d61d1ad':
        t_range = (dt.datetime(2018, 4, 6, 3), dt.datetime(2018, 4, 6, 22))
    elif v == 'a492edc0fe7347e4535491b955f4c035':
        t_range = (dt.datetime(2018, 4, 6, 1), dt.datetime(2018, 4, 6, 4, 30))
    else:
        t_range = (dt.datetime(2018, 4, 6, 15), dt.datetime(2018, 4, 6, 20))
    ax.set_xlim(t_range)
    ax.set_ylim(t_range)
    visitor_uses = sample1[['source','app_id']].drop_duplicates()
    ax.set_title('\n'.join(['visitor '+v]+
                           ['{} app {}'.format(visitor_uses.loc[i, 'source'], visitor_uses.loc[i, 'app_id']) for i in visitor_uses.index]))
    plt.tight_layout()
    f1.savefig('{}pres_visitor_{}.png'.format(plot_path, v), dpi=300)
    plt.close(f1)

# -------------------------------------------------------------------------------------------------------------------- #
### Distribution of ping gaps
### bins are [0, 1) etc
eps=0.000001
sb.distplot(dcr_pings.loc[(dcr_pings.source=='sdk') & (dcr_pings.cont_min==5),'drtn'].dropna(),
            kde=False, bins=[-eps] + [i+eps for i in range(1, 11)])

# -------------------------------------------------------------------------------------------------------------------- #
### How many large discrepancies between device and collection time are there?
gap_directions = ['prev','next']
timestamp_types = ['cl'] # ['cr','dv','cl']
sources = ['cloud','sdk']
for t_type in timestamp_types:
    f1, ax1 = plt.subplots(len(gap_directions), len(sources),
                           figsize=[len(sources) * 4, len(gap_directions) * 4])#, sharex=True, sharey=True)
    for j, source in enumerate(sources):
        if source == 'cloud':
            df = dcr_pings.loc[dcr_pings.actiontype == 'unified'].copy()
            t_lim = 80
        else:
            df = dcr_pings.loc[dcr_pings.actiontype != 'unified'].copy()
            t_lim = 8
        for i, name in enumerate(gap_directions):
            ax1[i,j].plot([0,df['gap_cl_{}'.format(name)].max()],[0,df['gap_cl_{}'.format(name)].max()], c='red', lw=1)
            ax1[i,j].set_xlabel('content_length (min)')
            ax1[i,j].set_ylabel('elapsed time (min)')
            # ax1[i,j].set_ylabel('{} gap to {} ping (min)'.format(time_names['dt_'+t_type], name))
            # ax1[i,j].set_title('{}, gap to {} ping'.format(source, name))
            ax1[i,j].set_xlim(0, t_lim)
            ax1[i,j].set_ylim(0, t_lim)
            for k, app in enumerate(df.app_id.value_counts().index):
                app_df = df.loc[df.app_id == app].copy()
                ax1[i,j].scatter(app_df['cont_min'],
                                 app_df['gap_{}_{}'.format(t_type, name)], s=2, alpha=0.2, c=pal[k], label=app)
        ### all
        # leg = ax1[0,j].legend(loc='upper left', frameon=True, framealpha=0.5, markerscale=5)
        # for lh in leg.legendHandles:
        #     lh.set_alpha(1)
    plt.tight_layout()
    f1.savefig('{}time_passage_comparisons_{}.png'.format(plot_path, t_type), dpi=300)
    plt.close(f1)

# -------------------------------------------------------------------------------------------------------------------- #
### Large number of sessions with collection time gap between content length and content length - 1.5 min
### Horizontal lines every 1.5 min in collection time gap
### (many different content lengths at that gap value, kind of scaling up with it but not strongly)

# # -------------------------------------------------------------------------------------------------------------------- #
# ### Credited time per second per person/app (timer pings, SDK only)
#
# crd_per_sec = dcr_pings.loc[dcr_pings.actiontype!='view']\
#                        .groupby(['visitor','app_id','source','dt_cl'])\
#                        .agg({'time':     'count',
#                              'cont_min': 'sum',
#                              'drtn':     'sum'})\
#                        .reset_index().sort_values('cont_min', ascending=False)
# crd_per_sec.head(10)
#
# crd_per_sec['diff'] = crd_per_sec.cont_min - crd_per_sec.drtn
# crd_per_sec['excess'] = crd_per_sec['diff'] > 1
# ids = crd_per_sec.loc[crd_per_sec.source=='sdk'].groupby(['visitor','app_id']).agg({'excess':'sum'}).reset_index()
#
# roll_samp = dcr_pings.loc[dcr_pings.source=='sdk', ['visitor','app_id','dt_cl','drtn','cont_min']]\
#                      .sort_values(['visitor','app_id','dt_cl']).set_index('dt_cl')
# roll_samp['roll_cont'] = np.nan
#
# n=10
# for i, ind in enumerate(ids.sort_values('excess', ascending=False).index[:n]):
#     print('{:.2f}'.format(i/n))
#     v, a = ids.loc[i, ['visitor','app_id']]
#     ind = (roll_samp.visitor == v) & (roll_samp.app_id == a)
#     roll_samp.loc[ind, 'roll_cont'] = roll_samp.loc[ind, 'cont_min'].rolling('4min').sum()
#
# -------------------------------------------------------------------------------------------------------------------- #
### Compare different timestamps
print((dcr_pings.time == dcr_pings.collection_timestamp).mean(),
      (dcr_pings.time == dcr_pings.device_timestamp).mean(),
      (dcr_pings.collection_timestamp == dcr_pings.device_timestamp).mean())

pal = ['blue','red','green']
f1, ax1 = plt.subplots(1, figsize=[12,12])
for i, a in enumerate(dcr_pings.actiontype.unique()):
    ax1.scatter(dcr_pings.loc[dcr_pings.actiontype==a, 'collection_timestamp'], dcr_pings.loc[dcr_pings.actiontype==a, 'time'], c=pal[i], alpha=0.1, s=1, label=a)
    ax1.set_xlabel('collection')
    ax1.set_ylabel('processing')
ax1.legend()
f1.savefig('{0}/prc_vs_cllct_time_{1}.png'.format(floc, vers))
plt.close(f1)

# -------------------------------------------------------------------------------------------------------------------- #

### 0/1 code for the five minutes of the ping
# segcode_parts = dcr_pings.segment_code.str.split('_', expand=True)
# segcode_parts['type'] = segcode_parts[3].str.slice(0, 1)
# segcode_parts['segcode_4'] = segcode_parts[3].str.slice(1).astype('int')
# segcode_parts['s4_large'] = segcode_parts['segcode_4'] > 9

# dcr_pings['ping_mins'] = segcode_parts[4]
# dcr_pings['segcode_4'] = segcode_parts['segcode_4']
### how do other parts relate?
### 0 is always 4 except for one ping,
### 1 is usually 00 except for ~150 which are 03 (no particular association w/ others)
### 2 is always 99
### ~2x as many D pings are short as are long, ~3x as many V pings
# segcode_parts.groupby(['s4_large','type',0, 1, 2])[3].count()

### Names of clients for particular ids?
client_ids = {'dsc': 'us-201051',
              'dtv': 'us-203735'}
dcr_pings['client_name'] = 'unknown'
for k in client_ids.keys():
    dcr_pings.loc[dcr_pings.client_id == client_ids[k], 'client_name'] = k

### Create device column analogous to panel data
dcr_pings.loc[dcr_pings.device_name.isnull(), 'device_name'] = ''
dcr_pings['device_name_clean'] = dcr_pings['device_name'].str.lower().str.replace('+',' ')
dcr_pings['device'] = 'Other'
for d in pnl_dvc_fracs.index:
    print('-- '+d)
    dcr_pings.loc[dcr_pings.device_name_clean.str.contains(d.lower()), 'device'] = d
for d in dvc_alias.keys():
    for a in dvc_alias[d]:
        print('-- '+a+' -> '+d)
        dcr_pings.loc[dcr_pings.device_name_clean.str.contains(a.lower()), 'device'] = d

### Prevalence of devices?
dcr_pings.device.value_counts()

### Assign a custom unique device id?
dvc_cols  = ['visitor','ipaddress']
### Create dvc_id, unique identifier for the combination of the above
dvc_id_map = dcr_pings[dvc_cols].drop_duplicates().reset_index()
dvc_id_map['dvc_id'] = dvc_id_map.index.astype('str').str.zfill(len(dvc_id_map.index.max().astype('str')))
dcr_pings = dcr_pings.merge(dvc_id_map, on=dvc_cols, how='left')

### Unique devices per type
dcr_pings.groupby(['device','device_type','physical_hardware_type']).agg({'dvc_id': pd.Series.nunique,
                                                                          'time':   'count'}).sort_index()
######################################################################################
app_map = pd.read_csv('DCR_data/app_names2.csv')
app_map.columns = ['app_id'] + [c for c in app_map.columns[1:]]
# prv_list = ['netflix','hulu','amazon','hbo','youtube','crackle']
# for p in prv_list:
#     app_map[p] = app_map['PLAYER_NAME'].str.lower().str.contains(p) | app_map['PLAYER_SHORT_DESCRIPTION'].str.lower().str.contains(p)
#
# assert app_map.loc[app_map[prv_list].sum(axis=1)>1].shape[0] == 0, 'ERROR: multiple matches found'
# ### Assign unmatched ones to 'other'
# app_map['other'] = app_map[prv_list].sum(axis=1)<1
# prv_list.append('other')
# ### Transform to single column
# app_map['provider'] = app_map[prv_list].idxmax(axis=1)
# app_map.provider.value_counts()

print('Join app name table')
dcr_pings2 = dcr_pings.merge(app_map[['app_id','PLAYER_TYPE','PLAYER_NAME','PLAYER_SHORT_DESCRIPTION']],
                             on='app_id', how='left')
dcr_pings2.sort_values(['dvc_id','dt_cl'], inplace=True)
### resolve any disagreements in client_id?
# for ind in dcr_pings2.loc[dcr_pings2.client_id != dcr_pings2.CLIENT_ID].index:
#     if dcr_pings2.loc[ind, 'client_id'].isnull() & dcr_pings2.loc[ind, 'CLIENT_ID'].notnull():
#         dcr_pings2.loc[ind, 'client_id'] = dcr_pings2.loc[ind, 'CLIENT_ID']
#     else:
#         print('Unresolved mismatch:')
#         print(dcr_pings2.loc[ind, ['client_id', 'CLIENT_ID']])

######################################################################################
### Determine which devices have segcode conflicts and remove them
### Assess how frequently segcode numbers are duplicated
# n_pings_per_time = dcr_pings.groupby(['dvc_id','dr_pr','segcode_4','actiontype']).time.count().reset_index()
# n_pings_per_time['is_dupl'] = n_pings_per_time.time > 1
# ### Duplication rate by segcode number
# confl_segcodes = n_pings_per_time.groupby('segcode_4').is_dupl.mean().sort_index()
# ### Duplication rate by device
# dvc_conflict = n_pings_per_time.groupby('dvc_id').is_dupl.mean()
# confl_dvcs = dvc_conflict.loc[dvc_conflict>0].index
# ### 70% of dvcs are unconflicted
# ### what % of events come from them? 34%
# dcr_pings['dvc_confl'] = (dcr_pings.dvc_id.isin(confl_dvcs))
# print('{0:0.1f}% of data comes from conflicted devices'.format(100*dcr_pings.dvc_confl.mean()))
#
# dcr_pings = dcr_pings.loc[~dcr_pings.dvc_confl]

######################################################################################
### client_id/app_ids present
client_app_pairs = dcr_pings2.groupby(['client_name','client_id',
                                      'app_id','PLAYER_TYPE','PLAYER_NAME','PLAYER_SHORT_DESCRIPTION']).count().time.sort_values().reset_index()

######################################################################################
### Ping times
### experimenting with different roundings
dcr_pings2['min_pr']  = dcr_pings2.dt.dt.round('min')
dcr_pings2['min_cl']  = dcr_pings2.dt_cl.dt.round('min')
dcr_pings2['min_us']  = dcr_pings2.dt_dv.dt.round('min')
# dcr_pings2['dt_5min'] = dcr_pings2.dt.dt.floor('h') + pd.to_timedelta(np.floor(pd.DatetimeIndex(dcr_pings2.dt).minute/5)*5, unit='m')

ok_time_range = pd.date_range(dcr_pings2[['min_pr','min_cl']].min().min(), dcr_pings2[['min_pr','min_cl']].max().max(), freq='min')

### Plot distribution against each type of timestamp
for time_col in ['min_pr','min_cl','min_us']:
    print('time column = '+time_col)
    pings_v_time = dcr_pings2.pivot_table(index=time_col, columns='actiontype', values='time', aggfunc='count')
    pings_v_time.columns = ['all-' + c for c in pings_v_time.columns]
    pings_v_time.sort_index(inplace=True)
    # pings_v_time = pings_v_time.reindex(pd.date_range(pings_v_time.index.min(), pings_v_time.index.max(), freq='s')).fillna(0)
    for client in client_ids.keys():
        for ind in client_app_pairs.loc[client_app_pairs.client_name == client].index:
            app, app_name = client_app_pairs.loc[ind, ['app_id','PLAYER_SHORT_DESCRIPTION']]
            pair = client+'_'+app_name
            print(pair)
            pings_v_time_client = dcr_pings2.loc[ (dcr_pings2.client_id == client_ids[client])
                                                & (dcr_pings2.app_id    == app)]\
                                            .pivot_table(index=time_col, columns='actiontype', values='time', aggfunc='count')
            pings_v_time_client.columns = [pair+'-'+c for c in pings_v_time_client.columns]
            pings_v_time = pings_v_time.join(pings_v_time_client, how='outer')
    pings_v_time = pings_v_time.reindex(ok_time_range)
    pings_v_time.fillna(0, inplace=True)

    # client_list = ['all'] + list(client_ids.keys())
    client_list = list(client_ids.keys())
    actions = list(dcr_pings2.actiontype.value_counts().index)
    actions = ['unified', 'timer', 'view'] + [c for c in actions if c not in ['unified', 'timer', 'view']]
    pal = sb.color_palette("Set1", len(actions), desat=0.8)
    ### plot all pings combined
    pair = 'all'
    f1, ax1 = plt.subplots(len(actions), 1, figsize=[20, 10])
    f1.autofmt_xdate();
    f1.subplots_adjust(bottom=0.1, left=0.05, right=0.98, hspace=.2)
    f1.text(0.5, 0.95, s='All', fontsize=14, ha='center')
    for i, action in enumerate(actions):
        ax1[i].set_title(action)
        ax1[i].plot(pings_v_time.index, pings_v_time[pair + '-' + action], c=pal[i], alpha=0.5, label=action)
        if i == len(actions) - 1:
            ax1[i].set_xlabel(time_col)
        ax1[i].set_ylabel('Pings per second')
        ax1[i].set_xlim(pings_v_time.index.min(), pings_v_time.index.max())
        ax1[i].xaxis.set_major_formatter(matplotlib.dates.DateFormatter('%y-%m-%d %H:%M'))
        for s in pings_v_time.index:
            if (s.minute == 0) & (s.second == 0):
                ax1[i].axvline(s, c='black', lw=1, alpha=0.2)
    f1.savefig('{0}/ping_time_distrn_{1}_{2}_{3}.png'.format(floc, vers, pair, time_col))
    plt.close(f1)
    ### Plot each client_id-app_id pair
    for j, client in enumerate(client_list):
        for app in client_app_pairs.loc[client_app_pairs.client_name == client, 'app_id']:
            app_name = app_map.loc[app_map.app_id == app, 'PLAYER_SHORT_DESCRIPTION'].iloc[0]
            pair = client + '_' + app_name
            # pair = client + '_' + app
            print(pair)
            cols = [c for c in pings_v_time.columns if pair in c]
            f1, ax1 = plt.subplots(len(actions), 1, figsize=[20, 10])
            f1.autofmt_xdate();
            f1.subplots_adjust(bottom=0.1, left=0.05, right=0.98, hspace=.2)
            if client == 'all':
                f1.text(client)
            else:
                f1.text(0.5, 0.95, s='{0} ({1}) - {2}'.format(client, client_ids[client], app_name), fontsize=14, ha='center')
            for i, action in enumerate(actions):
                ax1[i].set_title(action)
                if pair+'-'+action in cols:
                    ax1[i].plot(pings_v_time.index, pings_v_time[pair+'-'+action], c=pal[i], alpha=0.5, label=action)
                if i == len(actions) - 1:
                    ax1[i].set_xlabel(time_col)
                ax1[i].set_ylabel('Pings per second')
                ax1[i].set_xlim(pings_v_time.index.min(), pings_v_time.index.max())
                ax1[i].xaxis.set_major_formatter(matplotlib.dates.DateFormatter('%y-%m-%d %H:%M'))
                for s in pings_v_time.index:
                    if (s.minute==0) & (s.second==0):
                        ax1[i].axvline(s, c='black', lw=1, alpha=0.2)
            f1.savefig('{0}/ping_time_distrn_{1}_{2}_{3}.png'.format(floc, vers, pair.replace(' ','_'), time_col))
            plt.close(f1)


######################################################################################
### On-hour vs. not, unified pings
# unif_pings = dcr_pings.loc[dcr_pings.actiontype=='unified', ['client_id','client_name','app_id','dr_pr']]
# unif_pings['dt_min'] = unif_pings.dt.dt.round('min')
# unif_pings['on_hr'] = (pd.DatetimeIndex(unif_pings.dt_min).minute == 0) #& (pd.DatetimeIndex(unif_pings.dt).second == 0)

######################################################################################
### Duration distributions



######################################################################################
######################################################################################
### Understand IDs:
### are visitor and ipaddress 1-to-1? Just about
### most visitors have only 1 ipaddress associated (a few have more)
# dvc_per_ip_vs = dcr_pings.groupby(['visitor','ipaddress','device_name']).time.count().groupby(['visitor','ipaddress']).count()
# mult_dvcs = dvc_per_ip_vs.loc[dvc_per_ip_vs>1].reset_index()
#
# ### Duplicate devices:
# i=0
# dcr_pings.loc[  (dcr_pings.ipaddress==mult_dvcs.ipaddress[i])
#               & (dcr_pings.visitor == mult_dvcs.visitor[i]),
#               ['visitor','ipaddress','useragent','device_type','app_id','device_name','physical_hardware_type']].drop_duplicates()

### Unique devices??
dvcs = pd.DataFrame(dcr_pings.fillna('')\
                    .groupby(['dvc_id','ipaddress','visitor','useragent','device_type','app_id','device',
                              'device_name','physical_hardware_type'])\
                    .time.count().sort_index())
dvcs.columns = ['n_pings']
dvcs = dvcs.reset_index()

### Possible combinations of device/device_type/physical_hardware_type:
dcr_pings.groupby(['device','device_type','physical_hardware_type']).time.count()

### See devices with multiple names:
num_dvcs = dvcs.groupby(['ipaddress','visitor','useragent','device_type','app_id']).device_name.count()
dvcs.set_index(['ipaddress','visitor','useragent','device_type','app_id']).loc[num_dvcs.loc[num_dvcs > 1].index].reset_index()

######################################################################################
# ip = '173.255.141.2'
# subset = dcr_pings.loc[dcr_pings.ipaddress == ip,
#                        ['visitor','ipaddress','device_name','actiontype','dr_pr','gap']].sort_values(['ipaddress','dr_pr'])
# vs = pd.Series(range(len(subset.visitor.unique())),
#                  index = subset.visitor.unique())
# subset['vcode'] = list(vs.loc[subset.visitor])
# subset['change'] = (subset.vcode != subset.vcode.shift(1))
# subset['match_any'] = (subset.vcode <  subset.vcode.shift(1))
#
# (dcr_pings.groupby(['visitor','ipaddress']).time.count().groupby('visitor').count()).value_counts()
# ### and a few ip addresses have multiple visitors
# (dcr_pings.groupby(['visitor','ipaddress']).time.count().groupby('ipaddress').count()).value_counts()
#
# ### See pings from these addresses -- visitor maybe maps to device_name/ipaddress combo?
# ### Typically two different versions of amazon devices, e.g. Amazon Fire TV Stick -> Amazon Fire TV stick (2nd Gen)
# visitors_per_ip = dcr_pings.groupby(['visitor','ipaddress']).time.count().groupby('ipaddress').count()
# dcr_pings.loc[dcr_pings.ipaddress.isin(visitors_per_ip.loc[visitors_per_ip > 1].index)]

######################################################################################
### Compare device representation between census and panel
pnl_dvc_fracs = pd.read_csv('dvc_fraction_panel.csv', index_col=0).iloc[:,0].copy()
pnl_roku_types = ['Roku 1','Roku 2', 'Roku 3', 'Roku Streaming Stick']
pnl_dvc_fracs.loc['Roku'] = pnl_dvc_fracs.loc[pnl_roku_types].sum()
pnl_dvc_fracs.drop(pnl_roku_types, inplace=True)
pnl_dvc_fracs.sort_values(inplace=True, ascending=False)
# pnl_dvc_fracs.loc['everything else'] = pnl_dvc_fracs.loc[[c for c in pnl_dvc_fracs.index if c not in dvcs.device.unique() and 'Roku' not in c]].sum()

cns_dvc_fracs = dvcs.device.value_counts()
dvc_fracs = pd.DataFrame({'panel': pnl_dvc_fracs,'census': cns_dvc_fracs/dvcs.shape[0]}).fillna(0).sort_values('panel', ascending=False)
dvc_fracs.to_csv('DCR_data/dvc_fractions_both_{0}.csv'.format(vers))

dvc_tn_fracs = dcr_pings.groupby('device').content_length.sum() / dcr_pings.content_length.sum()

width=0.35
f1, ax1 = plt.subplots(2)
### fraction of devices
rects1 = ax1[0].bar(np.arange(dvc_fracs.shape[0]),         dvc_fracs.panel,  width, color='r', alpha=0.7, label='panel')
rects2 = ax1[0].bar(np.arange(dvc_fracs.shape[0]) + width, dvc_fracs.census, width, color='b', alpha=0.7, label='census')
ax1[0].set_xticks(np.arange(dvc_fracs.shape[0])+0.25);
ax1[0].set_xticklabels(dvc_fracs.index, rotation=30, ha='right');
ax1[0].legend(loc='upper right', frameon=True, framealpha=0.5)
ax1[0].set_ylabel('Device fraction')
ax1[0].set_title('Fraction of devices of each device type')
### fraction of tuning
rects3 = ax1[1].bar(np.arange(dvc_fracs.shape[0]) + width, dvc_tn_fracs.loc[dvc_fracs.index].fillna(0),  width, color='b', alpha=0.7)
ax1[1].set_xticks(np.arange(dvc_fracs.shape[0])+0.25);
ax1[1].set_xticklabels(dvc_fracs.index, rotation=30, ha='right');
ax1[1].set_ylabel('Tuning fraction')
ax1[1].set_title('Fraction of tuning per device type')
plt.tight_layout()
f1.savefig('{0}/device_distribution_comparison_{1}.png'.format(floc, vers))
plt.close(f1)

######################################################################################
### Segment code piece #4 vs datadate/time
### This doesn't work anymore, not sure why
# f1, ax1 = plt.subplots(1)
# f1.autofmt_xdate();
# ax1.plot(dcr_pings.dt, dcr_pings.segcode_4, alpha=0.3)
# ax1.xaxis.set_major_formatter(matplotlib.dates.DateFormatter('%y-%m-%d %H:%M'))
# ax1.set_xlabel('datadate or time')
# ax1.set_ylabel('segment_code piece #4')
# f1.savefig('DCR_data/segcode_vs_time_{0}.png'.format(vers))
# plt.close(f1)

######################################################################################
######################################################################################
# ### Aggregate on device/episode
#
# by_ep = dcr_pings.groupby(['visitor','ipaddress','device_name','device_type','app_id',
#                            'resource_group','content_id','program_name','episode_name','content_duration'])\
#                  .agg({'content_length': 'sum'}).reset_index()
#
# by_ep_gooddur = by_ep.loc[~by_ep.content_duration.isin([0, 86400])].copy()
# by_ep_gooddur['watched_more'] = by_ep_gooddur.content_length > by_ep_gooddur.content_duration + 5*60
#
#
# ######################################################################################
# ######################################################################################
# ### Assign session id and create sessions
# dcr_pings2['new_ssn'] = ( (dcr_pings2.actiontype == 'view')
#                         | (dcr_pings2.dvc_id           != dcr_pings2.dvc_id.shift(1))
#                         | (dcr_pings2.app_id           != dcr_pings2.app_id.shift(1))
#                         | (dcr_pings2.platform         != dcr_pings2.platform.shift(1))
#                         | (dcr_pings2.subresourcetype  != dcr_pings2.subresourcetype.shift(1))
#                         | (dcr_pings2.content_type     != dcr_pings2.content_type.shift(1))
#                         | (dcr_pings2.resource_group   != dcr_pings2.resource_group.shift(1))
#                         | (dcr_pings2.content_id       != dcr_pings2.content_id.shift(1))
#                         | (dcr_pings2.program_name     != dcr_pings2.program_name.shift(1))
#                         | (dcr_pings2.episode_name     != dcr_pings2.episode_name.shift(1))
#                         | (dcr_pings2.content_duration != dcr_pings2.content_duration.shift(1))
#                         )
# # | (dcr_pings2.gap.abs() <= 60)
# dcr_pings2['ssn_id'] = dcr_pings2.new_ssn.cumsum()
#
# ### Columns which should identify a given stream session
# strm_cols = ['platform','product','subresourcetype',
#              'dvc_id','device','app_id',
#              'content_type','resource_group','content_id','program_name','episode_name','content_duration']
# ### Time-related columns, which will be different in each ping
# tm_cols = ['datadate','time','dr_pr','st','ed','segment_code','ping_mins','content_length']
#
# ### Examine sessions
# ssn_cols = strm_cols + ['actiontype','dr_pr','sttm','edtm','content_length','ping_mins','gap','new_ssn','ssn_id']
# dcr_pings2[ssn_cols].head()
#
# # ---------------------------------------------------------------------------------- #
# ### Examine session properties
#
# ### New session pings that aren't view pings -- why did they break here? which properties changed?
# ### see: dvc_id == 000009
# ### -- content_id: what does it mean?
# ### -- multiple sessions at the same time? or all the pings coming through at once, but delayed?
# dcr_pings2.loc[(dcr_pings2.actiontype=='timer')
#              & (dcr_pings2.new_ssn), ssn_cols]
#
#
# # agg_dict = {}
# # for c in ['client_id', 'visitor', 'ipaddress', 'useragent', 'device_name', 'device_type', 'physical_hardware_type', 'app_id', 'platform', 'product',
# #           'content_type', 'actiontype', 'segment_code', 'content_length', 'content_duration', 'resource_group', 'content_id', 'program_name',
# #           'subresourcetype', 'devid', 'episode_name']:
# #     agg_dict[c] = pd.Series.nunique
# #
# # nvals_per_ssn = dcr_pings2.groupby('ssn_id').agg(agg_dict)
# # frac_nonunique = (nvals_per_ssn > 1).mean(axis=0)
#
# ### Need to preserve all other columns as well -- check for match? or just take first?
# ### is content_duration the drtn of each separate line?
# ssn_aggs = {'content_length'   : 'sum',
#             'time'             : 'count',
#             'dr_pr'               : 'first',
#             'segcode_4'        : 'first',
#             'dvc_id'           : 'first',
#             'app_id'           : 'first',
#             'platform'         : 'first',
#             'subresourcetype'  : 'first',
#             'content_type'     : 'first',
#             'resource_group'   : 'first',
#             'content_id'       : 'first',
#             'program_name'     : 'first',
#             'episode_name'     : 'first',
#             'content_duration' : 'first',
#             }
# dcr_ssns = dcr_pings2.groupby('ssn_id').agg(ssn_aggs)
# dcr_ssns = dcr_ssns.rename(columns={'time': 'n_pings'}).sort_values(['dvc_id','dr_pr'])
# dcr_ssns = dcr_ssns[['dvc_id', 'dr_pr', 'segcode_4', 'n_pings', 'content_length', 'episode_name', 'program_name', 'resource_group',
#                      'content_type', 'content_duration', 'content_id', 'app_id', 'platform', 'subresourcetype']]
#
# ######################################################################################
# ############################### Plot sessions ########################################
# ######################################################################################
# ssns_per_dvc = dcr_pings2.groupby('dvc_id').agg({'ssn_id': pd.Series.nunique}).ssn_id
# pings_per_ssn = dcr_pings2.groupby(['dvc_id','ssn_id']).dt.count().reset_index()
#
# ### Find dvc with at leat 10 sessions where at least one is long
# ### e.g 000585
# dvc_id = pings_per_ssn.loc[pings_per_ssn.dvc_id.isin(ssns_per_dvc.loc[ssns_per_dvc>=100].index)].sort_values('dr_pr', ascending=False).dvc_id.iloc[0]
#
# this_dvc_pings = pings_per_ssn.loc[pings_per_ssn.dvc_id == dvc_id].sort_values('dr_pr', ascending=False)
#
# ### Plot dt vs segcode for a particular device
# this_dvc_data = dcr_pings2.loc[(dcr_pings2.dvc_id==dvc_id)].sort_values(by=['dvc_id','dr_pr','segcode_4'])
#
# ### See ping sequence
# this_dvc_data[['dr_pr','segcode_4','actiontype','episode_name','program_name']].sort_values(by=['dr_pr','segcode_4','actiontype'], ascending=[True,True,False])
# ### Repeat segcodes per time
# this_dvc_data.groupby(['dr_pr','segcode_4','actiontype']).dvc_id.count()
# ### watched per episode?
# mins_per_ep = (this_dvc_data.groupby(['episode_name','program_name','content_duration']).content_length.sum()/60.).sort_values(ascending=False).reset_index()
#
# ### pick one episode
# this_ep = this_dvc_data.loc[this_dvc_data.episode_name == mins_per_ep.episode_name[0]]
# minute_pings = this_ep[['dr_pr','segcode_4','ping_mins','actiontype']].copy()
# for i in range(5):
#     minute_pings[i] = minute_pings.ping_mins.str.slice(i, i+1)
#
#
# plt.close('all')
# f1, ax1 = plt.subplots(1, figsize=[20, 10])
# # f1.subplots_adjust(bottom=0.1, top=0.95, left=0.05, right=0.98, hspace=.3)
# f1.autofmt_xdate()
# ax1.xaxis.set_major_formatter(matplotlib.dates.DateFormatter('%y-%m-%d %H:%M'));
# x_range = (this_dvc_data.dt.min()-dt.timedelta(seconds=1),
#            this_dvc_data.dt.max()+dt.timedelta(seconds=this_dvc_data['content_length'].max().astype('float')+1))
# ax1.set_xticks([t for t in pd.date_range(*x_range, freq='s') if (t.minute == 0) & (t.second==0)]);
# ax1.set_xlim(dt.datetime(2017, 6, 1, 3, 30), dt.datetime(2017, 6, 1, 10, 0))  #x_range)
# ax1.set_ylim(this_dvc_data.segcode_4.min()-0.5, this_dvc_data.segcode_4.max()+0.5)
# for j in this_dvc_data.index:
#     if this_dvc_data.loc[j, 'actiontype'] == 'view':
#         jtr = rd.uniform(-0.2, 0.2)
#         ax1.scatter(this_dvc_data.loc[j, 'dr_pr'], this_dvc_data.loc[j, 'segcode_4']+jtr, alpha=0.5, color='red')
#     else:
#         jtr = rd.uniform(-0.2, 0.2)
#         ax1.plot([this_dvc_data.loc[j, 'dr_pr'],
#                   this_dvc_data.loc[j, 'dr_pr']+pd.to_timedelta(this_dvc_data.loc[j, 'content_length'], unit='s')],
#                  [this_dvc_data.loc[j, 'segcode_4']+jtr, this_dvc_data.loc[j, 'segcode_4']+jtr],
#                  alpha=0.3, lw=2, color='blue')
# ax1.set_title('Sessions vs datadate, {0}, dvc_id = {1}'.format(date, dvc_id))
# ax1.set_xlabel('datadate')
# ax1.set_ylabel('segcode_4')
# plt.tight_layout()
# f1.savefig('DCR_data/dt_vs_seg_d{0}_{1}.png'.format(dvc_id, vers))
# plt.close(f1)
#
#
#
#
#
# ######################################################################################
# ### For 06/01:
# # # ssnid_list = [range(9550,9560), range(9560,9570)]
# # n_plots=3
# # ssnid_list = [range(this_dvc_pings.ssn_id.iloc[0]-5, this_dvc_pings.ssn_id.iloc[0]+5)]
# # j=0
# # while len(ssnid_list) < n_plots:
# #     j += 1
# #     ssnid = this_dvc_pings.ssn_id.iloc[j]
# #     print(j, ssnid, [ssnid not in ssns for ssns in ssnid_list], [ssnid-5 not in ssns for ssns in ssnid_list], [ssnid+5 not in ssns for ssns in ssnid_list])
# #     if all([ssnid not in ssns for ssns in ssnid_list] + [ssnid-5 not in ssns for ssns in ssnid_list] + [ssnid+5 not in ssns for ssns in ssnid_list]):
# #         print('added')
# #         ssnid_list.append(range(ssnid-5, ssnid+5))
# #
# # subsets = {}
# # for i in range(len(ssnid_list)):
# #     subsets[i] = dcr_pings2.loc[(dcr_pings2.dvc_id==dvc_id) & (dcr_pings2.ssn_id.isin(ssnid_list[i]))]
# #
# # plt.close('all')
# # f1, ax1 = plt.subplots(n_plots, figsize=[15, 3 * n_plots])
# # f1.subplots_adjust(bottom=0.1, top=0.95, left=0.05, right=0.98, hspace=.3)
# # f1.autofmt_xdate()
# # for i in range(n_plots):
# #     ax1[i].set_xlim(subsets[i].dt.min()-dt.timedelta(seconds=1),
# #                     subsets[i].dt.max()+dt.timedelta(seconds=subsets[i]['content_length'].max().astype('float')+1))
# #     ax1[i].set_xticklabels(ax1[i].get_xticklabels(), visible=True, rotation=30);
# #     ax1[i].set_ylim(subsets[i].segcode_4.min()-0.5, subsets[i].segcode_4.max()+0.5)
# #     ax1[i].xaxis.set_major_formatter(matplotlib.dates.DateFormatter('%H:%M:%S'));
# #     for j in subsets[i].index:
# #         if subsets[i].loc[j, 'actiontype'] == 'view':
# #             ax1[i].scatter(subsets[i].loc[j, 'dr_pr'], subsets[i].loc[j, 'segcode_4'], alpha=0.5, color='red')
# #         else:
# #             ax1[i].plot([subsets[i].loc[j, 'dr_pr'],
# #                          subsets[i].loc[j, 'dr_pr']+pd.to_timedelta(subsets[i].loc[j, 'content_length'], unit='s')],
# #                         [subsets[i].loc[j, 'segcode_4'], subsets[i].loc[j, 'segcode_4']], alpha=0.5, lw=2, color='blue')
# # ax1[0].set_title('Sessions vs datadate, {0}, dvc_id = {1}'.format(vers, dvc_id))
# # ax1[n_plots-1].set_xlabel('datadate')
# # f1.savefig('DCR_data/dt_vs_seg_d{0}_{1}.png'.format(dvc_id, vers))
# # plt.close(f1)
#
#
# ######################################################################################
# ### Plot segcode time?
# subset = dcr_pings2.loc[(dcr_pings2.dvc_id==dvc_id)]
#
# # subset = dcr_pings2.loc[(dcr_pings2.segcode_4==1) | ((dcr_pings2.segcode_4<10) & (dcr_pings2.actiontype=='timer'))]
#
# plt.close('all')
# f1, ax1 = plt.subplots(1, figsize=[20, 10])
# ax1.yaxis_date()
# ax1.yaxis.set_major_formatter(matplotlib.dates.DateFormatter('%H:%M'));
# d = dcr_pings2.dt.mode()[0]
# for row in subset.index:
#     startmin = 5*subset.loc[row, 'segcode_4']
#     if subset.loc[row, 'actiontype'] == 'view':
#         ax1.scatter(startmin, subset.loc[row, 'dr_pr'], c='red', alpha=0.5, s=20)
#     code = subset.loc[row, 'ping_mins']
#     for j in range(5):
#         if code[j] == '1':
#             ax1.scatter(startmin+j, subset.loc[row, 'dr_pr'], c='blue', alpha=0.5, s=4)
# ax1.set_xlim(0, 1440)
# ax1.set_ylim(subset.dt.min(), subset.dt.max())
# f1.savefig('DCR_data/dt_vs_segcode_early_d{0}_{1}.png'.format(dvc_id, vers))
# plt.close(f1)
#
# ######################################################################################
# ### what identifies a particular thing being watched?
# ### some content_ids uniquely identify a particular resource_group/program_name/episode_name combo,
# ### but some have many
#
# ### counts occurrences of each unique combo
# temp  = dcr_pings2.groupby(['program_name','resource_group','episode_name','content_id']).time.count()
# ### how many combos is each content_id associated with?
# temp2 = temp.reset_index().groupby(['content_id']).time.count().sort_values(ascending=False)
#
# ### This example has 168 different ones
# dcr_pings2.loc[dcr_pings2.content_id == 'us-203735_c01_39c27f9ee0b74d48b30bbb7ac897f12b', ['episode_name','program_name','resource_group']]\
#     .drop_duplicates().sort_values(by=['resource_group','program_name','episode_name'])
# ######################################################################################
