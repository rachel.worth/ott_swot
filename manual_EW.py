### Manual model
import numpy as np
import pandas as pd
pd.set_option('display.width', 160)
import matplotlib.pyplot as plt
import seaborn as sb
import random

#Set working directory
pathname = 'C:/Users/adamel01/BitBucket/ott_swot'
os.chdir(pathname)
os.getcwd()


### Import data
vers = 'v2_4'
dur  = '6mo'
df = pd.read_csv('{0}/swot_master_table_{1}.csv'.format(vers, dur), index_col=0)

### Convert durations to minutes, rounded, so get rid of precision difference between data sources
df[['duration','swotdur','swotstreamdur']] = (df[['duration','swotdur','swotstreamdur']]/60).round(0)

### Add some useful columns
df['vw_frac']    = 1 - df.swotdur / df.swotstreamdur
df['swot_frac']  = df.swotdur / df.swotstreamdur
df['swot_class'] = df.swot_frac > 0  # >= 0.05
df['hh']         = df.fsite.str.split('-', 1, expand=True).iloc[:, 0]
### v2.4 stopped outputing make and model, so this is for backwards compatibility
if vers == 'v2_4':
    df['mk_mdl'] = df.device
    df[['make','model']] = df.device.str.split(' ', 1, expand=True)
else:
    df['mk_mdl'] = df.make.str.cat([df.model], sep='-')

### Count devices per model/provider cell
n_dvcs  = df.pivot_table(index='mk_mdl', columns='provider', values='fsite', aggfunc=pd.Series.nunique).fillna(0)
### Mean swot fraction per cell
swot_mn = df.pivot_table(index='mk_mdl', columns='provider', values='swot_frac', aggfunc='mean')

### Show just those from larger bins
swot_mn.mask(n_dvcs < 30, other='')

########################################################################
###################### Bin devices #####################################
########################################################################
from manual_fns_EW4 import *

df['month'] = pd.to_datetime(df.start_time).map(lambda t: t.month)

predictions = model_swot(df, df, threshold=50)
calc_score(predictions)
########################################################################
########################################################################
model_versions = ['v0', 'v1', 'v2', 'v3', 'v4', 'v5', 'v6', 'v7', 'v8', 'v9']
score_cats = ['all', 'netflix', 'youtube', 'hulu', 'amazon', 'hbo', 'crackle']
thresholds = [10,20,30,50,75,100,125, 150, 200]
print('Get model scores vs threshold')
threshscores = {}
for v in model_versions:
    threshscores[v] = test_model_threshold(df, thresholds, score_cats, version=v)
### threshscores[v][t] = df of provider x month for version v, threshold t
### plot rmse over the months for each provider vs threshold
print('Get model scores vs threshold, time-averaged')
prov_v_thrsh = {}
for v in model_versions:
    prov_v_thrsh[v] = pd.DataFrame([], index=thresholds, columns=score_cats)
    for t in thresholds:
        prov_v_thrsh[v].loc[t] = (threshscores[v][t]**2)[[2,3,4,5,6]].mean(axis=1)**0.5
### prov_v_mnth[v] = df of thresholds x providers
# -------------------------------------------------------------------- #
for vers in model_versions:
    colors_th = sb.color_palette("Set1", len(thresholds), desat=0.7)
    f1, ax1a = plt.subplots(4,2, sharex=True, sharey=True, figsize = [14,14])
    f1.subplots_adjust(hspace=0.1, wspace=0.05, right=.98, bottom=.05, top=.95, left=0.05)
    ax1 = ax1a.flatten()
    for i, p in enumerate(score_cats):
        ax1[i].axhline(0, c='black', alpha=0.25)
        ax1[i].set_title(p)
        for j, t in enumerate(thresholds):
            ax1[i].plot(threshscores[vers][t].columns, 100.*threshscores[vers][t].loc[p], c=colors_th[j],
                        label='{0:3}: rmse {1: .2f}%'.format(t, 100.*(threshscores[vers][t].loc[p]**2).mean()**0.5))
            ax1[i].legend(loc='lower center')
    for col in [0,1]:
        ax1a[3,col].set_xticks([2,3,4,5,6]);
        ax1a[3,col].set_xticklabels(['Feb','Mar','Apr','May','Jun']);
    ax1[0].set_ylim(-5,5)
    f1.text(0.02, 0.5, 'Error (%)', ha='center', va='center', rotation='vertical', fontsize=14)
    f1.text(0.5, 0.02, 'Month', ha='center', va='center', fontsize=14)
    f1.text(0.5, 0.98, 'Error in total viewed minutes, using previous month to predict SWOT %', ha='center', va='center', fontsize=16)
    # plt.tight_layout()
    plt.savefig('ModelPlots/thresh_errors-{0}.png'.format(vers))
    plt.close(f1)
    ########################################################################
    colors_prv = ['black'] + list(sb.color_palette("Set1", len(score_cats)-1, desat=0.7))
    f1, ax1 = plt.subplots(1)
    for i, p in enumerate(prov_v_thrsh[vers].columns):
        ax1.plot(prov_v_thrsh[vers].index, 100.*prov_v_thrsh[vers][p], c=colors_prv[i], label=p)
    ax1.legend()
    ax1.set_xlabel('Threshold (# dvcs)')
    ax1.set_ylabel('RMSE (%)')
    ax1.set_title('Error per provider vs. bin size threshold')
    f1.savefig('ModelPlots/prvdr_v_thresh-{0}.png'.format(vers))
    plt.close(f1)

########################################################################
########################################################################
modelscores = test_model_version(df, score_cats, model_versions)
#EW
modelscores_3mo = test_model_version_3mo(df, score_cats, model_versions)
#df.loc[(df.month==i)|(df.month == (i+1))|(df.month==(i+2))]

# dvc_list = ['Smart TV','Play Station 4', 'X Box One', 'Amazon Fire Stick']
### Pick some example devices: top three, a middle one, and last one?
dvc_list = df.device.value_counts().index
dvc_scores = {}
for vers in model_versions:
    dvc_scores[vers] = pd.DataFrame([], columns=range(2, 7), index=dvc_list)
    for i in dvc_scores[vers].columns:
        dvc_scores[vers][i] = calc_score(model_swot(df.loc[df.month==(i-1)],
                                                    df.loc[df.month==i],
                                                    threshold = 50, binmodel=vers),
                                         groupcol='device')['reldiff']

colors = sb.color_palette("Set1", len(model_versions), desat=0.7)
f1, ax1 = plt.subplots(3, 5, sharex=True, sharey=True, figsize = [18,10])
f1.subplots_adjust(hspace=0.1, wspace=0.05, right=.98, bottom=.06, top=.94, left=0.05)
ax = ax1.flatten()
for i, p in enumerate(score_cats):
    ax[i].axhline(0, c='black', alpha=0.25)
    if p == 'all':
        ax[i].set_title('{0} ({1})'.format(p, int(n_dvcs.sum().sum())))
    else:
        ax[i].set_title('{0} ({1})'.format(p, int(n_dvcs[p].sum())))
    for j, m in enumerate(model_versions):
        ax[i].plot(modelscores[m].columns, 100.*modelscores[m].loc[p], c=colors[j],
                   label='{0}: {1: 0.2f}'.format(m, 100.*rms(modelscores[m].loc[p])))
    ax[i].legend(loc='upper right', ncol=2)
oldi = i
for j, d in enumerate(dvc_list[[0,1,2,3, 5, 10, 15, 18]]):
    i = j+oldi+1
    ax[i].axhline(0, c='black', alpha=0.25)
    ax[i].set_title('{0} ({1})'.format(d, int(n_dvcs.loc[d].sum())))
    for k, m in enumerate(model_versions):
        ax[i].plot(dvc_scores[m].columns, 100.*dvc_scores[m].loc[d], c=colors[k],
                   label='{0}: {1: 0.2f}'.format(m, 100.*rms(dvc_scores[m].loc[d])))
    ax[i].legend(loc='upper right', ncol=2)

for col in [0, 1, 2, 3]:
    ax1[2, col].set_xticks([2, 3, 4, 5, 6]);
    ax1[2, col].set_xticklabels(['Feb', 'Mar', 'Apr', 'May', 'Jun']);
ax[0].set_ylim(-10, 10)
f1.text(0.02, 0.5, 'Error (%)', ha='center', va='center', rotation='vertical', fontsize=14)
f1.text(0.5, 0.02, 'Month', ha='center', va='center', fontsize=14)
f1.text(0.5, 0.98, 'Error in total viewed minutes, using previous month to predict SWOT %',
        ha='center', va='center', fontsize=16)
plt.savefig('ModelPlots/model_v_month_v_prvdr.png')
plt.close(f1)

########################################################################
########################################################################
### construct bin comparison table
bins = {}
for v in ['v4','v5','v6','v7','v8','v9']:
    bins[v] = create_bins(df, binmodel=v).loc[dvc_list,['netflix','youtube','hulu','amazon','hbo','crackle']]
    bins[v]['dvc_score']     = 100*rms( dvc_scores[v], axis=1)
    bins[v].loc['prv_score'] = 100*rms(modelscores[v], axis=1)
    bins[v].loc['prv_score','dvc_score'] = 100*rms(modelscores['v4'].loc['all'])

### Relative change from v4 to v6
(bins['v6']['dvc_score'] - bins['v4']['dvc_score'])/bins['v4']['dvc_score']
(bins['v6'].loc['prv_score'] - bins['v4'].loc['prv_score'])/bins['v4'].loc['prv_score']

########################################################################
########################################################################
### Look at error per bin?
i = 5
modeldf   = df.loc[df.month==i]
predictdf = df.loc[df.month==(i+1)]
preds = model_swot(modeldf, predictdf, threshold=100, binmodel = 'v4')

bin_err = preds.groupby('bin_name').agg({'swotstreamdur':{'sm_strm':'sum'},
                                         'duration':     {'sm_tune':'sum'},
                                         'pred_drtn':    {'sm_pred':'sum'}})
bin_err.columns = bin_err.columns.droplevel(0)
bin_err = bin_err[['sm_strm','sm_tune','sm_pred']]
bin_err['err_min']  = bin_err.sm_pred - bin_err.sm_tune
bin_err['err_abs']  = bin_err.err_min.abs()
bin_err['err_frac'] = bin_err.err_min / bin_err.sm_tune
bin_err = bin_err.sort_values('err_abs')

bin_err

bin_err_mnth = pd.DataFrame([], index=preds.bin_name.unique(), columns=range(2, 7))
for month in bin_err_mnth:
    modeldf = df.loc[df.month == (month-1)]
    predictdf = df.loc[df.month == month]
    preds = model_swot(modeldf, predictdf, threshold=100, binmodel='v4')
    bin_err = preds.groupby('bin_name').agg({'swotstreamdur': {'sm_strm': 'sum'},
                                             'duration': {'sm_tune': 'sum'},
                                             'pred_drtn': {'sm_pred': 'sum'}})
    bin_err_mnth[month] = bin_err[('pred_drtn','sm_pred')] - bin_err[('duration','sm_tune')]





########################################################################
### Find devices with multiple providers used
n_prov_each = df.loc[df.provider.isin(['netflix','hulu'])]\
                .groupby(['fsite','device','provider']).start_time.count().reset_index()

both = n_prov_each.groupby(['fsite','device']).provider.count()
both = both.loc[both>1].reset_index()

compare = pd.DataFrame([], index=both.index, columns=['fsite','device','strm_ntfl','tune_ntfl','strm_hulu','tune_hulu'])
for i in compare.index:
    compare.loc[i, ['fsite','device']] = both.loc[i, ['fsite','device']]
    compare.loc[i, 'strm_ntfl'] = df.loc[(df.fsite    == compare.loc[i, 'fsite'])  &
                                         (df.device   == compare.loc[i, 'device']) &
                                         (df.provider == 'netflix'), 'swotstreamdur'].sum()
    compare.loc[i, 'tune_ntfl'] = df.loc[(df.fsite    == compare.loc[i, 'fsite'])  &
                                         (df.device   == compare.loc[i, 'device']) &
                                         (df.provider == 'netflix'), 'duration'     ].sum()
    compare.loc[i, 'strm_hulu'] = df.loc[(df.fsite    == compare.loc[i, 'fsite'])  &
                                         (df.device   == compare.loc[i, 'device']) &
                                         (df.provider == 'hulu'   ), 'swotstreamdur'].sum()
    compare.loc[i, 'tune_hulu'] = df.loc[(df.fsite    == compare.loc[i, 'fsite'])  &
                                         (df.device   == compare.loc[i, 'device']) &
                                         (df.provider == 'hulu'   ), 'duration'     ].sum()
compare['frac_ntfl'] = compare.tune_ntfl / compare.strm_ntfl
compare['frac_hulu'] = compare.tune_hulu / compare.strm_hulu
for c in ['strm_ntfl','tune_ntfl','strm_hulu','tune_hulu','frac_ntfl','frac_hulu']:
    compare[c] = compare[c].astype('float')

netflix_hulu = list(set(n_prov_each.loc[n_prov_each.provider=='netflix', 'fsite'].unique()).intersection(\
                    set(n_prov_each.loc[n_prov_each.provider=='hulu', 'fsite'].unique())))

n_lowhulu  = compare.loc[((compare.frac_ntfl > 0.7) & (compare.frac_hulu <0.8))].shape[0]
n_highboth = compare.loc[((compare.frac_ntfl > 0.7) & (compare.frac_hulu >=0.8))].shape[0]

### Compare device frequency in boxes with high netflix tuning fraction and low vs. high hulu tuning fraction
n1 = compare.loc[((compare.frac_ntfl > 0.7) & (compare.frac_hulu <0.8))].device.value_counts()/n_lowhulu
n2 = compare.loc[((compare.frac_ntfl > 0.7) & (compare.frac_hulu>=0.8))].device.value_counts()/n_highboth

pd.concat([n1, n2.loc[n1.index]], axis=1)

# plt.plot(compare.frac_ntfl, compare.frac_hulu, 'bo')


########################################################################
### Examine accuracy in smarttv vs others
#
# i = 5
# modeldf   = df.loc[df.month==i]
# predictdf = df.loc[df.month==(i+1)]
#
# bin_avgs(modeldf, create_bins_v4(modeldf, threshold=50))[0]
#
#
# preds = model_swot(modeldf, predictdf, threshold=50, binmodel = 'v4')
# preds['is_smarttv'] = preds.device == 'Smart TV'
# preds['relerr'] = ((preds.pred_drtn - preds.duration)/preds.duration).abs()
#
# category_aggs = preds.groupby(['provider','is_smarttv']).agg({'duration':'sum', 'pred_drtn':'sum'})
# category_aggs['rel_err'] = (category_aggs.pred_drtn - category_aggs.duration) / category_aggs.duration

########################################################################
########################################################################
# ### Examine monthly stability
# # bin_names30 = create_bins(df, threshold=30)
# # bin_names50 = create_bins(df, threshold=50)
# # bin_names60 = create_bins(df, threshold=60)
#
# sizes = [30, 50, 70, 100]
# monthly_vars = {}
# for n in sizes:
#     bin_names = create_bins_v2(df, threshold=n)
#     bin_info, bin_key = bin_avgs(df, bin_names)
#     monthly_var = pd.DataFrame(bin_info.n_dvcs)
#     monthly_var.columns = ['n_dvcs']
#     for m in df.month.unique():
#         monthly_var['m'+str(m)] = bin_avgs(df.loc[df.month == m], bin_names)[0]['mn_swotfrac']
#     for m in df.month.unique()[1:]:
#         monthly_var['del'+str(m)] = monthly_var['m'+str(m)] - monthly_var['m'+str(m-1)]
#     monthly_vars[n] = monthly_var
#
# with sb.color_palette("Paired", 12):
#     for n in sizes:
#         f1, ax1 = plt.subplots(2, figsize=[8, 8])
#         vars = monthly_vars[n]
#         for r in vars.index:
#             ax1[0].plot([vars.loc[r,'n_dvcs'] + i for i in [-2, -1, 0, 1, 2, 3]],
#                         vars.loc[r, ['m1','m2','m3','m4','m5','m6']], label=r)
#             ax1[1].plot(range(1, 7), vars.loc[r, ['m1', 'm2', 'm3', 'm4', 'm5', 'm6']], label=r)
#         # plt.plot(monthly_var.n_dvcs, monthly_var.del3, 'bo', label='del3')
#         # plt.plot(monthly_var.n_dvcs, monthly_var.del4, 'go', label='del4')
#         ax1[0].legend(loc='upper right')
#         ax1[0].set_xlabel('# devices in bin')
#         ax1[0].set_ylabel('mn_swotfrac')
#         ax1[0].set_title('Model stability over time, thresh={0}'.format(n))
#         ax1[1].set_xlabel('Month')
#         ax1[1].set_ylabel('mn_swotfrac')
#         f1.savefig('ModelPlots/stability{0}.png'.format(n))
#         plt.close(f1)

########################################################################